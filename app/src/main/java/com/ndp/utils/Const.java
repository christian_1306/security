package com.ndp.utils;

public class Const {
    public final static int CODE_SPLASH_SCREEN = 1;
    public final static int CODE_LOGIN = 2;
    public final static int CODE_HOME = 3;
    public final static int CODE_CUSTOMER = 4;
    public final static int CODE_CUSTOMER_VIEW = 5;
    public final static int CODE_ITEM = 6;
    public final static int CODE_ITEM_VIEW = 7;
    public final static int CODE_ORDER = 8;
    public final static int CODE_ORDER_SUMMARY = 9;
    public final static int CODE_PAYMENT_VOUCHER = 10;
    public final static int CODE_SHOPPING_CART = 11;
    public final static int CODE_VISIT = 12;
    public final static int CODE_PROMOTION = 12;
    public final static int CODE_COLLECTION = 13;
    public final static int CODE_REPORT = 14;
    public final static int CODE_CONSULT = 15;
    public final static int CODE_PENDING = 16;
    public final static int CODE_SYNC = 17;
    public final static int CODE_PREFERENCES = 18;
    public final static int CODE_TRANSFER = 19;
    public final static int CODE_RECEPTION = 20;
    public final static int CODE_PREVIEW = 21;
    public final static int CODE_APPROVE = 22;
    public final static int CODE_APPROVE_TRANSFER = 23;
    public final static int CODE_APPROVE_CREDITNOTE = 24;
    public final static int CODE_REPORT_TRANSFER = 25;
    public final static int CODE_CREATE_BUSINESS_ADD_DIRECTION = 323;
    public final static int CODE_CREATE_BUSINESS_ADD_CONTACT= 324;
    public final static int CODE_QUERY = 325;

    public final static int CODE_REQ_OK = 888;
    public final static int CODE_REQ_ALL = 30;
    public final static int CODE_REQ_ACCESS_NETWORK_STATE = 31;
    public final static int CODE_REQ_ACCESS_WIFI_STATE = 32;
    public final static int CODE_REQ_INTERNET = 33;
    public final static int CODE_REQ_ACCESS_FINE_LOCATION = 34;
    public final static int CODE_REQ_ACCESS_COARSE_LOCATION = 35;
    public final static int CODE_REQ_READ_PHONE_STATE = 36;
    public final static int CODE_REQ_CALL_PHONE = 37;
    public final static int CODE_REQ_BLUETOOTH = 38;
    public final static int CODE_REQ_WRITE_EXTERNAL_STORAGE = 39;

    public final static int RESULT_CODE_SET_ALL = 2012;
    public final static int RESULT_CODE_SET_ONE = 2012;

    public final static String PUT_USERNAME = "APP_USER";
    public final static String PUT_TIMEOUT = "APP_TIMEOUT";

    public final static String TYPE_WAREHOUSE_ALMACEN = "bo_ShipTo";
    public final static String TYPE_WAREHOUSE_FISCAL = "bo_BillTo";

}
