package com.ndp.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.orm.util.NamingHelper;

import com.ndp.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Useful {

    public static Activity getActivity(View view) {
        if (view.getContext() instanceof Activity) {
            return ((Activity) view.getContext());
        } else {
            Context context = view.getContext();
            while (context instanceof ContextWrapper) {
                if (context instanceof Activity)
                    return (Activity) context;
                context = ((ContextWrapper) context).getBaseContext();
            }
            return null;
        }
    }

    public static LatLng getRandomLocation(LatLng point, int radius) {

        List<LatLng> randomPoints = new ArrayList<>();
        List<Float> randomDistances = new ArrayList<>();
        Location myLocation = new Location("");
        myLocation.setLatitude(point.getLatitude());
        myLocation.setLongitude(point.getLongitude());

        //This is to generate 10 random points
        for(int i = 0; i<10; i++) {
            double x0 = point.getLatitude();
            double y0 = point.getLongitude();

            Random random = new Random();

            // Convert radius from meters to degrees
            double radiusInDegrees = radius / 111000f;

            double u = random.nextDouble();
            double v = random.nextDouble();
            double w = radiusInDegrees * Math.sqrt(u);
            double t = 2 * Math.PI * v;
            double x = w * Math.cos(t);
            double y = w * Math.sin(t);

            // Adjust the x-coordinate for the shrinking of the east-west distances
            double new_x = x / Math.cos(y0);

            double foundLatitude = new_x + x0;
            double foundLongitude = y + y0;
            LatLng randomLatLng = new LatLng(foundLatitude, foundLongitude);
            randomPoints.add(randomLatLng);
            Location l1 = new Location("");
            l1.setLatitude(randomLatLng.getLatitude());
            l1.setLongitude(randomLatLng.getLongitude());
            randomDistances.add(l1.distanceTo(myLocation));
        }
        //Get nearest point to the centre
        int indexOfNearestPointToCentre = randomDistances.indexOf(Collections.min(randomDistances));
        return randomPoints.get(indexOfNearestPointToCentre);
    }

    @SuppressWarnings("ConstantConditions")
    public static String getNameActivity(View view) {
        return getActivity(view).getClass().getSimpleName();
    }

    public static String getCallingName(Activity activity) {
        String name = "";
        if (activity != null && activity.getCallingActivity() != null) {
            name = activity.getCallingActivity().getClassName();
            return name.substring(name.lastIndexOf(".") + 1, name.length());
        }
        return name;
    }

    public static String getCallingName(View view) {
        return getCallingName(getActivity(view));
    }

    public static long timeBack(Context context, long back_pressed) {
        final int TIME_DELAY = 2000;
        if (back_pressed + TIME_DELAY <= System.currentTimeMillis()) {
            Toast.makeText(context, context.getString(R.string.mensaje_presiona_nuevamente), Toast.LENGTH_SHORT).show();
            return System.currentTimeMillis();
        } else {
            return -1;
        }
    }

    public static String[] verifyPermission(Activity activity) {

        ArrayList<String> access = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(activity
                , Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_DENIED) {
            access.add(Manifest.permission.ACCESS_NETWORK_STATE);
        }

        if (ContextCompat.checkSelfPermission(activity
                , Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_DENIED) {
            access.add(Manifest.permission.ACCESS_WIFI_STATE);
        }

        if (ContextCompat.checkSelfPermission(activity
                , Manifest.permission.INTERNET) == PackageManager.PERMISSION_DENIED) {
            access.add(Manifest.permission.INTERNET);
        }

        if (ContextCompat.checkSelfPermission(activity
                , Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED) {
            access.add(Manifest.permission.READ_PHONE_STATE);
        }

        if (ContextCompat.checkSelfPermission(activity
                , Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED) {
            access.add(Manifest.permission.CALL_PHONE);
        }

        if (ContextCompat.checkSelfPermission(activity
                , Manifest.permission.BLUETOOTH) == PackageManager.PERMISSION_DENIED) {
            access.add(Manifest.permission.BLUETOOTH);
        }

        if (ContextCompat.checkSelfPermission(activity
                , Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            access.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }


        String[] request = new String[access.size()];
        request = access.toArray(request);
        return request;
    }

    public static boolean requestAccess(Activity activity, String[] access){
         if (access.length > 0) {
            ActivityCompat.requestPermissions(activity, access, Const.CODE_REQ_ALL);
            return false;
        } else
            return true;
    }

    public static void selectSpinnerItemByValue(Spinner spnr, long value) {
        SimpleCursorAdapter adapter = (SimpleCursorAdapter) spnr.getAdapter();
        for (int position = 0; position < adapter.getCount(); position++) {
            if(adapter.getItemId(position) == value) {
                spnr.setSelection(position);
                return;
            }
        }
    }

    public static int getIndex(Spinner spinner, String code){
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(code)){
                return i;
            }
        }

        return 0;
    }

    public static boolean checkAndRequestPermissions(String[] appPermissions, Activity context,int PERMISSIONS_REQUEST_CODE)
    {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String perm : appPermissions)
        {
            if (ContextCompat.checkSelfPermission(context, perm) != PackageManager.PERMISSION_GRANTED)
            {
                listPermissionsNeeded.add(perm);
            }
        }

        if (!listPermissionsNeeded.isEmpty())
        {
            ActivityCompat.requestPermissions(context,
             listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
             PERMISSIONS_REQUEST_CODE
            );
            return false;
        }
        return true;
    }

    public static boolean validarSiEsCorreo(String string){
        String emailPattern = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@" +
                "[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
        Pattern pattern = Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!.-_'*()/¡]).{8,40})";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    public static boolean isEqualsString(String s1,String s2) {
        return s1.equals(s2);
    }

    public static String getCol(String namedObject){
        return NamingHelper.toSQLNameDefault(namedObject);
    }
}
