package com.ndp.utils.methods;

import android.content.Context;
import android.content.SharedPreferences;

import com.ndp.models.dao.DaoSession;
import com.ndp.models.dao.MemberDao;
import com.ndp.models.dao.MemberDaoDao;
import com.ndp.ui.activities.SplashScreenActivity;

import java.util.HashMap;
import java.util.List;

public class SessionManager2{

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    boolean isEquals = false;
    private static final String PREF_NAME = "SessionManagerPref";
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_userid = "userid";
    public static final String KEY_EMAIL = "email";
    private MemberDaoDao memberDao;
    private DaoSession daoSession;



    public SessionManager2(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        daoSession= SplashScreenActivity.getDaoSession();
    }


    public void createLoginSession(String userid, String email){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_userid, userid);
        editor.putString(KEY_EMAIL, email);
        editor.commit();
    }


    public boolean checkLogin(){

        if(!this.isLoggedIn()){
            /*Intent i = new Intent(_context, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);*/
            return false;
        }
        return true;

    }


    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_userid, pref.getString(KEY_userid, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        return user;
    }

    public void logoutUser(){

        editor.clear();
        editor.commit();

    }


    private boolean isLoggedIn(){
       memberDao=daoSession.getMemberDaoDao();
        List<MemberDao> userlog =memberDao.queryBuilder().where(MemberDaoDao.Properties.Email.eq(SessionManager.KEY_EMAIL)).list();
        if (userlog != null && userlog.size()>0) {
            isEquals = userlog.get(0).getEmail().equals(pref.getString(KEY_EMAIL,"notMailer"));
        }
        return pref.getBoolean(IS_LOGIN, false) && isEquals;
    }

}
