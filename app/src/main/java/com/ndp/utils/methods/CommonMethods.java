package com.ndp.utils.methods;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;

import java.util.Calendar;

import com.ndp.R;


public class CommonMethods {

    private static Activity activity;
    private static Fragment fragment;
    private static int idFragmento;

    public static ProgressDialog createProgressDialog(Context activity, String message) {
        final ProgressDialog progressDialog = new ProgressDialog(activity , R.style.MyAlertDialogStyle);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        return progressDialog;
    }
    public static String getTime (){
        Calendar calendar = Calendar.getInstance();

        String hour = (calendar.getTime().getHours() < 10 )?"0"+calendar.getTime().getHours():
                calendar.getTime().getHours()+"";

        String minutes = (calendar.getTime().getMinutes() < 10 )?"0"+calendar.getTime().getMinutes():
                calendar.getTime().getMinutes()+"";

        return hour+":"+minutes;
    }

    public static View.OnClickListener backActivity(Activity a) {
        activity = a;
        return btnBackActivity_Click;
    }

    public static View.OnClickListener backFragment(Fragment f, int idF) {
        fragment = f;
        idFragmento = idF;
        return btnBackFragment_Click;
    }

    private static View.OnClickListener btnBackActivity_Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            activity.finish();
        }
    };

    private static View.OnClickListener btnBackFragment_Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            fragment.getActivity().getSupportFragmentManager().beginTransaction().remove(fragment)
                    .addToBackStack(null).commit();
        }
    };
}
