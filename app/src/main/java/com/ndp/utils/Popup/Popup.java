package com.ndp.utils.Popup;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.annotation.IntDef;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Direccion;
import com.ndp.models.model.Tarifario;

public class Popup {
    //region Constant for messages
    private final static String MSG_SYMBOL_SUCCESS = "✓";
    private final static String MSG_SYMBOL_WARNING = "!";
    private final static String MSG_SYMBOL_ERROR = "X";

    public final static String MSG_TITLE_SUCCESS = "Confirmación";
    public final static String MSG_TITLE_WARNING = "Advertencia";
    public final static String MSG_TITLE_ERROR = "Error";

    public final static int MSG_TYPE_SUCCESS = 0;
    public final static int MSG_TYPE_WARNING = 1;
    public final static int MSG_TYPE_ERROR = 2;

    @IntDef({MSG_TYPE_SUCCESS, MSG_TYPE_WARNING, MSG_TYPE_ERROR})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TypeMsg {
    }
    //endregion

    //region Const for buttons
    public final static String BUTTON_NAME_ACCEPT = "Aceptar";
    public final static String BUTTON_NAME_CONTINUE = "Continuar";
    public final static String BUTTON_NAME_RETRY = "Reintentar";
    public final static String BUTTON_NAME_SAVE = "Guardar";
    public final static String BUTTON_NAME_PERMISSION = "Sí, conceder permisos";
    public final static String BUTTON_NAME_SETTINGS= "Ir Ajustes";
    public final static String BUTTON_NAME_notPERMISSION = "No, Salir";

    public final static int BUTTON_TYPE_ACCEPT = 0;
    public final static int BUTTON_TYPE_CONTINUE = 1;
    public final static int BUTTON_TYPE_RETRY = 2;
    public final static int BUTTON_TYPE_SAVE = 3;
    public final static int BUTTON_TYPE_PERMISSION = 4;
    public final static int BUTTON_TYPE_SETTINGS = 5;
    public final static int BUTTON_TYPE_SYNC = 8;

    @IntDef({BUTTON_TYPE_ACCEPT, BUTTON_TYPE_CONTINUE, BUTTON_TYPE_RETRY, BUTTON_TYPE_SAVE,BUTTON_TYPE_PERMISSION,BUTTON_TYPE_SETTINGS,BUTTON_TYPE_SYNC})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TypeButton {
    }
    //endregion

    //region AlertDialogs
    //region public static void showAlert(Context context, String message)

    public static void showAlert(Context context, String message) {
        showAlert(context, context.getString(R.string.titulo_error), message);
    }

    public static void showAlert(Context context, String title, String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setCancelable(false).show();//.setIcon(R.drawable.alerta).show();
    }
    //endregion

    //region  public static void ShowAlert()
    public static void ShowAlertDialog(final Context context
            , String title, String message
            , DialogInterface.OnClickListener positive) {

        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, positive)
                .setCancelable(false);
        alert.create();
        alert.show();
    }

    public static void ShowAlertDialog(final Context context
            , String title, String message
            , DialogInterface.OnClickListener positive
            , DialogInterface.OnClickListener negative
            , Boolean isCancelable) {

        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title)
                .setMessage(message)
                .setNegativeButton(android.R.string.cancel, positive)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (negative != null) negative.onClick(dialog, which);
                        dialog.dismiss();
                    }
                })
                .setCancelable(isCancelable);
        alert.create();
        alert.show();
    }

    public static void ShowAlertDialog(final Context context
            , String title, String message
            , DialogInterface.OnClickListener positive, Boolean cancelable ) {

        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, positive)
                .setCancelable(cancelable);
        alert.create();
        alert.show();
    }
    //endregion

    //region  public static void ShowAlert()
    public static void ShowAlertDialog(final Context context
            , String title, String message
            , DialogInterface.OnClickListener positive
            , final DialogInterface.OnClickListener negative) {

        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, positive)
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (negative != null) negative.onClick(dialog, which);
                        dialog.dismiss();
                    }
                })
                .setCancelable(false);
        alert.create();
        alert.show();
    }
    //endregion
    //endregion

    //region ShowDialog
    public static void ShowDialog(Context context
            , String message, String title
            , @TypeMsg int typeMessage
            , @TypeButton int typeButton
            , final View.OnClickListener action1
            , final View.OnClickListener action2
            , final DialogInterface.OnCancelListener cancel) {
        final Dialog dialog = new Dialog(context, R.style.Dialog_AppTheme_Transparent);
        boolean cancelable = (typeButton == BUTTON_TYPE_SAVE || typeButton == BUTTON_TYPE_PERMISSION || typeButton == BUTTON_TYPE_SETTINGS);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(cancelable);
        dialog.setContentView(R.layout.custom_dialog_alert);

        ((TextView) dialog.findViewById(R.id.dialog_alert_tvMensaje))
                .setText(message);

        ((TextView) dialog.findViewById(R.id.dialog_alert_tvTitulo))
                .setText(title);

        ((TextView) dialog.findViewById(R.id.dialog_alert_tvTipoMensaje))
                .setText(getSymbol(typeMessage));

        Button button;
        //region btnAceptar_Click
        button = dialog.findViewById(R.id.dialog_alert_btnAceptar);
        button.setText(typeButton == BUTTON_TYPE_PERMISSION ? BUTTON_NAME_PERMISSION : ((typeButton == BUTTON_TYPE_SETTINGS)? BUTTON_NAME_SETTINGS : getButtonName(typeButton)) );
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (action1 != null) action1.onClick(v);
                dialog.dismiss();
            }
        });
        //endregion

        //region btnCancelar_Click
        button = (Button) dialog.findViewById(R.id.dialog_alert_btnCancelar);
        if (typeButton == BUTTON_TYPE_SAVE) button.setText(BUTTON_NAME_RETRY);
        if (typeButton == BUTTON_TYPE_PERMISSION || typeButton == BUTTON_TYPE_SETTINGS) button.setText(BUTTON_NAME_notPERMISSION);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (action2 != null) action2.onClick(v);
                dialog.dismiss();
            }
        });
        //endregion

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (cancel != null) cancel.onCancel(dialog);
            }
        });

        dialog.show();
    }

    //region ShowDialog
    public static void ShowDialogCustomButton(Context context
            , String message, String title
            , String button1, String button2
            , @TypeMsg int typeMessage
            , @TypeButton int typeButton
            , final View.OnClickListener action1
            , final View.OnClickListener action2
            , final DialogInterface.OnCancelListener cancel) {
        final Dialog dialog = new Dialog(context, R.style.Dialog_AppTheme_Transparent);
        boolean cancelable = (typeButton == BUTTON_TYPE_SAVE || typeButton == BUTTON_TYPE_PERMISSION || typeButton == BUTTON_TYPE_SETTINGS || typeButton == BUTTON_TYPE_SYNC);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(cancelable);
        dialog.setContentView(R.layout.custom_dialog_alert);

        ((TextView) dialog.findViewById(R.id.dialog_alert_tvMensaje))
                .setText(message);

        ((TextView) dialog.findViewById(R.id.dialog_alert_tvTitulo))
                .setText(title);

        ((TextView) dialog.findViewById(R.id.dialog_alert_tvTipoMensaje))
                .setText(getSymbol(typeMessage));

        Button button;
        //region btnAceptar_Click
        button = dialog.findViewById(R.id.dialog_alert_btnAceptar);
        button.setText(button1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (action1 != null) action1.onClick(v);
                dialog.dismiss();
            }
        });
        //endregion

        //region btnCancelar_Click
        button = (Button) dialog.findViewById(R.id.dialog_alert_btnCancelar);
        button.setText(button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (action2 != null) action2.onClick(v);
                dialog.dismiss();
            }
        });
        //endregion

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (cancel != null) cancel.onCancel(dialog);
            }
        });

        dialog.show();
    }




    public static void ShowDialogHideButton(Context context
            , String message, String title
            , String button1, String button2
            , @TypeMsg int typeMessage
            , @TypeButton int typeButton
            , final View.OnClickListener action1
            , final View.OnClickListener action2
            , final DialogInterface.OnCancelListener cancel) {
        final Dialog dialog = new Dialog(context, R.style.Dialog_AppTheme_Transparent);
        boolean cancelable = (typeButton == BUTTON_TYPE_SAVE || typeButton == BUTTON_TYPE_PERMISSION || typeButton == BUTTON_TYPE_SETTINGS);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(cancelable);
        dialog.setContentView(R.layout.custom_dialog_alert);

        ((TextView) dialog.findViewById(R.id.dialog_alert_tvMensaje))
                .setText(message);

        ((TextView) dialog.findViewById(R.id.dialog_alert_tvTitulo))
                .setText(title);

        ((TextView) dialog.findViewById(R.id.dialog_alert_tvTipoMensaje))
                .setText(getSymbol(typeMessage));

        Button button;
        //region btnAceptar_Click
        button = dialog.findViewById(R.id.dialog_alert_btnAceptar);
        button.setText(button1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (action1 != null) action1.onClick(v);
                dialog.dismiss();
            }
        });
        //endregion

        //region btnCancelar_Click
        button = (Button) dialog.findViewById(R.id.dialog_alert_btnCancelar);
        if (action2 != null) {
            button.setText(button2);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    action2.onClick(v);
                    dialog.dismiss();
                }
            });
        } else {
            button.setVisibility(View.GONE);
        }
        //endregion

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (cancel != null) cancel.onCancel(dialog);
            }
        });

        dialog.show();
    }

    public static void ShowDialog(Context context, String message, String title
            , @TypeMsg int typeMessage
            , final View.OnClickListener action1) {
        ShowDialog(context, message, title, typeMessage, BUTTON_TYPE_ACCEPT, action1, null);
    }

    public static void ShowDialog(Context context, String message, String title
            , @TypeMsg int typeMessage
            , @TypeButton int typeButton
            , final View.OnClickListener action1
            , final View.OnClickListener action2) {
        ShowDialog(context, message, title, typeMessage, typeButton, action1, action2, null);
    }

    //endregion

    //region Get property
    private static String getSymbol(@TypeMsg int tipoMensaje) {
        if (tipoMensaje == MSG_TYPE_SUCCESS) {
            return MSG_SYMBOL_SUCCESS;
        } else if (tipoMensaje == MSG_TYPE_WARNING) {
            return MSG_SYMBOL_WARNING;
        } else {
            return MSG_SYMBOL_ERROR;
        }
    }

    private static String getButtonName(@TypeButton int tipoButton) {
        if (tipoButton == BUTTON_TYPE_ACCEPT) {
            return BUTTON_NAME_ACCEPT;
        } else if (tipoButton == BUTTON_TYPE_CONTINUE) {
            return BUTTON_NAME_CONTINUE;
        } else if (tipoButton == BUTTON_TYPE_RETRY) {
            return BUTTON_NAME_RETRY;
        } else {
            return BUTTON_NAME_SAVE;
        }
    }
    //endregion

    public static AlertDialog showDialog (String title, String msg, String positiveLabel,
                                   DialogInterface.OnClickListener positiveOnClick,
                                   String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
                                   boolean isCancelAble,Context context)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setCancelable(isCancelAble);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel,positiveOnClick);
        builder.setNegativeButton(negativeLabel,negativeOnClick);

        AlertDialog alert = builder.create();
        alert.show();
        return alert;
    }

    @SuppressLint("SetTextI18n")
    public static void showRadioButtonDialog(Activity _activity, List<Direccion> direccions, TextView view, String title) {

        Dialog dialog = new Dialog(_activity, R.style.Dialog_AppTheme_Transparent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_radiobutton_list);

        RadioGroup rg = dialog.findViewById(R.id.radio_group);
        TextView t1 = dialog.findViewById(R.id.dialog_alert_tvTitulo);
        t1.setText(title);

        Typeface typeface = ResourcesCompat.getFont(_activity, R.font.montserrat);

        for(int i = 0; i < direccions.size(); i++){

            RadioButton rb = new RadioButton(_activity);
            rb.setId(i);
            rb.setTypeface(typeface);
            rb.setText(direccions.get(i).getDireccion());
            rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    int childCount = group.getChildCount();
                    for (int x = 0; x < childCount; x++) {
                        RadioButton btn = (RadioButton) group.getChildAt(x);
                        if (btn.getId() == checkedId) {
                            if (view != null) {
                                view.setText(btn.getText().toString());
                            }
                        }
                    }
                }
            });

            rg.addView(rb);

            if (view != null && view.getText().toString().toLowerCase().equals(rb.getText().toString().toLowerCase())){
                rg.check(i);
            }
        }

        dialog.show();
    }

    @SuppressLint("SetTextI18n")
    public static void getDialogTarifario(Activity _activity, List<Tarifario> tarifarios, TextView view, String title) {

        Dialog dialog = new Dialog(_activity, R.style.Dialog_AppTheme_Transparent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_radiobutton_list);

        RadioGroup rg = dialog.findViewById(R.id.radio_group);
        TextView t1 = dialog.findViewById(R.id.dialog_alert_tvTitulo);
        t1.setText(title);

        Typeface typeface = ResourcesCompat.getFont(_activity, R.font.montserrat);

        for(int i = 0; i < tarifarios.size(); i++){

            RadioButton rb = new RadioButton(_activity);
            rb.setId(i);
            rb.setTypeface(typeface);
            rb.setText(tarifarios.get(i).getId() + "-" + tarifarios.get(i).getNombre());
            rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    int childCount = group.getChildCount();
                    for (int x = 0; x < childCount; x++) {
                        RadioButton btn = (RadioButton) group.getChildAt(x);
                        if (btn.getId() == checkedId) {
                            if (view != null) {
                                view.setText(btn.getText().toString());
                            }
                        }
                    }
                }
            });

            rg.addView(rb);

            if (view != null && view.getText().toString().equals(rb.getText().toString())){
                rg.check(i);
            }
        }

        dialog.show();
    }


}
