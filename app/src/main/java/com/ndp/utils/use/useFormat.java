package com.ndp.utils.use;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class useFormat {

    public static DecimalFormat decimalThousands() {
        DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
        simbolo.setDecimalSeparator('.');
        simbolo.setGroupingSeparator(',');
        //return new DecimalFormat("###,###.###", simbolo);
        return new DecimalFormat("###,##0.00", simbolo);
    }

    public static DecimalFormat decimalHundreds() {
        DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
        return new DecimalFormat("##0.00", simbolo);
    }

    public static double round(Object amount) {
        double value = (amount instanceof Double)
                ? (double) amount
                : Double.parseDouble(amount.toString());

        BigDecimal bd = new BigDecimal(value);
        return bd.setScale(0, RoundingMode.HALF_UP).doubleValue();
    }

    public static double round2(Object amount) {
        double value = (amount instanceof Double)
                ? (double) amount
                : Double.parseDouble(amount.toString());

        BigDecimal bd = new BigDecimal(value);
        return bd.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static double round3(Object amount) {
        double value = (amount instanceof Double)
                ? (double) amount
                : Double.parseDouble(amount.toString());

        BigDecimal bd = new BigDecimal(value);
        return bd.setScale(3, RoundingMode.HALF_UP).doubleValue();
    }

    public static String dateDay(Object value, int numberDays) {
        value = value == null ? new Date() : value;

        try {
            SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, numberDays); // Adding n number of days
            return simpleDate.format(calendar.getTime());
        } catch (Exception ex) {
            return value.toString();
        }
    }

    public static String date(Object value) {
        value = value == null ? new Date() : value;

        if (value instanceof Date) {
            return android.text.format.DateFormat.format("dd/MM/yyyy", (Date) value).toString();
        } else {
            try {
                SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                return simpleDate.format(simpleDate.parse((String) value)); //date(formatter.parse((String) value));
            } catch (Exception ex) {
                return value.toString();
            }
        }
    }

    public static String hora(Object value) {
        value = value == null ? new Date() : value;

        if (value instanceof Date) {
            return android.text.format.DateFormat.format("hh:mm aa", (Date) value).toString();
        } else {
            try {
                SimpleDateFormat simpleDate = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
                return simpleDate.format(simpleDate.parse((String) value)); //date(formatter.parse((String) value));
            } catch (Exception ex) {
                return value.toString();
            }
        }
    }

    public static String hora24(Date date) {
        Date getDate = date == null ? new Date() : date;
        return android.text.format.DateFormat.format("HH:mm", getDate).toString();
    }

    public static String checkTime(String timeCompare) {

        try {

            SimpleDateFormat simpleDate = new SimpleDateFormat("HH:mm", Locale.getDefault());
            Date date1 = simpleDate.parse(timeCompare);
            Date date2 = simpleDate.parse("08:00");
            Date date3 = simpleDate.parse("15:00");
            Date date4 = simpleDate.parse(hora24(null));

            if (((date4.getTime() - date1.getTime()) / 1000) / 60 >= 5) {
                return "";
            } else if (date1.before(date2))
                return "08:00";
            else if (!date1.equals(date2) && date1.before(date3))
                return "15:00";

            return "";
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return hora24(null);
    }

    public static String DateTime24(Date date) {
        Date getDate = date == null ? new Date() : date;
        return android.text.format.DateFormat.format("yyyy-MM-dd HH:mm:ss", getDate).toString();
    }
}
