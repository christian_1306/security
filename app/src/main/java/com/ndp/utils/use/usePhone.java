package com.ndp.utils.use;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.ndp.R;
import com.ndp.utils.Const;

public class usePhone {

    @SuppressWarnings("ConstantConditions")
    public static void hideKeyBoard(Activity activity) {
        final View focus = activity.getCurrentFocus();
        if (focus != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(focus.getWindowToken(), 0);
        }
    }

    public static void showKeyBoard(Activity activity){
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static boolean useWifiOrMobileData(Activity activity) {
        return isWifiConnected(activity) || isMobileDataConnected(activity);
    }

    public static boolean isWifiConnected(Activity activity) {
        WifiManager wifiMgr = (WifiManager) activity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifiMgr == null) return false;
        return (wifiMgr.isWifiEnabled() && wifiMgr.getConnectionInfo().getNetworkId() != -1);
    }

    public static boolean isMobileDataConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            if (cm == null) return false;
            Class<?> cmClass = Class.forName(cm.getClass().getName());
            Method method = cmClass.getDeclaredMethod("getMobileDataEnabled");
            method.setAccessible(true);
            return (boolean) (Boolean) method.invoke(cm);
        } catch (ClassNotFoundException | NoSuchMethodException
                | IllegalAccessException | InvocationTargetException ex) {
            return false;
        }
    }


    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "0.0.0.0";
    }


    @SuppressLint("HardwareIds")
    @SuppressWarnings("ConstantConditions")
    public static String getIMEI(Context context) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE)
                == PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                return manager.getImei();
            } else {
                TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                return manager.getDeviceId();
            }
        } else {
            ActivityCompat.requestPermissions((Activity) context, new String[]{
                    Manifest.permission.READ_PHONE_STATE}, Const.CODE_REQ_READ_PHONE_STATE);
        }
        return context.getString(R.string.digits_zero);
    }

    public static String getcodCreacionApp(Context context){
        Date newDate = new Date();
        @SuppressLint("SimpleDateFormat") String dateGen = new SimpleDateFormat("yyyyMMddHHmmss").format(newDate);
        return getIMEI(context) + dateGen;
    }

    public static boolean isOnline() {
        try {
            int timeoutMs = 2000;
            Socket sock = new Socket();
            sock.connect(new InetSocketAddress("8.8.8.8", 53), timeoutMs);
            sock.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static boolean isOnlineWait(Context context) {
        try {
            Process process = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com.pe");
            return (process.waitFor() == 0);
        } catch (IOException | InterruptedException ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
            return false;
        }
    }
}
