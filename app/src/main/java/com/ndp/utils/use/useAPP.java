package com.ndp.utils.use;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import com.ndp.models.dao.MemberDao;
import com.ndp.models.model.Almacen;
import com.ndp.models.model.Articulo;
import com.ndp.models.model.ArticuloXAlmacen;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.CondicionPago;
import com.ndp.models.model.CoreMotivo;
import com.ndp.models.model.CoreRuta;
import com.ndp.models.model.Departamento;
import com.ndp.models.model.Direccion;
import com.ndp.models.model.Impuesto;
import com.ndp.models.model.Lead;
import com.ndp.models.model.Member;
import com.ndp.models.model.Moneda;
import com.ndp.models.model.Municipio;
import com.ndp.models.model.PersonaContacto;
import com.ndp.models.model.Provincia;
import com.ndp.models.model.Tarifario;
import com.ndp.models.model.TarifarioXArticulo;
import com.ndp.models.model.User;
import com.ndp.ui.activities.LoginActivity;
import com.ndp.utils.methods.SessionManager;
import com.ndp.utils.methods.SessionManager2;

public class useAPP {

    public static String isNullOrEmpty(String value){
        return  (value != null && !value.isEmpty()) ? value : "-";
    }

    public static String isNullOrEmptyDouble(Double value){
        return  (value != null) ? String.valueOf(value) : "-";
    }

    public static String fmDecimal(@NonNull double value){
        DecimalFormat form = new DecimalFormat("###,###,##0.00");
        return form.format(value);
    }

    public static void getDate(Activity activity, TextView textView, Calendar cal){

        int mYear = cal.get(Calendar.YEAR);
        int mMonth = cal.get(Calendar.MONTH);
        int mDay = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        cal.set(year, monthOfYear, dayOfMonth);
                        textView.setText( getDigits(year) + "-" + getDigits((monthOfYear + 1)) +  "-" +  getDigits(dayOfMonth) );
                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());
        datePickerDialog.setTitle("");
        datePickerDialog.show();
    }

    public static void getDate(Activity activity, TextView textView, Calendar cal, Boolean isLimitedToday){

        int mYear = cal.get(Calendar.YEAR);
        int mMonth = cal.get(Calendar.MONTH);
        int mDay = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        cal.set(year, monthOfYear, dayOfMonth);
                        textView.setText( getDigits(year) + "-" + getDigits((monthOfYear + 1)) +  "-" +  getDigits(dayOfMonth) );
                    }
                }, mYear, mMonth, mDay);

        if (isLimitedToday)
                datePickerDialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis()) ;

        datePickerDialog.setTitle("");
        datePickerDialog.show();
    }

    private static String getDigits(@NonNull int value){
        String s1 = String.valueOf(value);
        if (s1.length() < 2)
            return "0"+value;
        else
            return s1;
    }

    public static void CerrarSesion(Activity activity,@NonNull SessionManager2 sessionManager){
        //MemberDao.deleteAll(MemberDao.class);
        Departamento.deleteAll(Departamento.class);
        Provincia.deleteAll(Provincia.class);
        Municipio.deleteAll(Municipio.class);
        CoreMotivo.deleteAll(CoreMotivo.class);
        CoreRuta.deleteAll(CoreRuta.class);
        Articulo.deleteAll(Articulo.class);
        ArticuloXAlmacen.deleteAll(ArticuloXAlmacen.class);
        TarifarioXArticulo.deleteAll(TarifarioXArticulo.class);
        Almacen.deleteAll(Almacen.class);
        Tarifario.deleteAll(Tarifario.class);
        Cliente.deleteAll(Cliente.class);
        Direccion.deleteAll(Direccion.class);
        PersonaContacto.deleteAll(PersonaContacto.class);
        Impuesto.deleteAll(Impuesto.class);
        Moneda.deleteAll(Moneda.class);
        CondicionPago.deleteAll(CondicionPago.class);
        Lead.deleteAll(Lead.class);
        sessionManager.logoutUser();
        activity.startActivity(new Intent(activity, LoginActivity.class));
        activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        activity.finish();
    }

    public static String getDecimalFormattedString(String value) {
        if (value != null && !value.equalsIgnoreCase("")) {
            StringTokenizer lst = new StringTokenizer(value, ".");
            String str1 = value;
            String str2 = "";
            if (lst.countTokens() > 1) {
                str1 = lst.nextToken();
                str2 = lst.nextToken();
            }
            String str3 = "";
            int i = 0;
            int j = -1 + str1.length();
            if (str1.charAt(-1 + str1.length()) == '.') {
                j--;
                str3 = ".";
            }
            for (int k = j; ; k--) {
                if (k < 0) {
                    if (str2.length() > 0)
                        str3 = str3 + "." + str2;
                    return str3;
                }
                if (i == 3) {
                    str3 = "," + str3;
                    i = 0;
                }
                str3 = str1.charAt(k) + str3;
                i++;
            }
        }
        return "";
    }

    public static String getTodayFormat(String format){
        //yyyyMMddHHmmss
        Date today = new Date();
        @SuppressLint("SimpleDateFormat") String todayFormat = new SimpleDateFormat(format).format(today);
        return todayFormat;
    }

    public static List<String> getTipoPersona(){
        List<String> list = new ArrayList<String>();
        list.add("TPU - Unipersonal");
        list.add("TPN - Natural");
        list.add("TPJ - Jurídica");
        list.add("SND - Sujeto No Domiciliado");

        return list;
    }

    public static List<String> getTipoQuery(){
        List<String> list = new ArrayList<String>();
        list.add("TIPO CONSULTA");
        list.add("1-ORDEN DE VENTA");
        list.add("2-FACTURA");
        list.add("3-VISITA");
        return list;
    }

    public static List<String> getEstadoQuery(){
        List<String> list = new ArrayList<String>();
        list.add("ESTADO");
        list.add("1-TODOS");
        list.add("2-ABIERTO");
        list.add("3-CERRADO");
        return list;
    }

    public static List<String> getTipoPending(){
        List<String> list = new ArrayList<String>();
        list.add("TIPO PENDIENTE");
        list.add("1-ORDEN DE VENTA");
        list.add("2-CLIENTES");
        list.add("3-LEADS");
        list.add("4-VISITAS");
        list.add("5-TODOS");
        return list;
    }

    public static List<String> getEstadoPending(){
        List<String> list = new ArrayList<String>();
        list.add("ESTADO ENVIO");
        list.add("1-PENDIENTES");
        list.add("2-FINALIZADOS");
        list.add("3-ERRORES");
        return list;
    }

    public static List<String> getTipoDocumento(){
        List<String> list = new ArrayList<String>();
        list.add("0 - Otros Tipos de Documentos");
        list.add("1 - Cédula de Identidad (CI)");
        list.add("2 - Carnet de Extranjero");
        list.add("3 - Número de Identificación Tributaria (NIT)");
        list.add("4 - Pasaporte");

        return list;
    }

    public static String getDescripcionPorTipo(int tipo) {
        String typeObject = "";
        switch (tipo) {
            case 1:
                typeObject = "Registrar Orden de Venta";
                break;
            case 2:
                typeObject = "Registrar Cliente";
                break;
            case 3:
                typeObject = "Registrar Lead";
                break;
            case 4:
                typeObject = "Registrar Visita";
                break;
        }

        return typeObject;
    }

    public static String getEstado(@NonNull String value){

        if (value.equals("bost_Open")) {
            return "Abierto";
        } else if (value.equals("bost_Close")) {
            return "Cerrado";
        } else {
            return value;
        }
    }


}
