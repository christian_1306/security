package com.ndp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class Shared {

    private final static String prf_Logueo = "prf_Logueo";
    public final static String login_imei = "imei";
    public final static String login_token = "token";
    public final static String login_usuario = "usuario";
    public final static String login_recordar = "recordar";
    public final static String login_hora = "hora";
    public final static String prf_Default = "_preferences";
    private final static String prf_CustomerFilter = "prf_CustomerFilter";
    public final static String CustomerFilter_Customer = "Customer";
    public final static String CustomerFilter_Lead = "Lead";


    public static SharedPreferences prf_Login(Context context) {
        return context.getSharedPreferences(prf_Logueo, MODE_PRIVATE);
    }

    public static void edit_Login(Activity activity, String Imei, String Usuario, Boolean Recordar, String Hora, String Token) {
        SharedPreferences.Editor editor = prf_Login(activity).edit();
        if (Imei != null) editor.putString(login_imei, Imei);
        if (Token != null) editor.putString(login_token, Token);
        if (Usuario != null) editor.putString(login_usuario, Usuario);
        if (Hora != null) editor.putString(login_hora, Hora);
        if (Recordar != null) editor.putBoolean(Shared.login_recordar, Recordar);
        editor.apply();
    }

    public static void clear_Login(Activity activity) {
        SharedPreferences.Editor editor = prf_Login(activity).edit();
        editor.clear();
        editor.apply();
    }

    //region Preferencia compartida por defecto "PreferenceManager.getDefaultSharedPreferences(this)"
    public static SharedPreferences prf_Default(Context context) {
        return context.getSharedPreferences(context.getPackageName() + prf_Default, MODE_PRIVATE);
    }
    //endregion

    public static void storeCustomerFilterStatus(Activity activity, boolean isCheckedCustomer, boolean isCheckedLead){
        SharedPreferences mSharedPreferences = activity.getSharedPreferences(prf_CustomerFilter, MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putBoolean(CustomerFilter_Customer, isCheckedCustomer);
        mEditor.putBoolean(CustomerFilter_Lead, isCheckedLead);
        mEditor.apply();
    }

    public static boolean getStatusCheckCustomer(Activity activity){
        SharedPreferences mSharedPreferences = activity.getSharedPreferences(prf_CustomerFilter, MODE_PRIVATE);
        return mSharedPreferences.getBoolean(CustomerFilter_Customer, false);
    }

    public static boolean getStatusCheckLead(Activity activity){
        SharedPreferences mSharedPreferences = activity.getSharedPreferences(prf_CustomerFilter, MODE_PRIVATE);
        return mSharedPreferences.getBoolean(CustomerFilter_Lead, false);
    }

}

