package com.ndp.models.dto;

import java.io.Serializable;

import com.ndp.models.model.Municipio;

public class MunicipioView implements Serializable {
    private int idMunicipio;
    private String nombre;
    private ProvinciaView provincia;

    public MunicipioView(){

    }

    public MunicipioView(Municipio municipio){
        this.idMunicipio = municipio.getIdMunicipio();
        this.nombre = municipio.getNombre();
    }

    public int getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(int idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ProvinciaView getProvincia() {
        return provincia;
    }

    public void setProvincia(ProvinciaView provincia) {
        this.provincia = provincia;
    }
}
