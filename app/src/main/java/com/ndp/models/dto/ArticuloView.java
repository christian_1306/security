package com.ndp.models.dto;

import java.io.Serializable;
import java.util.List;

public class ArticuloView implements Serializable {

    private Integer idArticulo;
    private String codigo;
    private String nombre;
    private String descripcion;
    private int estadoLogico;
    private String unidadMedida;
    private int varillasXTonelada;
    private Double peso;
    private List<TarifarioXArticuloView> listaPrecios;
    private List<ArticuloXAlmacenView> listaStocks;


    public ArticuloView(){

    }

    public Integer getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(Integer idArticulo) {
        this.idArticulo = idArticulo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getEstadoLogico() {
        return estadoLogico;
    }

    public void setEstadoLogico(int estadoLogico) {
        this.estadoLogico = estadoLogico;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public int getVarillasXTonelada() {
        return varillasXTonelada;
    }

    public void setVarillasXTonelada(int varillasXTonelada) {
        this.varillasXTonelada = varillasXTonelada;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public List<TarifarioXArticuloView> getListaPrecios() {
        return listaPrecios;
    }

    public void setListaPrecios(List<TarifarioXArticuloView> listaPrecios) {
        this.listaPrecios = listaPrecios;
    }

    public List<ArticuloXAlmacenView> getListaStocks() {
        return listaStocks;
    }

    public void setListaStocks(List<ArticuloXAlmacenView> listaStocks) {
        this.listaStocks = listaStocks;
    }
}
