package com.ndp.models.dto;

import java.io.Serializable;

public class PersonaContactoView implements Serializable {

    private int idPersonaContacto;
    private String nombres;
    private String cargo;
    private String telefono;
    private String correo;

    public PersonaContactoView(){

    }

    public int getIdPersonaContacto() {
        return idPersonaContacto;
    }

    public void setIdPersonaContacto(int idPersonaContacto) {
        this.idPersonaContacto = idPersonaContacto;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
}
