package com.ndp.models.dto;

import java.io.Serializable;

public class TarifarioView implements Serializable {

    private int codigoListaPrecio;
    private int idTarifario;
    private String nombre;
    private int estadoLogico;
    private AlmacenView almacen;


    public TarifarioView(){

    }

    public int getIdTarifario() {
        return idTarifario;
    }

    public void setIdTarifario(int idTarifario) {
        this.idTarifario = idTarifario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEstadoLogico() {
        return estadoLogico;
    }

    public void setEstadoLogico(int estadoLogico) {
        this.estadoLogico = estadoLogico;
    }

    public AlmacenView getAlmacen() {
        return almacen;
    }

    public void setAlmacen(AlmacenView almacen) {
        this.almacen = almacen;
    }

    public int getCodigoListaPrecio() {
        return codigoListaPrecio;
    }

    public void setCodigoListaPrecio(int codigoListaPrecio) {
        this.codigoListaPrecio = codigoListaPrecio;
    }
}
