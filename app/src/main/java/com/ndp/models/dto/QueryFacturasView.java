package com.ndp.models.dto;

import java.io.Serializable;

public class QueryFacturasView implements Serializable {

    private String CardCode;
    private String CardName;
    private String Comments;
    private Double DocTotal;
    private Double PaidToDate;
    private String DocumentStatus;
    private String DocCurrency;
    private String DocDueDate;
    private String NumAtCard;
    private Integer DocNum;
    private String U_BPVS_CODTRAS_APP;

    public QueryFacturasView() {}


    public String getCardCode() {
        return CardCode;
    }

    public void setCardCode(String cardCode) {
        CardCode = cardCode;
    }

    public String getCardName() {
        return CardName;
    }

    public void setCardName(String cardName) {
        CardName = cardName;
    }

    public String getComments() {
        return Comments;
    }

    public void setComments(String comments) {
        Comments = comments;
    }

    public Double getDocTotal() {
        return DocTotal;
    }

    public void setDocTotal(Double docTotal) {
        DocTotal = docTotal;
    }

    public String getDocumentStatus() {
        return DocumentStatus;
    }

    public void setDocumentStatus(String documentStatus) {
        DocumentStatus = documentStatus;
    }

    public String getDocCurrency() {
        return DocCurrency;
    }

    public void setDocCurrency(String docCurrency) {
        DocCurrency = docCurrency;
    }

    public String getDocDueDate() {
        return DocDueDate;
    }

    public void setDocDueDate(String docDueDate) {
        DocDueDate = docDueDate;
    }

    public String getNumAtCard() {
        return NumAtCard;
    }

    public void setNumAtCard(String numAtCard) {
        NumAtCard = numAtCard;
    }

    public Integer getDocNum() {
        return DocNum;
    }

    public void setDocNum(Integer docNum) {
        DocNum = docNum;
    }

    public String getU_BPVS_CODTRAS_APP() {
        return U_BPVS_CODTRAS_APP;
    }

    public void setU_BPVS_CODTRAS_APP(String u_BPVS_CODTRAS_APP) {
        U_BPVS_CODTRAS_APP = u_BPVS_CODTRAS_APP;
    }

    public Double getPaidToDate() {
        return PaidToDate;
    }

    public void setPaidToDate(Double paidToDate) {
        this.PaidToDate = paidToDate;
    }
}
