package com.ndp.models.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ndp.models.model.DetallePedido;
import com.ndp.models.model.Pedido;

public class PedidoView implements Serializable {

    private int idPedido;
    private double totalFinal;
    private double impuestoPedido;
    private double totalNeto;
    private String comentario;
    private String estado;
    private Date fechaEntrega;
    private double descuento;
    private int estadoLogico;
    private Date fechaEmision;
    private List<DetallePedidoView> detallesPedido;
    private ClienteView clienteSecundario;
    private int idAlmacen;
    private int idClientePrincipal;
    private int idClienteSecundario;
    private int idDireccionEntrega;
    private int idDireccionFacturacion;
    private int idMoneda;
    private int idVendedor;

    public PedidoView(Pedido pedido, ArrayList<DetallePedido> listaDetalles) {
        this.idPedido = pedido.getIdPedido();
        this.totalFinal = pedido.getTotalFinal();
        this.impuestoPedido = pedido.getImpuestoPedido();
        this.totalNeto = pedido.getTotalNeto();
        this.comentario = pedido.getComentario();
        this.estado = "GENERADO";
        this.fechaEntrega = pedido.getFechaEntrega();
        this.descuento = pedido.getDescuento();
        this.estadoLogico = 1; //Innecesario
       // this.idAlmacen = pedido.getAlmacen().getAlmacen();
        //this.idClientePrincipal = pedido.getClientePrincipal().getIdCliente();
        /*if(pedido.getClienteSecundario() == null)
            this.idClienteSecundario = 0;
        else
            this.idClienteSecundario = pedido.getClienteSecundario().getIdCliente();*/
       // this.idDireccionEntrega = pedido.getDireccionEntrega().getIdDireccion();
        //this.idDireccionFacturacion = pedido.getDireccionFacturacion().getIdDireccion();
        this.idMoneda = 1;

        if(pedido.getVendedor() == null)
            this.idVendedor = 0;
        else
            this.idVendedor = pedido.getVendedor().getIdVendedor();
        this.detallesPedido = new ArrayList<>();
        for(DetallePedido detallePedido: listaDetalles) {
            DetallePedidoView detallePedidoView = new DetallePedidoView(detallePedido);
            this.detallesPedido.add(detallePedidoView);
        }
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public double getTotalFinal() {
        return totalFinal;
    }

    public void setTotalFinal(double totalFinal) {
        this.totalFinal = totalFinal;
    }

    public double getImpuestoPedido() {
        return impuestoPedido;
    }

    public void setImpuestoPedido(double impuestoPedido) {
        this.impuestoPedido = impuestoPedido;
    }

    public double getTotalNeto() {
        return totalNeto;
    }

    public void setTotalNeto(double totalNeto) {
        this.totalNeto = totalNeto;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public int getEstadoLogico() {
        return estadoLogico;
    }

    public void setEstadoLogico(int estadoLogico) {
        this.estadoLogico = estadoLogico;
    }

    public List<DetallePedidoView> getDetallesPedido() {
        return detallesPedido;
    }

    public void setDetallesPedido(List<DetallePedidoView> detallesPedido) {
        this.detallesPedido = detallesPedido;
    }

    public ClienteView getClienteSecundario() {
        return clienteSecundario;
    }

    public void setClienteSecundario(ClienteView clienteSecundario) {
        this.clienteSecundario = clienteSecundario;
    }

    public int getIdAlmacen() {
        return idAlmacen;
    }

    public void setIdAlmacen(int idAlmacen) {
        this.idAlmacen = idAlmacen;
    }

    public int getIdClientePrincipal() {
        return idClientePrincipal;
    }

    public void setIdClientePrincipal(int idClientePrincipal) {
        this.idClientePrincipal = idClientePrincipal;
    }

    public int getIdClienteSecundario() {
        return idClienteSecundario;
    }

    public void setIdClienteSecundario(int idClienteSecundario) {
        this.idClienteSecundario = idClienteSecundario;
    }

    public int getIdDireccionEntrega() {
        return idDireccionEntrega;
    }

    public void setIdDireccionEntrega(int idDireccionEntrega) {
        this.idDireccionEntrega = idDireccionEntrega;
    }

    public int getIdDireccionFacturacion() {
        return idDireccionFacturacion;
    }

    public void setIdDireccionFacturacion(int idDireccionFacturacion) {
        this.idDireccionFacturacion = idDireccionFacturacion;
    }

    public int getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(int idMoneda) {
        this.idMoneda = idMoneda;
    }

    public int getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(int idVendedor) {
        this.idVendedor = idVendedor;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }
}
