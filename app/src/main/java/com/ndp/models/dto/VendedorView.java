package com.ndp.models.dto;

import java.io.Serializable;
import java.util.Date;

public class VendedorView implements Serializable {
    private Integer idVendedor;
    private String tipoDocIdentidad;
    private String numeroDocIdentidad;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String telefono;
    private Character sexo;
    private String correo;
    private String password;
    private Date fechaNacimiento;
    private int estadoLogico;
    private AlmacenView almacen;

    public VendedorView() {
//        super(2);
        this.almacen = null;
    }

    public VendedorView(Integer idVendedor, String tipoDocIdentidad,
                        String numeroDocIdentidad, String nombres, String apellidoPaterno,
                        String apellidoMaterno, String telefono, Character sexo,
                        String correo, String password, Date fechaNacimiento,
                        int estadoLogico, AlmacenView almacen) {
//        super(2);
        this.idVendedor = idVendedor;
        this.tipoDocIdentidad = tipoDocIdentidad;
        this.numeroDocIdentidad = numeroDocIdentidad;
        this.nombres = nombres;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.telefono = telefono;
        this.sexo = sexo;
        this.correo = correo;
        this.password = password;
        this.fechaNacimiento = fechaNacimiento;
        this.estadoLogico = estadoLogico;
        this.almacen = almacen;
    }

    public Integer getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(Integer idVendedor) {
        this.idVendedor = idVendedor;
    }

    public String getTipoDocIdentidad() {
        return tipoDocIdentidad;
    }

    public void setTipoDocIdentidad(String tipoDocIdentidad) {
        this.tipoDocIdentidad = tipoDocIdentidad;
    }

    public String getNumeroDocIdentidad() {
        return numeroDocIdentidad;
    }

    public void setNumeroDocIdentidad(String numeroDocIdentidad) {
        this.numeroDocIdentidad = numeroDocIdentidad;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEstadoLogico() {
        return estadoLogico;
    }

    public void setEstadoLogico(int estadoLogico) {
        this.estadoLogico = estadoLogico;
    }

    public AlmacenView getAlmacen() {
        return almacen;
    }

    public void setAlmacen(AlmacenView almacen) {
        this.almacen = almacen;
    }
}
