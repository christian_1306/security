package com.ndp.models.dto;

import java.io.Serializable;
import java.util.Date;

public class FacturaView implements Serializable{
    private Integer idFactura;
    private String numeroFactura;
    private double montoTotal;
    private double impuestoTotal;
    private Date fecha;
    private String estado;
    private ClienteView clientePrincipal;
    private ClienteView clienteSecundario;

    public FacturaView(){

    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public double getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(double montoTotal) {
        this.montoTotal = montoTotal;
    }

    public double getImpuestoTotal() {
        return impuestoTotal;
    }

    public void setImpuestoTotal(double impuestoTotal) {
        this.impuestoTotal = impuestoTotal;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public ClienteView getClientePrincipal() {
        return clientePrincipal;
    }

    public void setClientePrincipal(ClienteView clientePrincipal) {
        this.clientePrincipal = clientePrincipal;
    }

    public ClienteView getClienteSecundario() {
        return clienteSecundario;
    }

    public void setClienteSecundario(ClienteView clienteSecundario) {
        this.clienteSecundario = clienteSecundario;
    }
}
