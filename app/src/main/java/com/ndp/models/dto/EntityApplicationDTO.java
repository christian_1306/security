package com.ndp.models.dto;

import java.io.Serializable;
import java.time.LocalDate;

public class EntityApplicationDTO implements Serializable {

    private String id;
    private ApplicationDTO application;
    private LocalDate expirationDate;

    public EntityApplicationDTO() {
    }

    public EntityApplicationDTO(String id, ApplicationDTO application, LocalDate expirationDate) {
        this.id = id;
        this.application = application;
        this.expirationDate = expirationDate;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ApplicationDTO getApplication() {
        return this.application;
    }

    public void setApplication(ApplicationDTO application) {
        this.application = application;
    }

    public LocalDate getExpirationDate() {
        return this.expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }
}

