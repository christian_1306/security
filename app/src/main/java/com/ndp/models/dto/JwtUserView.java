package com.ndp.models.dto;

import java.io.Serializable;

public class JwtUserView implements Serializable{
    private Integer id;
    private String username;
    private String role;
    private String token;
    private ClienteView cliente;
    private VendedorView vendedor;

    public JwtUserView() {
    }

    public JwtUserView(Integer id, String username, String role) {
        this.id = id;
        this.username = username;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ClienteView getCliente() {
        return cliente;
    }

    public void setCliente(ClienteView cliente) {
        this.cliente = cliente;
    }

    public VendedorView getVendedor() {
        return vendedor;
    }

    public void setVendedor(VendedorView vendedor) {
        this.vendedor = vendedor;
    }
}
