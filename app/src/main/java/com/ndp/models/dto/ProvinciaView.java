package com.ndp.models.dto;

import java.io.Serializable;
import java.util.List;

public class ProvinciaView implements Serializable{
    private int idProvincia;
    private String nombre;
    private List<MunicipioView> listaMunicipios;
    private DepartamentoView departamento;

    public ProvinciaView(){

    }

    public int getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(int idProvincia) {
        this.idProvincia = idProvincia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<MunicipioView> getListaMunicipios() {
        return listaMunicipios;
    }

    public void setListaMunicipios(List<MunicipioView> listaMunicipios) {
        this.listaMunicipios = listaMunicipios;
    }

    public DepartamentoView getDepartamento() {
        return departamento;
    }

    public void setDepartamento(DepartamentoView departamento) {
        this.departamento = departamento;
    }
}
