package com.ndp.models.dto;

import java.io.Serializable;

public class DireccionView implements Serializable {

    private int idDireccion;
    private String direccion;
    private int idMunicipio;
    private String nombMunicipio;
    private String tipo;
    private String nombreDireccion;

    public DireccionView(){

    }


    public int getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(int idDireccion) {
        this.idDireccion = idDireccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(int idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    public String getNombMunicipio() {
        return nombMunicipio;
    }

    public void setNombMunicipio(String nombMunicipio) {
        this.nombMunicipio = nombMunicipio;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombreDireccion() {
        return nombreDireccion;
    }

    public void setNombreDireccion(String nombreDireccion) {
        this.nombreDireccion = nombreDireccion;
    }
}
