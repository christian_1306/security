package com.ndp.models.dto;

import java.io.Serializable;

public class CondicionPagoView implements Serializable {

    private Integer codigo;
    private String nombre;
    private Integer dias;
    private Integer meses;
    private Integer diasTolerancia;
    private Double descuentoGeneral;
    private Double limiteCredito;

    public CondicionPagoView(){

    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getDias() {
        return dias;
    }

    public void setDias(Integer dias) {
        this.dias = dias;
    }

    public Integer getMeses() {
        return meses;
    }

    public void setMeses(Integer meses) {
        this.meses = meses;
    }

    public Integer getDiasTolerancia() {
        return diasTolerancia;
    }

    public void setDiasTolerancia(Integer diasTolerancia) {
        this.diasTolerancia = diasTolerancia;
    }

    public Double getDescuentoGeneral() {
        return descuentoGeneral;
    }

    public void setDescuentoGeneral(Double descuentoGeneral) {
        this.descuentoGeneral = descuentoGeneral;
    }

    public Double getLimiteCredito() {
        return limiteCredito;
    }

    public void setLimiteCredito(Double limiteCredito) {
        this.limiteCredito = limiteCredito;
    }


}
