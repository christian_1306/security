package com.ndp.models.dto;

import java.io.Serializable;

public class UserView implements Serializable {

    private String codUsuario;
    private String nomUsuario;
    private String docUsuario;
    private String correoUsuario;
    private Integer tipoUsuario;
    private String almCodUsuario;
    private String almNomUsuario;
    private String tokenUsuario;
    private String fechaExpiracionUsuario;


    public UserView(String codUsuario, String nomUsuario, String docUsuario, String correoUsuario, Integer tipoUsuario, String almCodUsuario, String almNomUsuario, String tokenUsuario, String fechaExpiracionUsuario){
        this.codUsuario = codUsuario;
        this.nomUsuario = nomUsuario;
        this.docUsuario = docUsuario;
        this.correoUsuario = correoUsuario;
        this.tipoUsuario = tipoUsuario;
        this.almCodUsuario = almCodUsuario;
        this.almNomUsuario = almNomUsuario;
        this.tokenUsuario = tokenUsuario;
        this.fechaExpiracionUsuario = fechaExpiracionUsuario;
    }

    //region get & set
    public String getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(String codUsuario) {
        this.codUsuario = codUsuario;
    }

    public String getNomUsuario() {
        return nomUsuario;
    }

    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }

    public Integer getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(Integer tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getDocUsuario() {
        return docUsuario;
    }

    public void setDocUsuario(String docUsuario) {
        this.docUsuario = docUsuario;
    }

    public String getCorreoUsuario() {
        return correoUsuario;
    }

    public void setCorreoUsuario(String correoUsuario) {
        this.correoUsuario = correoUsuario;
    }

    public String getTokenUsuario() {
        return tokenUsuario;
    }

    public void setTokenUsuario(String tokenUsuario) {
        this.tokenUsuario = tokenUsuario;
    }

    public String getFechaExpiracionUsuario() {
        return fechaExpiracionUsuario;
    }

    public void setFechaExpiracionUsuario(String fechaExpiracionUsuario) {
        this.fechaExpiracionUsuario = fechaExpiracionUsuario;
    }

    public String getAlmCodUsuario() {
        return almCodUsuario;
    }

    public void setAlmCodUsuario(String almCodUsuario) {
        this.almCodUsuario = almCodUsuario;
    }

    public String getAlmNomUsuario() {
        return almNomUsuario;
    }

    public void setAlmNomUsuario(String almNomUsuario) {
        this.almNomUsuario = almNomUsuario;
    }
    //endregion
}
