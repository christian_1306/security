package com.ndp.models.dto;

import java.io.Serializable;
import java.util.List;

public class EntityDTO implements Serializable {

    private String idEntity;
    private FederationDTO federation;
    private String ruc;
    private String businessName;
    private String address;
    private String enabled;
    private List<EntityApplicationDTO> licenses;

    public EntityDTO() {
    }

    public EntityDTO( String idEntity, FederationDTO federation, String ruc, String businessName, String address, String enabled, List<EntityApplicationDTO> licenses) {

        this.idEntity = idEntity;
        this.federation = federation;
        this.ruc = ruc;
        this.businessName = businessName;
        this.address = address;
        this.enabled = enabled;
        this.licenses = licenses;
    }


    public EntityDTO( String idEntity, String ruc, String businessName, String address, String enabled, List<EntityApplicationDTO> licenses) {

        this.idEntity = idEntity;
        this.ruc = ruc;
        this.businessName = businessName;
        this.address = address;
        this.enabled = enabled;
        this.licenses = licenses;
    }

    public String getIdEntity() {
        return idEntity;
    }

    public void setIdEntity(String idEntity) {
        this.idEntity = idEntity;
    }

    public FederationDTO getFederation() {
        return federation;
    }

    public void setFederation(FederationDTO federation) {
        this.federation = federation;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public List<EntityApplicationDTO> getLicenses() {
        return licenses;
    }

    public void setLicenses(List<EntityApplicationDTO> licenses) {
        this.licenses = licenses;
    }
}
