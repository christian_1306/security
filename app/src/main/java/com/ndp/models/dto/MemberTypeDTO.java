package com.ndp.models.dto;

import java.io.Serializable;

public class MemberTypeDTO implements Serializable {

    private String code;
    private String name;

    public MemberTypeDTO() {
    }

    public MemberTypeDTO( String code, String name) {

        this.code = code;
        this.name = name;
    }



    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}