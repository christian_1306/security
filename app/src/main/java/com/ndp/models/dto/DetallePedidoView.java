package com.ndp.models.dto;

import java.io.Serializable;

import com.ndp.models.model.DetallePedido;

public class DetallePedidoView implements Serializable {

    private int idDetallePedido;
    private int cantidad;
    private double subtotalNeto;
    private int estadoLogico;
    private int pesoTotal;
    private double subtotalFinal;
    private double impuestoDetallePedido;
    private int esPerdido;
    //    private ArticuloView articulo;
//    private MonedaView moneda;
//    private TarifarioView tarifario;
//    private int idArticulo;
//    private int idMoneda;
//    private int idTarifario;
    private int idTarifarioXArticulo;

    public DetallePedidoView() {
    }

    public DetallePedidoView(DetallePedido detallePedido) {
        this.cantidad = detallePedido.getCantidad();
        if(detallePedido.isEsPerdida())
            this.esPerdido = 1;
        else
            this.esPerdido = 0;
        this.impuestoDetallePedido = detallePedido.getImpuestoDetallePedido();
        this.pesoTotal = detallePedido.getPesoTotal();
        this.subtotalFinal = detallePedido.getSubtotalFinal();
        this.subtotalNeto = detallePedido.getSubtotalNeto();
        //this.idTarifarioXArticulo = detallePedido.getTarifarioXArticulo().getIdTarifarioXArticulo();
    }

    public int getIdDetallePedido() {
        return idDetallePedido;
    }

    public void setIdDetallePedido(int idDetallePedido) {
        this.idDetallePedido = idDetallePedido;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getSubtotalNeto() {
        return subtotalNeto;
    }

    public void setSubtotalNeto(double subtotalNeto) {
        this.subtotalNeto = subtotalNeto;
    }

    public int getEstadoLogico() {
        return estadoLogico;
    }

    public void setEstadoLogico(int estadoLogico) {
        this.estadoLogico = estadoLogico;
    }

    public int getPesoTotal() {
        return pesoTotal;
    }

    public void setPesoTotal(int pesoTotal) {
        this.pesoTotal = pesoTotal;
    }

    public double getSubtotalFinal() {
        return subtotalFinal;
    }

    public void setSubtotalFinal(double subtotalFinal) {
        this.subtotalFinal = subtotalFinal;
    }

    public double getImpuestoDetallePedido() {
        return impuestoDetallePedido;
    }

    public void setImpuestoDetallePedido(double impuestoDetallePedido) {
        this.impuestoDetallePedido = impuestoDetallePedido;
    }

    public int getEsPerdido() {
        return esPerdido;
    }

    public void setEsPerdido(int esPerdido) {
        this.esPerdido = esPerdido;
    }


    public int getIdTarifarioXArticulo() {
        return idTarifarioXArticulo;
    }

    public void setIdTarifarioXArticulo(int idTarifarioXArticulo) {
        this.idTarifarioXArticulo = idTarifarioXArticulo;
    }
}

