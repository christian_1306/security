package com.ndp.models.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


public class MemberDTO implements Serializable {


    private String idMember;
    private List<EntityDTO> entities;
    private MemberTypeDTO memberType;
    private String documentType;
    private String documentNumber;
    private String surname;
    private String name;
    private String sex;
    private Date birthday;
    private String address;
    private String email;
    private List<String> phoneNumbers;
    private String enabled;
    private String blocked;
    private String activated;
    private RoleDTO role;
    private String unlimitedSessionUser;
    private String protocolCode;
    private List<RoleDTO> roles;

    public MemberDTO() {
    }


    public MemberDTO(String idMember, List<EntityDTO> entities, MemberTypeDTO memberType, String documentType, String documentNumber, String surname, String name, String sex, Date birthday, String address, String email, List<String> phoneNumbers, String enabled, String blocked, String activated, RoleDTO role, String unlimitedSessionUser, String protocolCode, List<RoleDTO> roles) {
        this.idMember = idMember;
        this.entities = entities;
        this.memberType = memberType;
        this.documentType = documentType;
        this.documentNumber = documentNumber;
        this.surname = surname;
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
        this.address = address;
        this.email = email;
        this.phoneNumbers = phoneNumbers;
        this.enabled = enabled;
        this.blocked = blocked;
        this.activated = activated;
        this.role = role;
        this.unlimitedSessionUser = unlimitedSessionUser;
        this.protocolCode = protocolCode;
        this.roles = roles;
    }



    public String getIdMember() {
        return idMember;
    }

    public void setIdMember(String idMember) {
        this.idMember = idMember;
    }

    public List<EntityDTO> getEntities() {
        return entities;
    }

    public void setEntities(List<EntityDTO> entities) {
        this.entities = entities;
    }

    public MemberTypeDTO getMemberType() {
        return memberType;
    }

    public void setMemberType(MemberTypeDTO memberType) {
        this.memberType = memberType;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getBlocked() {
        return blocked;
    }

    public void setBlocked(String blocked) {
        this.blocked = blocked;
    }

    public String getActivated() {
        return activated;
    }

    public void setActivated(String activated) {
        this.activated = activated;
    }

    public RoleDTO getRole() {
        return role;
    }

    public void setRole(RoleDTO role) {
        this.role = role;
    }

    public String getUnlimitedSessionUser() {
        return unlimitedSessionUser;
    }

    public void setUnlimitedSessionUser(String unlimitedSessionUser) {
        this.unlimitedSessionUser = unlimitedSessionUser;
    }

    public String getProtocolCode() {
        return protocolCode;
    }

    public void setProtocolCode(String protocolCode) {
        this.protocolCode = protocolCode;
    }

    public List<RoleDTO> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleDTO> roles) {
        this.roles = roles;
    }
}

