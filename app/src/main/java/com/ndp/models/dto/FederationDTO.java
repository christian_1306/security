package com.ndp.models.dto;

import java.io.Serializable;

public class FederationDTO implements Serializable {

    private String code="123";
    private String name;
    private EntityDTO ownerEntity;

    public FederationDTO() {
    }

    public FederationDTO(String code, String name, EntityDTO ownerEntity) {
        this.code = code;
        this.name = name;
        this.ownerEntity = ownerEntity;
    }

    public FederationDTO(String name, EntityDTO ownerEntity) {
        this.name = name;
        this.ownerEntity = ownerEntity;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EntityDTO getOwnerEntity() {
        return this.ownerEntity;
    }

    public void setOwnerEntity(EntityDTO ownerEntity) {
        this.ownerEntity = ownerEntity;
    }
}
