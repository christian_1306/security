package com.ndp.models.dto;

import java.io.Serializable;

public class TarifarioXArticuloView implements Serializable {

    private int codTarifario;
    private String idImpuesto;
    private String idMoneda;
    private double impuestoArticulo;
    private String incluyeImpuesto;
    private double precioDefaultNeto;
    private double precioDefautFinal;


    public TarifarioXArticuloView(){

    }


    public String getIdImpuesto() {
        return idImpuesto;
    }

    public void setIdImpuesto(String idImpuesto) {
        this.idImpuesto = idImpuesto;
    }

    public String getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(String idMoneda) {
        this.idMoneda = idMoneda;
    }

    public double getImpuestoArticulo() {
        return impuestoArticulo;
    }

    public void setImpuestoArticulo(double impuestoArticulo) {
        this.impuestoArticulo = impuestoArticulo;
    }

    public String getIncluyeImpuesto() {
        return incluyeImpuesto;
    }

    public void setIncluyeImpuesto(String incluyeImpuesto) {
        this.incluyeImpuesto = incluyeImpuesto;
    }

    public double getPrecioDefaultNeto() {
        return precioDefaultNeto;
    }

    public void setPrecioDefaultNeto(double precioDefaultNeto) {
        this.precioDefaultNeto = precioDefaultNeto;
    }

    public double getPrecioDefautFinal() {
        return precioDefautFinal;
    }

    public void setPrecioDefautFinal(double precioDefautFinal) {
        this.precioDefautFinal = precioDefautFinal;
    }

    public int getCodTarifario() {
        return codTarifario;
    }

    public void setCodTarifario(int codTarifario) {
        this.codTarifario = codTarifario;
    }
}
