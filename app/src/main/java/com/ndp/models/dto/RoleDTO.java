package com.ndp.models.dto;

import java.io.Serializable;
import java.util.List;

public class RoleDTO implements Serializable {

    private String roleId;
    private String code;
    private String name;
    private String description;
    private String enabled;
    private EntityDTO entity;
    private List<PermissionDTO> permissions;

    public RoleDTO() {
    }

    public RoleDTO(String roleId, String code, String name, String description, String enabled, EntityDTO entity, List<PermissionDTO> permissions) {
        this.roleId = roleId;
        this.code = code;
        this.name = name;
        this.description = description;
        this.enabled = enabled;
        this.entity = entity;
        this.permissions = permissions;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public EntityDTO getEntity() {
        return entity;
    }

    public void setEntity(EntityDTO entity) {
        this.entity = entity;
    }

    public List<PermissionDTO> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionDTO> permissions) {
        this.permissions = permissions;
    }
}
