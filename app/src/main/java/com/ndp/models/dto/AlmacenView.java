package com.ndp.models.dto;

import java.io.Serializable;
import java.util.List;

public class AlmacenView implements Serializable {

    private int idAlmacen;
    private String codAlmacen;
    private String nombreAlmacen;
    private int estadoLogico;
    private List<TarifarioView> listaTarifarios;


    public AlmacenView(){

    }

    public int getIdAlmacen() {
        return idAlmacen;
    }

    public void setIdAlmacen(int idAlmacen) {
        this.idAlmacen = idAlmacen;
    }

    public String getNombreAlmacen() {
        return nombreAlmacen;
    }

    public void setNombreAlmacen(String nombreAlmacen) {
        this.nombreAlmacen = nombreAlmacen;
    }

    public int getEstadoLogico() {
        return estadoLogico;
    }

    public void setEstadoLogico(int estadoLogico) {
        this.estadoLogico = estadoLogico;
    }

    public List<TarifarioView> getListaTarifarios() {
        return listaTarifarios;
    }

    public void setListaTarifarios(List<TarifarioView> listaTarifarios) {
        this.listaTarifarios = listaTarifarios;
    }

    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }
}
