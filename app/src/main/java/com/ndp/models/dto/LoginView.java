package com.ndp.models.dto;

import java.io.Serializable;

public class LoginView  implements Serializable {
    private ClienteView cliente;
    private VendedorView vendedor;

    public LoginView(){

    }

    public ClienteView getCliente() {
        return cliente;
    }

    public void setCliente(ClienteView cliente) {
        this.cliente = cliente;
    }

    public VendedorView getVendedor() {
        return vendedor;
    }

    public void setVendedor(VendedorView vendedor) {
        this.vendedor = vendedor;
    }
}
