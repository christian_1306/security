package com.ndp.models.dto;

import java.io.Serializable;

public class MonedaView implements Serializable {

    private String idMoneda;
    private String nombreMoneda;

    public MonedaView(){}

    public String getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(String idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getNombreMoneda() {
        return nombreMoneda;
    }

    public void setNombreMoneda(String nombreMoneda) {
        this.nombreMoneda = nombreMoneda;
    }
}
