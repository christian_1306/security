package com.ndp.models.dto;

import java.io.Serializable;
import java.util.List;

public class DepartamentoView implements Serializable {

    private Integer idDepartamento;
    private String nombre;
    private List<ProvinciaView> listaProvincias;

    public DepartamentoView(){

    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<ProvinciaView> getListaProvincias() {
        return listaProvincias;
    }

    public void setListaProvincias(List<ProvinciaView> listaProvincias) {
        this.listaProvincias = listaProvincias;
    }
}
