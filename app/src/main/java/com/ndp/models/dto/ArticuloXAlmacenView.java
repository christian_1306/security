package com.ndp.models.dto;

import java.io.Serializable;

public class ArticuloXAlmacenView implements Serializable {

    private int idArticuloXAlmacen;
    private int stock;
    private int comprometido;
    private int disponible;
    private int solicitado;
    private String codAlmacen;

    public ArticuloXAlmacenView(){

    }

    public int getIdArticuloXAlmacen() {
        return idArticuloXAlmacen;
    }

    public void setIdArticuloXAlmacen(int idArticuloXAlmacen) {
        this.idArticuloXAlmacen = idArticuloXAlmacen;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getComprometido() {
        return comprometido;
    }

    public void setComprometido(int comprometido) {
        this.comprometido = comprometido;
    }

    public int getDisponible() {
        return disponible;
    }

    public void setDisponible(int disponible) {
        this.disponible = disponible;
    }

    public int getSolicitado() {
        return solicitado;
    }

    public void setSolicitado(int solicitado) {
        this.solicitado = solicitado;
    }

    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }
}
