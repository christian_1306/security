package com.ndp.models.dto;

import java.io.Serializable;

public class PermissionDTO implements Serializable {

    private String PermissionId;
    private String roleId;
    private String roleName;
    private String objectId;
    private String objectCode;
    private String objectName;
    private String objectDescription;
    private String enabled;

    public PermissionDTO() {
    }

    public PermissionDTO(String permissionId, String roleId, String roleName, String objectId, String objectCode, String objectName, String objectDescription, String enabled) {
        PermissionId = permissionId;
        this.roleId = roleId;
        this.roleName = roleName;
        this.objectId = objectId;
        this.objectCode = objectCode;
        this.objectName = objectName;
        this.objectDescription = objectDescription;
        this.enabled = enabled;
    }

    public String getPermissionId() {
        return PermissionId;
    }

    public void setPermissionId(String permissionId) {
        PermissionId = permissionId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getObjectCode() {
        return objectCode;
    }

    public void setObjectCode(String objectCode) {
        this.objectCode = objectCode;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectDescription() {
        return objectDescription;
    }

    public void setObjectDescription(String objectDescription) {
        this.objectDescription = objectDescription;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }
}

