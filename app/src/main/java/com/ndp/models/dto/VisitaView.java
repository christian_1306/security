package com.ndp.models.dto;

import java.io.Serializable;
import java.util.Date;

import com.ndp.models.model.Visita;

public class VisitaView implements Serializable {
    private Integer idVisita;
    private Date fechaVisita;
    private String motivo;
    private String comentario;
    private int estado;
    private ClienteView cliente;
    private DireccionView direccion;
    private Integer idCliente;
    private Integer idDireccion;
    private Integer idVendedor;

    public VisitaView(){

    }

    public VisitaView(Visita visita, int idVendedor){
        this.idVisita = visita.getIdVisita();
        this.fechaVisita = visita.getFechaVisita();
        this.motivo = visita.getMotivo();
        this.comentario = visita.getComentario();
        this.estado = visita.getEstado();
        //this.cliente = new ClienteView(visita.getCliente());
        //this.direccion = new DireccionView(visita.getDireccion());
        //this.idCliente = visita.getCliente().getIdCliente();
        //this.idDireccion = visita.getDireccion().getIdDireccion();
        this.idVendedor = idVendedor;
    }

    public Integer getIdVisita() {
        return idVisita;
    }

    public void setIdVisita(Integer idVisita) {
        this.idVisita = idVisita;
    }

    public Date getFechaVisita() {
        return fechaVisita;
    }

    public void setFechaVisita(Date fechaVisita) {
        this.fechaVisita = fechaVisita;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public ClienteView getCliente() {
        return cliente;
    }

    public void setCliente(ClienteView cliente) {
        this.cliente = cliente;
    }

    public DireccionView getDireccion() {
        return direccion;
    }

    public void setDireccion(DireccionView direccion) {
        this.direccion = direccion;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(Integer idDireccion) {
        this.idDireccion = idDireccion;
    }

    public Integer getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(Integer idVendedor) {
        this.idVendedor = idVendedor;
    }
}
