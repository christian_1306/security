package com.ndp.models.dto;

import java.io.Serializable;

public class CoreRutaView implements Serializable {

    private int id;
    private int estado;
    private String descripcion;

    public CoreRutaView(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
