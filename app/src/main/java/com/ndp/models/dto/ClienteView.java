package com.ndp.models.dto;

import java.io.Serializable;
import java.util.List;


public class ClienteView implements Serializable {

    private String cardCode;
    private String razonSocial;
    private String comentario;
    private String numeroDocIdentidad;
    private String telefono;
    private String correo;
    private double deuda;
    private double lineaCredito;
    private double saldoCredito;
    private String codAlmacen;
    private String nombAlmacen;
    private String condicionDePago;
    private List<DireccionView> listaDirecciones;
    private List<PersonaContactoView> personaContacto;
    private Integer codListaPrecio;
    private String nombListaPrecio;

    private String tipoPersona;
    private Integer tipoDocumento;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;

    private Integer estadoLogico;

    public ClienteView(){}

    public String getNumeroDocIdentidad() {
        return numeroDocIdentidad;
    }

    public void setNumeroDocIdentidad(String numeroDocIdentidad) {
        this.numeroDocIdentidad = numeroDocIdentidad;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }


    public double getDeuda() {
        return deuda;
    }

    public void setDeuda(double deuda) {
        this.deuda = deuda;
    }

    public double getLineaCredito() {
        return lineaCredito;
    }

    public void setLineaCredito(double lineaCredito) {
        this.lineaCredito = lineaCredito;
    }

    public double getSaldoCredito() {
        return saldoCredito;
    }

    public void setSaldoCredito(double saldoCredito) {
        this.saldoCredito = saldoCredito;
    }

    public Integer getEstadoLogico() {
        return estadoLogico;
    }

    public void setEstadoLogico(Integer estadoLogico) {
        this.estadoLogico = estadoLogico;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getCondicionDePago() {
        return condicionDePago;
    }

    public void setCondicionDePago(String condicionDePago) {
        this.condicionDePago = condicionDePago;
    }

    public Integer getCodListaPrecio() {
        return codListaPrecio;
    }

    public void setCodListaPrecio(Integer codListaPrecio) {
        this.codListaPrecio = codListaPrecio;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }

    public String getNombAlmacen() {
        return nombAlmacen;
    }

    public void setNombAlmacen(String nombAlmacen) {
        this.nombAlmacen = nombAlmacen;
    }

    public List<DireccionView> getListaDirecciones() {
        return listaDirecciones;
    }

    public void setListaDirecciones(List<DireccionView> listaDirecciones) {
        this.listaDirecciones = listaDirecciones;
    }

    public List<PersonaContactoView> getPersonaContacto() {
        return personaContacto;
    }

    public void setPersonaContacto(List<PersonaContactoView> personaContacto) {
        this.personaContacto = personaContacto;
    }

    public String getNombListaPrecio() {
        return nombListaPrecio;
    }

    public void setNombListaPrecio(String nombListaPrecio) {
        this.nombListaPrecio = nombListaPrecio;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public Integer getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(Integer tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }
}
