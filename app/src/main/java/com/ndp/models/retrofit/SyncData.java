package com.ndp.models.retrofit;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.ndp.models.appRest.restView;
import com.ndp.models.model.Almacen;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.DetallePedido;
import com.ndp.models.model.Direccion;
import com.ndp.models.model.Factura;
import com.ndp.models.model.Pedido;
import com.ndp.models.model.PersonaContacto;
import com.ndp.models.model.Tarifario;
import com.ndp.models.model.Vendedor;
import com.ndp.models.model.Visita;
import com.ndp.models.dto.AlmacenView;
import com.ndp.models.dto.ClienteView;
import com.ndp.models.dto.DepartamentoView;
import com.ndp.models.dto.DetallePedidoView;
import com.ndp.models.dto.DireccionView;
import com.ndp.models.dto.FacturaView;
import com.ndp.models.dto.JwtUserView;
import com.ndp.models.dto.LoginView;
import com.ndp.models.dto.PedidoView;
import com.ndp.models.dto.TarifarioView;
import com.ndp.models.dto.VendedorView;
import com.ndp.models.dto.VisitaView;
import com.ndp.ui.activities.HomeActivity;
import com.ndp.ui.activities.LoginActivity;
import com.ndp.data.network.AcerosCallback;
import com.ndp.data.network.RestClient;
import retrofit2.Call;
import retrofit2.Response;

public class SyncData implements Serializable{

    public SyncData(){
    }

    public boolean fillDepartments(StringBuilder outMessage/*,String token*/){
        Call<restView<List<DepartamentoView>>> call = new RestClient().getWebservices().listDepartments(/*token*/);
        try {
            Response<restView<List<DepartamentoView>>> response = call.execute();
            restView<List<DepartamentoView>> answer = response.body();
            if (answer != null) {
                   /* if(answer.getErrorCode()) {
                        List<DepartamentoView> departmentsView = answer.getData();
                        insertDepartmentsInDB(departmentsView);
                        return true;
                    }*/
            }
        }catch (Exception ex){
            Log.e("FillDepartmentsError","Exception fill departments",ex);
            outMessage.append(ex.getMessage());
        }
        return false;
    }

    private void insertDepartmentsInDB(List<DepartamentoView> departmentsView){


    }

    public boolean fillWarehouses(StringBuilder outMessage){
        Call<restView<List<AlmacenView>>> call = new RestClient().getWebservices().listAllWarehouses();
        try {
            Response<restView<List<AlmacenView>>> response = call.execute();
            restView<List<AlmacenView>> answer = response.body();
            if (answer != null){
                /*if(answer.getErrorCode()) {
                    List<AlmacenView> almacenesView = answer.getData();
                    insertWarehousesInDB(almacenesView);
                    return true;
                }*/
            }

        }
        catch (Exception ex){
            Log.e("FillWarehouses","Exception fill warehouses",ex);
            outMessage.append(ex.getMessage());
        }
        return false;
    }

    private void insertWarehousesInDB(List<AlmacenView> almacenesView){
        for(AlmacenView almacenView : almacenesView){
            List<Almacen> listaAlmacenes = Almacen.find(Almacen.class,"id_almacen = ?",String.valueOf(almacenView.getIdAlmacen()));
            Almacen almacen;
            if(listaAlmacenes.size()==0){//es nuevo almacen
                almacen = new Almacen(almacenView);
            }
            else{//actualizar datos de almacen
                almacen = listaAlmacenes.get(0);
                //almacen.actualizarDatos(almacenView);
            }
            almacen.save();
            for(TarifarioView tarifarioView : almacenView.getListaTarifarios()){
                List<Tarifario> listaTarifarios = Tarifario.find(Tarifario.class,"id_tarifario = ?",String.valueOf(tarifarioView.getIdTarifario()));
                Tarifario tarifario;
                if(listaTarifarios.size()==0){//es nuevo tarifario
                    tarifario = new Tarifario(tarifarioView,almacen);
                }
                else{//actualizar datos de un tarifario
                    tarifario = listaTarifarios.get(0);
                    tarifario.actualizarDatos(tarifarioView,almacen);
                }
                tarifario.save();
            }
        }
    }

    public boolean fillBills(ApiData data/*,String token*/){
        Call<restView<List<FacturaView>>> call = new RestClient().getWebservices().listAllBills(data/*,token*/);
        try {
            Response<restView<List<FacturaView>>> response = call.execute();
            restView<List<FacturaView>> answer = response.body();
            if (answer != null){
                /*if(answer.getErrorCode()) {
                    List<FacturaView> facturasView = answer.getData();
                    insertBillsInDB(facturasView);
                    return true;
                }*/
            }
        }catch (Exception ex){
            Log.e("FillBillsError","Exception fill bills",ex);
        }
        return false;
    }

    private void insertBillsInDB(List<FacturaView> facturasView){
        for(FacturaView facturaView : facturasView){
            Factura factura = new Factura(facturaView);
            factura.save();
        }
    }

    public boolean fillVisits(ApiData data/*,String token*/){
        Call<restView<List<VisitaView>>> call = new RestClient().getWebservices().listAllVisits(data/*,token*/);
        try {
            Response<restView<List<VisitaView>>> response = call.execute();
            Log.d("onResponseVisits", "onResponseVisits");
            restView<List<VisitaView>> answer = response.body();
            if (answer != null) {
                /*if(answer.getErrorCode()) {
                    List<VisitaView> visitasView = answer.getData();
                    insertVisitsInDB(visitasView);
                    return true;
                }*/
            }

        }catch (Exception ex){
            Log.e("FillVisitsClientsError","Exception fill visits",ex);
        }
        return false;
    }


    private void insertVisitsInDB(List<VisitaView> visitasView){
        for(VisitaView visitaView : visitasView){
            Visita visita = new Visita(visitaView);
            visita.save();
        }
    }

    public boolean fillClients(ApiData data/*,String token*/){
        Call<restView<List<ClienteView>>> call = new RestClient().getWebservices().listClients(data/*,token*/);
        try {
            Response<restView<List<ClienteView>>> response = call.execute();
            restView<List<ClienteView>> answer = response.body();
            if (answer != null){
                /*if(answer.getErrorCode()) {
                    List<ClienteView> clienteView = answer.getData();
                    insertClientsInDB(clienteView);
                    return true;
                }*/
            }
        }catch (Exception ex){
            Log.e("FillClientsError","Exception fill clients",ex);
        }
        return false;
    }

    private void insertClientsInDB(List<ClienteView> clientsView){
        for (ClienteView clienteView : clientsView) {
            Cliente cliente = new Cliente(clienteView,0);
            Almacen almacen = null;
            Tarifario tarifario = null;
          //  if(cliente.getTipoCliente()==1){
               // List<Cliente> listaClientes = Cliente.find(Cliente.class,"id_cliente = ?",String.valueOf(clienteView.getIdCliente()));
                //PersonaContacto personaContacto = null;
               // if(listaClientes.size()==0){//Es nuevo cliente
                 //   personaContacto = new PersonaContacto(clienteView.getPersonaContacto());
                //}
               // else{//Es una actualizacion
                    /*cliente = listaClientes.get(0);
                    cliente.actualizarDatos(clienteView);
                    List<PersonaContacto> listaPersonasContactos = PersonaContacto.find(PersonaContacto.class,"id_persona_contacto = ?",String.valueOf(clienteView.getPersonaContacto().getIdPersonaContacto()));
                    if(listaPersonasContactos.size()!=0) {
                        personaContacto.actualizarDatos(clienteView.getPersonaContacto());
                    }*/
              //  }
                /*almacen = Almacen.find(Almacen.class,"id_almacen = ?",String.valueOf(clienteView.getAlmacen().getIdAlmacen())).get(0);
                tarifario = Tarifario.find(Tarifario.class,"id_tarifario = ?",String.valueOf(clienteView.getTarifario().getCodListaPrecio())).get(0);
                cliente.setAlmacen(almacen);
                cliente.setTarifario(tarifario);
                cliente.setPersonaContacto(personaContacto);*/
          //  }
           // else{
          //      continue;
          //  }
            cliente.save();
           /* if(cliente.getTipoCliente()==1){
                for(ClienteView clienteView_2 : clienteView.getListaClientes()){
                    List<Cliente> clientes = Cliente.find(Cliente.class,"id_cliente = ?",String.valueOf(clienteView_2.getIdCliente()));
                    Cliente cliente_2 = null;
                    if(clientes.size()== 0){
                        cliente_2 = new Cliente(clienteView_2);
                        cliente_2.save();
                    }
                    else{
                        cliente_2 = clientes.get(0);
                    }
                    ClientePrincipalSecundario clientePrincipalSecundario = new ClientePrincipalSecundario();
                    clientePrincipalSecundario.setClientePrincipal(cliente);
                    clientePrincipalSecundario.setClienteSecundario(cliente_2);
                    clientePrincipalSecundario.save();
                }
            }*/
            for (DireccionView direccionView : clienteView.getListaDirecciones()) {
               // Municipio municipio = Municipio.findWithQuery(Municipio.class, "SELECT * FROM MUNICIPIO WHERE id_municipio = " + direccionView.getMunicipio().getIdMunicipio()).get(0);
                //Direccion direccion = new Direccion(direccionView, cliente, municipio);
                //direccion.save();
            }
        }
    }

    public boolean fillOrders(ApiData data){
        Call<restView<List<PedidoView>>> call = new RestClient().getWebservices().listAllOrders(data);
        try {
            Response<restView<List<PedidoView>>> response = call.execute();
            restView<List<PedidoView>> answer = response.body();
            if (answer != null) {
                /*if(answer.getErrorCode()) {
                    List<PedidoView> pedidosView = answer.getData();
                    insertOrdersInDB(pedidosView);
                    return true;
                }*/
            }
        }catch (Exception ex){
            Log.e("FillOrdersSellerError","Exception fill orders seller",ex);
        }
        return false;
    }

    private void insertOrdersInDB(List<PedidoView> pedidosView){
        for(PedidoView pedidoView : pedidosView){
            Pedido pedido = new Pedido(pedidoView);
            pedido.save();
            for(DetallePedidoView detallePedidoView : pedidoView.getDetallesPedido()){
                DetallePedido detallePedido = new DetallePedido(detallePedidoView,pedido);
                detallePedido.save();
            }
        }
    }

    public boolean fillItemsByWarehouseAndRate(int id_almacen,int id_tarifario, final Activity activity,ApiData data){
        Call<restView<AlmacenView>> call = new RestClient().getWebservices().listItemsByWarehouseAndRate(data,id_almacen,id_tarifario);
        try {
            Response<restView<AlmacenView>> response = call.execute();
            restView<AlmacenView> answer = response.body();
            if (answer != null) {
               /* if (answer.getErrorCode()) {
                    AlmacenView almacenView = answer.getData();
                    Almacen almacen = Almacen.find(Almacen.class, "id_almacen = ?", String.valueOf(almacenView.getAlmacen())).get(0);
                    for (ArticuloView articuloView : almacenView.getListaArticulos()) {
                        Articulo articulo = new Articulo(articuloView);
                        articulo.save();
                    }

                    for (ArticuloXAlmacenView articuloXAlmacenView : almacenView.getListaArticuloXAlmacen()) {
                        ArticuloXAlmacen articuloXAlmacen = new ArticuloXAlmacen(articuloXAlmacenView, almacen);
                        articuloXAlmacen.save();
                    }

                    for (TarifarioView tarifarioView : almacenView.getListaTarifarios()) {

                        List<Tarifario> temp = Tarifario.find(Tarifario.class, "id_tarifario = ?",
                                String.valueOf(tarifarioView.getCodigoListaPrecio()));
                        Tarifario tarifario;
                        if (temp != null || temp.size() != 0)
                            tarifario = temp.get(0);
                        else {
                            tarifario = new Tarifario(tarifarioView, almacen);
                            tarifario.save();
                        }

                        for (TarifarioXArticuloView tarifarioXArticuloView : tarifarioView.getListaTarifarioXArticulo()) {
                            TarifarioXArticulo tarifarioXArticulo = new TarifarioXArticulo(tarifarioXArticuloView,
                                    tarifario);
                            tarifarioXArticulo.save();
                        }
                    }
                }*/
                return true;
            }
        }catch (Exception ex){
            Log.e("FillItemsRateError","Exception fill items rate",ex);
        }
        return false;
    }

    public void startLogin(final String usuario, String password, final LoginActivity loginActivity){

        Call<restView<JwtUserView>> call  = new RestClient().getWebservices().login(usuario,password);
        call.enqueue(new AcerosCallback<restView<JwtUserView>>(){
            @Override
            public void onResponse(@NonNull Call<restView<JwtUserView>> call, @NonNull Response<restView<JwtUserView>> response) {
                restView<JwtUserView> answer = response.body();
                if(answer!=null){
                    /*if(!answer.getErrorCode()) {
                        String mensaje = answer.getMessage();
                        Toast.makeText(loginActivity, mensaje, Toast.LENGTH_LONG).show();
                        return;
                    }*/

                    JwtUserView loginView = answer.getData();
                    if(loginView.getCliente() != null) {
                        ClienteView clienteView = loginView.getCliente();
                        Cliente cliente = new Cliente(clienteView,0);
                      //  Tarifario tarifario = Tarifario.find(Tarifario.class,"id_tarifario = ?",String.valueOf(clienteView.getTarifario().getCodListaPrecio())).get(0);
                        //Tarifario tarifario = new Tarifario(clienteView.getTarifario());
                     //   cliente.setTarifario(tarifario);
                     //   cliente.setAlmacen(Almacen.find(Almacen.class,"id_almacen = ?",String.valueOf(clienteView.getAlmacen().getIdAlmacen())).get(0));
                        cliente.save();
                        for(DireccionView direccionView : clienteView.getListaDirecciones()){
                         //   Municipio municipio = Municipio.find(Municipio.class,"id_municipio = ?",String.valueOf(direccionView.getMunicipio().getIdMunicipio())).get(0);
                       //     Direccion direccion = new Direccion(direccionView,cliente,municipio);
                      //      direccion.save();
                        }
                    }
                    else {
                        VendedorView vendedorView = loginView.getVendedor();
                        Vendedor vendedor = new Vendedor(vendedorView);
                        vendedor.save();
                    }
                    Intent intent = new Intent(loginActivity, HomeActivity.class);
                    if(loginView.getCliente() != null)
                        intent.putExtra("cliente", usuario);
                    else
                        intent.putExtra("vendedor", usuario);
                    //intent.putExtra("token",loginView.getToken());
                    loginActivity.startActivity(intent);
                    loginActivity.finish();
                }
            }

            @Override
            public void onFailure(@NonNull Call<restView<JwtUserView>> call, @NonNull Throwable t) {
            }
        });

    }

    public void registrarClienteEnDb(Cliente cliente, PersonaContacto personaContacto, int id_tarifario, int id_vendedor,
                                      final Activity activity, ArrayList<Cliente> selectedClients,ArrayList<Direccion> addresses,
                                      ApiData<ClienteView> data){
       // ClienteView clienteView = new ClienteView(cliente,personaContacto,selectedClients,addresses,id_tarifario,id_vendedor);
        //data.setData(clienteView);
        Call<restView<ClienteView>> call = new RestClient().getWebservices().saveClient(data);
        call.enqueue(new AcerosCallback<restView<ClienteView>>(){

            @Override
            public void onResponse(@NonNull Call<restView<ClienteView>> call, @NonNull Response<restView<ClienteView>> response) {
                //super.onResponse(call, response);
                restView<ClienteView> answer = response.body();
                if(answer!=null){
                    //TODO intent to next activity
                    /*if(answer.getErrorCode()) {
                        Toast.makeText(activity, "Cliente registrado", Toast.LENGTH_SHORT).show();
                        activity.finish();
                    }
                    else{
                        Toast.makeText(activity,answer.getMessage(),Toast.LENGTH_SHORT).show();
                        return;
                    }*/
                }
            }

            @Override
            public void onFailure(@NonNull Call<restView<ClienteView>> call, @NonNull Throwable t) {
                //super.onFailure(call, t);
                Toast.makeText(activity,"Error en la conexión ",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void registrarVisitaEnDb(Visita visita,ApiData<VisitaView> data,final Activity activity,int id_vendedor) {
        VisitaView visitaView = new VisitaView(visita,id_vendedor);
        data.setData(visitaView);
        Call<restView<VisitaView>> call = new RestClient().getWebservices().saveVisit(data);
        call.enqueue(new AcerosCallback<restView<VisitaView>>(){

            @Override
            public void onResponse(@NonNull Call<restView<VisitaView>> call, @NonNull Response<restView<VisitaView>> response) {
                //super.onResponse(call, response);
                restView<VisitaView> answer = response.body();
                if(answer!=null){
                    //TODO intent to next activity
                   /* if(answer.getErrorCode()) {
                        Toast.makeText(activity,"Visita registrado exitosamente",Toast.LENGTH_SHORT).show();
                        activity.finish();
                    }
                    else{
                        Toast.makeText(activity,answer.getMessage(),Toast.LENGTH_SHORT).show();
                        return;
                    }*/
                }
            }

            @Override
            public void onFailure(@NonNull Call<restView<VisitaView>> call, @NonNull Throwable t) {
                //super.onFailure(call, t);
                Toast.makeText(activity,"Error en la conexión ",Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void RegistrarPedidoEnDb(Pedido pedido, ArrayList<DetallePedido> listaDetalles,final Activity activity,ApiData<PedidoView> data) {
        PedidoView pedidoView = new PedidoView(pedido, listaDetalles);
        data.setData(pedidoView);
        Call<restView<PedidoView>> call = new RestClient().getWebservices().saveOrder(data);
        call.enqueue(new AcerosCallback<restView<PedidoView>>(){

            @Override
            public void onResponse(@NonNull Call<restView<PedidoView>> call, @NonNull Response<restView<PedidoView>> response) {
                //super.onResponse(call, response);
                restView<PedidoView> answer = response.body();
                if(answer!=null){
                   /* if(answer.getErrorCode()) {
                        Log.d("orderAlert", "order register");
                        Toast.makeText(activity, "Pedido registrado", Toast.LENGTH_SHORT).show();
                        activity.finish();
                    }
                    else{
                        Toast.makeText(activity,answer.getMessage(),Toast.LENGTH_SHORT).show();
                        return;
                    }*/
                }
            }

            @Override
            public void onFailure(@NonNull Call<restView<PedidoView>> call, @NonNull Throwable t) {
                Toast.makeText(activity,"Error en la conexión ",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void changePasswordInDB(String password_nuevo_1, int idUsuario, int tipoUsuario,final Activity activity,ApiData data){
        Call<restView<LoginView>> call = new RestClient().getWebservices().changePassword(idUsuario,password_nuevo_1,tipoUsuario,data);
        call.enqueue(new AcerosCallback<restView<LoginView>>(){
            @Override
            public void onResponse(@NonNull Call<restView<LoginView>> call, @NonNull Response<restView<LoginView>> response) {
                restView<LoginView> answer = response.body();
                if(answer!=null){
                   /* if(answer.getErrorCode()) {
                        Log.d("orderAlert", "order register");
                        Toast.makeText(activity, "Contraseña cambiada correctamente", Toast.LENGTH_SHORT).show();
                        activity.finish();
                    }
                    else{
                        Toast.makeText(activity,answer.getMessage(),Toast.LENGTH_SHORT).show();
                        return;
                    }*/
                }
            }

            @Override
            public void onFailure(@NonNull Call<restView<LoginView>> call, @NonNull Throwable t) {
                Toast.makeText(activity,"Error en la conexión ",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean listAllItems(ApiData apiData){
        Call<restView<List<AlmacenView>>> call = new RestClient().getWebservices().listAllItems(apiData);
        try {
            Response<restView<List<AlmacenView>>> response = call.execute();
            restView<List<AlmacenView>> answer = response.body();
            if (answer != null) {
                /*if (answer.getErrorCode()) {
                    List<AlmacenView> almacenesView = answer.getData();
                    for(AlmacenView almacenView : almacenesView) {
                        Almacen almacen = Almacen.find(Almacen.class, "id_almacen = ?", String.valueOf(almacenView.getAlmacen())).get(0);
                        if(almacenView.getListaArticulos()!=null) {
                            for (ArticuloView articuloView : almacenView.getListaArticulos()) {
                                Articulo articulo = new Articulo(articuloView);
                                articulo.save();
                            }
                        }

                        for (ArticuloXAlmacenView articuloXAlmacenView : almacenView.getListaArticuloXAlmacen()) {
                            ArticuloXAlmacen articuloXAlmacen = new ArticuloXAlmacen(articuloXAlmacenView, almacen);
                            articuloXAlmacen.save();
                        }

                        for (TarifarioView tarifarioView : almacenView.getListaTarifarios()) {

                            List<Tarifario> temp = Tarifario.find(Tarifario.class, "id_tarifario = ?",
                                    String.valueOf(tarifarioView.getCodigoListaPrecio()));
                            Tarifario tarifario;
                            if (temp != null || temp.size() != 0)
                                tarifario = temp.get(0);
                            else {
                                tarifario = new Tarifario(tarifarioView, almacen);
                                tarifario.save();
                            }

                            for (TarifarioXArticuloView tarifarioXArticuloView : tarifarioView.getListaTarifarioXArticulo()) {
                                TarifarioXArticulo tarifarioXArticulo = new TarifarioXArticulo(tarifarioXArticuloView,
                                        tarifario);
                                tarifarioXArticulo.save();
                            }
                        }
                    }
                }*/
                return true;
            }
        }catch (Exception ex){
            Log.e("FillItemsRateError","Exception fill items rate",ex);
        }
        return false;
    }
}
