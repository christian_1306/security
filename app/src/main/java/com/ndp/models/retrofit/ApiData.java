package com.ndp.models.retrofit;

import java.io.Serializable;

public class ApiData<T> implements Serializable {
    private Integer idVendedor;
    private Integer idCliente;
    private String imei;
    private String ip;
    private boolean forzar;
    private T data;

    public ApiData(){
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public boolean isForzar() {
        return forzar;
    }

    public void setForzar(boolean forzar) {
        this.forzar = forzar;
    }

    public Integer getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(Integer idVendedor) {
        this.idVendedor = idVendedor;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getImei() {
        return imei;
    }


    public void setImei(String imei) {
        this.imei = imei;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
