package com.ndp.models.model;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.List;

import com.ndp.models.dto.MonedaView;
import com.ndp.utils.Useful;

public class Moneda extends SugarRecord {
    @Unique
    private String idMoneda;

    private String nombre_moneda;

    public Moneda() {}

    public Moneda(String idMoneda, String nombre_moneda) {
        this.idMoneda = idMoneda;
        this.nombre_moneda = nombre_moneda;
    }

    public Moneda(MonedaView moneda) {
        this.idMoneda = moneda.getIdMoneda();
        this.nombre_moneda = moneda.getNombreMoneda();
    }

    public String getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(String idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getNombre_moneda() {
        return nombre_moneda;
    }

    public void setNombre_moneda(String nombre_moneda) {
        this.nombre_moneda = nombre_moneda;
    }

    public void saveOrUpdate(){
        List<Moneda> monedas = (Moneda.find(Moneda.class, Useful.getCol("idMoneda") + " = ?", String.valueOf(idMoneda)));
        if (monedas.size() > 0 && monedas.get(0).getId() != null){
            monedas.get(0).setNombre_moneda(nombre_moneda);
            monedas.get(0).update();
        }
        else
        {
            Moneda.save(new Moneda(idMoneda, nombre_moneda));
        }
    }
}
