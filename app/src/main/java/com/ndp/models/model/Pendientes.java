package com.ndp.models.model;

import java.util.Date;

public class Pendientes implements Comparable<Pendientes> {

    private String codCreacionApp;
    private Date fechaRegistro;
    private Integer typeObject;
    private String descripcionObject;
    private Integer estadoMigracion;
    private String mensajeMigracion;

    public Pendientes () {}

    public String getCodCreacionApp() {
        return codCreacionApp;
    }

    public void setCodCreacionApp(String codCreacionApp) {
        this.codCreacionApp = codCreacionApp;
    }


    public String getDescripcionObject() {
        return descripcionObject;
    }

    public void setDescripcionObject(String descripcionObject) {
        this.descripcionObject = descripcionObject;
    }

    public Integer getEstadoMigracion() {
        return estadoMigracion;
    }

    public void setEstadoMigracion(Integer estadoMigracion) {
        this.estadoMigracion = estadoMigracion;
    }

    public String getMensajeMigracion() {
        return mensajeMigracion;
    }

    public void setMensajeMigracion(String mensajeMigracion) {
        this.mensajeMigracion = mensajeMigracion;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getTypeObject() {
        return typeObject;
    }

    public void setTypeObject(Integer typeObject) {
        this.typeObject = typeObject;
    }

    @Override
    public int compareTo(Pendientes o) {
        if (getFechaRegistro() == null || o.getFechaRegistro() == null)
            return 0;
        return getFechaRegistro().compareTo(o.getFechaRegistro());
    }
}
