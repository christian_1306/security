package com.ndp.models.model;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.List;

import com.ndp.models.dto.TarifarioXArticuloView;
import com.ndp.utils.Useful;

public class TarifarioXArticulo extends SugarRecord {

    private int codTarifario;
    private String itemCode;
    private double precioDefaultFinal;
    private double precioDefaultNeto;
    private double impuestoArticulo;
    private String incluyeImpuesto;
    private String impuesto;
    private String moneda;
    private Articulo articulo;

    @Ignore
    private String nombreListPrice;

    public TarifarioXArticulo() {
    }

    public TarifarioXArticulo(int codTarifario, double precioDefaultFinal, double precioDefaultNeto, double impuestoArticulo,
                              String incluyeImpuesto, String impuesto, String moneda, Articulo articulo)
    {
        this.codTarifario = codTarifario;
        this.precioDefaultFinal = precioDefaultFinal;
        this.precioDefaultNeto = precioDefaultNeto;
        this.impuestoArticulo = impuestoArticulo;
        this.incluyeImpuesto = incluyeImpuesto;
        this.impuesto = impuesto;
        this.moneda = moneda;
        this.itemCode = articulo.getCodigo();
        this.articulo = articulo;
    }

    public TarifarioXArticulo(TarifarioXArticuloView tarifarioXArticuloView, Articulo articulo) {
        this.codTarifario = tarifarioXArticuloView.getCodTarifario();
        this.precioDefaultFinal = tarifarioXArticuloView.getPrecioDefautFinal();
        this.precioDefaultNeto = tarifarioXArticuloView.getPrecioDefaultNeto();
        this.impuestoArticulo = tarifarioXArticuloView.getImpuestoArticulo();
        this.incluyeImpuesto = tarifarioXArticuloView.getIncluyeImpuesto();
        this.impuesto = tarifarioXArticuloView.getIdImpuesto();
        this.moneda = tarifarioXArticuloView.getIdMoneda();
        this.itemCode = articulo.getCodigo();
        this.articulo = articulo;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public double getPrecioDefaultFinal() {
        return precioDefaultFinal;
    }

    public void setPrecioDefaultFinal(double precioDefaultFinal) {
        this.precioDefaultFinal = precioDefaultFinal;
    }

    public double getPrecioDefaultNeto() {
        return precioDefaultNeto;
    }

    public void setPrecioDefaultNeto(double precioDefaultNeto) {
        this.precioDefaultNeto = precioDefaultNeto;
    }

    public double getImpuestoArticulo() {
        return impuestoArticulo;
    }

    public void setImpuestoArticulo(double impuestoArticulo) {
        this.impuestoArticulo = impuestoArticulo;
    }

    public List<TarifarioXArticulo> listarPorTarifario(int codTarifario){
        return TarifarioXArticulo.find(TarifarioXArticulo.class, Useful.getCol("codTarifario") + " = ?",String.valueOf(codTarifario));
    }

    public int getCodTarifario() {
        return codTarifario;
    }

    public void setCodTarifario(int codTarifario) {
        this.codTarifario = codTarifario;
    }

    public String getIncluyeImpuesto() {
        return incluyeImpuesto;
    }

    public void setIncluyeImpuesto(String incluyeImpuesto) {
        this.incluyeImpuesto = incluyeImpuesto;
    }

    public String getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(String impuesto) {
        this.impuesto = impuesto;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public void deleteAll(Articulo articulo){
        List<Articulo> articulos =
                (Articulo.find(Articulo.class, Useful.getCol("codigo")+" = ?", String.valueOf(articulo.getCodigo())));
        if (articulos != null && articulos.size() > 0)
        {
            List<TarifarioXArticulo> tarifarioXArticulos =
                    (TarifarioXArticulo.find(TarifarioXArticulo.class, Useful.getCol("articulo")+" = ?", String.valueOf(articulos.get(0).getId())));
            if (tarifarioXArticulos != null && tarifarioXArticulos.size() > 0){
                for (TarifarioXArticulo tarifarioXArticulo : tarifarioXArticulos){
                    tarifarioXArticulo.delete();
                }
            }
        }
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getNombreListPrice() {
        return nombreListPrice;
    }

    public void setNombreListPrice(String nombreListPrice) {
        this.nombreListPrice = nombreListPrice;
    }
}
