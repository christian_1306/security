package com.ndp.models.model;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.List;

import com.ndp.models.dto.CoreRutaView;
import com.ndp.utils.Useful;

public class CoreRuta extends SugarRecord {

    @Unique
    private int idRuta;

    private int estado;
    private String descripcion;

    public CoreRuta() { }

    public CoreRuta(int id, int estado, String descripcion) {
        this.idRuta = id;
        this.estado = estado;
        this.descripcion = descripcion;
    }

    public CoreRuta(CoreRutaView coreRutaView){
        this.idRuta = coreRutaView.getId();
        this.estado = coreRutaView.getEstado();
        this.descripcion = coreRutaView.getDescripcion();
    }


    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(int idRuta) {
        this.idRuta = idRuta;
    }

    public void saveOrUpdate(){
        List<CoreRuta> coreRutaList = (CoreRuta.find(CoreRuta.class, Useful.getCol("idRuta") + " = ?", String.valueOf(idRuta)));
        if (coreRutaList.size() > 0 && coreRutaList.get(0).getId() != null){
            coreRutaList.get(0).setDescripcion(descripcion);
            coreRutaList.get(0).setEstado(estado);
            coreRutaList.get(0).update();
        }
        else
        {
            CoreRuta.save(new CoreRuta(idRuta,estado, descripcion));
        }
    }
}
