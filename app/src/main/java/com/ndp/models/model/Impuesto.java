package com.ndp.models.model;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.List;

import com.ndp.models.dto.ImpuestoView;
import com.ndp.utils.Useful;

public class Impuesto extends SugarRecord {
    @Unique
    private String idImpuesto;

    private String nombreImpuesto;
    private double porcentajeImpuesto;

    public Impuesto() { }

    public Impuesto(String idImpuesto, String nombreImpuesto, double porcentajeImpuesto) {
        this.idImpuesto = idImpuesto;
        this.nombreImpuesto = nombreImpuesto;
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public Impuesto(ImpuestoView impuesto) {
        this.idImpuesto = impuesto.getIdImpuesto();
        this.nombreImpuesto = impuesto.getNombreImpuesto();
        this.porcentajeImpuesto = impuesto.getPorcentajeDescuento();
    }


    public String getIdImpuesto() {
        return idImpuesto;
    }

    public void setIdImpuesto(String idImpuesto) {
        this.idImpuesto = idImpuesto;
    }

    public String getNombreImpuesto() {
        return nombreImpuesto;
    }

    public void setNombreImpuesto(String nombreImpuesto) {
        this.nombreImpuesto = nombreImpuesto;
    }

    public double getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(double porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public void saveOrUpdate(){
        List<Impuesto> impuestos = (Impuesto.find(Impuesto.class, Useful.getCol("idImpuesto") + " = ?", String.valueOf(idImpuesto)));
        if (impuestos.size() > 0 && impuestos.get(0).getId() != null){
            impuestos.get(0).setNombreImpuesto(nombreImpuesto);
            impuestos.get(0).setPorcentajeImpuesto(porcentajeImpuesto);
            impuestos.get(0).update();
        }
        else
        {
            Impuesto.save(new Impuesto(idImpuesto, nombreImpuesto, porcentajeImpuesto));
        }
    }
}
