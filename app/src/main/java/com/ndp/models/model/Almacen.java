package com.ndp.models.model;

import android.support.annotation.NonNull;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.List;

import com.ndp.models.dto.AlmacenView;
import com.ndp.utils.Useful;

public class Almacen extends SugarRecord {
    @Unique
    private String codAlmacen;

    private String nombre;
    private int isActive;

    public Almacen() {
    }

    public Almacen(String codAlmacen, String nombre, int isActive) {
        this.codAlmacen = codAlmacen;
        this.nombre = nombre;
        this.isActive = isActive;
    }

    public Almacen(AlmacenView almacenView){
        this.codAlmacen = almacenView.getCodAlmacen();
        this.nombre = almacenView.getNombreAlmacen();
        this.isActive = almacenView.getEstadoLogico();
    }

    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    @NonNull
    @Override
    public String toString() {
        return this.nombre;
    }

    public Almacen findAlmacen (String codAlmacen)
    {
        List<Almacen> almacen = Almacen.find(Almacen.class, Useful.getCol("codAlmacen") + " = ?", codAlmacen );
        if (almacen != null && almacen.size() > 0)
        {
            return almacen.get(0);
        }
        return null;
    }

    public void saveOrUpdate(){
        List<Almacen> almacen = (Almacen.find(Almacen.class, Useful.getCol("codAlmacen") + " = ?", String.valueOf(codAlmacen)));
        if (almacen.size() > 0 && almacen.get(0).getId() != null){
            almacen.get(0).setNombre(nombre);
            almacen.get(0).setIsActive(isActive);
            almacen.get(0).update();
        }
        else
        {
            Almacen.save(new Almacen(codAlmacen,nombre, isActive));
        }
    }
}
