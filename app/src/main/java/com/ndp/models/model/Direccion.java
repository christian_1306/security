package com.ndp.models.model;

import com.orm.SugarRecord;

import com.ndp.models.dto.DireccionView;

public class Direccion extends SugarRecord  {

    private Long id;

    private String cardCode;
    private String nombreDireccion;
    private String direccion;
    private String tipo;
    private Cliente cliente;

    public Direccion() {}

    public Direccion(DireccionView direccionView, Cliente cliente){
        this.direccion = direccionView.getDireccion();
        this.tipo = direccionView.getTipo();
        this.nombreDireccion = direccionView.getNombreDireccion();
        this.cardCode = cliente.getCardCode();
        this.cliente = cliente;
    }


    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getNombreDireccion() {
        return nombreDireccion;
    }

    public void setNombreDireccion(String nombreDireccion) {
        this.nombreDireccion = nombreDireccion;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
}
