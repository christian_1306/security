package com.ndp.models.model;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.List;

import com.ndp.models.dto.CondicionPagoView;
import com.ndp.utils.Useful;

public class CondicionPago extends SugarRecord {

    @Unique
    private Integer codigo;

    private String nombre;
    private Integer dias;
    private Integer meses;
    private Integer diasTolerancia;
    private Double descuentoGeneral;
    private Double limiteCredito;
    private Integer isActive;

    public CondicionPago () {}

    public CondicionPago(Integer codigo, String nombre, Integer dias,
                         Integer meses, Integer diasTolerancia, Double descuentoGeneral, Double limiteCredito, Integer isActive) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.dias = dias;
        this.meses = meses;
        this.diasTolerancia = diasTolerancia;
        this.descuentoGeneral = descuentoGeneral;
        this.limiteCredito = limiteCredito;
        this.isActive = isActive;
    }

    public CondicionPago(CondicionPagoView condicionPagoView){
        this.codigo = condicionPagoView.getCodigo();
        this.nombre = condicionPagoView.getNombre();
        this.dias = condicionPagoView.getDias();
        this.meses = condicionPagoView.getMeses();
        this.diasTolerancia = condicionPagoView.getDiasTolerancia();
        this.descuentoGeneral = condicionPagoView.getDescuentoGeneral();
        this.limiteCredito = condicionPagoView.getLimiteCredito();
    }

    public void saveOrUpdate(){
        List<CondicionPago> condicionPagoList = (CondicionPago.find(CondicionPago.class, Useful.getCol("codigo") + " = ?", String.valueOf(codigo)));
        if (condicionPagoList.size() > 0 && condicionPagoList.get(0).getId() != null){
            condicionPagoList.get(0).setNombre(nombre);
            condicionPagoList.get(0).setDias(dias);
            condicionPagoList.get(0).setMeses(meses);
            condicionPagoList.get(0).setDiasTolerancia(diasTolerancia);
            condicionPagoList.get(0).setDescuentoGeneral(descuentoGeneral);
            condicionPagoList.get(0).setLimiteCredito(limiteCredito);
            condicionPagoList.get(0).setIsActive(1);
            condicionPagoList.get(0).update();
        }
        else
        {
            CondicionPago.save(new CondicionPago(codigo, nombre, dias,
                    meses, diasTolerancia, descuentoGeneral, limiteCredito, 1));
        }
    }


    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getDias() {
        return dias;
    }

    public void setDias(Integer dias) {
        this.dias = dias;
    }

    public Integer getMeses() {
        return meses;
    }

    public void setMeses(Integer meses) {
        this.meses = meses;
    }

    public Integer getDiasTolerancia() {
        return diasTolerancia;
    }

    public void setDiasTolerancia(Integer diasTolerancia) {
        this.diasTolerancia = diasTolerancia;
    }

    public Double getDescuentoGeneral() {
        return descuentoGeneral;
    }

    public void setDescuentoGeneral(Double descuentoGeneral) {
        this.descuentoGeneral = descuentoGeneral;
    }

    public Double getLimiteCredito() {
        return limiteCredito;
    }

    public void setLimiteCredito(Double limiteCredito) {
        this.limiteCredito = limiteCredito;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }
}
