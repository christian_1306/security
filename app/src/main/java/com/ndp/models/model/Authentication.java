package com.ndp.models.model;

import com.google.gson.annotations.SerializedName;

public class Authentication {

    @SerializedName("code")
    private String errorCode;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private User data;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getData() {
        return data;
    }

    public void setData(User data) {
        this.data = data;
    }
}
