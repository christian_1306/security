package com.ndp.models.model;

import com.orm.SugarRecord;

//Esta clase será temporal ya que después se agregará un atributo de tipo Cliente a la clase Cliente
public class ClientePrincipalSecundario extends SugarRecord {
    private Cliente clientePrincipal;
    private Cliente clienteSecundario;

    public ClientePrincipalSecundario() {
    }

    public ClientePrincipalSecundario(Cliente clientePrincipal, Cliente clienteSecundario) {
        this.clientePrincipal = clientePrincipal;
        this.clienteSecundario = clienteSecundario;
    }

    public Cliente getClientePrincipal() {
        return clientePrincipal;
    }

    public void setClientePrincipal(Cliente clientePrincipal) {
        this.clientePrincipal = clientePrincipal;
    }

    public Cliente getClienteSecundario() {
        return clienteSecundario;
    }

    public void setClienteSecundario(Cliente clienteSecundario) {
        this.clienteSecundario = clienteSecundario;
    }
}
