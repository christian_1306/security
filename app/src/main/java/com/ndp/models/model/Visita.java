package com.ndp.models.model;

import com.orm.SugarRecord;

import java.sql.Time;
import java.util.Date;

import com.ndp.models.dto.VisitaView;

public class Visita extends SugarRecord {
    private int idVisita;
    private Direccion direccion;
    private Cliente cliente;
    private Vendedor vendedor;
    private Date fechaVisita;
    private Time horaVisita;
    private String motivo;
    private String comentario;
    private int estado;

    public Visita() {
    }

    public Visita(VisitaView visitaView){
        this.idVisita = visitaView.getIdVisita();
        this.direccion = Direccion.find(Direccion.class,"id_direccion = ?",String.valueOf(visitaView.getDireccion().getIdDireccion())).get(0);
        //this.cliente = Cliente.find(Cliente.class,"id_cliente = ?",String.valueOf(visitaView.getCliente().getIdCliente())).get(0);
        this.vendedor = Vendedor.find(Vendedor.class,"id_vendedor = ?",String.valueOf(visitaView.getIdVendedor())).get(0);
        this.fechaVisita = visitaView.getFechaVisita();
        this.motivo = visitaView.getMotivo();
        this.comentario = visitaView.getComentario();
        this.estado = visitaView.getEstado();
    }

    public Visita(int idVisita, Direccion direccion, Cliente cliente, Date fechaVisita, Time horaVisita,
                  String motivo, String comentario, int estado) {
        this.idVisita = idVisita;
        this.direccion = direccion;
        this.cliente = cliente;
        this.fechaVisita = fechaVisita;
        this.horaVisita = horaVisita;
        this.motivo = motivo;
        this.comentario = comentario;
        this.estado = estado;
    }

    public int getIdVisita() {
        return idVisita;
    }

    public void setIdVisita(int idVisita) {
        this.idVisita = idVisita;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getFechaVisita() {
        return fechaVisita;
    }

    public void setFechaVisita(Date fechaVisita) {
        this.fechaVisita = fechaVisita;
    }

    public Time getHoraVisita() {
        return horaVisita;
    }

    public void setHoraVisita(Time horaVisita) {
        this.horaVisita = horaVisita;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }
}
