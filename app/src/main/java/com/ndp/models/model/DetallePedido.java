package com.ndp.models.model;

import com.orm.SugarRecord;

import com.ndp.models.dto.DetallePedidoView;

public class DetallePedido extends SugarRecord {
    private Pedido pedido;
    private int cantidad;
    private double subtotalNeto;
    private int estado;
    private int pesoTotal;
    private double subtotalFinal;
    private double impuestoDetallePedido;
    private boolean esPerdida;
    private TarifarioXArticulo tarifarioXArticulo;

    public DetallePedido() {
    }

    public DetallePedido(Pedido pedido, int cantidad, double subtotalNeto,
                         int estado, int pesoTotal, double subtotalFinal,
                         double impuestoDetallePedido) {
        this.pedido = pedido;
        this.cantidad = cantidad;
        this.subtotalNeto = subtotalNeto;
        this.estado = estado;
        this.pesoTotal = pesoTotal;
        this.subtotalFinal = subtotalFinal;
        this.impuestoDetallePedido = impuestoDetallePedido;
    }

    public DetallePedido(DetallePedidoView detallePedidoView, Pedido pedido){
        this.pedido = pedido;
        this.cantidad = detallePedidoView.getCantidad();
        this.subtotalNeto = detallePedidoView.getSubtotalNeto();
        this.estado = detallePedidoView.getEstadoLogico();
        this.pesoTotal = detallePedidoView.getPesoTotal();
        this.subtotalFinal = detallePedidoView.getSubtotalFinal();
        this.subtotalNeto = detallePedidoView.getSubtotalNeto();
        this.impuestoDetallePedido = detallePedidoView.getImpuestoDetallePedido();
        if(detallePedidoView.getEsPerdido() == 1) this.esPerdida = true;
        else this.esPerdida = false;
        this.tarifarioXArticulo = TarifarioXArticulo.find(TarifarioXArticulo.class,"id_tarifario_x_articulo = ?",String.valueOf(detallePedidoView.getIdTarifarioXArticulo())).get(0);
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }


    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getSubtotalNeto() {
        return subtotalNeto;
    }

    public void setSubtotalNeto(double subtotalNeto) {
        this.subtotalNeto = subtotalNeto;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getPesoTotal() {
        return pesoTotal;
    }

    public void setPesoTotal(int pesoTotal) {
        this.pesoTotal = pesoTotal;
    }


    public double getSubtotalFinal() {
        return subtotalFinal;
    }

    public void setSubtotalFinal(double subtotalFinal) {
        this.subtotalFinal = subtotalFinal;
    }

    public double getImpuestoDetallePedido() {
        return impuestoDetallePedido;
    }

    public void setImpuestoDetallePedido(double impuestoDetallePedido) {
        this.impuestoDetallePedido = impuestoDetallePedido;
    }


    public boolean isEsPerdida() {
        return esPerdida;
    }

    public void setEsPerdida(boolean esPerdida) {
        this.esPerdida = esPerdida;
    }

    public TarifarioXArticulo getTarifarioXArticulo() {
        return tarifarioXArticulo;
    }

    public void setTarifarioXArticulo(TarifarioXArticulo tarifarioXArticulo) {
        this.tarifarioXArticulo = tarifarioXArticulo;
    }
}
