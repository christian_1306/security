package com.ndp.models.model;

import android.support.annotation.NonNull;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.List;

import com.ndp.models.dto.TarifarioView;
import com.ndp.utils.Useful;

public class Tarifario extends SugarRecord {
    @Unique
    private int codigoListaPrecio;

    private String nombre;
    private int isActive;
    private String warehouseCode;
    private Almacen almacen;

    public Tarifario() {
    }

    public Tarifario(int codigoListaPrecio, String nombre, int isActive, Almacen almacen) {
        this.codigoListaPrecio = codigoListaPrecio;
        this.nombre = nombre;
        this.isActive = isActive;
        this.almacen = almacen;
        this.warehouseCode = almacen.getCodAlmacen();
    }

    public Tarifario(TarifarioView tarifarioView, Almacen almacen){
        this.codigoListaPrecio = tarifarioView.getCodigoListaPrecio();
        this.nombre = tarifarioView.getNombre();
        this.isActive = tarifarioView.getEstadoLogico();
        this.almacen = almacen;
        this.warehouseCode = almacen.getCodAlmacen();
    }

    public Tarifario(TarifarioView tarifarioView) {
        this.codigoListaPrecio = tarifarioView.getIdTarifario();
        this.nombre = tarifarioView.getNombre();
        this.isActive = tarifarioView.getEstadoLogico();
    }

    public void actualizarDatos(TarifarioView tarifarioView,Almacen almacen){
        this.nombre = tarifarioView.getNombre();
        this.isActive = tarifarioView.getEstadoLogico();
        this.almacen = almacen;
    }

    public int getCodigoListaPrecio() {
        return codigoListaPrecio;
    }

    public void setCodigoListaPrecio(int codigoListaPrecio) {
        this.codigoListaPrecio = codigoListaPrecio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    @NonNull
    @Override
    public String toString() {
        return this.nombre;
    }

    public void saveOrUpdate(){
        List<Tarifario> tarifario = (Tarifario.find(Tarifario.class, Useful.getCol("codigoListaPrecio")+" = ?", String.valueOf(codigoListaPrecio)));
        if (tarifario.size() > 0 && tarifario.get(0).getId() != null){
            tarifario.get(0).setNombre(nombre);
            tarifario.get(0).setIsActive(isActive);
            tarifario.get(0).setAlmacen(almacen);
            tarifario.get(0).update();
        }
        else
        {
            Tarifario.save(new Tarifario(codigoListaPrecio, nombre, isActive, almacen));
        }
    }

    public String getWarehouseCode() {
        return warehouseCode;
    }

    public void setWarehouseCode(String warehouseCode) {
        this.warehouseCode = warehouseCode;
    }
}
