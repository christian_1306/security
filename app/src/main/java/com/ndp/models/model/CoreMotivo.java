package com.ndp.models.model;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.List;

import com.ndp.models.dto.CoreMotivoView;
import com.ndp.utils.Useful;

public class CoreMotivo extends SugarRecord {

    @Unique
    private int idMotivo;

    private int estado;
    private String descripcion;

    public CoreMotivo() { }

    public CoreMotivo(int id, int estado, String descripcion) {
        this.idMotivo = id;
        this.estado = estado;
        this.descripcion = descripcion;
    }

    public CoreMotivo(CoreMotivoView coreMotivoView){
        this.idMotivo = coreMotivoView.getId();
        this.estado = coreMotivoView.getEstado();
        this.descripcion = coreMotivoView.getDescripcion();
    }


    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdRuta() {
        return idMotivo;
    }

    public void setIdRuta(int idRuta) {
        this.idMotivo = idRuta;
    }

    public void saveOrUpdate(){
        List<CoreMotivo> coreMotivoList = (CoreMotivo.find(CoreMotivo.class, Useful.getCol("idMotivo") + " = ?", String.valueOf(idMotivo)));
        if (coreMotivoList.size() > 0 && coreMotivoList.get(0).getId() != null){
            coreMotivoList.get(0).setDescripcion(descripcion);
            coreMotivoList.get(0).setEstado(estado);
            coreMotivoList.get(0).update();
        }
        else
        {
            CoreMotivo.save(new CoreMotivo(idMotivo,estado, descripcion));
        }
    }
}
