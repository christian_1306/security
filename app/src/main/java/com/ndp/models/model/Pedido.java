package com.ndp.models.model;

import com.orm.SugarRecord;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import com.ndp.models.dto.PedidoView;

public class Pedido extends SugarRecord {
    private int idPedido;
    private Cliente clientePrincipal;
    private Direccion direccionEntrega;
    private Cliente clienteSecundario;
    private Vendedor vendedor;
    private double totalFinal;
    private double impuestoPedido;
    private double totalNeto;
    private String comentario;
    private Date fechaEmision;
    private int estado;
    private Time hora;
    private Moneda moneda;
    private Direccion direccionFacturacion;
    private Date fechaEntrega;
    private double descuento;
    private Almacen almacen;

    public Pedido() {
    }

    public Pedido(PedidoView pedidoView){
        this.idPedido = pedidoView.getIdPedido();
        this.clientePrincipal = Cliente.find(Cliente.class,"id_cliente = ?",String.valueOf(pedidoView.getIdClientePrincipal())).get(0);
        List<Cliente> clientes = Cliente.find(Cliente.class,"id_cliente = ?",String.valueOf(pedidoView.getIdClienteSecundario()));
        if(clientes.size()!=0) this.clienteSecundario = clientes.get(0);
        this.direccionEntrega = Direccion.find(Direccion.class,"id_direccion = ?",String.valueOf(pedidoView.getIdDireccionEntrega())).get(0);
        this.direccionFacturacion = Direccion.find(Direccion.class,"id_direccion = ?",String.valueOf(pedidoView.getIdDireccionFacturacion())).get(0);
        List<Vendedor> vendedors = Vendedor.find(Vendedor.class,"id_vendedor = ?",String.valueOf(pedidoView.getIdVendedor()));
        if(vendedors.size()!=0) this.vendedor = vendedors.get(0);
        this.totalFinal = pedidoView.getTotalFinal();
        this.totalNeto = pedidoView.getTotalNeto();
        this.impuestoPedido = pedidoView.getImpuestoPedido();
        this.comentario = pedidoView.getComentario();
        this.estado = pedidoView.getEstadoLogico();
        this.fechaEntrega = pedidoView.getFechaEntrega();
        this.fechaEmision = pedidoView.getFechaEmision();
        this.descuento = pedidoView.getDescuento();
        this.almacen = Almacen.find(Almacen.class,"id_almacen = ?",String.valueOf(pedidoView.getIdAlmacen())).get(0);
        this.moneda = Moneda.find(Moneda.class,"id_moneda = ?",String.valueOf(pedidoView.getIdMoneda())).get(0);
    }

    public Pedido(int idPedido, Cliente clientePrincipal, Direccion direccionEntrega,
                  Cliente clienteSecundario, Vendedor vendedor, double totalFinal, double impuestoPedido,
                  double totalNeto, String comentario, Date fechaEmision, int estado, Time hora, Moneda moneda,
                  Direccion direccionFacturacion, Date fechaEntrega, double descuento, Almacen almacen) {
        this.idPedido = idPedido;
        this.clientePrincipal = clientePrincipal;
        this.direccionEntrega = direccionEntrega;
        this.clienteSecundario = clienteSecundario;
        this.vendedor = vendedor;
        this.totalFinal = totalFinal;
        this.impuestoPedido = impuestoPedido;
        this.totalNeto = totalNeto;
        this.comentario = comentario;
        this.fechaEmision = fechaEmision;
        this.estado = estado;
        this.hora = hora;
        this.moneda = moneda;
        this.direccionFacturacion = direccionFacturacion;
        this.fechaEntrega = fechaEntrega;
        this.descuento = descuento;
        this.almacen = almacen;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public Cliente getClientePrincipal() {
        return clientePrincipal;
    }

    public void setClientePrincipal(Cliente clientePrincipal) {
        this.clientePrincipal = clientePrincipal;
    }

    public Direccion getDireccionEntrega() {
        return direccionEntrega;
    }

    public void setDireccionEntrega(Direccion direccionEntrega) {
        this.direccionEntrega = direccionEntrega;
    }

    public Cliente getClienteSecundario() {
        return clienteSecundario;
    }

    public void setClienteSecundario(Cliente clienteSecundario) {
        this.clienteSecundario = clienteSecundario;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public double getTotalFinal() {
        return totalFinal;
    }

    public void setTotalFinal(double totalFinal) {
        this.totalFinal = totalFinal;
    }

    public double getImpuestoPedido() {
        return impuestoPedido;
    }

    public void setImpuestoPedido(double impuestoPedido) {
        this.impuestoPedido = impuestoPedido;
    }

    public double getTotalNeto() {
        return totalNeto;
    }

    public void setTotalNeto(double totalNeto) {
        this.totalNeto = totalNeto;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public Direccion getDireccionFacturacion() {
        return direccionFacturacion;
    }

    public void setDireccionFacturacion(Direccion direccionFacturacion) {
        this.direccionFacturacion = direccionFacturacion;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    public List<DetallePedido> listarDetalles(){
        return DetallePedido.find(DetallePedido.class,"pedido = ?",String.valueOf(getId()));
    }
}
