package com.ndp.models.model;

import com.orm.SugarRecord;

import java.util.List;

import com.ndp.models.dto.ArticuloXAlmacenView;

public class ArticuloXAlmacen extends SugarRecord {

    private String itemCode;
    private String warehouseCode;
    private int stock;
    private int comprometido;
    private int solicitado;
    private int disponible;
    private Articulo articulo;

    public ArticuloXAlmacen() {}

    public ArticuloXAlmacen(ArticuloXAlmacenView articuloXAlmacenView, Articulo articulo) {
        Almacen alm = new Almacen();
        this.stock = articuloXAlmacenView.getStock();
        this.comprometido = articuloXAlmacenView.getComprometido();
        this.solicitado = articuloXAlmacenView.getSolicitado();
        this.disponible = articuloXAlmacenView.getDisponible();
        this.articulo = articulo;
        this.itemCode = articulo.getCodigo();
        this.warehouseCode = articuloXAlmacenView.getCodAlmacen();
    }


    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getComprometido() {
        return comprometido;
    }

    public void setComprometido(int comprometido) {
        this.comprometido = comprometido;
        this.stock = this.comprometido + this.disponible;
    }

    public int getDisponible() {
        return disponible;
    }

    public void setDisponible(int disponible) {
        this.disponible = disponible;
        this.stock = this.comprometido + this.disponible;
    }

    public ArticuloXAlmacen(Articulo articulo, Almacen almacen, int stock, int comprometido, int solicitado, int disponible, String itemCode, String warehouseCode) {
        this.stock = stock;
        this.comprometido = comprometido;
        this.solicitado = solicitado;
        this.disponible = disponible;
        this.articulo = articulo;
        this.warehouseCode = warehouseCode;
        this.itemCode = itemCode;
    }

    public List<ArticuloXAlmacen> obtenerArticulosXAlmacen(long idArticulo){
        return ArticuloXAlmacen.find(ArticuloXAlmacen.class,
                "articulo = ?", String.valueOf(idArticulo));
    }

    public List<ArticuloXAlmacen> getArticuloAlmacen(long idArticulo, long idAlmacen) {
        return ArticuloXAlmacen.find(ArticuloXAlmacen.class,
                "articulo = ? and almacen = ?", String.valueOf(idArticulo),
                String.valueOf(idAlmacen));
    }

    public int getSolicitado() {
        return solicitado;
    }

    public void setSolicitado(int solicitado) {
        this.solicitado = solicitado;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getWarehouseCode() {
        return warehouseCode;
    }

    public void setWarehouseCode(String warehouseCode) {
        this.warehouseCode = warehouseCode;
    }
}
