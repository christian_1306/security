package com.ndp.models.model;

import com.orm.SugarRecord;

import com.ndp.models.dto.PersonaContactoView;

public class PersonaContacto extends SugarRecord {

    private String cardCode;
    private String nombres;
    private String cargo;
    private String telefono;
    private String correo;
    private Cliente cliente;

    public PersonaContacto(){

    }

    public PersonaContacto(String cardCode, String nombres, String cargo, String telefono, String correo, Cliente cliente) {
        this.cardCode = cardCode;
        this.nombres = nombres;
        this.cargo = cargo;
        this.telefono = telefono;
        this.correo = correo;
        this.cliente = cliente;
    }

    public PersonaContacto(PersonaContactoView personaContactoView,  Cliente cliente){
        this.cardCode = cliente.getCardCode();
        this.nombres = personaContactoView.getNombres();
        this.cargo = personaContactoView.getCargo();
        this.telefono = personaContactoView.getTelefono();
        this.correo = personaContactoView.getCorreo();
        this.cliente = cliente;
    }

    public String getNombre() {
        return nombres;
    }

    public void setNombre(String nombre) {
        this.nombres = nombre;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }
}
