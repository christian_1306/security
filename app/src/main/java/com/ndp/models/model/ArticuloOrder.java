package com.ndp.models.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ArticuloOrder implements Parcelable {

    private String codigo;

    private String nombre;
    private double peso;
    private String descripcion;
    private int isActive;
    private String unidadMedida;
    private int varillasPorTonelada;
    //TODO: Price
    private int codTarifario;
    private double precioDefaultFinal;
    private double precioDefaultNeto;
    private double impuestoArticulo;
    private String incluyeImpuesto;
    private String impuesto;
    private String moneda;
    //TODO: Almacen
    private String warehouseCode;
    private int stock;
    private int comprometido;
    private int solicitado;
    private int disponible;
    //TODO: Linea
    private int cantidadOrder;

    public ArticuloOrder() {

    }


    protected ArticuloOrder(Parcel in) {
        codigo = in.readString();
        nombre = in.readString();
        peso = in.readDouble();
        descripcion = in.readString();
        isActive = in.readInt();
        unidadMedida = in.readString();
        varillasPorTonelada = in.readInt();
        codTarifario = in.readInt();
        precioDefaultFinal = in.readDouble();
        precioDefaultNeto = in.readDouble();
        impuestoArticulo = in.readDouble();
        incluyeImpuesto = in.readString();
        impuesto = in.readString();
        moneda = in.readString();
        warehouseCode = in.readString();
        stock = in.readInt();
        comprometido = in.readInt();
        solicitado = in.readInt();
        disponible = in.readInt();
        cantidadOrder = in.readInt();
    }

    public static final Creator<ArticuloOrder> CREATOR = new Creator<ArticuloOrder>() {
        @Override
        public ArticuloOrder createFromParcel(Parcel in) {
            return new ArticuloOrder(in);
        }

        @Override
        public ArticuloOrder[] newArray(int size) {
            return new ArticuloOrder[size];
        }
    };

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public int getVarillasPorTonelada() {
        return varillasPorTonelada;
    }

    public void setVarillasPorTonelada(int varillasPorTonelada) {
        this.varillasPorTonelada = varillasPorTonelada;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getCodTarifario() {
        return codTarifario;
    }

    public void setCodTarifario(int codTarifario) {
        this.codTarifario = codTarifario;
    }

    public double getPrecioDefaultFinal() {
        return precioDefaultFinal;
    }

    public void setPrecioDefaultFinal(double precioDefaultFinal) {
        this.precioDefaultFinal = precioDefaultFinal;
    }

    public double getPrecioDefaultNeto() {
        return precioDefaultNeto;
    }

    public void setPrecioDefaultNeto(double precioDefaultNeto) {
        this.precioDefaultNeto = precioDefaultNeto;
    }

    public double getImpuestoArticulo() {
        return impuestoArticulo;
    }

    public void setImpuestoArticulo(double impuestoArticulo) {
        this.impuestoArticulo = impuestoArticulo;
    }

    public String getIncluyeImpuesto() {
        return incluyeImpuesto;
    }

    public void setIncluyeImpuesto(String incluyeImpuesto) {
        this.incluyeImpuesto = incluyeImpuesto;
    }

    public String getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(String impuesto) {
        this.impuesto = impuesto;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getWarehouseCode() {
        return warehouseCode;
    }

    public void setWarehouseCode(String warehouseCode) {
        this.warehouseCode = warehouseCode;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getComprometido() {
        return comprometido;
    }

    public void setComprometido(int comprometido) {
        this.comprometido = comprometido;
    }

    public int getSolicitado() {
        return solicitado;
    }

    public void setSolicitado(int solicitado) {
        this.solicitado = solicitado;
    }

    public int getDisponible() {
        return disponible;
    }

    public void setDisponible(int disponible) {
        this.disponible = disponible;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(codigo);
        dest.writeString(nombre);
        dest.writeDouble(peso);
        dest.writeString(descripcion);
        dest.writeInt(isActive);
        dest.writeString(unidadMedida);
        dest.writeInt(varillasPorTonelada);
        dest.writeInt(codTarifario);
        dest.writeDouble(precioDefaultFinal);
        dest.writeDouble(precioDefaultNeto);
        dest.writeDouble(impuestoArticulo);
        dest.writeString(incluyeImpuesto);
        dest.writeString(impuesto);
        dest.writeString(moneda);
        dest.writeString(warehouseCode);
        dest.writeInt(stock);
        dest.writeInt(comprometido);
        dest.writeInt(solicitado);
        dest.writeInt(disponible);
        dest.writeInt(cantidadOrder);
    }

    public int getCantidadOrder() {
        return cantidadOrder;
    }

    public void setCantidadOrder(int cantidadOrder) {
        this.cantidadOrder = cantidadOrder;
    }

}
