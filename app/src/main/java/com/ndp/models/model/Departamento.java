package com.ndp.models.model;

import android.support.annotation.NonNull;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.List;

import com.ndp.models.dto.DepartamentoView;
import com.ndp.utils.Useful;

public class Departamento extends SugarRecord {
    @Unique
    private int idDepartamento;

    private String nombre;

    public Departamento() {
    }

    public Departamento(int idDepartamento, String nombre) {
        this.idDepartamento = idDepartamento;
        this.nombre = nombre;
    }

    public Departamento(DepartamentoView departamentoView){
        this.idDepartamento = departamentoView.getIdDepartamento();
        this.nombre = departamentoView.getNombre();
    }

    public int getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(int idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @NonNull
    @Override
    public String toString() {
        return this.nombre;
    }

    public void saveOrUpdate(){
        List<Departamento> departamento = (Departamento.find(Departamento.class, Useful.getCol("idDepartamento")+" = ?", String.valueOf(idDepartamento)));
        if (departamento.size() > 0 && departamento.get(0).getId() != null){
            departamento.get(0).setNombre(nombre);
            departamento.get(0).update();
        }
        else
        {
            Departamento.save(new Departamento(idDepartamento,nombre));
        }
    }

}
