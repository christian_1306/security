package com.ndp.models.model;

import android.support.annotation.NonNull;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.List;

import com.ndp.models.dto.MunicipioView;
import com.ndp.utils.Useful;

public class Municipio extends SugarRecord {
    @Unique
    private int idMunicipio;

    private String nombre;
    private int idProvincia;
    private Provincia provincia;

    public Municipio() {
    }

    public Municipio(int idMunicipio, String nombre, int idProvincia, Provincia provincia) {
        this.idMunicipio = idMunicipio;
        this.nombre = nombre;
        this.idProvincia = idProvincia;
        this.provincia = provincia;
    }

    public Municipio(MunicipioView municipioView,Provincia provincia){
        this.idMunicipio = municipioView.getIdMunicipio();
        this.nombre = municipioView.getNombre();
        this.idProvincia = provincia.getIdProvincia();
        this.provincia = provincia;
    }

    public int getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(int idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Provincia getProvincia() {
        return provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    @NonNull
    @Override
    public String toString() {
        return this.nombre;
    }

    public void saveOrUpdate(){
        List<Municipio> municipio = (Municipio.find(Municipio.class, Useful.getCol("idMunicipio")+" = ?", String.valueOf(idMunicipio)));
        if (municipio.size() > 0 && municipio.get(0).getId() != null){
            municipio.get(0).setNombre(nombre);
            municipio.get(0).setProvincia(provincia);
            municipio.get(0).setIdProvincia(idProvincia);
            municipio.get(0).update();
        }
        else
        {
            Municipio.save(new Municipio(idMunicipio,nombre,idProvincia,provincia));
        }
    }

    public int getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(int idProvincia) {
        this.idProvincia = idProvincia;
    }
}
