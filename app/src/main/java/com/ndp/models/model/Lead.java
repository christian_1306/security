package com.ndp.models.model;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.List;

import com.ndp.models.dto.LeadView;
import com.ndp.utils.Useful;

public class Lead extends SugarRecord {
    @Unique
    private String cardCode;

    private String razonSocial;
    private String numeroDocIdentidad;
    private String telefono;
    private String correo;
    private String tipoPersona;
    private Integer tipoDocumento;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private int isActive;

    public Lead() {}

    public Lead(LeadView leadView) {
        this.cardCode = leadView.getCardCode();
        this.razonSocial = leadView.getCardName();
        this.numeroDocIdentidad = leadView.getNumeroDocIdentidad();
        this.telefono = leadView.getTelefono();
        this.correo = leadView.getCorreo();
        this.tipoPersona = leadView.getTipoPersona();
        this.tipoDocumento = leadView.getTipoDocumento();
        this.nombre = leadView.getNombre();
        this.apellidoPaterno = leadView.getApellidoPaterno();
        this.apellidoMaterno = leadView.getApellidoMaterno();
        this.isActive = leadView.getEstadoLogico();
    }

    public Lead(String cardCode, String razonSocial, String numeroDocIdentidad, String telefono,
                String correo, String tipoPersona, Integer tipoDocumento, String nombre,
                String apellidoPaterno, String apellidoMaterno, int isActive) {
        this.cardCode = cardCode;
        this.razonSocial = razonSocial;
        this.numeroDocIdentidad = numeroDocIdentidad;
        this.telefono = telefono;
        this.correo = correo;
        this.tipoPersona = tipoPersona;
        this.tipoDocumento = tipoDocumento;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.isActive = isActive;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getNumeroDocIdentidad() {
        return numeroDocIdentidad;
    }

    public void setNumeroDocIdentidad(String numeroDocIdentidad) {
        this.numeroDocIdentidad = numeroDocIdentidad;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }


    public void saveOrUpdate(){
        List<Lead> leads = (Lead.find(Lead.class, Useful.getCol("cardCode") + " = ?", String.valueOf(cardCode)));
        if (leads.size() > 0 && leads.get(0).getId() != null){
            leads.get(0).setRazonSocial(razonSocial);
            leads.get(0).setNumeroDocIdentidad(numeroDocIdentidad);
            leads.get(0).setTelefono(telefono);
            leads.get(0).setCorreo(correo);
            leads.get(0).setTipoPersona(tipoPersona);
            leads.get(0).setTipoDocumento(tipoDocumento);
            leads.get(0).setNombre(nombre);
            leads.get(0).setApellidoPaterno(apellidoPaterno);
            leads.get(0).setApellidoMaterno(apellidoMaterno);
            leads.get(0).setIsActive(1);
            leads.get(0).update();
        }
        else
        {
            Lead.save(new Lead(cardCode, razonSocial, numeroDocIdentidad, telefono,
                    correo, tipoPersona, tipoDocumento, nombre,
                    apellidoPaterno, apellidoMaterno, 1));
        }
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public Integer getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(Integer tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }
}
