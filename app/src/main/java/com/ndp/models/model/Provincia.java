package com.ndp.models.model;

import android.support.annotation.NonNull;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.List;

import com.ndp.models.dto.ProvinciaView;
import com.ndp.utils.Useful;

public class Provincia extends SugarRecord {
    @Unique
    private int idProvincia;

    private String nombre;
    private int idDepartamento;
    private Departamento departamento;

    public Provincia() {
    }

    public Provincia(int idProvincia, String nombre, int idDepartamento, Departamento departamento) {
        this.idProvincia = idProvincia;
        this.nombre = nombre;
        this.idDepartamento = idDepartamento;
        this.departamento = departamento;
    }

    public Provincia(ProvinciaView provinciaView, Departamento departamento){
        this.idProvincia = provinciaView.getIdProvincia();
        this.nombre = provinciaView.getNombre();
        this.idDepartamento = departamento.getIdDepartamento();
        this.departamento = departamento;
    }

    public int getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(int idProvincia) {
        this.idProvincia = idProvincia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @NonNull
    @Override
    public String toString() {
        return this.nombre;
    }

    public void saveOrUpdate(){
        List<Provincia> provincia = (Provincia.find(Provincia.class, Useful.getCol("idProvincia")+" = ?", String.valueOf(idProvincia)));
        if (provincia.size() > 0 && provincia.get(0).getId() != null){
            provincia.get(0).setNombre(nombre);
            provincia.get(0).setDepartamento(departamento);
            provincia.get(0).setIdDepartamento(idDepartamento);
            provincia.get(0).update();
        }
        else
        {
            Provincia.save(new Provincia(idProvincia,nombre,idDepartamento,departamento));
        }
    }

    public int getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(int idDepartamento) {
        this.idDepartamento = idDepartamento;
    }
}
