package com.ndp.models.model;

import com.orm.SugarRecord;

import java.util.Date;

import com.ndp.models.dto.FacturaView;

public class Factura extends SugarRecord {
    private int idFactura;
    private Cliente cliente;
    private Cliente referencia;
    private String numeroFactura;
    private double montoTotal;
    private Vendedor vendedor;
    private double impuestoTotal;
    private Date fecha;
    private String estado;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Factura() {
    }

    public Factura(FacturaView facturaView){
        this.idFactura = facturaView.getIdFactura();
        //this.cliente = Cliente.find(Cliente.class,"id_cliente = ?",String.valueOf(facturaView.getClientePrincipal().getIdCliente())).get(0);
        //this.referencia = Cliente.find(Cliente.class,"id_cliente = ?",String.valueOf(facturaView.getClienteSecundario().getIdCliente())).get(0);
        this.numeroFactura = facturaView.getNumeroFactura();
        this.montoTotal = facturaView.getMontoTotal();
        this.impuestoTotal = facturaView.getImpuestoTotal();
        this.fecha = facturaView.getFecha();
        this.estado = facturaView.getEstado();
    }

    public Factura(int idFactura, Cliente cliente, String numeroFactura, double montoTotal,
                   Vendedor vendedor, double impuestoTotal, Date fecha,String estado,Cliente referencia) {
        this.idFactura = idFactura;
        this.cliente = cliente;
        this.numeroFactura = numeroFactura;
        this.montoTotal = montoTotal;
        this.vendedor = vendedor;
        this.impuestoTotal = impuestoTotal;
        this.fecha = fecha;
        this.estado = estado;
        this.referencia = referencia;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public double getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(double montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public double getImpuestoTotal() {
        return impuestoTotal;
    }

    public void setImpuestoTotal(double impuestoTotal) {
        this.impuestoTotal = impuestoTotal;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Cliente getReferencia() { return referencia;    }

    public void setReferencia(Cliente referencia) {this.referencia = referencia;  }
}
