package com.ndp.models.model;

import com.orm.SugarRecord;

import java.util.Date;

import com.ndp.models.dto.VendedorView;

public class Vendedor extends SugarRecord {
    private int idVendedor;
    private String tipoDocumentoIdentidad;
    private String numeroDocumentoIdentidad;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String telefono;
    private char sexo;
    private String correo;
    private String password;
    private Date fechaNacimiento;
    private int estado;
    private Almacen almacen;

    public Vendedor() {
    }

    public Vendedor(VendedorView vendedorView){
        this.idVendedor = vendedorView.getIdVendedor();
        this.tipoDocumentoIdentidad = vendedorView.getTipoDocIdentidad();
        this.numeroDocumentoIdentidad = vendedorView.getNumeroDocIdentidad();
        this.nombres = vendedorView.getNombres();
        this.apellidoPaterno = vendedorView.getApellidoPaterno();
        this.apellidoMaterno = vendedorView.getApellidoMaterno();
        this.telefono = vendedorView.getTelefono();
        this.sexo = vendedorView.getSexo();
        this.correo = vendedorView.getCorreo();
        this.password = vendedorView.getPassword();
        this.fechaNacimiento = vendedorView.getFechaNacimiento();
        this.estado = vendedorView.getEstadoLogico();
        this.almacen = Almacen.find(Almacen.class,"id_almacen = ?",String.valueOf(vendedorView.getAlmacen().getIdAlmacen())).get(0);
    }

    public Vendedor(int idVendedor, String tipoDocumentoIdentidad, String numeroDocumentoIdentidad,
                    String nombres, String apellidoPaterno, String apellidoMaterno, String telefono,
                    char sexo, String correo, String password, Date fechaNacimiento, int estado,
                    Almacen almacen) {
        this.idVendedor = idVendedor;
        this.tipoDocumentoIdentidad = tipoDocumentoIdentidad;
        this.numeroDocumentoIdentidad = numeroDocumentoIdentidad;
        this.nombres = nombres;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.telefono = telefono;
        this.sexo = sexo;
        this.correo = correo;
        this.password = password;
        this.fechaNacimiento = fechaNacimiento;
        this.estado = estado;
        this.almacen = almacen;
    }

    public int getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(int idVendedor) {
        this.idVendedor = idVendedor;
    }

    public String getTipoDocumentoIdentidad() {
        return tipoDocumentoIdentidad;
    }

    public void setTipoDocumentoIdentidad(String tipoDocumentoIdentidad) {
        this.tipoDocumentoIdentidad = tipoDocumentoIdentidad;
    }

    public String getNumeroDocumentoIdentidad() {
        return numeroDocumentoIdentidad;
    }

    public void setNumeroDocumentoIdentidad(String numeroDocumentoIdentidad) {
        this.numeroDocumentoIdentidad = numeroDocumentoIdentidad;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }
}
