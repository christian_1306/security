package com.ndp.models.model;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.io.Serializable;
import java.util.List;

import com.ndp.models.dto.ArticuloView;
import com.ndp.utils.Useful;

import static com.ndp.utils.Useful.getCol;

public class Articulo extends SugarRecord implements Serializable {

    @Unique
    private String codigo;

    private String nombre;
    private double peso;
    private String descripcion;
    private int isActive;
    private String unidadMedida;
    private int varillasPorTonelada;

    public Articulo() {

    }

    public Articulo(String codigo, String nombre, double peso, String descripcion, int isActive,
                    String unidadMedida, int varillasPorTonelada) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.peso = peso;
        this.descripcion = descripcion;
        this.isActive = isActive;
        this.unidadMedida = unidadMedida;
        this.varillasPorTonelada = varillasPorTonelada;
    }

    public Articulo(ArticuloView articuloView) {
        this.codigo = articuloView.getCodigo();
        this.nombre = articuloView.getNombre();
        this.peso = articuloView.getPeso();
        this.descripcion = articuloView.getDescripcion();
        this.isActive = articuloView.getEstadoLogico();
        this.unidadMedida = articuloView.getUnidadMedida();
        this.varillasPorTonelada = articuloView.getVarillasXTonelada();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public int getVarillasPorTonelada() {
        return varillasPorTonelada;
    }

    public void setVarillasPorTonelada(int varillasPorTonelada) {
        this.varillasPorTonelada = varillasPorTonelada;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void saveOrUpdate(){
        List<Articulo> articulos = (Articulo.find(Articulo.class, Useful.getCol("codigo")+" = ?", String.valueOf(codigo)));
        if (articulos.size() > 0 && articulos.get(0).getId() != null){
            articulos.get(0).setNombre(nombre);
            articulos.get(0).setPeso(peso);
            articulos.get(0).setDescripcion(descripcion);
            articulos.get(0).setIsActive(isActive);
            articulos.get(0).setUnidadMedida(unidadMedida);
            articulos.get(0).setVarillasPorTonelada(varillasPorTonelada);
            articulos.get(0).update();
        }
        else
        {
            Articulo.save(new Articulo(codigo, nombre, peso, descripcion, isActive, unidadMedida, varillasPorTonelada));
        }
    }

    public void deletePrice(){
        List<TarifarioXArticulo> prices = Select.from(TarifarioXArticulo.class)
                .where(Condition.prop(getCol("itemCode")).eq(String.valueOf(codigo)))
                .list();
        if (prices != null && prices.size() > 0){
            for (TarifarioXArticulo precio : prices){
                precio.delete();
            }
        }
    }

    public void deleteStock(){
        List<ArticuloXAlmacen> articuloXAlmacens = Select.from(ArticuloXAlmacen.class)
                .where(Condition.prop(getCol("itemCode")).eq(String.valueOf(codigo)))
                .list();
        if (articuloXAlmacens != null && articuloXAlmacens.size() > 0){
            for (ArticuloXAlmacen articuloXAlmacen : articuloXAlmacens){
                articuloXAlmacen.delete();
            }
        }
    }
}
