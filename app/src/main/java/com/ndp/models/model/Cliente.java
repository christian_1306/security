package com.ndp.models.model;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;
import com.orm.dsl.Unique;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.io.Serializable;
import java.util.List;

import com.ndp.models.dto.ClienteView;
import com.ndp.utils.Useful;

import static com.ndp.utils.Useful.getCol;

//TODO: Serializable solo para ActivityResult
public class Cliente extends SugarRecord implements Serializable {

    private Long id;

    @Unique
    private String cardCode;

    private String razonSocial;
    private String comentario;
    private String numeroDocIdentidad;
    private String telefono;
    private String correo;
    private double deuda;
    private double lineaCredito;
    private double saldoCredito;
    private String codAlmacen;
    private String nombAlmacen;
    private String condicionDePago;
    private Integer idTarifario;
    private String nombListaPrecio;

    private String tipoPersona;
    private Integer tipoDocumento;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;

    private Integer isActive;
    private Integer isLead;

    @Ignore
    private LatLng coordinates;

    public Cliente() {}

    public Cliente(ClienteView clienteView, Integer isLead){
        this.setCardCode(clienteView.getCardCode());
        this.setRazonSocial(clienteView.getRazonSocial());
        this.setComentario(clienteView.getComentario());
        this.setNumeroDocIdentidad(clienteView.getNumeroDocIdentidad());
        this.setTelefono(clienteView.getTelefono());
        this.setCorreo(clienteView.getCorreo());
        this.setDeuda(clienteView.getDeuda());
        this.setLineaCredito(clienteView.getLineaCredito());
        this.setSaldoCredito(clienteView.getSaldoCredito());
        this.setCodAlmacen(clienteView.getCodAlmacen());
        this.setNombAlmacen(clienteView.getNombAlmacen());
        this.setCondicionDePago(clienteView.getCondicionDePago());
        this.setIdTarifario(clienteView.getCodListaPrecio());
        this.setNombListaPrecio(clienteView.getNombListaPrecio());

        this.setTipoPersona(clienteView.getTipoPersona());
        this.setTipoDocumento(clienteView.getTipoDocumento());
        this.setNombre(clienteView.getNombre());
        this.setApellidoPaterno(clienteView.getApellidoPaterno());
        this.setApellidoMaterno(clienteView.getApellidoMaterno());

        this.setIsActive(clienteView.getEstadoLogico());
        this.setIsLead(isLead);
    }


    public Cliente(String cardCode,
                   String razonSocial,
                   String comentario,
                   String numeroDocIdentidad,
                   String telefono,
                   String correo,
                   double deuda,
                   double lineaCredito,
                   double saldoCredito,
                   String codAlmacen,
                   String nombAlmacen,
                   String condicionDePago,
                   Integer idTarifario,
                   String nombListaPrecio,
                   String tipoPersona, Integer tipoDocumento, String nombre,
                   String apellidoPaterno, String apellidoMaterno,
                   Integer isActive,
                   Integer isLead)
    {
        this.cardCode = cardCode;
        this.razonSocial = razonSocial;
        this.comentario = comentario;
        this.numeroDocIdentidad = numeroDocIdentidad;
        this.telefono = telefono;
        this.correo = correo;
        this.deuda = deuda;
        this.lineaCredito = lineaCredito;
        this.saldoCredito = saldoCredito;
        this.codAlmacen = codAlmacen;
        this.nombAlmacen = nombAlmacen;
        this.condicionDePago = condicionDePago;
        this.idTarifario = idTarifario;
        this.nombListaPrecio = nombListaPrecio;
        this.tipoPersona = tipoPersona;
        this.tipoDocumento = tipoDocumento;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.isActive = isActive;
        this.isLead = isLead;
    }


    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getNumeroDocIdentidad() {
        return numeroDocIdentidad;
    }

    public void setNumeroDocIdentidad(String numeroDocIdentidad) {
        this.numeroDocIdentidad = numeroDocIdentidad;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public double getDeuda() {
        return deuda;
    }

    public void setDeuda(double deuda) {
        this.deuda = deuda;
    }

    public double getLineaCredito() {
        return lineaCredito;
    }

    public void setLineaCredito(double lineaCredito) {
        this.lineaCredito = lineaCredito;
    }

    public double getSaldoCredito() {
        return saldoCredito;
    }

    public void setSaldoCredito(double saldoCredito) {
        this.saldoCredito = saldoCredito;
    }

    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }

    public String getNombAlmacen() {
        return nombAlmacen;
    }

    public void setNombAlmacen(String nombAlmacen) {
        this.nombAlmacen = nombAlmacen;
    }

    public String getCondicionDePago() {
        return condicionDePago;
    }

    public void setCondicionDePago(String condicionDePago) {
        this.condicionDePago = condicionDePago;
    }

    public Integer getIdTarifario() {
        return idTarifario;
    }

    public void setIdTarifario(Integer idTarifario) {
        this.idTarifario = idTarifario;
    }

    public String getNombListaPrecio() {
        return nombListaPrecio;
    }

    public void setNombListaPrecio(String nombListaPrecio) {
        this.nombListaPrecio = nombListaPrecio;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }


    public void saveOrUpdate(){
        List<Cliente> clientes = (Cliente.find(Cliente.class, Useful.getCol("cardCode") + " = ?", String.valueOf(cardCode)));

        if (clientes.size() > 0 && clientes.get(0).getId() != null){
            clientes.get(0).setRazonSocial(razonSocial);
            clientes.get(0).setComentario(comentario);
            clientes.get(0).setNumeroDocIdentidad(numeroDocIdentidad);
            clientes.get(0).setTelefono(telefono);
            clientes.get(0).setCorreo(correo);
            clientes.get(0).setDeuda(deuda);
            clientes.get(0).setLineaCredito(lineaCredito);
            clientes.get(0).setSaldoCredito(saldoCredito);
            clientes.get(0).setCodAlmacen(codAlmacen);
            clientes.get(0).setNombAlmacen(nombAlmacen);
            clientes.get(0).setCondicionDePago(condicionDePago);
            clientes.get(0).setIdTarifario(idTarifario);
            clientes.get(0).setNombListaPrecio(nombListaPrecio);
            clientes.get(0).setTipoPersona(tipoPersona);
            clientes.get(0).setTipoDocumento(tipoDocumento);
            clientes.get(0).setNombre(nombre);
            clientes.get(0).setApellidoPaterno(apellidoPaterno);
            clientes.get(0).setApellidoMaterno(apellidoMaterno);
            clientes.get(0).setIsActive(isActive);
            clientes.get(0).setIsLead(isLead);
            clientes.get(0).update();
        }
        else
        {
            Cliente.save(new Cliente( cardCode, razonSocial, comentario, numeroDocIdentidad, telefono,
                                      correo, deuda, lineaCredito, saldoCredito, codAlmacen, nombAlmacen,
                                      condicionDePago, idTarifario, nombListaPrecio, tipoPersona, tipoDocumento, nombre,
                                      apellidoPaterno, apellidoMaterno, isActive, isLead ));
        }
    }

    public void deleteDir(){
        List<Direccion> direccions = Select.from(Direccion.class)
                .where(Condition.prop(getCol("cardCode")).eq(String.valueOf(cardCode)))
                .list();
        if (direccions != null && direccions.size() > 0){
            for (Direccion direccion : direccions){
                direccion.delete();
            }
        }
    }

    public void deleteConctact(){
        List<PersonaContacto> personaContactos = Select.from(PersonaContacto.class)
                .where(Condition.prop(getCol("cardCode")).eq(String.valueOf(cardCode)))
                .list();
        if (personaContactos != null && personaContactos.size() > 0){
            for (PersonaContacto personaContacto : personaContactos){
                personaContacto.delete();
            }
        }
    }

    public Integer getIsLead() {
        return isLead;
    }

    public void setIsLead(Integer isLead) {
        this.isLead = isLead;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public Integer getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(Integer tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public LatLng getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(LatLng coordinates) {
        this.coordinates = coordinates;
    }
}
