package com.ndp.models.dao;

import com.ndp.models.dto.EntityApplicationDTO;
import com.ndp.models.dto.EntityDTO;
import com.ndp.models.dto.FederationDTO;
import com.ndp.models.model.Member;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.ToOne;

import java.util.List;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

@Entity(nameInDb = "t_entity")
public class EntityDao {

    @Id(autoincrement = true)
    private Long id;

    private String idEntity;
//Relacion ToOne a Federation
    private Long federationId;
    @ToOne(joinProperty = "federationId")
    private FederationDao federation;
  //Fin de ralacion to One
    private String ruc;
    private String businessName;
    private String address;
    private String enabled;//char
    //REalacion ToMany a EntityApplication Unidirecional
    @ToMany(referencedJoinProperty ="entityId" )
    private List<EntityApplicationDao>licenses;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 1208813989)
    private transient EntityDaoDao myDao;
    @Generated(hash = 327068493)
    public EntityDao(Long id, String idEntity,Long federationId, String ruc,
            String businessName, String address, String enabled) {
        this.id = id;
        this.idEntity = idEntity;
        this.federationId = federationId;
        this.ruc = ruc;
        this.businessName = businessName;
        this.address = address;
        this.enabled = enabled;
    }

    public EntityDao(Long id, String idEntity, String ruc, String businessName, String address, String enabled) {
        this.id = id;
        this.idEntity = idEntity;
        this.ruc = ruc;
        this.businessName = businessName;
        this.address = address;
        this.enabled = enabled;
    }


    public EntityDao(EntityDTO entityDTO){
        this.idEntity=entityDTO.getIdEntity();
        this.federation=new FederationDao(entityDTO.getFederation());
        this.ruc=entityDTO.getRuc();
        this.businessName=entityDTO.getBusinessName();
        this.address=entityDTO.getAddress();
        this.enabled=entityDTO.getEnabled();
    }
    @Generated(hash = 1837595255)
    public EntityDao() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getIdEntity() {
        return this.idEntity;
    }
    public void setIdEntity(String idEntity) {
        this.idEntity = idEntity;
    }
    public Long getFederationId() {
        return this.federationId;
    }
    public void setFederationId(Long federationId) {
        this.federationId = federationId;
    }
    public String getRuc() {
        return this.ruc;
    }
    public void setRuc(String ruc) {
        this.ruc = ruc;
    }
    public String getBusinessName() {
        return this.businessName;
    }
    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }
    public String getAddress() {
        return this.address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getEnabled() {
        return this.enabled;
    }
    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }
    @Generated(hash = 916181529)
    private transient Long federation__resolvedKey;
    /** To-one relationship, resolved on first access. */
    @Generated(hash = 793085181)
    public FederationDao getFederation() {
        Long __key = this.federationId;
        if (federation__resolvedKey == null
                || !federation__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            FederationDaoDao targetDao = daoSession.getFederationDaoDao();
            FederationDao federationNew = targetDao.load(__key);
            synchronized (this) {
                federation = federationNew;
                federation__resolvedKey = __key;
            }
        }
        return federation;
    }
    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 158316011)
    public void setFederation(FederationDao federation) {
        synchronized (this) {
            this.federation = federation;
            federationId = federation == null ? null : federation.getId();
            federation__resolvedKey = federationId;
        }
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }
    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 2133745969)
    public List<EntityApplicationDao> getLicenses() {
        if (licenses == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            EntityApplicationDaoDao targetDao = daoSession.getEntityApplicationDaoDao();
            List<EntityApplicationDao> licensesNew = targetDao._queryEntityDao_Licenses(id);
            synchronized (this) {
                if (licenses == null) {
                    licenses = licensesNew;
                }
            }
        }
        return licenses;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 1628605138)
    public synchronized void resetLicenses() {
        licenses = null;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 391185788)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getEntityDaoDao() : null;
    }

}
