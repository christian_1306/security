package com.ndp.models.dao;

import com.ndp.models.dto.PermissionDTO;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity(nameInDb = "t_permission")
public class PermissionDao {

    @Id(autoincrement = true)
    private Long id;

    private String idPermission;
    private String roleId;
    private String roleName;
    private String objectId;
    private String objectCode;
    private String objectName;
    private String objectDescription;
    private String enabled;//char
    private Long idRolee;//recibe el @toOne de role

    @Generated(hash = 1396452821)
    public PermissionDao(Long id, String idPermission, String roleId,
            String roleName, String objectId, String objectCode, String objectName,
            String objectDescription, String enabled, Long idRolee) {
        this.id = id;
        this.idPermission = idPermission;
        this.roleId = roleId;
        this.roleName = roleName;
        this.objectId = objectId;
        this.objectCode = objectCode;
        this.objectName = objectName;
        this.objectDescription = objectDescription;
        this.enabled = enabled;
        this.idRolee = idRolee;
    }

    public PermissionDao(String idPermission, String roleId, String roleName, String objectId, String objectCode, String objectName, String objectDescription, String enabled, Long idRolee) {
        this.idPermission = idPermission;
        this.roleId = roleId;
        this.roleName = roleName;
        this.objectId = objectId;
        this.objectCode = objectCode;
        this.objectName = objectName;
        this.objectDescription = objectDescription;
        this.enabled = enabled;
        this.idRolee = idRolee;
    }

    public PermissionDao(PermissionDTO permissionDTO){
        this.idPermission=permissionDTO.getPermissionId();
        this.roleId=permissionDTO.getRoleId();
        this.roleName=permissionDTO.getRoleName();
        this.objectId=permissionDTO.getObjectId();
        this.objectCode=permissionDTO.getObjectCode();
        this.objectName=permissionDTO.getObjectName();
        this.objectDescription=permissionDTO.getObjectDescription();
        this.enabled=permissionDTO.getEnabled();

    }
    @Generated(hash = 2072015358)
    public PermissionDao() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getIdPermission() {
        return this.idPermission;
    }
    public void setIdPermission(String idPermission) {
        this.idPermission = idPermission;
    }
    public String getRoleId() {
        return this.roleId;
    }
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }
    public String getRoleName() {
        return this.roleName;
    }
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
    public String getObjectId() {
        return this.objectId;
    }
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }
    public String getObjectCode() {
        return this.objectCode;
    }
    public void setObjectCode(String objectCode) {
        this.objectCode = objectCode;
    }
    public String getObjectName() {
        return this.objectName;
    }
    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }
    public String getObjectDescription() {
        return this.objectDescription;
    }
    public void setObjectDescription(String objectDescription) {
        this.objectDescription = objectDescription;
    }
    public String getEnabled() {
        return this.enabled;
    }
    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }
    public Long getIdRolee() {
        return this.idRolee;
    }
    public void setIdRolee(Long idRolee) {
        this.idRolee = idRolee;
    }

}
