package com.ndp.models.dao;


import com.ndp.models.dto.FederationDTO;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

@Entity(nameInDb = "t_federation")
public class FederationDao {

     @Id(autoincrement = true)
    private Long id;
    private String code;
    private String name;

    //relacion toOne
    private Long entityId;

    @ToOne(joinProperty = "entityId")
   private EntityDao ownerEntity;
/** Used to resolve relations */
@Generated(hash = 2040040024)
private transient DaoSession daoSession;
/** Used for active entity operations. */
@Generated(hash = 2125719729)
private transient FederationDaoDao myDao;
@Generated(hash = 1209773518)
public FederationDao(Long id, String code, String name, Long entityId) {
    this.id = id;
    this.code = code;
    this.name = name;
    this.entityId = entityId;
}

public FederationDao(FederationDTO federationDTO){
    this.code=federationDTO.getCode();
    this.name=federationDTO.getName();
    this.ownerEntity=new EntityDao(federationDTO.getOwnerEntity());
}

    public FederationDao(String code, String name, Long entityId, EntityDao ownerEntity, DaoSession daoSession, FederationDaoDao myDao, Long ownerEntity__resolvedKey) {
        this.code = code;
        this.name = name;
        this.entityId = entityId;
        this.ownerEntity = ownerEntity;
        this.daoSession = daoSession;
        this.myDao = myDao;
        this.ownerEntity__resolvedKey = ownerEntity__resolvedKey;
    }

    @Generated(hash = 2063429875)
public FederationDao() {
}
public Long getId() {
    return this.id;
}
public void setId(Long id) {
    this.id = id;
}
public String getCode() {
    return this.code;
}
public void setCode(String code) {
    this.code = code;
}
public String getName() {
    return this.name;
}
public void setName(String name) {
    this.name = name;
}
public Long getEntityId() {
    return this.entityId;
}
public void setEntityId(Long entityId) {
    this.entityId = entityId;
}
@Generated(hash = 1822755701)
private transient Long ownerEntity__resolvedKey;
/** To-one relationship, resolved on first access. */
@Generated(hash = 1397138514)
public EntityDao getOwnerEntity() {
    Long __key = this.entityId;
    if (ownerEntity__resolvedKey == null
            || !ownerEntity__resolvedKey.equals(__key)) {
        final DaoSession daoSession = this.daoSession;
        if (daoSession == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        EntityDaoDao targetDao = daoSession.getEntityDaoDao();
        EntityDao ownerEntityNew = targetDao.load(__key);
        synchronized (this) {
            ownerEntity = ownerEntityNew;
            ownerEntity__resolvedKey = __key;
        }
    }
    return ownerEntity;
}

/** called by internal mechanisms, do not call yourself. */
@Generated(hash = 970898404)
public void setOwnerEntity(EntityDao ownerEntity) {
    synchronized (this) {
        this.ownerEntity = ownerEntity;
        entityId = ownerEntity == null ? null : ownerEntity.getId();
        ownerEntity__resolvedKey = entityId;
    }
}
/**
 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
 * Entity must attached to an entity context.
 */
@Generated(hash = 128553479)
public void delete() {
    if (myDao == null) {
        throw new DaoException("Entity is detached from DAO context");
    }
    myDao.delete(this);
}
/**
 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
 * Entity must attached to an entity context.
 */
@Generated(hash = 1942392019)
public void refresh() {
    if (myDao == null) {
        throw new DaoException("Entity is detached from DAO context");
    }
    myDao.refresh(this);
}
/**
 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
 * Entity must attached to an entity context.
 */
@Generated(hash = 713229351)
public void update() {
    if (myDao == null) {
        throw new DaoException("Entity is detached from DAO context");
    }
    myDao.update(this);
}
/** called by internal mechanisms, do not call yourself. */
@Generated(hash = 1678960562)
public void __setDaoSession(DaoSession daoSession) {
    this.daoSession = daoSession;
    myDao = daoSession != null ? daoSession.getFederationDaoDao() : null;
}

}
