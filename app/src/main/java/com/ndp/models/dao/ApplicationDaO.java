package com.ndp.models.dao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity(nameInDb = "t_application")
public class ApplicationDaO {

    @Id(autoincrement = true)
    private Long id;
    private String objectId;
    private String code;
    private String name;
    private String url;
    private String logoPath;

    @Generated(hash = 501585464)
    public ApplicationDaO() {
    }
    @Generated(hash = 1172121695)
    public ApplicationDaO(Long id, String objectId, String code, String name,
            String url, String logoPath) {
        this.id = id;
        this.objectId = objectId;
        this.code = code;
        this.name = name;
        this.url = url;
        this.logoPath = logoPath;
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getObjectId() {
        return this.objectId;
    }
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }
    public String getCode() {
        return this.code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getUrl() {
        return this.url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getLogoPath() {
        return this.logoPath;
    }
    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

}
