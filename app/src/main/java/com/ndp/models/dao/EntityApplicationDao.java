package com.ndp.models.dao;

import com.ndp.models.dto.ApplicationDTO;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.ToOne;

import java.time.LocalDate;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

@Entity(nameInDb = "t_Entity_application")
public class EntityApplicationDao {

    @Id(autoincrement = true)
    private Long id;  //atributo
    private String EntityApplicationId; //atributo
//relacion @ToOne
    private Long applicationId;
    @ToOne(joinProperty = "applicationId")
    private ApplicationDaO application;//atributo
//fin de relacion @ToOne
    private String expirationDate;//atributo
    private Long entityId;//respuesta de una relacion
    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;
    /** Used for active entity operations. */
    @Generated(hash = 1220908319)
    private transient EntityApplicationDaoDao myDao;
    @Generated(hash = 290271240)
    public EntityApplicationDao(Long id, String EntityApplicationId,
            Long applicationId, String expirationDate, Long entityId) {
        this.id = id;
        this.EntityApplicationId = EntityApplicationId;
        this.applicationId = applicationId;
        this.expirationDate = expirationDate;
        this.entityId = entityId;
    }
    @Generated(hash = 1290197785)
    public EntityApplicationDao() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getEntityApplicationId() {
        return this.EntityApplicationId;
    }
    public void setEntityApplicationId(String EntityApplicationId) {
        this.EntityApplicationId = EntityApplicationId;
    }
    public Long getApplicationId() {
        return this.applicationId;
    }
    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }
    public String getExpirationDate() {
        return this.expirationDate;
    }
    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }
    public Long getEntityId() {
        return this.entityId;
    }
    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }
    @Generated(hash = 110579603)
    private transient Long application__resolvedKey;
    /** To-one relationship, resolved on first access. */
    @Generated(hash = 1230835167)
    public ApplicationDaO getApplication() {
        Long __key = this.applicationId;
        if (application__resolvedKey == null
                || !application__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            ApplicationDaODao targetDao = daoSession.getApplicationDaODao();
            ApplicationDaO applicationNew = targetDao.load(__key);
            synchronized (this) {
                application = applicationNew;
                application__resolvedKey = __key;
            }
        }
        return application;
    }
    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 2000222699)
    public void setApplication(ApplicationDaO application) {
        synchronized (this) {
            this.application = application;
            applicationId = application == null ? null : application.getId();
            application__resolvedKey = applicationId;
        }
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }
    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1040460007)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getEntityApplicationDaoDao() : null;
    }

}
