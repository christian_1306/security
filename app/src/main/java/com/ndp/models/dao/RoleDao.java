package com.ndp.models.dao;

import com.ndp.models.dto.EntityDTO;
import com.ndp.models.dto.PermissionDTO;
import com.ndp.models.dto.RoleDTO;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.ToOne;

import java.util.ArrayList;
import java.util.List;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.NotNull;

@Entity(nameInDb = "t_role")
public class RoleDao {
    @Id(autoincrement = true)
    private Long id;
    private String idRole;
    private String code;
    private String name;
    private String description;
    private String enabled;//char
    private long memberId;//respuesta de member (greenDAO) de la lista de roles

    //relacin to One a Entity
    private long entityId;
    @ToOne(joinProperty = "entityId")
    private EntityDao entidad;//atributo

    @ToMany(referencedJoinProperty = "idRolee")
    private List<PermissionDao> permissions;
    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;
    /** Used for active entity operations. */
    @Generated(hash = 135085956)
    private transient RoleDaoDao myDao;
    @Generated(hash = 1699501293)
    private transient Long entidad__resolvedKey;




   //metodo RoleDao to RoleDTo

    public RoleDao(RoleDTO roleDTO){
        this.idRole=roleDTO.getRoleId();
        this.code=roleDTO.getCode();
        this.name=roleDTO.getName();
        this.description=roleDTO.getDescription();
        this.enabled=roleDTO.getEnabled();
        this.entidad=new EntityDao(roleDTO.getEntity());
        this.permissions=new ArrayList<>();
        for (PermissionDTO p:roleDTO.getPermissions()) {
            this.permissions.add(new PermissionDao(p));
        }
    }




    @Generated(hash = 1613238553)
    public RoleDao(Long id, String idRole, String code, String name,
            String description, String enabled, long memberId, long entityId) {
        this.id = id;
        this.idRole = idRole;
        this.code = code;
        this.name = name;
        this.description = description;
        this.enabled = enabled;
        this.memberId = memberId;
        this.entityId = entityId;
    }




    @Generated(hash = 1529881911)
    public RoleDao() {
    }




    public Long getId() {
        return this.id;
    }




    public void setId(Long id) {
        this.id = id;
    }




    public String getIdRole() {
        return this.idRole;
    }




    public void setIdRole(String idRole) {
        this.idRole = idRole;
    }




    public String getCode() {
        return this.code;
    }




    public void setCode(String code) {
        this.code = code;
    }




    public String getName() {
        return this.name;
    }




    public void setName(String name) {
        this.name = name;
    }




    public String getDescription() {
        return this.description;
    }




    public void setDescription(String description) {
        this.description = description;
    }




    public String getEnabled() {
        return this.enabled;
    }




    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }




    public long getMemberId() {
        return this.memberId;
    }




    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }




    public long getEntityId() {
        return this.entityId;
    }




    public void setEntityId(long entityId) {
        this.entityId = entityId;
    }




    /** To-one relationship, resolved on first access. */
    @Generated(hash = 1294069401)
    public EntityDao getEntidad() {
        long __key = this.entityId;
        if (entidad__resolvedKey == null || !entidad__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            EntityDaoDao targetDao = daoSession.getEntityDaoDao();
            EntityDao entidadNew = targetDao.load(__key);
            synchronized (this) {
                entidad = entidadNew;
                entidad__resolvedKey = __key;
            }
        }
        return entidad;
    }




    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 2029474625)
    public void setEntidad(@NotNull EntityDao entidad) {
        if (entidad == null) {
            throw new DaoException(
                    "To-one property 'entityId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.entidad = entidad;
            entityId = entidad.getId();
            entidad__resolvedKey = entityId;
        }
    }




    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1442263255)
    public List<PermissionDao> getPermissions() {
        if (permissions == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            PermissionDaoDao targetDao = daoSession.getPermissionDaoDao();
            List<PermissionDao> permissionsNew = targetDao
                    ._queryRoleDao_Permissions(id);
            synchronized (this) {
                if (permissions == null) {
                    permissions = permissionsNew;
                }
            }
        }
        return permissions;
    }




    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 853623651)
    public synchronized void resetPermissions() {
        permissions = null;
    }




    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }




    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }




    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }




    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 889487049)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getRoleDaoDao() : null;
    }






}


