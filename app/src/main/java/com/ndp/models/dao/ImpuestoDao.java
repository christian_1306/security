package com.ndp.models.dao;

import org.greenrobot.greendao.annotation.Entity;

import java.io.Serializable;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

@Entity(nameInDb = "t_tax")
public class ImpuestoDao {

    @Id(autoincrement = true)
    private Long id;


    private String idImpuesto;


    private String codImpuesto;

    private String codCompany;

    private String descImpuesto;

    private String statusImpuesto;

    private String logicstatusImpuesto;

    private String rateImpuesto;

    @Generated(hash = 41187431)
    public ImpuestoDao(Long id, String idImpuesto, String codImpuesto,
            String codCompany, String descImpuesto, String statusImpuesto,
            String logicstatusImpuesto, String rateImpuesto) {
        this.id = id;
        this.idImpuesto = idImpuesto;
        this.codImpuesto = codImpuesto;
        this.codCompany = codCompany;
        this.descImpuesto = descImpuesto;
        this.statusImpuesto = statusImpuesto;
        this.logicstatusImpuesto = logicstatusImpuesto;
        this.rateImpuesto = rateImpuesto;
    }

    @Generated(hash = 1810013778)
    public ImpuestoDao() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdImpuesto() {
        return this.idImpuesto;
    }

    public void setIdImpuesto(String idImpuesto) {
        this.idImpuesto = idImpuesto;
    }

    public String getCodImpuesto() {
        return this.codImpuesto;
    }

    public void setCodImpuesto(String codImpuesto) {
        this.codImpuesto = codImpuesto;
    }

    public String getCodCompany() {
        return this.codCompany;
    }

    public void setCodCompany(String codCompany) {
        this.codCompany = codCompany;
    }

    public String getDescImpuesto() {
        return this.descImpuesto;
    }

    public void setDescImpuesto(String descImpuesto) {
        this.descImpuesto = descImpuesto;
    }

    public String getStatusImpuesto() {
        return this.statusImpuesto;
    }

    public void setStatusImpuesto(String statusImpuesto) {
        this.statusImpuesto = statusImpuesto;
    }

    public String getLogicstatusImpuesto() {
        return this.logicstatusImpuesto;
    }

    public void setLogicstatusImpuesto(String logicstatusImpuesto) {
        this.logicstatusImpuesto = logicstatusImpuesto;
    }

    public String getRateImpuesto() {
        return this.rateImpuesto;
    }

    public void setRateImpuesto(String rateImpuesto) {
        this.rateImpuesto = rateImpuesto;
    }



    
}







