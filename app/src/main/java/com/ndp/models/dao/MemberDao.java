package com.ndp.models.dao;


import com.ndp.models.dto.EntityDTO;
import com.ndp.models.dto.MemberDTO;
import com.ndp.models.dto.MemberTypeDTO;
import com.ndp.models.dto.RoleDTO;
import com.ndp.utils.GreenConverter;
import com.orm.dsl.Ignore;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Transient;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinEntity;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.ToOne;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.NotNull;

@Entity(nameInDb = "t_member")
public class MemberDao {

    @Id(autoincrement = true)
    private Long id;
    private String idMember;

    //RElacion de 1 a 1 Un Miembro solo tiene un typodeMember
    private Long memberTypeId;

    @ToOne(joinProperty ="memberTypeId" )//unidirecional
    private MemberTypeDao memberType;


    //relacion de 1 a muchos un miembro puede pertenecer o tener muchas empresas
    @ToMany
            @JoinEntity(
                    entity = MembersWithEntities.class,
                    sourceProperty = "MemberId",
                    targetProperty = "EntityId"
            )
    private List<EntityDao>entities;

    private String documentType;
    private String documentNumber;
    private String surname;
    private String name;
    private String sex;//char
    private Date birthday;
    private String address;
    private String email;
    @Convert(converter = GreenConverter.class, columnType = String.class)
    private List<String> phoneNumbers;
    private String enabled;//char
    private String blocked;//char
    private String activated;//char

    //Relacion de To One
    private long roleId;
    @ToOne(joinProperty = "roleId")//unidirecional
    private RoleDao role;


    private String unlimitedSessionUser;//char
    private String protocolCode;

    @ToMany(referencedJoinProperty = "memberId")
    private List<RoleDao> roles;

 //Codigo Generado

/** Used to resolve relations */
@Generated(hash = 2040040024)
private transient DaoSession daoSession;
/** Used for active entity operations. */
@Generated(hash = 1153956396)
private transient MemberDaoDao myDao;



public MemberDao(MemberDTO memberDTO){
    this.idMember=memberDTO.getIdMember();
    this.memberType = new MemberTypeDao(memberDTO.getMemberType());

    this.entities=new ArrayList<>();
    for (EntityDTO e:memberDTO.getEntities()) {
        this.entities.add(new EntityDao(e));
    }
    this.documentType=memberDTO.getDocumentType();
    this.documentNumber=memberDTO.getDocumentNumber();
    this.surname=memberDTO.getSurname();
    this.name=memberDTO.getName();
    this.sex=memberDTO.getSex();
    this.birthday=memberDTO.getBirthday();
    this.address=memberDTO.getAddress();
    this.email=memberDTO.getEmail();
    this.phoneNumbers=memberDTO.getPhoneNumbers();
    this.enabled=memberDTO.getEnabled();
    this.blocked=memberDTO.getBlocked();
    this.activated=memberDTO.getActivated();
    this.role=new RoleDao(memberDTO.getRole());
    this.unlimitedSessionUser=memberDTO.getUnlimitedSessionUser();
    this.protocolCode=memberDTO.getProtocolCode();
    this.roles=new ArrayList<>();
    for (RoleDTO r:memberDTO.getRoles()) {
        this.roles.add(new RoleDao(r));
    }

}//final del MemberDaoToMemberDto

@Generated(hash = 718860465)
public MemberDao(Long id, String idMember, Long memberTypeId,
        String documentType, String documentNumber, String surname, String name,
        String sex, Date birthday, String address, String email,
        List<String> phoneNumbers, String enabled, String blocked,
        String activated, long roleId, String unlimitedSessionUser,
        String protocolCode) {
    this.id = id;
    this.idMember = idMember;
    this.memberTypeId = memberTypeId;
    this.documentType = documentType;
    this.documentNumber = documentNumber;
    this.surname = surname;
    this.name = name;
    this.sex = sex;
    this.birthday = birthday;
    this.address = address;
    this.email = email;
    this.phoneNumbers = phoneNumbers;
    this.enabled = enabled;
    this.blocked = blocked;
    this.activated = activated;
    this.roleId = roleId;
    this.unlimitedSessionUser = unlimitedSessionUser;
    this.protocolCode = protocolCode;
}

@Generated(hash = 905039747)
public MemberDao() {
}

public Long getId() {
    return this.id;
}

public void setId(Long id) {
    this.id = id;
}

public String getIdMember() {
    return this.idMember;
}

public void setIdMember(String idMember) {
    this.idMember = idMember;
}

public Long getMemberTypeId() {
    return this.memberTypeId;
}

public void setMemberTypeId(Long memberTypeId) {
    this.memberTypeId = memberTypeId;
}

public String getDocumentType() {
    return this.documentType;
}

public void setDocumentType(String documentType) {
    this.documentType = documentType;
}

public String getDocumentNumber() {
    return this.documentNumber;
}

public void setDocumentNumber(String documentNumber) {
    this.documentNumber = documentNumber;
}

public String getSurname() {
    return this.surname;
}

public void setSurname(String surname) {
    this.surname = surname;
}

public String getName() {
    return this.name;
}

public void setName(String name) {
    this.name = name;
}

public String getSex() {
    return this.sex;
}

public void setSex(String sex) {
    this.sex = sex;
}

public Date getBirthday() {
    return this.birthday;
}

public void setBirthday(Date birthday) {
    this.birthday = birthday;
}

public String getAddress() {
    return this.address;
}

public void setAddress(String address) {
    this.address = address;
}

public String getEmail() {
    return this.email;
}

public void setEmail(String email) {
    this.email = email;
}

public List<String> getPhoneNumbers() {
    return this.phoneNumbers;
}

public void setPhoneNumbers(List<String> phoneNumbers) {
    this.phoneNumbers = phoneNumbers;
}

public String getEnabled() {
    return this.enabled;
}

public void setEnabled(String enabled) {
    this.enabled = enabled;
}

public String getBlocked() {
    return this.blocked;
}

public void setBlocked(String blocked) {
    this.blocked = blocked;
}

public String getActivated() {
    return this.activated;
}

public void setActivated(String activated) {
    this.activated = activated;
}

public long getRoleId() {
    return this.roleId;
}

public void setRoleId(long roleId) {
    this.roleId = roleId;
}

public String getUnlimitedSessionUser() {
    return this.unlimitedSessionUser;
}

public void setUnlimitedSessionUser(String unlimitedSessionUser) {
    this.unlimitedSessionUser = unlimitedSessionUser;
}

public String getProtocolCode() {
    return this.protocolCode;
}

public void setProtocolCode(String protocolCode) {
    this.protocolCode = protocolCode;
}

@Generated(hash = 155851967)
private transient Long memberType__resolvedKey;

/** To-one relationship, resolved on first access. */
@Generated(hash = 1838571012)
public MemberTypeDao getMemberType() {
    Long __key = this.memberTypeId;
    if (memberType__resolvedKey == null
            || !memberType__resolvedKey.equals(__key)) {
        final DaoSession daoSession = this.daoSession;
        if (daoSession == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        MemberTypeDaoDao targetDao = daoSession.getMemberTypeDaoDao();
        MemberTypeDao memberTypeNew = targetDao.load(__key);
        synchronized (this) {
            memberType = memberTypeNew;
            memberType__resolvedKey = __key;
        }
    }
    return memberType;
}

/** called by internal mechanisms, do not call yourself. */
@Generated(hash = 483802237)
public void setMemberType(MemberTypeDao memberType) {
    synchronized (this) {
        this.memberType = memberType;
        memberTypeId = memberType == null ? null : memberType.getId();
        memberType__resolvedKey = memberTypeId;
    }
}

@Generated(hash = 312471022)
private transient Long role__resolvedKey;

/** To-one relationship, resolved on first access. */
@Generated(hash = 1512845413)
public RoleDao getRole() {
    long __key = this.roleId;
    if (role__resolvedKey == null || !role__resolvedKey.equals(__key)) {
        final DaoSession daoSession = this.daoSession;
        if (daoSession == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        RoleDaoDao targetDao = daoSession.getRoleDaoDao();
        RoleDao roleNew = targetDao.load(__key);
        synchronized (this) {
            role = roleNew;
            role__resolvedKey = __key;
        }
    }
    return role;
}

/** called by internal mechanisms, do not call yourself. */
@Generated(hash = 1203319013)
public void setRole(@NotNull RoleDao role) {
    if (role == null) {
        throw new DaoException(
                "To-one property 'roleId' has not-null constraint; cannot set to-one to null");
    }
    synchronized (this) {
        this.role = role;
        roleId = role.getId();
        role__resolvedKey = roleId;
    }
}

/**
 * To-many relationship, resolved on first access (and after reset).
 * Changes to to-many relations are not persisted, make changes to the target entity.
 */
@Generated(hash = 1491489581)
public List<EntityDao> getEntities() {
    if (entities == null) {
        final DaoSession daoSession = this.daoSession;
        if (daoSession == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        EntityDaoDao targetDao = daoSession.getEntityDaoDao();
        List<EntityDao> entitiesNew = targetDao._queryMemberDao_Entities(id);
        synchronized (this) {
            if (entities == null) {
                entities = entitiesNew;
            }
        }
    }
    return entities;
}

/** Resets a to-many relationship, making the next get call to query for a fresh result. */
@Generated(hash = 1399978717)
public synchronized void resetEntities() {
    entities = null;
}

/**
 * To-many relationship, resolved on first access (and after reset).
 * Changes to to-many relations are not persisted, make changes to the target entity.
 */
@Generated(hash = 1093534238)
public List<RoleDao> getRoles() {
    if (roles == null) {
        final DaoSession daoSession = this.daoSession;
        if (daoSession == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        RoleDaoDao targetDao = daoSession.getRoleDaoDao();
        List<RoleDao> rolesNew = targetDao._queryMemberDao_Roles(id);
        synchronized (this) {
            if (roles == null) {
                roles = rolesNew;
            }
        }
    }
    return roles;
}

/** Resets a to-many relationship, making the next get call to query for a fresh result. */
@Generated(hash = 254386649)
public synchronized void resetRoles() {
    roles = null;
}

/**
 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
 * Entity must attached to an entity context.
 */
@Generated(hash = 128553479)
public void delete() {
    if (myDao == null) {
        throw new DaoException("Entity is detached from DAO context");
    }
    myDao.delete(this);
}

/**
 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
 * Entity must attached to an entity context.
 */
@Generated(hash = 1942392019)
public void refresh() {
    if (myDao == null) {
        throw new DaoException("Entity is detached from DAO context");
    }
    myDao.refresh(this);
}

/**
 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
 * Entity must attached to an entity context.
 */
@Generated(hash = 713229351)
public void update() {
    if (myDao == null) {
        throw new DaoException("Entity is detached from DAO context");
    }
    myDao.update(this);
}

/** called by internal mechanisms, do not call yourself. */
@Generated(hash = 2002477063)
public void __setDaoSession(DaoSession daoSession) {
    this.daoSession = daoSession;
    myDao = daoSession != null ? daoSession.getMemberDaoDao() : null;
}





}
