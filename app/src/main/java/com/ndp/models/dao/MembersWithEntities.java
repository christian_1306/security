package com.ndp.models.dao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class MembersWithEntities{

    @Id(autoincrement = true)
    private Long id;
    private Long MemberId;
    private Long EntityId;
    @Generated(hash = 2084103551)
    public MembersWithEntities(Long id, Long MemberId, Long EntityId) {
        this.id = id;
        this.MemberId = MemberId;
        this.EntityId = EntityId;
    }
    @Generated(hash = 1448979634)
    public MembersWithEntities() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getMemberId() {
        return this.MemberId;
    }
    public void setMemberId(Long MemberId) {
        this.MemberId = MemberId;
    }
    public Long getEntityId() {
        return this.EntityId;
    }
    public void setEntityId(Long EntityId) {
        this.EntityId = EntityId;
    }


}
