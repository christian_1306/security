package com.ndp.models.dao;


import com.ndp.models.dto.MemberTypeDTO;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity(nameInDb = "t_membertype")
public class MemberTypeDao {

    @Id(autoincrement = true)
    private Long id;
    private String code;
    private String name;
    @Generated(hash = 718714692)
    public MemberTypeDao(Long id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    MemberTypeDao(MemberTypeDTO memberTypeDTO){

        this.code=memberTypeDTO.getCode();
        this.name=memberTypeDTO.getName();

    }
    @Generated(hash = 1425212317)
    public MemberTypeDao() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getCode() {
        return this.code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }

}
