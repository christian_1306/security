package com.ndp.models.appRest;

import com.orm.SugarRecord;

import java.io.Serializable;

public class apiOrderDetail  extends SugarRecord implements Serializable {

    private String codAlmacen;
    private String codArticulo;
    private String codImpuesto;
    private String unidad;
    private int cantidadVarillas;
    private int codListaPrecio;
    private double pesoTotal;
    private double impuestoDetalle;
    private double subtotalNeto;
    private double precioUnitario;
    private double subtotalFinal;
    private String codCreacionApp;

    public apiOrderDetail () {}


    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }

    public String getCodArticulo() {
        return codArticulo;
    }

    public void setCodArticulo(String codArticulo) {
        this.codArticulo = codArticulo;
    }

    public String getCodImpuesto() {
        return codImpuesto;
    }

    public void setCodImpuesto(String codImpuesto) {
        this.codImpuesto = codImpuesto;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public int getCantidadVarillas() {
        return cantidadVarillas;
    }

    public void setCantidadVarillas(int cantidadVarillas) {
        this.cantidadVarillas = cantidadVarillas;
    }

    public int getCodListaPrecio() {
        return codListaPrecio;
    }

    public void setCodListaPrecio(int codListaPrecio) {
        this.codListaPrecio = codListaPrecio;
    }

    public double getPesoTotal() {
        return pesoTotal;
    }

    public void setPesoTotal(double pesoTotal) {
        this.pesoTotal = pesoTotal;
    }

    public double getImpuestoDetalle() {
        return impuestoDetalle;
    }

    public void setImpuestoDetalle(double impuestoDetalle) {
        this.impuestoDetalle = impuestoDetalle;
    }

    public double getSubtotalNeto() {
        return subtotalNeto;
    }

    public void setSubtotalNeto(double subtotalNeto) {
        this.subtotalNeto = subtotalNeto;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public double getSubtotalFinal() {
        return subtotalFinal;
    }

    public void setSubtotalFinal(double subtotalFinal) {
        this.subtotalFinal = subtotalFinal;
    }

    public String getCodCreacionApp() {
        return codCreacionApp;
    }

    public void setCodCreacionApp(String codCreacionApp) {
        this.codCreacionApp = codCreacionApp;
    }
}
