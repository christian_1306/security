package com.ndp.models.appRest;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.Date;

public class apiCreateLead extends SugarRecord implements Serializable {

    private String cardCode;
    private String cardName;
    private String numeroDocIdentidad;
    private String correo;
    private String telefono;
    private String tipoPersona;
    private Integer tipoDocumento;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String codCreacionApp;
    private String userAccount;
    private Integer typeObject;
    private Date fechaRegistro;
    private Integer estadoMigracion;
    private String mensajeMigracion;

    public apiCreateLead () {}

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getCodCreacionApp() {
        return codCreacionApp;
    }

    public void setCodCreacionApp(String codCreacionApp) {
        this.codCreacionApp = codCreacionApp;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNumeroDocIdentidad() {
        return numeroDocIdentidad;
    }

    public void setNumeroDocIdentidad(String numeroDocIdentidad) {
        this.numeroDocIdentidad = numeroDocIdentidad;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public Integer getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(Integer tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public Integer getEstadoMigracion() {
        return estadoMigracion;
    }

    public void setEstadoMigracion(Integer estadoMigracion) {
        this.estadoMigracion = estadoMigracion;
    }

    public String getMensajeMigracion() {
        return mensajeMigracion;
    }

    public void setMensajeMigracion(String mensajeMigracion) {
        this.mensajeMigracion = mensajeMigracion;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getTypeObject() {
        return typeObject;
    }

    public void setTypeObject(Integer typeObject) {
        this.typeObject = typeObject;
    }
}
