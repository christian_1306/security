package com.ndp.models.appRest;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class apiOrderHeader extends SugarRecord implements Serializable {

    private String cardCode;
    private String cardName;
    private String codCreacionApp;
    private String comentario;
    private String correoNotificacion;
    private String direccionEntrega;
    private String direccionFacturacion;
    private String fechaDocumento;
    private String fechaEntrega;
    private String nit;
    private String moneda;
    private String nomDirEntrega;
    private String nomDirFacturacion;
    private String terminalCreacion;
    private String usuarioCreacion;
    private double impuestoPedido;
    private double totalNeto;
    private double descuento;
    private double totalFinal;

    @Ignore
    private ArrayList<apiOrderDetail> detallesPedido;

    private String userAccount;
    private Integer typeObject;
    private Date fechaRegistro;
    private Integer estadoMigracion;
    private String mensajeMigracion;

    public apiOrderHeader() {}

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCodCreacionApp() {
        return codCreacionApp;
    }

    public void setCodCreacionApp(String codCreacionApp) {
        this.codCreacionApp = codCreacionApp;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getCorreoNotificacion() {
        return correoNotificacion;
    }

    public void setCorreoNotificacion(String correoNotificacion) {
        this.correoNotificacion = correoNotificacion;
    }

    public String getDireccionEntrega() {
        return direccionEntrega;
    }

    public void setDireccionEntrega(String direccionEntrega) {
        this.direccionEntrega = direccionEntrega;
    }

    public String getDireccionFacturacion() {
        return direccionFacturacion;
    }

    public void setDireccionFacturacion(String direccionFacturacion) {
        this.direccionFacturacion = direccionFacturacion;
    }

    public String getFechaDocumento() {
        return fechaDocumento;
    }

    public void setFechaDocumento(String fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getNomDirEntrega() {
        return nomDirEntrega;
    }

    public void setNomDirEntrega(String nomDirEntrega) {
        this.nomDirEntrega = nomDirEntrega;
    }

    public String getNomDirFacturacion() {
        return nomDirFacturacion;
    }

    public void setNomDirFacturacion(String nomDirFacturacion) {
        this.nomDirFacturacion = nomDirFacturacion;
    }

    public String getTerminalCreacion() {
        return terminalCreacion;
    }

    public void setTerminalCreacion(String terminalCreacion) {
        this.terminalCreacion = terminalCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public double getImpuestoPedido() {
        return impuestoPedido;
    }

    public void setImpuestoPedido(double impuestoPedido) {
        this.impuestoPedido = impuestoPedido;
    }

    public double getTotalNeto() {
        return totalNeto;
    }

    public void setTotalNeto(double totalNeto) {
        this.totalNeto = totalNeto;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public double getTotalFinal() {
        return totalFinal;
    }

    public void setTotalFinal(double totalFinal) {
        this.totalFinal = totalFinal;
    }

    public ArrayList<apiOrderDetail> getDetallesPedido() {
        return detallesPedido;
    }

    public void setDetallesPedido(ArrayList<apiOrderDetail> detallesPedido) {
        this.detallesPedido = detallesPedido;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public Integer getEstadoMigracion() {
        return estadoMigracion;
    }

    public void setEstadoMigracion(Integer estadoMigracion) {
        this.estadoMigracion = estadoMigracion;
    }

    public String getMensajeMigracion() {
        return mensajeMigracion;
    }

    public void setMensajeMigracion(String mensajeMigracion) {
        this.mensajeMigracion = mensajeMigracion;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getTypeObject() {
        return typeObject;
    }

    public void setTypeObject(Integer typeObject) {
        this.typeObject = typeObject;
    }
}
