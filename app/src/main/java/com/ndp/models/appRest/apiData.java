package com.ndp.models.appRest;

import java.io.Serializable;

public class apiData<T> implements Serializable {


    private String appname;
    private String email;
    private Integer forzar;
    private String imei;
    private String ip;
    private String token;
    private String version;
    private T data;

    public apiData() {

    }

    public apiData(String appname, String email, Integer forzar, String imei, String ip, String token,
                    String version, T data) {
        this.appname = appname;
        this.email = email;
        this.forzar = forzar;
        this.imei = imei;
        this.ip = ip;
        this.token = token;
        this.version = version;
        this.data = data;
    }


    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getForzar() {
        return forzar;
    }

    public void setForzar(Integer forzar) {
        this.forzar = forzar;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}