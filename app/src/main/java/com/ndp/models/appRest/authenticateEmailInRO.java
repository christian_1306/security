package com.ndp.models.appRest;

import com.google.gson.annotations.SerializedName;

public class authenticateEmailInRO {

    @SerializedName("appName")
    private String applicationName;
    @SerializedName("versionApp")
    private String version;
    @SerializedName("correo")
    private String user;
    @SerializedName("pin")
    private String pin;

    public authenticateEmailInRO(String ApplicationName, String Version, String User, String Pin) {
        applicationName = ApplicationName;
        version = Version;
        user = User;
        pin = Pin;
    }
}
