package com.ndp.models.appRest;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class restView<T> implements Serializable {

    private String code;
    private String message;
    private String errorCode;
    private List<String> errors;
    private T data;

    public restView() {

    }


    public restView(String success, String message) {
        this.errorCode = success;
        this.message = message;
    }

    public restView(String success, String message, T data) {
        this.errorCode = success;
        this.message = message;
        this.data = data;
    }

    public restView(String success, final String message, final List<String> errors) {
        this.errorCode = success;
        this.message = message;
        this.errors = errors;
    }

    public restView(String success, final String message, final String error) {
        this.errorCode = success;
        this.message = message;
        errors = Arrays.asList(error);
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(final List<String> errors) {
        this.errors = errors;
    }

    public void setError(final String error) {
        errors = Arrays.asList(error);
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}