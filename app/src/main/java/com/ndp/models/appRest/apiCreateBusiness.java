package com.ndp.models.appRest;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class apiCreateBusiness extends SugarRecord implements Serializable {

    private Integer idCliente;
    private String cardCode;
    private String razonSocial;
    private String numeroDocIdentidad;
    private String correo;
    private String telefono;
    private Double deuda;
    private Double lineaCredito;
    private Double saldoCredito;
    private String comentario;
    private String tipoPersona;
    private Integer tipoDocumento;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String codCreacionApp;
    private Integer estadoLogico;
    private Integer codListaPrecio;
    private String nombListaPrecio;
    private Integer condicionDePago;

    @Ignore
    private ArrayList<apiContactoDetail> listContactos;
    @Ignore
    private ArrayList<apiDireccionDetail> listaDirecciones;

    private String userAccount;
    private Integer typeObject;
    private Date fechaRegistro;
    private Integer estadoMigracion;
    private String mensajeMigracion;

    public apiCreateBusiness() {}


    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNumeroDocIdentidad() {
        return numeroDocIdentidad;
    }

    public void setNumeroDocIdentidad(String numeroDocIdentidad) {
        this.numeroDocIdentidad = numeroDocIdentidad;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Double getDeuda() {
        return deuda;
    }

    public void setDeuda(Double deuda) {
        this.deuda = deuda;
    }

    public Double getLineaCredito() {
        return lineaCredito;
    }

    public void setLineaCredito(Double lineaCredito) {
        this.lineaCredito = lineaCredito;
    }

    public Double getSaldoCredito() {
        return saldoCredito;
    }

    public void setSaldoCredito(Double saldoCredito) {
        this.saldoCredito = saldoCredito;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public Integer getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(Integer tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getCodCreacionApp() {
        return codCreacionApp;
    }

    public void setCodCreacionApp(String codCreacionApp) {
        this.codCreacionApp = codCreacionApp;
    }

    public Integer getEstadoLogico() {
        return estadoLogico;
    }

    public void setEstadoLogico(Integer estadoLogico) {
        this.estadoLogico = estadoLogico;
    }

    public Integer getCodListaPrecio() {
        return codListaPrecio;
    }

    public void setCodListaPrecio(Integer codListaPrecio) {
        this.codListaPrecio = codListaPrecio;
    }

    public String getNombListaPrecio() {
        return nombListaPrecio;
    }

    public void setNombListaPrecio(String nombListaPrecio) {
        this.nombListaPrecio = nombListaPrecio;
    }

    public Integer getCondicionDePago() {
        return condicionDePago;
    }

    public void setCondicionDePago(Integer condicionDePago) {
        this.condicionDePago = condicionDePago;
    }

    public ArrayList<apiContactoDetail> getListContactos() {
        return listContactos;
    }

    public void setListContactos(ArrayList<apiContactoDetail> listContactos) {
        this.listContactos = listContactos;
    }

    public ArrayList<apiDireccionDetail> getListaDirecciones() {
        return listaDirecciones;
    }

    public void setListaDirecciones(ArrayList<apiDireccionDetail> listaDirecciones) {
        this.listaDirecciones = listaDirecciones;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public Integer getEstadoMigracion() {
        return estadoMigracion;
    }

    public void setEstadoMigracion(Integer estadoMigracion) {
        this.estadoMigracion = estadoMigracion;
    }

    public String getMensajeMigracion() {
        return mensajeMigracion;
    }

    public void setMensajeMigracion(String mensajeMigracion) {
        this.mensajeMigracion = mensajeMigracion;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getTypeObject() {
        return typeObject;
    }

    public void setTypeObject(Integer typeObject) {
        this.typeObject = typeObject;
    }
}
