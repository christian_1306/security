package com.ndp.models.appRest;

import com.google.gson.annotations.SerializedName;

public class authenticatePasswordInRO {

    @SerializedName("currentHash")
    private String currentHash;
    @SerializedName("newHash")
    private String newHash;
    @SerializedName("email")
    private String user;
    @SerializedName("token")
    private String token;

    public authenticatePasswordInRO(String CurrentHash, String NewHash, String User, String Token) {
        currentHash = CurrentHash;
        newHash = NewHash;
        user = User;
        token = Token;
    }
}
