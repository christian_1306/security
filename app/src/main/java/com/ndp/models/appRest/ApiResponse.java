package com.ndp.models.appRest;

public class ApiResponse<T> {
    private boolean successful;
    private String code;
    private String message;
    private T data;

    public ApiResponse() {
        this.successful = false;
    }

    public ApiResponse(String code, String message) {
        this.successful = false;
        this.code = code;
        this.message = message;
    }

    public ApiResponse(boolean successful, String code, String message, T data) {
        this.successful = successful;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public boolean isSuccessful() {
        return this.successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
