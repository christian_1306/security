package com.ndp.models.appRest;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.Date;

public class apiCreateVisit extends SugarRecord implements Serializable {

    private String cardCode;
    private String cardName;
    private String codCreacionApp;
    private String comentario;
    private String direccion;
    private String fechaVisita;
    private String latitud;
    private String longitud;
    private String motivo;
    private String ruta;
    private Integer estadoVisita;
    private String usuario;
    private Integer municipio;
    private String userAccount;
    private Integer typeObject;
    private Date fechaRegistro;
    private Integer estadoMigracion;
    private String mensajeMigracion;

    public apiCreateVisit () {}

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getCodCreacionApp() {
        return codCreacionApp;
    }

    public void setCodCreacionApp(String codCreacionApp) {
        this.codCreacionApp = codCreacionApp;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFechaVisita() {
        return fechaVisita;
    }

    public void setFechaVisita(String fechaVisita) {
        this.fechaVisita = fechaVisita;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Integer getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Integer municipio) {
        this.municipio = municipio;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public Integer getEstadoMigracion() {
        return estadoMigracion;
    }

    public void setEstadoMigracion(Integer estadoMigracion) {
        this.estadoMigracion = estadoMigracion;
    }

    public String getMensajeMigracion() {
        return mensajeMigracion;
    }

    public void setMensajeMigracion(String mensajeMigracion) {
        this.mensajeMigracion = mensajeMigracion;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getTypeObject() {
        return typeObject;
    }

    public void setTypeObject(Integer typeObject) {
        this.typeObject = typeObject;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public Integer getEstadoVisita() {
        return estadoVisita;
    }

    public void setEstadoVisita(Integer estadoVisita) {
        this.estadoVisita = estadoVisita;
    }
}
