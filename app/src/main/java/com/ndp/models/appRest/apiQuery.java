package com.ndp.models.appRest;

import java.io.Serializable;


public class apiQuery implements Serializable {

    private String cardCode;
    private String fechaInicio;
    private String fechaFin;
    private Integer idMunicipio;
    private String codCreacionApp;
    private Integer estadoMigracion;
    private String usuarioCreacion;
    private Integer estadoFactura;

    public apiQuery() {}


    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getCodCreacionApp() {
        return codCreacionApp;
    }

    public void setCodCreacionApp(String codCreacionApp) {
        this.codCreacionApp = codCreacionApp;
    }


    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Integer getEstadoMigracion() {
        return estadoMigracion;
    }

    public void setEstadoMigracion(Integer estadoMigracion) {
        this.estadoMigracion = estadoMigracion;
    }

    public Integer getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(Integer idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    public Integer getEstadoFactura() {
        return estadoFactura;
    }

    public void setEstadoFactura(Integer estadoFactura) {
        this.estadoFactura = estadoFactura;
    }
}
