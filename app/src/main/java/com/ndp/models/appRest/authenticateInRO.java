package com.ndp.models.appRest;

import com.google.gson.annotations.SerializedName;

public class authenticateInRO {

    @SerializedName("applicationName")
    private String applicationName;
    @SerializedName("version")
    private String version;
    @SerializedName("usuario")
    private String user;
    @SerializedName("hash")
    private String hash;
    @SerializedName("imei")
    private String imei;

    public authenticateInRO(String ApplicationName, String Version, String User, String Hash, String imei) {
        this.applicationName = ApplicationName;
        this.version = Version;
        this.user = User;
        this.hash = Hash;
        this.imei = imei;
    }
}
