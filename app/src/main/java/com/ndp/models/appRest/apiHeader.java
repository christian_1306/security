package com.ndp.models.appRest;

import java.io.Serializable;

public class apiHeader implements Serializable {

    private String identifier;
    private String signature;
    private String payload;
    private String fingerprint;

    public apiHeader() {
    }

    public apiHeader(String identifier, String signature, String payload, String fingerprint) {
        this.identifier = identifier;
        this.signature = signature;
        this.payload = payload;
        this.fingerprint = fingerprint;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }
}
