package com.ndp.models.appRest;

import com.google.gson.annotations.SerializedName;

    public class authenticateInRO2 {

        @SerializedName("applicationName")
        private String applicationName;
        @SerializedName("version")
        private String version;
        @SerializedName("identifier")
        private String identifier;
        @SerializedName("signature")
        private String signature;
        @SerializedName("fingerprint")
        private String fingerprint;
        @SerializedName("payload")
        private String payload;

        public authenticateInRO2(String ApplicationName, String Version, String Identifier, String Signature, String Fingerprint,String Payload) {
            this.applicationName = ApplicationName;
            this.version = Version;
            this.identifier =Identifier;
            this.signature = Signature;
            this.fingerprint = Fingerprint;
            this.payload=Payload;
        }
    }


