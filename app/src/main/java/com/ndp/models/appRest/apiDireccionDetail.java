package com.ndp.models.appRest;

import com.orm.SugarRecord;

import java.io.Serializable;


public class apiDireccionDetail extends SugarRecord implements Serializable {

    private int idDireccion;
    private String nombreDireccion;
    private String direccion;
    private String tipo;
    private int idMunicipio;
    private String nombMunicipio;
    private String codCreacionBusiness;

    public apiDireccionDetail () {}


    public int getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(int idDireccion) {
        this.idDireccion = idDireccion;
    }

    public String getNombreDireccion() {
        return nombreDireccion;
    }

    public void setNombreDireccion(String nombreDireccion) {
        this.nombreDireccion = nombreDireccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(int idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    public String getNombMunicipio() {
        return nombMunicipio;
    }

    public void setNombMunicipio(String nombMunicipio) {
        this.nombMunicipio = nombMunicipio;
    }

    public String getCodCreacionBusiness() {
        return codCreacionBusiness;
    }

    public void setCodCreacionBusiness(String codCreacionBusiness) {
        this.codCreacionBusiness = codCreacionBusiness;
    }
}
