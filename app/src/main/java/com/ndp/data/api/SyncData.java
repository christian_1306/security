package com.ndp.data.api;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import java.util.List;

import com.ndp.R;
import com.ndp.models.appRest.ApiResponse;
import com.ndp.models.appRest.apiCreateBusiness;
import com.ndp.models.appRest.apiCreateLead;
import com.ndp.models.appRest.apiCreateVisit;
import com.ndp.models.appRest.apiData;
import com.ndp.models.appRest.apiOrderHeader;
import com.ndp.models.appRest.apiQuery;
import com.ndp.models.appRest.restView;
import com.ndp.models.appRest.authenticateEmailInRO;
import com.ndp.models.appRest.authenticateInRO;
import com.ndp.models.appRest.authenticatePasswordInRO;
import com.ndp.models.dao.DaoSession;
import com.ndp.models.dao.MemberDao;
import com.ndp.models.dto.AlmacenView;
import com.ndp.models.dto.ArticuloView;
import com.ndp.models.dto.ArticuloXAlmacenView;
import com.ndp.models.dto.ClienteView;
import com.ndp.models.dto.CondicionPagoView;
import com.ndp.models.dto.CoreMotivoView;
import com.ndp.models.dto.CoreRutaView;
import com.ndp.models.dto.DepartamentoView;
import com.ndp.models.dto.DireccionView;
import com.ndp.models.dto.ImpuestoView;
import com.ndp.models.dto.LeadView;
import com.ndp.models.dto.MemberDTO;
import com.ndp.models.dto.MonedaView;
import com.ndp.models.dto.MunicipioView;
import com.ndp.models.dto.PersonaContactoView;
import com.ndp.models.dto.ProvinciaView;
import com.ndp.models.dto.QueryVisitaView;
import com.ndp.models.dto.TarifarioView;
import com.ndp.models.dto.TarifarioXArticuloView;
import com.ndp.models.dto.UserView;
import com.ndp.models.model.Almacen;
import com.ndp.models.model.Articulo;
import com.ndp.models.model.ArticuloXAlmacen;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.CondicionPago;
import com.ndp.models.model.CoreMotivo;
import com.ndp.models.model.CoreRuta;
import com.ndp.models.model.Departamento;
import com.ndp.models.model.Direccion;
import com.ndp.models.model.Impuesto;
import com.ndp.models.model.Lead;
import com.ndp.models.model.Member;
import com.ndp.models.model.Moneda;
import com.ndp.models.model.Municipio;
import com.ndp.models.model.PersonaContacto;
import com.ndp.models.model.Provincia;
import com.ndp.models.model.Tarifario;
import com.ndp.models.model.TarifarioXArticulo;
import com.ndp.models.model.User;
import com.ndp.ui.activities.SplashScreenActivity;
import com.ndp.utils.Shared;
import com.ndp.utils.methods.SessionManager;
import com.ndp.utils.use.usePhone;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Response;

import static com.ndp.BuildConfig.APP_NAME;
import static com.ndp.BuildConfig.APP_VERSION;

public class SyncData {

    private Activity activity;
    private String imeiCode;
    private String appName;
    private String appVersion;
    private String appUser;
    private String appToken;
    private String appIP;
    private String payload;
    private int forzar;
    private SharedPreferences shrPreLogin;
    private Member userInRO;
    private MemberDao memberDao;
    private DaoSession daoSession;


    //region init
    public SyncData(Activity activity, int forzar) {
        shrPreLogin = Shared.prf_Login(activity);
        this.activity = activity;
        this.appName = APP_NAME;
        this.appVersion = APP_VERSION;
        this.appToken = shrPreLogin.getString(Shared.login_token, null);
        this.appUser = shrPreLogin.getString(Shared.login_usuario, null);
        this.imeiCode = usePhone.getIMEI(activity);
        this.forzar = forzar;
        this.payload="jj";
        this.appIP = usePhone.getIPAddress(true);
        daoSession= SplashScreenActivity.getDaoSession();
    }
    //endregion

    //region setForzar
    public void setForzar(int forzar){
        this.forzar = forzar;
    }
    //endregion

    //region SyncLogin

    public boolean SyncLogin(String hash, StringBuilder outMessage) {
        try {
            outMessage.setLength(0);
            Call<restView<UserView>> callApplication = APIAdapter.getApiService(appToken,0)
                    .setAuthenticate(new authenticateInRO(appName, appVersion, appUser, hash, imeiCode));

            Response<restView<UserView>> response = callApplication.execute();

            if (response.isSuccessful()) {
                restView<UserView> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {
                        if (respuesta.getData() != null) {
                            if (respuesta.getData().getTipoUsuario() == 1 || respuesta.getData().getTipoUsuario() == 2) {
                                outMessage.append(activity.getString(R.string.sincronizacion_usuario_autenticado));
                                User.deleteAll(User.class);
                                Shared.clear_Login(activity);
                                User userInRO = new User(respuesta.getData());
                                userInRO.save();
                                Shared.edit_Login(activity, imeiCode, appUser, true, null, respuesta.getData().getTokenUsuario());
                                SessionManager sessionManager = new SessionManager(activity);
                                sessionManager.createLoginSession(respuesta.getData().getCodUsuario(),respuesta.getData().getCorreoUsuario());
                                return true;
                            } else {
                                outMessage.append(activity.getString(R.string.type_not_pass));
                                return false;
                            }
                        }
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion
    /*
    public boolean SyncLogin2(String signature,StringBuilder outMessage) {
        try {
            outMessage.setLength(0);
            Call<restView<UserView>> callApplication = APIAdapter.getApiService(appToken,0)
                    .setAuthenticate2(new apiHeader(payload,signature,appUser,imeiCode),new authenticateInRO2(appName, appVersion,appUser,signature,imeiCode,payload));

            Response<restView<UserView>> response = callApplication.execute();
            if (response.isSuccessful()) {

                restView<UserView> respuesta = response.body();
                if (respuesta != null) {

                    String errorCode =  respuesta.getCode();
                    if (errorCode.equals("SUCCESS")) {
                        if (respuesta.getData() != null) {
                            if (respuesta.getData().getTipoUsuario() == 1 || respuesta.getData().getTipoUsuario() == 2) {
                                outMessage.append(activity.getString(R.string.sincronizacion_usuario_autenticado));
                                User.deleteAll(User.class);
                                Shared.clear_Login(activity);
                                User userInRO = new User(respuesta.getData());
                                userInRO.save();
                                Shared.edit_Login(activity, imeiCode, appUser, true, null, respuesta.getData().getTokenUsuario());
                                SessionManager sessionManager = new SessionManager(activity);
                                sessionManager.createLoginSession(respuesta.getData().getCodUsuario(),respuesta.getData().getCorreoUsuario());
                                return true;
                            } else {
                                outMessage.append(activity.getString(R.string.type_not_pass));
                                return false;
                            }
                        }
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {

                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {

                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
*/
    public Boolean SyncLogin2(String signature, StringBuilder outMessage) {
        try {
            outMessage.setLength(0);
            Call<ApiResponse<MemberDTO>> callApplication = APIAdapter.getApiService(appToken,0).setAuthenticate2(appUser,signature,payload,imeiCode);

            Response<ApiResponse<MemberDTO>> response = callApplication.execute();


            if(response.isSuccessful()){

                Headers headers = response.headers();

                ApiResponse<MemberDTO> respuesta = response.body();
             String token = headers.get("Token");
                if (respuesta != null) {

                    String Code =  respuesta.getCode();
                    if (Code.equals("SUCCESS")) {
                        if (respuesta.getData() != null) {
                            if (respuesta.getData().getMemberType().getCode().equals("usr")  || respuesta.getData().getMemberType().getCode().equals("sync")  ) {
                                outMessage.append(activity.getString(R.string.sincronizacion_usuario_autenticado));
                                Member.deleteAll(Member.class);
                                Shared.clear_Login(activity);
                                Member userInRO = new Member(respuesta.getData());

                                userInRO.save();



                                Shared.edit_Login(activity,imeiCode,appUser, true, null, token);
                                SessionManager sessionManager = new SessionManager(activity);
                                sessionManager.createLoginSession(respuesta.getData().getIdMember(),respuesta.getData().getEmail());

                               return true;
                            } else {
                                outMessage.append(activity.getString(R.string.type_not_pass));
                                return false;
                            }
                        }
                     return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                   return false;
                }

            } else {
                outMessage.append(response.message());
             return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    public Member SyncLogin3(String signature, StringBuilder outMessage) {

        try {
            outMessage.setLength(0);
            Call<ApiResponse<MemberDTO>> callApplication = APIAdapter.getApiService(appToken,0).setAuthenticate2(appUser,signature,payload,imeiCode);

            Response<ApiResponse<MemberDTO>> response = callApplication.execute();

            if(response.isSuccessful()){


                Headers headers = response.headers();

                ApiResponse<MemberDTO> respuesta = response.body();
                String token = headers.get("Token");
                if (respuesta != null) {


                    String Code =  respuesta.getCode();
                    if (Code.equals("SUCCESS")) {
                        if (respuesta.getData() != null) {
                            if (respuesta.getData().getMemberType().getCode().equals("usr")  || respuesta.getData().getMemberType().getCode().equals("sync")  ) {
                                outMessage.append(activity.getString(R.string.sincronizacion_usuario_autenticado));
                                Member.deleteAll(Member.class);
                                Shared.clear_Login(activity);
                                userInRO = new Member(respuesta.getData());

                                userInRO.save();

                                Shared.edit_Login(activity,imeiCode,appUser, true, null, token);
                                SessionManager sessionManager = new SessionManager(activity);
                                sessionManager.createLoginSession(respuesta.getData().getIdMember(),respuesta.getData().getEmail());

                            } else {
                                outMessage.append(activity.getString(R.string.type_not_pass));
                          //      return false;
                            }
                        }
                        //return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                       // return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    //return false;
                }

            } else {
                outMessage.append(response.message());
             //   return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            //return false;
        }
        return userInRO;
    }
    public Boolean SyncLogin4(String signature, StringBuilder outMessage) {
        try {
            outMessage.setLength(0);
            Call<ApiResponse<MemberDTO>> callApplication = APIAdapter.getApiService(appToken,0).setAuthenticate2(appUser,signature,payload,imeiCode);

            Response<ApiResponse<MemberDTO>> response = callApplication.execute();


            if(response.isSuccessful()){

                Headers headers = response.headers();

                ApiResponse<MemberDTO> respuesta = response.body();

                String token = headers.get("Token");
                if (respuesta != null) {

                    String Code =  respuesta.getCode();
                    if (Code.equals("SUCCESS")) {
                        if (respuesta.getData() != null) {
                            if (respuesta.getData().getMemberType().getCode().equals("usr")  || respuesta.getData().getMemberType().getCode().equals("sync")  ) {
                                outMessage.append(activity.getString(R.string.sincronizacion_usuario_autenticado));
                                //Member.deleteAll(Member.class);
                                Shared.clear_Login(activity);
                                memberDao = new MemberDao(respuesta.getData());
                                //userInRO2.save();
                                daoSession.getMemberDaoDao().insertOrReplace(memberDao);
                                Shared.edit_Login(activity,imeiCode,appUser, true, null, token);
                                SessionManager sessionManager = new SessionManager(activity);
                                sessionManager.createLoginSession(respuesta.getData().getIdMember(),respuesta.getData().getEmail());

                                return true;
                            } else {
                                outMessage.append(activity.getString(R.string.type_not_pass));
                                return false;
                            }
                        }
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }

    //region SyncAuthenticationEmail
    public boolean SyncAuthenticationEmail(String appUser, String appPIN,final StringBuilder timeOut,final StringBuilder outMessage) {
        try {
            outMessage.setLength(0);
            Call<restView<Integer>> callApplication = APIAdapter.getApiService(appToken,0)
                    .setAuthenticateEmail(new authenticateEmailInRO(appName, appVersion, appUser, appPIN));

            Response<restView<Integer>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<Integer> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    timeOut.append(respuesta.getData());

                    if (errorCode == 0) {
                        outMessage.append(activity.getString(R.string.sincronizacion_email_autenticado));
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion

    //region SyncCheckPIN
    public boolean SyncCheckPIN(String appUser, String appPIN, StringBuilder outMessage) {
        try {
            outMessage.setLength(0);
            Call<restView<Nullable>> callApplication = APIAdapter.getApiService(appToken,0)
                    .setCheckPIN(new authenticateEmailInRO(appName, appVersion, appUser, appPIN));

            Response<restView<Nullable>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<Nullable> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());

                    if (errorCode == 0) {
                        outMessage.append(respuesta.getMessage());
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion

    //region SyncChangePasswordEmail
    public boolean SyncChangePasswordEmail(String currentHash, String newHash, String appUser, String appToken, StringBuilder outMessage) {
        try {
            outMessage.setLength(0);
            Call<restView<Nullable>> callApplication = APIAdapter.getApiService(appToken,0)
                    .setChangePasswordEmail(new authenticatePasswordInRO(currentHash, newHash, appUser, appToken));

            Response<restView<Nullable>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<Nullable> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {
                        outMessage.append(respuesta.getMessage());
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion

    //region SyncDepartments
    public boolean SyncDepartments(StringBuilder outMessage){
        try {
            outMessage.setLength(0);
            Call<restView<List<DepartamentoView>>> callApplication = APIAdapter.getApiService(appToken,0)
                    .getlistDepartments(new apiData<Nullable>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, null));

            Response<restView<List<DepartamentoView>>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<List<DepartamentoView>> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {
                        outMessage.append(respuesta.getMessage());
                        List<DepartamentoView> departmentsView = respuesta.getData();

                        if (respuesta.getData() != null && respuesta.getData().size() > 0) {
                            for (DepartamentoView departamentoView : departmentsView) {
                                Departamento departamento = new Departamento(departamentoView);
                                departamento.saveOrUpdate();

                                if (departamentoView.getListaProvincias() != null && departamentoView.getListaProvincias().size() > 0) {
                                    for (ProvinciaView provinciaView : departamentoView.getListaProvincias()) {
                                        Provincia provincia = new Provincia(provinciaView, departamento);
                                        provincia.saveOrUpdate();

                                        if (provinciaView.getListaMunicipios() != null && provinciaView.getListaMunicipios().size() > 0) {
                                            for (MunicipioView municipioView : provinciaView.getListaMunicipios()) {
                                                Municipio municipio = new Municipio(municipioView, provincia);
                                                municipio.saveOrUpdate();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion

    //region SyncAlmacenes
    public boolean SyncAlmacenes(StringBuilder outMessage){
        try {
            outMessage.setLength(0);
            Call<restView<List<AlmacenView>>> callApplication = APIAdapter.getApiService(appToken,0)
                    .getlistWarehouses(new apiData<Nullable>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, null));

            Response<restView<List<AlmacenView>>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<List<AlmacenView>> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {
                        outMessage.append(respuesta.getMessage());
                        List<AlmacenView> almacenViews = respuesta.getData();

                        if (respuesta.getData() != null && respuesta.getData().size() > 0) {
                            for (AlmacenView almacenView : almacenViews) {

                                Almacen almacen = new Almacen(almacenView);
                                almacen.saveOrUpdate();

                                if (almacenView.getListaTarifarios() != null && almacenView.getListaTarifarios().size() > 0) {
                                    for (TarifarioView tarifarioView : almacenView.getListaTarifarios()) {
                                        Tarifario tarifario = new Tarifario(tarifarioView, almacen);
                                        tarifario.saveOrUpdate();
                                    }
                                }
                            }
                        }
                        return true;
                    } else  if (forzar != 0 && errorCode == 8001) {
                        outMessage.append(respuesta.getMessage());
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion

    //region SyncItems
    public boolean SyncItems(StringBuilder outMessage){
        try {
            outMessage.setLength(0);
            Call<restView<List<ArticuloView>>> callApplication = APIAdapter.getApiService(appToken,0)
                    .getlistItems(new apiData<Nullable>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, null));

            Response<restView<List<ArticuloView>>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<List<ArticuloView>> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {
                        outMessage.append(respuesta.getMessage());
                        List<ArticuloView> articuloViews = respuesta.getData();

                        if (respuesta.getData() != null && respuesta.getData().size() > 0) {
                            for (ArticuloView articuloView : articuloViews) {

                                Articulo articulo = new Articulo(articuloView);
                                articulo.deletePrice();
                                articulo.deleteStock();
                                articulo.saveOrUpdate();

                                if (articuloView.getListaPrecios() != null && articuloView.getListaPrecios().size() > 0) {
                                    for (TarifarioXArticuloView tarifarioView : articuloView.getListaPrecios()) {
                                        TarifarioXArticulo tarifarioXArticulo = new TarifarioXArticulo(tarifarioView, articulo);
                                        tarifarioXArticulo.save();
                                    }
                                }

                                if (articuloView.getListaStocks() != null && articuloView.getListaStocks().size() > 0) {
                                    for (ArticuloXAlmacenView articuloXAlmacenView : articuloView.getListaStocks()) {
                                        ArticuloXAlmacen articuloXAlmacen = new ArticuloXAlmacen(articuloXAlmacenView, articulo);
                                        articuloXAlmacen.save();
                                    }
                                }
                            }
                        }
                        return true;
                    } else  if (forzar != 0 && errorCode == 8001) {
                        outMessage.append(respuesta.getMessage());
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion

    //region SyncCustomers
    public boolean SyncCustomers(StringBuilder outMessage){
        try {
            outMessage.setLength(0);
            Call<restView<List<ClienteView>>> callApplication = APIAdapter.getApiService(appToken,0)
                    .getlistCustomers(new apiData<Nullable>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, null));

            Response<restView<List<ClienteView>>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<List<ClienteView>> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {
                        outMessage.append(respuesta.getMessage());
                        List<ClienteView> clienteViews = respuesta.getData();

                        if (respuesta.getData() != null && respuesta.getData().size() > 0) {
                            for (ClienteView clienteView : clienteViews) {

                                Cliente cliente = new Cliente(clienteView,0);
                                cliente.deleteDir();
                                cliente.deleteConctact();
                                cliente.saveOrUpdate();

                                if (clienteView.getListaDirecciones() != null && clienteView.getListaDirecciones().size() > 0) {

                                    for (DireccionView direccionView : clienteView.getListaDirecciones()) {
                                        Direccion direccion = new Direccion(direccionView, cliente);
                                        direccion.save();
                                    }
                                }

                                if (clienteView.getPersonaContacto() != null && clienteView.getPersonaContacto().size() > 0) {

                                    for (PersonaContactoView personaContactoView : clienteView.getPersonaContacto()) {
                                        PersonaContacto personaContacto = new PersonaContacto(personaContactoView, cliente);
                                        personaContacto.save();
                                    }
                                }
                            }
                        }
                        return true;
                    } else  if (errorCode == 8001) {
                        outMessage.append(respuesta.getMessage());
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion

    //region SyncLeads
    public boolean SyncLeads(StringBuilder outMessage){
        try {
            outMessage.setLength(0);
            Call<restView<List<LeadView>>> callApplication = APIAdapter.getApiService(appToken,0)
                    .getlistLeads(new apiData<Nullable>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, null));

            Response<restView<List<LeadView>>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<List<LeadView>> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {
                        outMessage.append(respuesta.getMessage());
                        List<LeadView> leadViews = respuesta.getData();

                        if (respuesta.getData() != null && respuesta.getData().size() > 0) {
                            for (LeadView leadView : leadViews) {
                                if (leadView.getCardCode() != null) {
                                    Lead lead = new Lead(leadView);
                                    lead.saveOrUpdate();
                                }
                            }
                        }
                        return true;
                    } else  if (errorCode == 8001) {
                        outMessage.append(respuesta.getMessage());
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion

    //region SyncTaxs
    public boolean SyncTaxs(StringBuilder outMessage){
        try {
            outMessage.setLength(0);
            Call<restView<List<ImpuestoView>>> callApplication = APIAdapter.getApiService(appToken,0)
                    .getlistTaxs(new apiData<Nullable>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, null));

            Response<restView<List<ImpuestoView>>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<List<ImpuestoView>> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {
                        outMessage.append(respuesta.getMessage());
                        List<ImpuestoView> impuestoViews = respuesta.getData();

                        if (respuesta.getData() != null && respuesta.getData().size() > 0) {
                            for (ImpuestoView impuestoView : impuestoViews) {

                                Impuesto impuesto = new Impuesto(impuestoView);
                                impuesto.saveOrUpdate();

                            }
                        }
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion

    //region SyncCoins
    public boolean SyncCoins(StringBuilder outMessage){
        try {
            outMessage.setLength(0);
            outMessage.append(activity.getString(R.string.sincronizacion_titulo_sync));
            Call<restView<List<MonedaView>>> callApplication = APIAdapter.getApiService(appToken,0)
                    .getlistCurrencies(new apiData<Nullable>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, null));

            Response<restView<List<MonedaView>>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<List<MonedaView>> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {
                        outMessage.append(respuesta.getMessage());
                        List<MonedaView> monedaViews = respuesta.getData();

                        if (respuesta.getData() != null && respuesta.getData().size() > 0) {
                            for (MonedaView monedaView : monedaViews) {

                                Moneda moneda = new Moneda(monedaView);
                                moneda.saveOrUpdate();

                            }
                        }
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion


    //region SyncCoins
    public boolean SyncPaymentConditions(StringBuilder outMessage){
        try {
            outMessage.setLength(0);
            outMessage.append(activity.getString(R.string.sincronizacion_titulo_sync));
            Call<restView<List<CondicionPagoView>>> callApplication = APIAdapter.getApiService(appToken,0)
                    .getlistPaymentCondition(new apiData<Nullable>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, null));

            Response<restView<List<CondicionPagoView>>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<List<CondicionPagoView>> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {
                        outMessage.append(respuesta.getMessage());
                        List<CondicionPagoView> condicionPagoViews = respuesta.getData();

                        if (respuesta.getData() != null && respuesta.getData().size() > 0) {
                            for (CondicionPagoView condicionPagoView : condicionPagoViews) {

                                CondicionPago condicionPago = new CondicionPago(condicionPagoView);
                                condicionPago.saveOrUpdate();

                            }
                        }
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion

    //region SyncSaveOrder
    public boolean SyncSaveOrder(StringBuilder outMessage,apiOrderHeader apiOrderHeader){
        try {
            outMessage.setLength(0);
            Call<restView<Nullable>> callApplication = APIAdapter.getApiService(appToken,0)
                    .saveOrder(new apiData<apiOrderHeader>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, apiOrderHeader));

            Response<restView<Nullable>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<Nullable> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {
                        outMessage.append(respuesta.getMessage());
                        apiOrderHeader.setEstadoMigracion(2);
                        apiOrderHeader.setMensajeMigracion(respuesta.getMessage() != null ? respuesta.getMessage() : "Pedido registrado");
                        if (apiOrderHeader.getId() != null)
                            apiOrderHeader.update();
                        else
                            apiOrderHeader.save();
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        apiOrderHeader.setEstadoMigracion(3);
                        apiOrderHeader.setMensajeMigracion(respuesta.getMessage() != null ? respuesta.getMessage() : "Pedido con error");
                        if (apiOrderHeader.getId() != null)
                            apiOrderHeader.update();
                        else
                            apiOrderHeader.save();
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion

    //region SyncSaveLead
    public boolean SyncSaveLead(StringBuilder outMessage, apiCreateLead apiCreateLead){
        try {
            outMessage.setLength(0);
            Call<restView<Nullable>> callApplication = APIAdapter.getApiService(appToken,0)
                    .saveLead(new apiData<apiCreateLead>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, apiCreateLead));

            Response<restView<Nullable>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<Nullable> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {
                        outMessage.append(respuesta.getMessage());
                        apiCreateLead.setEstadoMigracion(2);
                        apiCreateLead.setMensajeMigracion(respuesta.getMessage() != null ? respuesta.getMessage() : "Lead Registrado");
                        if (apiCreateLead.getId() != null)
                            apiCreateLead.update();
                        else
                            apiCreateLead.save();
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        apiCreateLead.setEstadoMigracion(3);
                        apiCreateLead.setMensajeMigracion(respuesta.getMessage() != null ? respuesta.getMessage() : "Lead con error");
                        if (apiCreateLead.getId() != null)
                            apiCreateLead.update();
                        else
                            apiCreateLead.save();
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion

    //region SyncSaveBusiness
    public boolean SyncSaveBusiness(StringBuilder outMessage, apiCreateBusiness apiCreateBusiness){
        try {
            outMessage.setLength(0);
            Call<restView<Nullable>> callApplication = APIAdapter.getApiService(appToken,0)
                    .saveBusiness(new apiData<apiCreateBusiness>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, apiCreateBusiness));

            Response<restView<Nullable>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<Nullable> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {
                        outMessage.append(respuesta.getMessage());
                        apiCreateBusiness.setEstadoMigracion(2);
                        apiCreateBusiness.setMensajeMigracion(respuesta.getMessage() != null ? respuesta.getMessage() : "Cliente registrado");
                        if (apiCreateBusiness.getId() != null)
                            apiCreateBusiness.update();
                        else
                            apiCreateBusiness.save();
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        apiCreateBusiness.setEstadoMigracion(3);
                        apiCreateBusiness.setMensajeMigracion(respuesta.getMessage() != null ? respuesta.getMessage() : "Cliente con error");
                        if (apiCreateBusiness.getId() != null)
                            apiCreateBusiness.update();
                        else
                            apiCreateBusiness.save();
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion


    //region SyncUpdateSyncLast
    public boolean SyncUpdateSyncLast(StringBuilder outMessage) {
        try {
            outMessage.setLength(0);
            Call<restView<Boolean>> callApplication = APIAdapter.getApiService(appToken,0)
                    .updateSyncLast(new apiData<Nullable>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, null));

            Response<restView<Boolean>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<Boolean> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {
                        outMessage.append(respuesta.getMessage());
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }
            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion


    //region SyncSaveVisit
    public boolean SyncSaveVisit(StringBuilder outMessage, apiCreateVisit apiCreateVisit){
        try {
            outMessage.setLength(0);
            Call<restView<Integer>> callApplication = APIAdapter.getApiService(appToken,0)
                    .saveVisit(new apiData<apiCreateVisit>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, apiCreateVisit));

            Response<restView<Integer>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<Integer> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {
                        outMessage.append(respuesta.getMessage());
                        apiCreateVisit.setEstadoMigracion(2);
                        apiCreateVisit.setMensajeMigracion(respuesta.getMessage() != null ? respuesta.getMessage() : "Visita registrada");
                        if (apiCreateVisit.getId() != null)
                            apiCreateVisit.update();
                        else
                            apiCreateVisit.save();
                        return true;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        apiCreateVisit.setEstadoMigracion(3);
                        apiCreateVisit.setMensajeMigracion(respuesta.getMessage() != null ? respuesta.getMessage() : "Visita con error");
                        if (apiCreateVisit.getId() != null)
                            apiCreateVisit.update();
                        else
                            apiCreateVisit.save();
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion

    //region SyncListVisit
    public List<QueryVisitaView> SyncListVisit(StringBuilder outMessage, apiQuery apiQueryData){
        try {
            outMessage.setLength(0);
            Call<restView<List<QueryVisitaView>>> callApplication = APIAdapter.getApiService(appToken,0)
                    .getlistVisitas(new apiData<apiQuery>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, apiQueryData));

            Response<restView<List<QueryVisitaView>>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<List<QueryVisitaView>> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {

                        outMessage.append(respuesta.getMessage());
                        return respuesta.getData();

                    } else  if (errorCode == 8001) {
                        outMessage.append(respuesta.getMessage());
                        return null;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");

                        return null;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return null;
                }

            } else {
                outMessage.append(response.message());
                return null;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return null;
        }
    }
    //endregion

    //region SyncListOrder
    /*public List<QueryPedidoView> SyncListOrder(StringBuilder outMessage, apiQuery apiQueryData){
        try {
            outMessage.setLength(0);
            Call<restView<List<QueryPedidoView>>> callApplication = APIAdapter.getApiService(appToken)
                    .getlistPedidos(new apiData<apiQuery>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, apiQueryData));

            Response<restView<List<QueryPedidoView>>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<List<QueryPedidoView>> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {

                        outMessage.append(respuesta.getMessage());
                        return respuesta.getData();

                    } else  if (errorCode == 8001) {
                        outMessage.append(respuesta.getMessage());
                        return null;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");

                        return null;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return null;
                }

            } else {
                outMessage.append(response.message());
                return null;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return null;
        }
    }*/
    //endregion

    //region SyncListInvoces
    public String  SyncListInvoces(StringBuilder outMessage, apiQuery apiQueryData){
        try {
            outMessage.setLength(0);
            Call<restView<String>> callApplication = APIAdapter.getApiService(appToken,0)
                    .getlistFacturas(new apiData<apiQuery>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, apiQueryData));

            Response<restView<String>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<String> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {

                        outMessage.append(respuesta.getMessage());
                        return respuesta.getData();

                    } else  if (errorCode == 8001) {
                        outMessage.append(respuesta.getMessage());
                        return null;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");

                        return null;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return null;
                }

            } else {
                outMessage.append(response.message());
                return null;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return null;
        }
    }
    //endregion

    //region SyncListMotivos
    public Boolean SyncListMotivos(StringBuilder outMessage) {

        try {
            outMessage.setLength(0);
            Call<restView<List<CoreMotivoView>>> callApplication = APIAdapter.getApiService(appToken,0)
                    .getlistMotivos(new apiData<Nullable>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, null));

            Response<restView<List<CoreMotivoView>>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<List<CoreMotivoView>> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {

                        outMessage.append(respuesta.getMessage());
                        List<CoreMotivoView> coreMotivoViews = respuesta.getData();

                        if (respuesta.getData() != null && respuesta.getData().size() > 0) {
                            for (CoreMotivoView coreMotivoView : coreMotivoViews) {

                                CoreMotivo coreMotivo = new CoreMotivo(coreMotivoView);
                                coreMotivo.saveOrUpdate();

                            }
                        }
                        return true;

                    }  else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");

                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion

    //region SyncListRutas
    public Boolean SyncListRutas(StringBuilder outMessage) {

        try {
            outMessage.setLength(0);
            Call<restView<List<CoreRutaView>>> callApplication = APIAdapter.getApiService(appToken,0)
                    .getlistRutas(new apiData<Nullable>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, null));

            Response<restView<List<CoreRutaView>>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<List<CoreRutaView>> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {

                        outMessage.append(respuesta.getMessage());
                        List<CoreRutaView> coreRutaViews = respuesta.getData();

                        if (respuesta.getData() != null && respuesta.getData().size() > 0) {
                            for (CoreRutaView coreRutaView : coreRutaViews) {

                                CoreRuta coreRuta = new CoreRuta(coreRutaView);
                                coreRuta.saveOrUpdate();

                            }
                        }
                        return true;

                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");

                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion

    //region SyncListDrafts
    public String  SyncListDrafts(StringBuilder outMessage, apiQuery apiQueryData){
        try {
            outMessage.setLength(0);
            Call<restView<String>> callApplication = APIAdapter.getApiService(appToken,0)
                    .getlistDrafts(new apiData<apiQuery>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, apiQueryData));

            Response<restView<String>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<String> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {

                        outMessage.append(respuesta.getMessage());
                        return respuesta.getData();

                    } else  if (errorCode == 8001) {
                        outMessage.append(respuesta.getMessage());
                        return null;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");

                        return null;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return null;
                }

            } else {
                outMessage.append(response.message());
                return null;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return null;
        }
    }
    //endregion

    //region SyncListOrders
    public String  SyncListOrders(StringBuilder outMessage, apiQuery apiQueryData){
        try {
            outMessage.setLength(0);
            Call<restView<String>> callApplication = APIAdapter.getApiService(appToken,0)
                    .getlistOrders(new apiData<apiQuery>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, apiQueryData));

            Response<restView<String>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<String> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {

                        outMessage.append(respuesta.getMessage());
                        return respuesta.getData();

                    } else  if (errorCode == 8001) {
                        outMessage.append(respuesta.getMessage());
                        return null;
                    } else {
                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");

                        return null;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return null;
                }

            } else {
                outMessage.append(response.message());
                return null;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return null;
        }
    }
    //endregion


    //region SyncCustomersAlone
    public boolean SyncCustomersAlone(StringBuilder outMessage,String CardCode){
        try {
            outMessage.setLength(0);
            Call<restView<ClienteView>> callApplication = APIAdapter.getApiService(appToken,0)
                    .getlistCustomersAlone(new apiData<String>(appName, appUser, forzar, imeiCode, appIP, appToken, appVersion, CardCode));

            Response<restView<ClienteView>> response = callApplication.execute();
            if (response.isSuccessful()) {
                restView<ClienteView> respuesta = response.body();
                if (respuesta != null) {
                    int errorCode =  Integer.parseInt(respuesta.getCode());
                    if (errorCode == 0) {
                        outMessage.append(respuesta.getMessage());
                        ClienteView clienteView = respuesta.getData();

                        Cliente cliente = new Cliente(clienteView,0);
                        cliente.deleteDir();
                        cliente.deleteConctact();
                        cliente.saveOrUpdate();

                        if (clienteView.getListaDirecciones() != null && clienteView.getListaDirecciones().size() > 0) {

                            for (DireccionView direccionView : clienteView.getListaDirecciones()) {
                                Direccion direccion = new Direccion(direccionView, cliente);
                                direccion.save();
                            }
                        }

                        if (clienteView.getPersonaContacto() != null && clienteView.getPersonaContacto().size() > 0) {

                            for (PersonaContactoView personaContactoView : clienteView.getPersonaContacto()) {
                                PersonaContacto personaContacto = new PersonaContacto(personaContactoView, cliente);
                                personaContacto.save();
                            }
                        }

                        return true;
                    } else {

                        Object msg = respuesta.getMessage();
                        outMessage.append(msg != null ? msg.toString() : "");
                        return false;
                    }
                } else {
                    outMessage.append(activity.getString(R.string.sincronizacion_servicio_sin_respuesta));
                    return false;
                }

            } else {
                outMessage.append(response.message());
                return false;
            }

        } catch (Exception ex) {
            outMessage.append(ex.getMessage());
            return false;
        }
    }
    //endregion
}
