package com.ndp.data.api;


import android.support.annotation.Nullable;

import java.util.List;

import com.ndp.models.appRest.ApiResponse;
import com.ndp.models.appRest.apiData;
import com.ndp.models.appRest.apiHeader;
import com.ndp.models.appRest.authenticateInRO2;
import com.ndp.models.appRest.restView;
import com.ndp.models.appRest.authenticateEmailInRO;
import com.ndp.models.appRest.authenticateInRO;
import com.ndp.models.appRest.authenticatePasswordInRO;
import com.ndp.models.dto.AlmacenView;
import com.ndp.models.dto.ArticuloView;
import com.ndp.models.dto.ClienteView;
import com.ndp.models.dto.CondicionPagoView;
import com.ndp.models.dto.CoreMotivoView;
import com.ndp.models.dto.CoreRutaView;
import com.ndp.models.dto.DepartamentoView;
import com.ndp.models.dto.ImpuestoView;
import com.ndp.models.dto.LeadView;
import com.ndp.models.dto.MemberDTO;
import com.ndp.models.dto.MonedaView;
import com.ndp.models.dto.QueryPedidoView;
import com.ndp.models.dto.QueryVisitaView;
import com.ndp.models.dto.UserView;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

public interface APIService {

    //@POST("auth/login")
    //Call<restView<UserView>> setAuthenticate3(@Header("Content-Range") apiHeader apiHeader);

   // @POST("auth/login")
    //Call<restView<UserView>> setAuthenticate2(@Header("Content-Range") apiHeader apiHeader);


    //@POST("auth/login")
  //  Call<restView<UserView>> setAuthenticate2(@Header("Content-Range") apiHeader apiHeader, @Body authenticateInRO2 authenticate);

    @POST("auth/login")
    Call<ApiResponse<MemberDTO>> setAuthenticate2(@Header("identifier")String identifier, @Header("signature")String signature, @Header("payload")String payload, @Header("fingerprint")String fingerprint);

    @POST("login")
    Call<restView<UserView>> setAuthenticate(@Body authenticateInRO authenticate);

    @POST("usuario/sendPIN")
    Call<restView<Integer>> setAuthenticateEmail(@Body authenticateEmailInRO authenticateEmailInRO);

    @POST("usuario/checkPIN")
    Call<restView<Nullable>> setCheckPIN(@Body authenticateEmailInRO authenticateEmailInRO);

    @POST("usuario/editPasswordOutApp")
    Call<restView<Nullable>> setChangePasswordEmail(@Body authenticatePasswordInRO authenticatePasswordInRO);

    @POST("departamentos/list")
    Call<restView<List<DepartamentoView>>> getlistDepartments(@Body apiData apiData);

    @POST("almacenes/listAll")
    Call<restView<List<AlmacenView>>> getlistWarehouses(@Body apiData apiData);

    @POST("articulos/listAll")
    Call<restView<List<ArticuloView>>> getlistItems(@Body apiData apiData);

    @POST("clientes/listAll")
    Call<restView<List<ClienteView>>> getlistCustomers(@Body apiData apiData);

    @POST("leads/list")
    Call<restView<List<LeadView>>> getlistLeads(@Body apiData apiData);

    @POST("impuesto/list")
    Call<restView<List<ImpuestoView>>> getlistTaxs(@Body apiData apiData);

    @POST("moneda/list")
    Call<restView<List<MonedaView>>> getlistCurrencies(@Body apiData apiData);

    @POST("condicionPago/list")
    Call<restView<List<CondicionPagoView>>> getlistPaymentCondition(@Body apiData apiData);

    @POST("pedidos/save")
    Call<restView<Nullable>> saveOrder(@Body apiData apiData);

    @POST("leads/save")
    Call<restView<Nullable>> saveLead(@Body apiData apiData);

    @POST("clientes/save")
    Call<restView<Nullable>> saveBusiness(@Body apiData apiData);

    @POST("usuario/updateSyncLast")
    Call<restView<Boolean>> updateSyncLast(@Body apiData apiData);

    @POST("visitas/save")
    Call<restView<Integer>> saveVisit(@Body apiData apiData);

    @POST("visitas/listAll")
    Call<restView<List<QueryVisitaView>>> getlistVisitas(@Body apiData apiData);

    @POST("pedidos/listAll")
    Call<restView<List<QueryPedidoView>>> getlistPedidos(@Body apiData apiData);

    @POST("facturas/listAll")
    Call<restView<String>> getlistFacturas(@Body apiData apiData);

    @POST("motivos/listAll")
    Call<restView<List<CoreMotivoView>>> getlistMotivos(@Body apiData apiData);

    @POST("rutas/listAll")
    Call<restView<List<CoreRutaView>>> getlistRutas(@Body apiData apiData);

    @POST("pedidos/listDrafts")
    Call<restView<String>> getlistDrafts(@Body apiData apiData);

    @POST("pedidos/listOrders")
    Call<restView<String>> getlistOrders(@Body apiData apiData);

    @POST("clientes/getClient")
    Call<restView<ClienteView>> getlistCustomersAlone(@Body apiData apiData);
}

