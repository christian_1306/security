package com.ndp.data.api;


import android.support.annotation.NonNull;
import android.util.Log;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import com.ndp.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIAdapter {

    public static APIService getApiService(String Token, int typeUrl) {

        String BASE_URL;
        String BASE_URL_SECENG = BuildConfig.BASE_URL_SECENG; //0
        String BASE_URL_SECOPE = BuildConfig.BASE_URL_SECOPE; //1
        String BASE_URL_ADMENG = BuildConfig.BASE_URL_ADMENG; //2
        String BASE_URL_ADMOPE = BuildConfig.BASE_URL_ADMOPE; //3
        String BASE_URL_TAKENG = BuildConfig.BASE_URL_TAKENG; //4
        String BASE_URL_TAKOPE = BuildConfig.BASE_URL_TAKOPE; //5

        switch (typeUrl) {
            case 0 :
                BASE_URL = BASE_URL_SECENG;
                break;
            case 1 :
                BASE_URL = BASE_URL_SECOPE;
                break;
            case 2 :
                BASE_URL = BASE_URL_ADMENG;
                break;
            case 3 :
                BASE_URL = BASE_URL_ADMOPE;
                break;
            case 4 :
                BASE_URL = BASE_URL_TAKENG;
                break;
            case 5 :
                BASE_URL = BASE_URL_TAKOPE;
                break;
            default:
                BASE_URL = BuildConfig.BASE_URL;
        }


        //region DEBUGGING
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override public void log(@NonNull String message) {
                Log.d("APIAdapter Interceptor",message);
            }
        });
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        //endregion

        //region OkHttpClient
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)  // add logging as last interceptor (this is the important line)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(@NonNull Interceptor.Chain chain) throws IOException {
                        Request original = chain.request();
                        // Request customization: add request headers
                        Request.Builder requestBuilder = original.newBuilder()
                                //.addHeader("Cache-Control", "no-cache")
                                .addHeader("Authorization", "Bearer "+Token);

                        Request request = requestBuilder.build();
                        return chain.proceed(request);
                    }
                    })
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS).build();
        //endregion

        //region Retrofit
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();
        //endregion

        return retrofit.create(APIService.class);
    }

}
