package com.ndp.data.network;

public interface Urls {
    String LOGIN = "usuario/login";
    String LIST_CLIENTS = "clientes/listAll";
    String LIST_DEPARTMENTS = "departamentos/list";
    String LIST_ALL_WAREHOUSES = "almacenes/listAll";
    String LIST_ALL_BILLS = "facturas/listAll";
    String LIST_ALL_VISITS = "visitas/listAll";
    String SAVE_CLIENT = "clientes/save";
    String SAVE_VISIT = "visitas/save";
    String LIST_ITEMS = "almacenes/loadItems";
    String LIST_ALL_ORDERS = "pedidos/listAll";
    String CHANGE_PASSWORD = "usuario/editPassword";
    String SAVE_ORDER = "pedidos/save";
    String LIST_ALL_ITEMS = "almacenes/loadAllItems";
}
