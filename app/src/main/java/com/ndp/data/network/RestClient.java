package com.ndp.data.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;


import com.ndp.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RestClient {
    private final AcerosServices webServices;
    private static final String BASE_URL = BuildConfig.BASE_URL;
    private static final int MAX_TIME = 240;

    public RestClient(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder().
                readTimeout(MAX_TIME, TimeUnit.SECONDS).
                connectTimeout(MAX_TIME, TimeUnit.SECONDS).
                addInterceptor(interceptor).
                build();

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();

        webServices = retrofit.create(AcerosServices.class);
    }

    public AcerosServices getWebservices(){
        return webServices;
    }
}
