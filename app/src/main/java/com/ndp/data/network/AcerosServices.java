package com.ndp.data.network;
import java.util.List;

import com.ndp.models.appRest.restView;
import com.ndp.models.retrofit.ApiData;
import com.ndp.models.dto.AlmacenView;
import com.ndp.models.dto.ClienteView;
import com.ndp.models.dto.DepartamentoView;
import com.ndp.models.dto.FacturaView;
import com.ndp.models.dto.JwtUserView;
import com.ndp.models.dto.LoginView;
import com.ndp.models.dto.PedidoView;
import com.ndp.models.dto.VisitaView;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface AcerosServices {

    // TODO: 4/01/19
    @POST(Urls.LOGIN)
    Call<restView<JwtUserView>> login(@Query("email") String email, @Query("password") String password);

    @POST(Urls.LIST_CLIENTS)
    Call<restView<List<ClienteView>>> listClients(@Body ApiData apiData/*,@Query("token") String token*/);

    @GET(Urls.LIST_DEPARTMENTS)
    Call<restView<List<DepartamentoView>>> listDepartments(/*@Query("token") String token*/);

    @GET(Urls.LIST_ALL_WAREHOUSES)
    Call<restView<List<AlmacenView>>> listAllWarehouses();

    @POST(Urls.LIST_ALL_BILLS)
    Call<restView<List<FacturaView>>> listAllBills(@Body ApiData apiData/*,@Query("token") String token*/);

    @POST(Urls.LIST_ALL_VISITS)
    Call<restView<List<VisitaView>>> listAllVisits(@Body ApiData apiData/*,@Query("token") String token*/);

    @POST(Urls.LIST_ITEMS)
    Call<restView<AlmacenView>> listItemsByWarehouseAndRate(@Body ApiData apiData, @Query("idAlmacen") Integer idAlmacen, @Query("idTarifario") Integer idTarifario);

    @POST(Urls.LIST_ALL_ORDERS)
    Call<restView<List<PedidoView>>> listAllOrders(@Body ApiData apiData);

    @POST(Urls.SAVE_CLIENT)
    Call<restView<ClienteView>> saveClient(@Body ApiData<ClienteView> apiData);

    @POST(Urls.LIST_ALL_ITEMS)
    Call<restView<List<AlmacenView>>> listAllItems(@Body ApiData apiData);

    @POST(Urls.SAVE_VISIT)
    Call<restView<VisitaView>> saveVisit(@Body ApiData<VisitaView> apiData);

    @POST(Urls.SAVE_ORDER)
    Call<restView<PedidoView>> saveOrder(@Body ApiData<PedidoView> apiData);

    @PUT(Urls.CHANGE_PASSWORD)
    Call<restView<LoginView>> changePassword(@Query("idUsuario")Integer idUsuario, @Query("newPassword") String newPassword, @Query("tipoUsuario") Integer tipoUsuario, @Body ApiData apiData);
}
