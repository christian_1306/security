package com.ndp.data.network;

import android.support.annotation.NonNull;
import android.util.Log;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AcerosCallback<T> implements Callback<T> {

    private static String LOG_TAG = "ACEROS TAG LOG";

    protected AcerosCallback() {}

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        Log.d(LOG_TAG, String.valueOf(response));
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        Log.d(LOG_TAG, String.valueOf(t));
    }
}

