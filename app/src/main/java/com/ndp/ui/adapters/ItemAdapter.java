package com.ndp.ui.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Articulo;
import com.ndp.ui.activities.ItemInfoActivity;


public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> implements Filterable {

    private List<Articulo> lstArticulos;
    private List<Articulo> lstArticulosFiltered;
    private Integer CODE_ACTIVITY;
    private Activity activity;
    private Boolean isTrueAction;

    public ItemAdapter(List<Articulo> lstArticulos, Integer CODE_ACTIVITY, Boolean isTrueAction, Activity activity) {
        this.lstArticulos = lstArticulos;
        this.lstArticulosFiltered = lstArticulos;
        this.CODE_ACTIVITY = CODE_ACTIVITY;
        this.isTrueAction = isTrueAction;
        this.activity = activity;
    }

    //region Adapter Methods

    //region Evitan que los elementos se repitan
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    //endregion

    @Override
    public int getItemCount() {
        return lstArticulos != null ? lstArticulos.size() : 0;
    }

    @Override
    @NonNull
    public ItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") int i) {
            viewHolder.tv_itemname.setText(lstArticulos.get(i).getNombre());
            viewHolder.tv_itemcode.setText("COD SAP: "+lstArticulos.get(i).getCodigo());
            viewHolder.tv_unidad.setText("UNIDAD: "+((lstArticulos.get(i).getUnidadMedida() != null) ? lstArticulos.get(i).getUnidadMedida(): "SUM"));
            viewHolder.lyt_card_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, ItemInfoActivity.class);
                    intent.putExtra(activity.getResources().getString(R.string.obj_item), lstArticulos.get(i));
                    intent.putExtra(activity.getResources().getString(R.string.changeCardName), false);
                    activity.startActivity(intent);
                }
            });
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void insert(int position, Articulo data) {
        lstArticulos.add(position, data);
        notifyItemInserted(position);
    }

    public void remove(Articulo data) {
        int position = lstArticulos.indexOf(data);
        lstArticulos.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    lstArticulos = lstArticulosFiltered;
                } else {
                    List<Articulo> filteredList = new ArrayList<>();
                    for (Articulo rowFilter : lstArticulosFiltered) {
                        if (rowFilter.getNombre().toLowerCase().contains(charString.toLowerCase())
                                || rowFilter.getCodigo().toLowerCase().contains(charSequence)) {
                            filteredList.add(rowFilter);
                        }
                    }

                    lstArticulos = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = lstArticulos;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                lstArticulos = (List<Articulo>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
    //endregion

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout lyt_card_item;
        private TextView tv_itemname;
        private TextView tv_itemcode;
        private TextView tv_unidad;

        ViewHolder(View itemView) {
            super(itemView);
            lyt_card_item = itemView.findViewById(R.id.lyt_card_item);
            tv_itemname = itemView.findViewById(R.id.tv_itemname);
            tv_itemcode = itemView.findViewById(R.id.tv_itemcode);
            tv_unidad = itemView.findViewById(R.id.tv_unidad);
        }
    }
}