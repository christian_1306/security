package com.ndp.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.ndp.R;
import com.ndp.models.model.Direccion;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

     ArrayList<Direccion> listDirecciones;
     private boolean isForNewClient = true;

    public AddressAdapter(ArrayList<Direccion> listDirecciones) {
        this.listDirecciones = listDirecciones;
    }

    public void setForNewClient(boolean forNewClient) {
        isForNewClient = forNewClient;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_item_address, viewGroup, false);
        return new ViewHolder(view, isForNewClient);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.tvIndice.setText(String.valueOf(i+1) + ".");
        viewHolder.tvDireccion.setText(listDirecciones.get(i).toString());
    }

    @Override
    public int getItemCount() {
        return listDirecciones.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvIndice;
        TextView tvDireccion;
        ImageView ivBorrar;
        boolean isForNewClient;
        public ViewHolder(@NonNull View itemView, boolean isForNewClient) {
            super(itemView);
            this.isForNewClient = isForNewClient;
            tvIndice = itemView.findViewById(R.id.tv_indice);
            tvDireccion = itemView.findViewById(R.id.tv_direccion_agregar);
            ivBorrar = itemView.findViewById(R.id.iv_delete_address);

            ivBorrar.setOnClickListener(this);
            if(!isForNewClient) ivBorrar.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View v) {
            if(v.equals(ivBorrar)) {
                removeAt(getPosition());
            }
        }
    }

    private void removeAt(int position) {
        listDirecciones.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeRemoved(position, listDirecciones.size());
    }
}
