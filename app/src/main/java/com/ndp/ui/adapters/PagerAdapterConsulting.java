package com.ndp.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ndp.models.model.Vendedor;
import com.ndp.ui.fragments.AccountStatusSellerFragment;
import com.ndp.ui.fragments.GeneratedOrdersFragment;
import com.ndp.ui.fragments.VisitsMadeFragment;

public class PagerAdapterConsulting extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    private Vendedor vendedor;

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public PagerAdapterConsulting(FragmentManager fm, int mNumOfTabs){
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position){
        switch(position){
            case 0:
                GeneratedOrdersFragment generatedOrdersFragment = new GeneratedOrdersFragment();
                generatedOrdersFragment.setPending(false);
                return generatedOrdersFragment;
            case 1:
                VisitsMadeFragment visitsMadeFragment = new VisitsMadeFragment();
                visitsMadeFragment.setPending(false);
                visitsMadeFragment.setVendedor(vendedor);
                return visitsMadeFragment;
            case 2:
                //FRAGMENTO DE ESTADOS DE CUENTA QUE FALTA REALIZAR
                AccountStatusSellerFragment accountStatusSellerFragment = new AccountStatusSellerFragment();
                return accountStatusSellerFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount(){
        return mNumOfTabs;
    }
}
