package com.ndp.ui.adapters;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ndp.R;

import java.util.List;


import com.ndp.models.model.Cliente;
import com.ndp.models.model.Vendedor;
import com.ndp.ui.activities.OrderSeller;
import com.ndp.ui.activities.SelectClientActivity;
import com.ndp.ui.fragments.AllClientsFragment;
import com.ndp.ui.fragments.OrderContentFragment;

public class ClienteAdapter extends RecyclerView.Adapter<ClienteAdapter.ViewHolder> {
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvRazonSocial, tvNumeroDocumento,tvDireccion,tvMunicipio;
        private List<Cliente> listaClientes;
        private Fragment fragment;
        private Vendedor vendedor;
        private SelectClientActivity selectClientActivity;
        private boolean esVisita;


        public boolean isEsVisita() {
            return esVisita;
        }

        public void setEsVisita(boolean esVisita) {
            this.esVisita = esVisita;
        }

        public SelectClientActivity getSelectClientActivity() {
            return selectClientActivity;
        }

        public void setSelectClientActivity(SelectClientActivity selectClientActivity) {
            this.selectClientActivity = selectClientActivity;
        }

        public ViewHolder(View itemView, Fragment fragment, List<Cliente> listaClientes,
                          Vendedor vendedor) {
            super(itemView);
            this.listaClientes = listaClientes;
            this.fragment = fragment;
            this.vendedor = vendedor;
            itemView.setOnClickListener(this);
            setElements(itemView);
        }

        private void setElements(View itemView) {
            tvRazonSocial = itemView.findViewById(R.id.tvRazonSocial);
            tvNumeroDocumento = itemView.findViewById(R.id.tvNumeroDocumento);
            tvDireccion = itemView.findViewById(R.id.tvDireccion);
            tvMunicipio = itemView.findViewById(R.id.tvMunicipio);
        }

        @Override
        public void onClick(View v) {
            AllClientsFragment allClientsFragment = null;
            if(!esVisita) {
                allClientsFragment = (AllClientsFragment)fragment;
                if(allClientsFragment.getId_fragmento() == 1) {
                    //ESTOY EN EL BOTON DE CLIENTES
                    Cliente cliente = listaClientes.get(getPosition());
                    allClientsFragment.openDetailItem(cliente);
                } else if(allClientsFragment.getId_fragmento() == 2) {
                    //ESTOY EN EL BOTON DE PEDIDO

                    OrderContentFragment orderContentFragment = new OrderContentFragment();
                    orderContentFragment.setCliente(listaClientes.get(getPosition()));
                    orderContentFragment.setVendedor(vendedor);
                    activityOrderSeller.setEstoyEnPedido(true);

                    allClientsFragment.getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_order_seller, orderContentFragment).addToBackStack(null).commit();
                }
            }
            else{
                //ESTOY EN EL BOTON DE VISITAS
                Cliente cliente = listaClientes.get(getPosition());
                selectClientActivity.seleccionarCliente(cliente);
            }
        }
    }
    public List<Cliente> listaClientes;
    private Fragment fragment;
    private Vendedor vendedor;
    private SelectClientActivity selectClientActivity;
    private boolean esVisita;
    public OrderSeller activityOrderSeller;

    public OrderSeller getActivityOrderSeller() {
        return activityOrderSeller;
    }

    public void setActivityOrderSeller(OrderSeller activityOrderSeller) {
        this.activityOrderSeller = activityOrderSeller;
    }

    public boolean isEsVisita() {
        return esVisita;
    }

    public void setEsVisita(boolean esVisita) {
        this.esVisita = esVisita;
    }

    public ClienteAdapter(List<Cliente> listaClientes){this.listaClientes = listaClientes;}

    public ClienteAdapter(List<Cliente> listaClientes, SelectClientActivity selectClientActivity){
        this.listaClientes = listaClientes;
        this.selectClientActivity = selectClientActivity;
    }
    public ClienteAdapter(List<Cliente> listaClientes, Fragment fragment) {
        this.listaClientes = listaClientes;
        this.fragment = fragment;
    }

    public ClienteAdapter(List<Cliente> listaClientes, Fragment fragment, Vendedor vendedor) {
        this.listaClientes = listaClientes;
        this.fragment = fragment;
        this.vendedor = vendedor;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup,int i){
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_item_client,viewGroup,false);
        ViewHolder viewHolder = new ViewHolder(view, fragment, listaClientes, vendedor);
        if(esVisita==true) {
            viewHolder.setSelectClientActivity(selectClientActivity);
            viewHolder.setEsVisita(true);
        }
        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Cliente cliente = listaClientes.get(i);
        viewHolder.tvRazonSocial.setText(cliente.getCardCode() +"\n"+ cliente.getRazonSocial());
        viewHolder.tvRazonSocial.setText(cliente.getNumeroDocIdentidad());
        /*viewHolder.tvNumeroDocumento.setText(cliente.getNumeroDocumento());
        if(cliente.getTipoCliente() == 1) {
            Direccion direccion = cliente.listarDirecciones().get(0);
            viewHolder.tvDireccion.setText(direccion.getNombre());
            viewHolder.tvMunicipio.setText(direccion.getMunicipio().getNombre());
        }
        else{
            viewHolder.tvDireccion.setText("-");
            viewHolder.tvMunicipio.setText("-");
        }*/
    }

    @Override
    public int getItemCount() {
        return listaClientes.size();
    }
}

