package com.ndp.ui.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import com.ndp.R;
import com.ndp.models.model.TarifarioXArticulo;
import com.ndp.utils.use.useAPP;


public class ItemPriceAdapter extends RecyclerView.Adapter<ItemPriceAdapter.ViewHolder> {

    private List<TarifarioXArticulo> lstTarifarioXArticulos;
    private List<TarifarioXArticulo> lstTarifarioXArticulosFiltered;
    private Integer CODE_ACTIVITY;
    private Activity activity;
    private Boolean isTrueAction;

    public ItemPriceAdapter(List<TarifarioXArticulo> lstTarifarioXArticulos, Integer CODE_ACTIVITY, Boolean isTrueAction, Activity activity) {
        this.lstTarifarioXArticulos = lstTarifarioXArticulos;
        this.lstTarifarioXArticulosFiltered = lstTarifarioXArticulos;
        this.CODE_ACTIVITY = CODE_ACTIVITY;
        this.isTrueAction = isTrueAction;
        this.activity = activity;
    }

    //region Adapter Methods

    //region Evitan que los elementos se repitan
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    //endregion

    @Override
    public int getItemCount() {
        return lstTarifarioXArticulos != null ? lstTarifarioXArticulos.size() : 0;
    }

    @Override
    @NonNull
    public ItemPriceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_item_price, viewGroup, false);
        return new ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") int i) {
            viewHolder.tv_price_name.setText(String.valueOf(lstTarifarioXArticulos.get(i).getCodTarifario())
                    + " - "
                    + ((lstTarifarioXArticulos.get(i).getNombreListPrice() != null && !lstTarifarioXArticulos.get(i).getNombreListPrice().isEmpty()) ? lstTarifarioXArticulos.get(i).getNombreListPrice() : "SN"));
            viewHolder.tv_price_neto.setText("PRECIO NETO: " + (lstTarifarioXArticulos.get(i).getMoneda() != null ? lstTarifarioXArticulos.get(i).getMoneda() : "BS")  +" "+(useAPP.fmDecimal(lstTarifarioXArticulos.get(i).getPrecioDefaultNeto())));
            viewHolder.tv_price_final.setText("PRECIO FINAL: "+ (lstTarifarioXArticulos.get(i).getMoneda() != null ? lstTarifarioXArticulos.get(i).getMoneda() : "BS")  +" "+(useAPP.fmDecimal(lstTarifarioXArticulos.get(i).getPrecioDefaultFinal())));
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void insert(int position, TarifarioXArticulo data) {
        lstTarifarioXArticulos.add(position, data);
        notifyItemInserted(position);
    }

    public void remove(TarifarioXArticulo data) {
        int position = lstTarifarioXArticulos.indexOf(data);
        lstTarifarioXArticulos.remove(position);
        notifyItemRemoved(position);
    }

    //endregion

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout lyt_card_itemPrice;
        private TextView tv_price_name;
        private TextView tv_price_neto;
        private TextView tv_price_final;

        ViewHolder(View itemView) {
            super(itemView);
            lyt_card_itemPrice = itemView.findViewById(R.id.lyt_card_itemPrice);
            tv_price_name = itemView.findViewById(R.id.tv_price_name);
            tv_price_neto = itemView.findViewById(R.id.tv_price_neto);
            tv_price_final = itemView.findViewById(R.id.tv_price_final);
        }
    }
}