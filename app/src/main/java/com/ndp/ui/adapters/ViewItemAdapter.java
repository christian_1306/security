package com.ndp.ui.adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Articulo;
import com.ndp.models.model.DetallePedido;

public class ViewItemAdapter extends RecyclerView.Adapter<ViewItemAdapter.ViewHolder>{
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvIndiceArticulo,tvPrecio,tvTotal,tv_sin_stock;
        TextView tv_cantidad_varillas,tv_cantidad_toneladas,tv_nombre_articulo;
        RelativeLayout rlvItem;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            setElements();
        }

        private void setElements(){
            tv_sin_stock = itemView.findViewById(R.id.tv_sin_stock);
            tvIndiceArticulo = itemView.findViewById(R.id.tv_item_indice);
            tvPrecio = itemView.findViewById(R.id.tv_item_price);
            tvTotal = itemView.findViewById(R.id.tv_item_total);
            tv_cantidad_varillas = itemView.findViewById(R.id.tv_item_cant_varillas);
            tv_cantidad_toneladas = itemView.findViewById(R.id.tv_item_cant_toneladas);
            tv_nombre_articulo = itemView.findViewById(R.id.tv_item_name);
            rlvItem = itemView.findViewById(R.id.rl_item_name);
        }

        public void displayData(int i){
            DetallePedido detallePedido = detallePedidos.get(i);
            Articulo articulo = detallePedido.getTarifarioXArticulo().getArticulo();
           // Tarifario tarifario = detallePedido.getTarifarioXArticulo().getTarifario();

            tvIndiceArticulo.setText(String.valueOf(i+1));
            tv_cantidad_varillas.setText(String.valueOf(detallePedido.getCantidad()));
            tv_cantidad_toneladas.setText(String.valueOf(detallePedido.getPesoTotal()));

          /*  TarifarioXArticulo tarifarioXArticulo = TarifarioXArticulo.find(TarifarioXArticulo.class,
                    "tarifario = ? and articulo = ?",String.valueOf(tarifario.getId())
                    ,String.valueOf(articulo.getId())).get(0);
            DecimalFormat df = new DecimalFormat("#.00");
            tvPrecio.setText(String.valueOf(df.format(tarifarioXArticulo.getPrecioDefaultFinal())));*/
            ///tvTotal.setText(String.valueOf(df.format(detallePedido.getSubtotalFinal())));
            tv_nombre_articulo.setText(articulo.getNombre());
            if(detallePedido.isEsPerdida()) resaltarPerdida();
            else resaltarNoEsPerdida();
        }

        private void resaltarNoEsPerdida(){
            rlvItem.setBackgroundColor(Color.parseColor("#cecdcd"));
            tv_sin_stock.setVisibility(View.GONE);
        }

        private void resaltarPerdida(){
            rlvItem.setBackgroundColor(Color.parseColor("#FFCBAE"));
        }
    }

    private List<DetallePedido> detallePedidos;

    @Override
    public int getItemCount(){
        return detallePedidos.size();
    }

    public ViewItemAdapter(List<DetallePedido> detallePedidos){
        this.detallePedidos = detallePedidos;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.displayData(i);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup,int i){
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_item_view_item, viewGroup, false);
        return new ViewHolder(view);
    }
}
