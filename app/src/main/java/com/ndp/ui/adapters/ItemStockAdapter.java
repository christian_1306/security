package com.ndp.ui.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import com.ndp.R;
import com.ndp.models.model.ArticuloXAlmacen;
import com.ndp.utils.use.useAPP;


public class ItemStockAdapter extends RecyclerView.Adapter<ItemStockAdapter.ViewHolder> {

    private List<ArticuloXAlmacen> lstArticuloXAlmacens;
    private List<ArticuloXAlmacen> lstArticuloXAlmacensFiltered;
    private Integer CODE_ACTIVITY;
    private Activity activity;
    private Boolean isTrueAction;

    public ItemStockAdapter(List<ArticuloXAlmacen> lstArticuloXAlmacens, Integer CODE_ACTIVITY, Boolean isTrueAction, Activity activity) {
        this.lstArticuloXAlmacens = lstArticuloXAlmacens;
        this.lstArticuloXAlmacensFiltered = lstArticuloXAlmacens;
        this.CODE_ACTIVITY = CODE_ACTIVITY;
        this.isTrueAction = isTrueAction;
        this.activity = activity;
    }

    //region Adapter Methods

    //region Evitan que los elementos se repitan
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    //endregion

    @Override
    public int getItemCount() {
        return lstArticuloXAlmacens != null ? lstArticuloXAlmacens.size() : 0;
    }

    @Override
    @NonNull
    public ItemStockAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_item_stock, viewGroup, false);
        return new ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") int i) {
            viewHolder.tv_warehouse_name.setText(lstArticuloXAlmacens.get(i).getWarehouseCode());
            viewHolder.tv_stock.setText("STOCK: "+(useAPP.fmDecimal(lstArticuloXAlmacens.get(i).getStock())));
            viewHolder.tv_comprometido.setText("COMPROMETIDO: "+(useAPP.fmDecimal(lstArticuloXAlmacens.get(i).getComprometido())));
            viewHolder.tv_disponible.setText("DISPONIBLE: "+(useAPP.fmDecimal(lstArticuloXAlmacens.get(i).getDisponible())));
            viewHolder.tv_solicitado.setText("SOLICITADO: "+(useAPP.fmDecimal(lstArticuloXAlmacens.get(i).getSolicitado())));
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void insert(int position, ArticuloXAlmacen data) {
        lstArticuloXAlmacens.add(position, data);
        notifyItemInserted(position);
    }

    public void remove(ArticuloXAlmacen data) {
        int position = lstArticuloXAlmacens.indexOf(data);
        lstArticuloXAlmacens.remove(position);
        notifyItemRemoved(position);
    }

    //endregion

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout lyt_card_warehouse;
        private TextView tv_warehouse_name;
        private TextView tv_stock;
        private TextView tv_comprometido;
        private TextView tv_disponible;
        private TextView tv_solicitado;

        ViewHolder(View itemView) {
            super(itemView);
            lyt_card_warehouse = itemView.findViewById(R.id.lyt_card_warehouse);
            tv_warehouse_name = itemView.findViewById(R.id.tv_warehouse_name);
            tv_stock = itemView.findViewById(R.id.tv_stock);
            tv_comprometido = itemView.findViewById(R.id.tv_comprometido);
            tv_disponible = itemView.findViewById(R.id.tv_disponible);
            tv_solicitado = itemView.findViewById(R.id.tv_solicitado);
        }
    }
}