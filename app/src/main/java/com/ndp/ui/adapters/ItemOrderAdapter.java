package com.ndp.ui.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.travijuu.numberpicker.library.Enums.ActionEnum;
import com.travijuu.numberpicker.library.Interface.ValueChangedListener;
import com.travijuu.numberpicker.library.NumberPicker;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;

import com.ndp.R;
import com.ndp.models.model.ArticuloOrder;
import com.ndp.ui.activities.OrderActivity;
import com.ndp.utils.Popup.Popup;

import static com.ndp.utils.use.useAPP.fmDecimal;
import static com.ndp.utils.use.useAPP.isNullOrEmpty;
import static com.ndp.utils.use.useAPP.isNullOrEmptyDouble;


public class ItemOrderAdapter extends RecyclerView.Adapter<ItemOrderAdapter.ViewHolder> implements Filterable {

    private ArrayList<ArticuloOrder> articuloOrders;
    private ArrayList<ArticuloOrder> articuloOrdersFiltered;
    private Activity activity;
    private Boolean isCustomer;

    public ItemOrderAdapter(ArrayList<ArticuloOrder> articuloOrders, Activity activity, Boolean isCustomer) {
        this.articuloOrders = articuloOrders;
        this.articuloOrdersFiltered = articuloOrders;
        this.activity = activity;
        this.isCustomer = isCustomer;
    }

    //region Adapter Methods

    //region Evitan que los elementos se repitan
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    //endregion

    @Override
    public int getItemCount() {
        return articuloOrders != null ? articuloOrders.size() : 0;
    }


    @Override
    @NonNull
    public ItemOrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_order, viewGroup, false);
        return new ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") int i) {

        ArticuloOrder item =  articuloOrders.get(i);

        viewHolder.tv_item_name.setText(isNullOrEmpty(item.getNombre()));
        viewHolder.tv_item_moneda.setText(isNullOrEmpty(item.getMoneda()));
        viewHolder.tv_item_price.setText(isNullOrEmptyDouble(item.getPrecioDefaultNeto()));
        viewHolder.tv_item_conversion.setText(" "+Integer.toString(item.getVarillasPorTonelada()) + " " + isNullOrEmpty(item.getUnidadMedida()));

        if (isCustomer) {
            viewHolder.lyt_units_convert.setVisibility(View.GONE);
            viewHolder.lyt_units.setVisibility(View.GONE);
            viewHolder.tv_item_cantidad.setEnabled(true);
            viewHolder.tv_item_cantidad.setActionEnabled(ActionEnum.INCREMENT,true);
            viewHolder.tv_item_cantidad.setActionEnabled(ActionEnum.DECREMENT,true);
            viewHolder.tv_item_cantidad.setValueChangedListener(new ValueChangedListener() {
                @Override
                public void valueChanged(int value, ActionEnum action) {
                    double line = (value * item.getVarillasPorTonelada())
                            * item.getPrecioDefaultNeto();
                    viewHolder.tv_item_total.setText(fmDecimal(line));
                    item.setCantidadOrder(viewHolder.tv_item_cantidad.getValue());
                }
            });

            viewHolder.tv_item_cantidad.setValue(item.getCantidadOrder());
            viewHolder.tv_item_total.setText(fmDecimal((item.getCantidadOrder() * item.getVarillasPorTonelada())
                    * item.getPrecioDefaultNeto()));
        } else {
            viewHolder.lyt_units_convert.setVisibility(View.VISIBLE);
            viewHolder.lyt_units.setVisibility(View.VISIBLE);

            viewHolder.tv_item_stock_unidad.setText(activity.getString(R.string.desc_stock) + " " + isNullOrEmpty(item.getUnidadMedida()) + activity.getString(R.string.dos_puntos));

            switch (item.getDisponible() > 0 ? 1 : 0) {
                case 0:
                    viewHolder.tv_item_stock.setText(Integer.toString(0));
                    viewHolder.tv_item_stock.setTextColor(Color.RED);
                    viewHolder.tv_item_stock_conversion.setText(Integer.toString(0));
                    viewHolder.tv_item_stock_conversion.setTextColor(Color.RED);
                    viewHolder.tv_item_cantidad.setEnabled(false);
                    viewHolder.tv_item_cantidad.setActionEnabled(ActionEnum.INCREMENT,false);
                    viewHolder.tv_item_cantidad.setActionEnabled(ActionEnum.DECREMENT,false);
                    viewHolder.tv_item_cantidad.setValue(0);
                    viewHolder.tv_item_total.setText(fmDecimal(0));
                    break;
                case 1:
                    viewHolder.tv_item_stock.setText(Integer.toString(item.getDisponible()));
                    int valueMax = (int) item.getDisponible()/item.getVarillasPorTonelada();
                    viewHolder.tv_item_stock_conversion.setText(Integer.toString(valueMax));
                    viewHolder.tv_item_stock.setTextColor(activity.getResources().getColor(R.color.Gray_DimGray));
                    viewHolder.tv_item_stock_conversion.setTextColor(activity.getResources().getColor(R.color.Gray_DimGray));
                    viewHolder.tv_item_cantidad.setEnabled(true);
                    viewHolder.tv_item_cantidad.setActionEnabled(ActionEnum.INCREMENT,true);
                    viewHolder.tv_item_cantidad.setActionEnabled(ActionEnum.DECREMENT,true);
                    viewHolder.tv_item_cantidad.setValueChangedListener(new ValueChangedListener() {
                        @Override
                        public void valueChanged(int value, ActionEnum action) {
                            double line = (value * item.getVarillasPorTonelada())
                                    * item.getPrecioDefaultNeto();
                            viewHolder.tv_item_total.setText(fmDecimal(line));
                            item.setCantidadOrder(viewHolder.tv_item_cantidad.getValue());
                            if  (value == valueMax) {
                                MDToast.makeText(activity, isNullOrEmpty(item.getNombre()) + ": Limite Permitido ("+String.valueOf(valueMax)+")", MDToast.LENGTH_LONG, MDToast.TYPE_INFO).show();
                            }
                        }
                    });
                    viewHolder.tv_item_cantidad.setMax(valueMax);
                    viewHolder.tv_item_cantidad.setValue(item.getCantidadOrder());

                    viewHolder.tv_item_total.setText(fmDecimal((item.getCantidadOrder() * item.getVarillasPorTonelada())
                            * item.getPrecioDefaultNeto()));

                    break;
            }
        }


        if (activity instanceof OrderActivity){
            viewHolder.tv_item_cantidad.setMin(1);
        }


    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void removeItem(int position) {
        try {
            articuloOrders.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, articuloOrders.size());
        } catch (Exception ex){
            Popup.showAlert(activity, ex.getMessage());
        }
    }

    public void restoreItem(ArticuloOrder articuloOrder, int position) {
        try {
            articuloOrders.add(position, articuloOrder);
            notifyItemInserted(position);
        } catch (Exception ex){
            Popup.showAlert(activity, ex.getMessage());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    articuloOrders = articuloOrdersFiltered;
                } else {
                    ArrayList<ArticuloOrder> filteredList = new ArrayList<>();
                    for (ArticuloOrder rowFilter : articuloOrdersFiltered) {
                        if (rowFilter.getNombre().toLowerCase().contains(charString.toLowerCase())
                                || rowFilter.getCodigo().contains(charSequence)) {
                            filteredList.add(rowFilter);
                        }
                    }
                    articuloOrders = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = articuloOrders;
                filterResults.count = articuloOrders.size();
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                articuloOrders = (ArrayList<ArticuloOrder>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    //endregion

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_item_name;
        private TextView tv_item_moneda;
        private TextView tv_item_price;
        private TextView tv_item_total;
        private TextView tv_item_conversion;
        private TextView tv_item_stock;
        private TextView tv_item_stock_unidad;
        private TextView tv_item_stock_conversion;
        private NumberPicker tv_item_cantidad;
        private LinearLayout lyt_units_convert;
        private LinearLayout lyt_units;

        ViewHolder(View itemView) {
            super(itemView);
            tv_item_name = itemView.findViewById(R.id.tv_item_name);
            tv_item_moneda = itemView.findViewById(R.id.tv_item_moneda);
            tv_item_price = itemView.findViewById(R.id.tv_item_price);
            tv_item_total = itemView.findViewById(R.id.tv_item_total);
            tv_item_conversion = itemView.findViewById(R.id.tv_item_conversion);
            tv_item_stock = itemView.findViewById(R.id.tv_item_stock);
            tv_item_stock_conversion = itemView.findViewById(R.id.tv_item_stock_conversion);
            tv_item_stock_unidad = itemView.findViewById(R.id.tv_item_stock_unidad);
            tv_item_cantidad = itemView.findViewById(R.id.tv_item_cantidad);
            lyt_units_convert = itemView.findViewById(R.id.lyt_units_convert);
            lyt_units = itemView.findViewById(R.id.lyt_units);
        }
    }
}