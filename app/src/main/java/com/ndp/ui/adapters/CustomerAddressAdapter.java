package com.ndp.ui.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import com.ndp.R;
import com.ndp.models.appRest.apiDireccionDetail;

import static com.ndp.utils.Const.TYPE_WAREHOUSE_ALMACEN;


public class CustomerAddressAdapter extends RecyclerView.Adapter<CustomerAddressAdapter.ViewHolder> {

    private List<apiDireccionDetail> apiDireccionDetailList;
    private Activity activity;
    private LinearLayout lyt_empty_direccion;
    private RecyclerView recyclerView;

    public CustomerAddressAdapter(List<apiDireccionDetail> apiDireccionDetailList, Activity activity, LinearLayout lyt_empty_direccion, RecyclerView recyclerView) {
        this.apiDireccionDetailList = apiDireccionDetailList;
        this.activity = activity;
        this.lyt_empty_direccion = lyt_empty_direccion;
        this.recyclerView = recyclerView;
    }

    //region Adapter Methods

    //region Evitan que los elementos se repitan
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    //endregion

    @Override
    public int getItemCount() {
        return apiDireccionDetailList != null ? apiDireccionDetailList.size() : 0;
    }

    @Override
    @NonNull
    public CustomerAddressAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_customer_address, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") int i) {

        viewHolder.tv_address_name.setText(apiDireccionDetailList.get(i).getNombreDireccion());
        viewHolder.tv_address_type.setText(
                apiDireccionDetailList.get(i).getTipo().equals(TYPE_WAREHOUSE_ALMACEN) ? "ALMACEN" : "FISCAL");
        viewHolder.tv_address.setText(apiDireccionDetailList.get(i).getDireccion());
        viewHolder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove(apiDireccionDetailList.get(i));
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void insert(int position, apiDireccionDetail data) {
        apiDireccionDetailList.add(position, data);
        notifyItemInserted(position);
    }

    public void remove(apiDireccionDetail data) {
        int position = apiDireccionDetailList.indexOf(data);
        apiDireccionDetailList.remove(position);
        notifyItemRemoved(position);
        if (apiDireccionDetailList.size() == 0) {
            lyt_empty_direccion.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            apiDireccionDetailList.clear();
        }
    }
    //endregion

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_delete;
        private TextView tv_address_type;
        private TextView tv_address_name;
        private TextView tv_address;

        ViewHolder(View itemView) {
            super(itemView);
            img_delete = itemView.findViewById(R.id.img_delete);
            tv_address_type = itemView.findViewById(R.id.tv_address_type);
            tv_address_name = itemView.findViewById(R.id.tv_address_name);
            tv_address = itemView.findViewById(R.id.tv_address);
        }
    }
}