package com.ndp.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Pedido;
import com.ndp.ui.fragments.GeneratedOrdersFragment;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_nombre_cliente,tv_fecha_emision,tv_total,tv_estado_pedido,tv_referencia;
        private List<Pedido> pedidos;
        private boolean isPending;
        private Fragment fragment;

        public Fragment getFragment() {
            return fragment;
        }

        public void setFragment(Fragment fragment) {
            this.fragment = fragment;
        }

        public boolean isPending() {
            return isPending;
        }

        public void setPending(boolean pending) {
            isPending = pending;
        }

        public ViewHolder(View itemView, List<Pedido> pedidos,boolean isPending){
            super(itemView);
            this.pedidos = pedidos;
            this.isPending = isPending;
            setElements(itemView);
            itemView.setOnClickListener(this);
            BlockElements();
        }

        private void setElements(View itemView) {
            tv_nombre_cliente = itemView.findViewById(R.id.tv_nombre_cliente);
            tv_fecha_emision = itemView.findViewById(R.id.tv_fecha_emision);
            tv_total = itemView.findViewById(R.id.tv_total);
            tv_estado_pedido = itemView.findViewById(R.id.tv_estado_pedido);
            tv_referencia = itemView.findViewById(R.id.tv_referencia);
        }

        private void BlockElements(){
            if (this.isPending) tv_estado_pedido.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View v) {
            int position = getPosition();
            Pedido pedidoSeleccionado = pedidos.get(position);
            if(this.fragment instanceof GeneratedOrdersFragment && !isPending){
                GeneratedOrdersFragment generatedOrdersFragment = (GeneratedOrdersFragment)fragment;
                generatedOrdersFragment.openDetail(pedidoSeleccionado);
            }
        }
    }

    public List<Pedido> pedidos;
    private Fragment fragment;

    public boolean isPending() {
        return isPending;
    }

    public void setPending(boolean pending) {
        isPending = pending;
    }

    private boolean isPending;

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public OrderAdapter(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    public OrderAdapter(List<Pedido> pedidos, Fragment fragment,boolean isPending) {
        this.pedidos = pedidos;
        this.fragment = fragment;
        this.isPending = isPending;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i){
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_item_order,viewGroup,false);
        ViewHolder viewHolder = new ViewHolder(view, pedidos,this.isPending);
        viewHolder.setFragment(this.fragment);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Pedido pedido = pedidos.get(i);
        viewHolder.tv_nombre_cliente.setText(pedido.getClientePrincipal().getRazonSocial());
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String fechaEmision = dateFormat.format(pedido.getFechaEmision());
        viewHolder.tv_fecha_emision.setText(fechaEmision);
        DecimalFormat dfTotal = new DecimalFormat("#.00");
        viewHolder.tv_total.setText(dfTotal.format(pedido.getTotalFinal()));
        if(pedidos.get(i).getClienteSecundario()!=null)
        viewHolder.tv_referencia.setText(pedido.getClienteSecundario().getRazonSocial());
    }

    @Override
    public int getItemCount() {
        return pedidos.size();
    }
}
