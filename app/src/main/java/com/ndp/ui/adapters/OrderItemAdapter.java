package com.ndp.ui.adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Almacen;
import com.ndp.models.model.Articulo;
import com.ndp.models.model.ArticuloXAlmacen;
import com.ndp.models.model.DetallePedido;
import com.ndp.models.model.Tarifario;
import com.ndp.models.model.TarifarioXArticulo;
import com.ndp.ui.fragments.OrderItemFragment;

public class OrderItemAdapter extends RecyclerView.Adapter<OrderItemAdapter.ViewHolder> {

    private OrderItemFragment orderItemFragment;
//    private boolean isItemForOrder;
//    private String query;

    private boolean isForSeller = false;

    private long id_tarifario;
//    private boolean isForView = false;
    private List<TarifarioXArticulo> listaTarifarioXArticulo;
    private List<Articulo> listaArticulos;
    private ArrayList<DetallePedido> listaDetalles;
    private ArrayList<Articulo> listaArticulosSeleccionados;
    private Almacen almacen;

    public List<TarifarioXArticulo> getListaTarifarioXArticulo() {
        return listaTarifarioXArticulo;
    }

    public void setListaTarifarioXArticulo(List<TarifarioXArticulo> listaTarifarioXArticulo) {
        this.listaTarifarioXArticulo = listaTarifarioXArticulo;
    }


    public ArrayList<DetallePedido> getListaDetalles() {
        return listaDetalles;
    }

    public void setListaDetalles(ArrayList<DetallePedido> listaDetalles) {
        this.listaDetalles = listaDetalles;
    }

    public long getId_tarifario() {
        return id_tarifario;
    }

    public void setId_tarifario(long id_tarifario) {
        this.id_tarifario = id_tarifario;
    }

    public List<Articulo> getListaArticulos() {
        return listaArticulos;
    }

    public void setListaArticulos(List<Articulo> listaArticulos) {
        this.listaArticulos = listaArticulos;
    }

    public OrderItemAdapter(List<Articulo> listaArticulos, OrderItemFragment orderItemFragment,
                            boolean isForSeller, Almacen almacen) {
        this.listaArticulos = listaArticulos;
        this.orderItemFragment = orderItemFragment;
        this.isForSeller = isForSeller;
        this.almacen = almacen;
        this.listaTarifarioXArticulo = new ArrayList<>();
//        this.isItemForOrder = isItemForOrder;
        this.listaDetalles = new ArrayList<>();
        this.listaArticulosSeleccionados = new ArrayList<>();
    }

    @NonNull
    @Override
    public OrderItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_item_order_popup, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderItemAdapter.ViewHolder viewHolder, int i) {
        viewHolder.displayData(i);
    }

    @Override
    public int getItemCount() {
        return listaArticulos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, TextWatcher {

        RelativeLayout rlNombreArticulo;
        TextView tvIndiceArticulo;
        TextView tvNombreArticulo;
        TextView tvEtiquetaStock;
        TextView tvStock;
        TextView tvPrecio;
        TextView tvTotal;
        TextView tvCantidadConversion;
        TextView tvUnidadConversion;
        ImageView btnMenos;
        EditText etCantidad;
        ImageView btnMas;
        CardView cvItem;

        Articulo articuloSeleccionado = null;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            setElements();
            blockElements();
        }

        private void setElements() {
            tvNombreArticulo = itemView.findViewById(R.id.tv_item_name);
            tvEtiquetaStock = itemView.findViewById(R.id.tv_etiqueta_stock);
            tvStock = itemView.findViewById(R.id.tv_stock_item);
            tvPrecio = itemView.findViewById(R.id.tv_item_price);
            tvTotal = itemView.findViewById(R.id.tv_item_total);
            tvCantidadConversion = itemView.findViewById(R.id.tv_item_cantidad_conversion);
            btnMenos = itemView.findViewById(R.id.iv_item_menos);
            etCantidad = itemView.findViewById(R.id.et_item_cant);
            btnMas = itemView.findViewById(R.id.iv_item_mas);
            cvItem = itemView.findViewById(R.id.cv_item_order);
            btnMenos.setOnClickListener(this);
            btnMas.setOnClickListener(this);
            etCantidad.addTextChangedListener(this);
        }

        private void blockElements() {
            if(isForSeller) return;

            //EL CLIENTE NO PUEDE VER EL STOCK DE LOS ARTICULOS
            tvEtiquetaStock.setVisibility(View.GONE);
            tvStock.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View v) {
            int cantidad = 0;
            int cantidad_toneladas = orderItemFragment.getCantidad_toneladas();
            String cadCantidad = etCantidad.getText().toString();
            if(!cadCantidad.isEmpty()) cantidad = Integer.parseInt(cadCantidad);
            switch (v.getId()) {
                case R.id.iv_item_menos:
                    if(cantidad == 0) return;
                    --cantidad_toneladas;
                    --cantidad;
                    etCantidad.setText(String.valueOf(cantidad));
                    return;
                case R.id.iv_item_mas:
                    if(isForSeller){
                        int stock = Integer.valueOf(tvStock.getText().toString());
                        int varillas = articuloSeleccionado.getVarillasPorTonelada()*(cantidad+1);
                        if(stock < varillas){
                            Toast.makeText(orderItemFragment.getActivity(),"No se puede agregar más debido a insuficiencia de stock",Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                    ++cantidad_toneladas;
                    if(cantidad_toneladas>30){
                        Toast.makeText(orderItemFragment.getActivity(),"No se puede pedir más de 30 toneladas",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    orderItemFragment.setCantidad_toneladas(cantidad_toneladas);
                    ++cantidad;
                    etCantidad.setText(String.valueOf(cantidad));
                    return;
            }
        }

        public void displayData(int i) {

            //SETEAR INDICE Y NOMBRE DEL ARTICULO
            articuloSeleccionado = listaArticulos.get(i);
            //tvIndiceArticulo.setText(String.valueOf(i+1));
            tvNombreArticulo.setText(articuloSeleccionado.getNombre());

            //SETEAR STOCK
            if(isForSeller){
                ArticuloXAlmacen articuloXAlmacen = ArticuloXAlmacen.find(ArticuloXAlmacen.class,"articulo = ? and almacen = ?",String.valueOf(articuloSeleccionado.getId()),String.valueOf(almacen.getId())).get(0);
                int stock = articuloXAlmacen.getStock();
                if(stock == 0) {
                    btnMas.setEnabled(false);
                    btnMenos.setEnabled(false);
                    etCantidad.setEnabled(false);
                    cvItem.setBackgroundColor(Color.parseColor("#ffba5c"));
                }
                tvStock.setText(String.valueOf(stock));
                //resaltarSinStock();
            }

            displayPrices();

            //ACTUALIZA LA CANTIDAD SI ES QUE YA HAY UN REGISTRO EN LOS DETALLES DEL PEDIDO
            int index = listaArticulosSeleccionados.indexOf(listaArticulos.get(i));
            if(index == -1) return;

            etCantidad.setText(String.valueOf(listaDetalles.get(index).getPesoTotal()));
            tvCantidadConversion.setText(String.valueOf(listaDetalles.get(index).getCantidad()));
        }

        private void displayPrices() {
            //EL CODIGO DEBAJO BUSCA EL PRECIO DEL ARTICULO CORRESPONDIENTE
            List<TarifarioXArticulo> tarifarioXArticulo = TarifarioXArticulo.find(TarifarioXArticulo.class,
                    "articulo = ? and tarifario = ?", String.valueOf(articuloSeleccionado.getId()),
                    String.valueOf(id_tarifario));
            DecimalFormat df = new DecimalFormat("#.00");
            if(tarifarioXArticulo.size() != 0)
                tvPrecio.setText(df.format(tarifarioXArticulo.get(0).getPrecioDefaultFinal()));
            else
                tvPrecio.setText("0");
        }

        private void resaltarSinStock() {
            if(tvStock.getText().toString().equals("0"))
                rlNombreArticulo.setBackgroundColor(Color.parseColor("#FFCBAE"));
            else
                rlNombreArticulo.setBackgroundColor(Color.parseColor("#cecdcd"));
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            int index = listaArticulosSeleccionados.indexOf(articuloSeleccionado);
            int cantidad = 0;
            articuloSeleccionado = listaArticulos.get(getPosition());

            if (etCantidad.getText().toString().isEmpty() ||
                    (Integer.parseInt(etCantidad.getText().toString())) == 0) { //ELIMINAR ARTICULO SI SE ENCUENTRA REGISTADO
                if(index != -1) {
                    listaDetalles.remove(index);
                    listaArticulosSeleccionados.remove(articuloSeleccionado);
                    listaTarifarioXArticulo.remove(index);
                    tvCantidadConversion.setText("");
                }
            }
            else {
                List<TarifarioXArticulo> tarifarioXArticulo = Tarifario.find(TarifarioXArticulo.class,
                        "articulo = ? and tarifario = ?", String.valueOf(articuloSeleccionado.getId()),
                        String.valueOf(id_tarifario));
                cantidad = Integer.parseInt(etCantidad.getText().toString());
                if(index == -1) {
                    addNewDetallePedido(cantidad, tarifarioXArticulo);
                }else {
                    actualizarDetallePedido(index, cantidad, tarifarioXArticulo);
                }
            }
            orderItemFragment.getOrderDetailFragment().ActualizarTotales(listaDetalles);
        }

        private void addNewDetallePedido(int cantidad, List<TarifarioXArticulo> tarifarioXArticulo) {
            int cantidadVarillas = articuloSeleccionado.getVarillasPorTonelada()*cantidad;
            double subtotalNeto = tarifarioXArticulo.get(0).getPrecioDefaultNeto()*cantidadVarillas;
            double impuestoArticulo = tarifarioXArticulo.get(0).getImpuestoArticulo() * cantidadVarillas;
            double subtotalFinal = tarifarioXArticulo.get(0).getPrecioDefaultFinal() * cantidadVarillas;
            DecimalFormat df = new DecimalFormat("#.00");

            DetallePedido newDetallePedido = new DetallePedido();
            newDetallePedido.setEstado(1);
            newDetallePedido.setTarifarioXArticulo(tarifarioXArticulo.get(0));
            newDetallePedido.setCantidad(cantidadVarillas);
            newDetallePedido.setPesoTotal(cantidad);
            newDetallePedido.setSubtotalNeto(subtotalNeto);
            newDetallePedido.setImpuestoDetallePedido(impuestoArticulo);
            newDetallePedido.setSubtotalFinal(subtotalFinal);
            listaDetalles.add(newDetallePedido);
            listaArticulosSeleccionados.add(articuloSeleccionado);
            listaTarifarioXArticulo.add(tarifarioXArticulo.get(0));
            tvCantidadConversion.setText(String.valueOf(cantidadVarillas));
            actualizarMontos(newDetallePedido);
        }

        private void actualizarDetallePedido(int index, int cantidad, List<TarifarioXArticulo> tarifarioXArticulo){
            int cantidadVarillas = articuloSeleccionado.getVarillasPorTonelada()*cantidad;
            double subtotalNeto = tarifarioXArticulo.get(0).getPrecioDefaultNeto()*cantidadVarillas;
            double impuestoArticulo = tarifarioXArticulo.get(0).getImpuestoArticulo() * cantidadVarillas;
            double subtotalFinal = tarifarioXArticulo.get(0).getPrecioDefaultFinal() * cantidadVarillas;
            DecimalFormat df = new DecimalFormat("#.00");

            listaDetalles.get(index).setCantidad(cantidadVarillas);
            listaDetalles.get(index).setPesoTotal(cantidad);
            listaDetalles.get(index).setSubtotalNeto(subtotalNeto);
            listaDetalles.get(index).setImpuestoDetallePedido(impuestoArticulo);
            listaDetalles.get(index).setSubtotalFinal(subtotalFinal);
            tvCantidadConversion.setText(String.valueOf(cantidadVarillas));
            actualizarMontos(listaDetalles.get(index));
        }

        @Override
        public void afterTextChanged(Editable s) {

        }

        private void actualizarMontos(DetallePedido detallePedido) {
            DecimalFormat df = new DecimalFormat("#.00");
            tvTotal.setText(df.format(detallePedido.getSubtotalFinal()));
        }

    }
}
