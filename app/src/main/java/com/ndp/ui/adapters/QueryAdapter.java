package com.ndp.ui.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import com.ndp.R;
import com.ndp.models.dto.QueryFacturasView;
import com.ndp.models.dto.QueryPedidoView;
import com.ndp.models.dto.QueryVisitaView;

import static com.ndp.utils.use.useAPP.fmDecimal;
import static com.ndp.utils.use.useAPP.getEstado;
import static com.ndp.utils.use.useAPP.isNullOrEmpty;


public class QueryAdapter<T> extends RecyclerView.Adapter<QueryAdapter<T>.ViewHolder> {

    private List<T> objList;
    private Integer typeObject;
    private Activity activity;
    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public QueryAdapter(List<T> objList, Integer typeObject, Activity activity) {
        this.objList = objList;
        this.typeObject = typeObject;
        this.activity = activity;
    }

    //region Adapter Methods

    //region Evitan que los elementos se repitan
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    //endregion

    @Override
    public int getItemCount() {
        return objList != null ? objList.size() : 0;
    }

    @Override
    @NonNull
    public QueryAdapter<T>.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_query, viewGroup, false);
        return new ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") int i) {
            switch (typeObject) {
                case 1:
                    QueryPedidoView order = (QueryPedidoView) objList.get(i);
                    viewHolder.tv_name.setText(isNullOrEmpty(order.getCardName()));
                    viewHolder.tv_number.setText(isNullOrEmpty(order.getNumAtCard()));
                    String isOrderAction = "";

                    if (order.getIsOrder() == 0)
                        isOrderAction = "Borrador";
                    else
                        isOrderAction = "Orden de Venta";

                    if (order.getU_BPVS_SALELOST() != null && order.getU_BPVS_SALELOST().equals("Y")) {
                        viewHolder.tv_descripcion00.setTextColor(activity.getResources().getColor(R.color.use_Red_low));
                        String auxString = isOrderAction + " Venta Perdida" + " | DocNum SAP: " + String.valueOf(order.getDocNum()) ;
                        viewHolder.tv_descripcion00.setText(auxString);
                    } else {
                        viewHolder.tv_descripcion00.setTextColor(activity.getResources().getColor(R.color.textColorSecondary));
                        viewHolder.tv_descripcion00.setText(isOrderAction + " | DocNum SAP: " + String.valueOf(order.getDocNum()));
                    }

                    viewHolder.tv_descripcion01.setText("Fec Entrega: " + order.getDocDueDate());
                    viewHolder.tv_descripcion02.setText("Estado: " + getEstado(order.getDocumentStatus()));
                    viewHolder.tv_descripcion03.setText("Cod APP: " + isNullOrEmpty(order.getU_BPVS_CODTRAS_APP()));
                    viewHolder.tv_descripcion04.setText("Total Documento: " + isNullOrEmpty(order.getDocCurrency() + " " + fmDecimal(order.getDocTotal())));
                    break;
                case 2:
                    QueryFacturasView invoice = (QueryFacturasView) objList.get(i);
                    viewHolder.tv_name.setText(isNullOrEmpty(invoice.getCardName()));
                    viewHolder.tv_number.setText(isNullOrEmpty(invoice.getNumAtCard()));
                    viewHolder.tv_descripcion00.setTextColor(activity.getResources().getColor(R.color.textColorSecondary));
                    viewHolder.tv_descripcion00.setText("DocNum SAP: " + String.valueOf(invoice.getDocNum()));
                    viewHolder.tv_descripcion01.setText("Fec Vencimiento: " + invoice.getDocDueDate());
                    viewHolder.tv_descripcion05.setText("Estado: " + getEstado(invoice.getDocumentStatus()));
                    viewHolder.tv_descripcion05.setVisibility(View.VISIBLE);
                    viewHolder.tv_descripcion02.setText("Cod APP: " + isNullOrEmpty(invoice.getU_BPVS_CODTRAS_APP()));
                    viewHolder.tv_descripcion03.setText("Pendiente por pagar: " + isNullOrEmpty(invoice.getDocCurrency() + " " + fmDecimal(invoice.getDocTotal()
                            - (invoice.getPaidToDate() != null ? invoice.getPaidToDate() : 0  ))));
                    viewHolder.tv_descripcion04.setText("Total Documento: " + isNullOrEmpty(invoice.getDocCurrency() + " " + fmDecimal(invoice.getDocTotal())));
                    break;
                case 3:
                    QueryVisitaView visita = (QueryVisitaView) objList.get(i);
                    viewHolder.tv_name.setText(isNullOrEmpty(visita.getCardName()));
                    viewHolder.tv_number.setText("");
                    viewHolder.tv_descripcion01.setText("Fecha Visita: " + isNullOrEmpty(visita.getFechaVisita()));
                    viewHolder.tv_descripcion02.setText("Motivo: " + isNullOrEmpty(visita.getMotivo()));
                    viewHolder.tv_descripcion03.setText("Comentario: " +isNullOrEmpty(visita.getComentario()));
                    viewHolder.tv_descripcion00.setVisibility(View.GONE);
                    viewHolder.tv_descripcion04.setVisibility(View.GONE);
                    break;
            }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    //endregion

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout rlyt_query;
        private ImageView img_type_query;
        private TextView tv_name;
        private TextView tv_number;
        private TextView tv_descripcion00;
        private TextView tv_descripcion01;
        private TextView tv_descripcion02;
        private TextView tv_descripcion03;
        private TextView tv_descripcion04;
        private TextView tv_descripcion05;

        ViewHolder(View itemView) {
            super(itemView);
            rlyt_query = itemView.findViewById(R.id.rlyt_query);
            img_type_query = itemView.findViewById(R.id.img_type_query);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_number = itemView.findViewById(R.id.tv_number);
            tv_descripcion00 = itemView.findViewById(R.id.tv_descripcion00);
            tv_descripcion01 = itemView.findViewById(R.id.tv_descripcion01);
            tv_descripcion02 = itemView.findViewById(R.id.tv_descripcion02);
            tv_descripcion03 = itemView.findViewById(R.id.tv_descripcion03);
            tv_descripcion04 = itemView.findViewById(R.id.tv_descripcion04);
            tv_descripcion05 = itemView.findViewById(R.id.tv_descripcion05);
        }
    }
}