package com.ndp.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.Factura;

public class AccountStatusAdapter extends RecyclerView.Adapter<AccountStatusAdapter.ViewHolder> {

    private Cliente cliente = null;
    private List<Factura> listaFacturas = new ArrayList<>();
    public AccountStatusAdapter(Cliente cliente) {
        this.cliente = cliente;
        //listaFacturas = cliente.listarFacturas();
    }

    public AccountStatusAdapter(List<Factura> facturas){
        this.listaFacturas = facturas;
    }

    @NonNull
    @Override
    public AccountStatusAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_item_account_state, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AccountStatusAdapter.ViewHolder viewHolder, int i) {
        viewHolder.displayData(i);
    }

    @Override
    public int getItemCount() {
        return listaFacturas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvNumeroFactura,tvReferencia,tvFecha,tvEstado;
        private TextView tvMonto;

        private Factura factura;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            setElements();
        }

        private void setElements(){
            tvNumeroFactura = itemView.findViewById(R.id.tv_texto_numero_factura);
            tvReferencia = itemView.findViewById(R.id.tv_nombre_referencia);
            tvFecha = itemView.findViewById(R.id.tv_numero_fecha);
            tvEstado = itemView.findViewById(R.id.tv_nombre_estado);
            tvMonto = itemView.findViewById(R.id.tv_monto_total);
        }

        public void displayData(int i) {
            factura = listaFacturas.get(i);
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            String fechaFactura = df.format(factura.getFecha());
            DecimalFormat dfTotal = new DecimalFormat("#.00");

            tvNumeroFactura.setText(factura.getNumeroFactura());
            if(factura.getReferencia()!=null) tvReferencia.setText(factura.getReferencia().getRazonSocial());
            tvFecha.setText(fechaFactura);
            tvEstado.setText(factura.getEstado());
            tvMonto.setText(dfTotal.format(factura.getMontoTotal()));
        }
    }
}
