package com.ndp.ui.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Pendientes;

import static com.ndp.utils.use.useAPP.getDescripcionPorTipo;


public class PendingAdapter extends RecyclerView.Adapter<PendingAdapter.ViewHolder> implements Filterable {

    private List<Pendientes> lstPendientes;
    private List<Pendientes> lstPendientesFiltered;
    private Integer CODE_ACTIVITY;
    private Activity activity;
    private Boolean isTrueAction;
    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private Listener callback;

    public interface Listener {
        void onSyncPending(int position);
    }

    public PendingAdapter(List<Pendientes> lstPendientes, Integer CODE_ACTIVITY, Boolean isTrueAction, Activity activity, Listener callback) {
        this.lstPendientes = lstPendientes;
        this.lstPendientesFiltered = lstPendientes;
        this.CODE_ACTIVITY = CODE_ACTIVITY;
        this.isTrueAction = isTrueAction;
        this.activity = activity;
        this.callback = callback;
    }

    //region Adapter Methods

    //region Evitan que los elementos se repitan
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    //endregion

    @Override
    public int getItemCount() {
        return lstPendientes != null ? lstPendientes.size() : 0;
    }

    @Override
    @NonNull
    public PendingAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_pending, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") int i) {

        if (lstPendientes != null && lstPendientes.size() > 0) {

            viewHolder.tv_tipo.setText(getDescripcionPorTipo(lstPendientes.get(i).getTypeObject()));
            viewHolder.tv_descripcion.setText(lstPendientes.get(i).getDescripcionObject());
            viewHolder.tv_fecha_registro.setText(dateFormat.format(lstPendientes.get(i).getFechaRegistro()));

            switch (lstPendientes.get(i).getEstadoMigracion()) {
                case 1:
                    viewHolder.rlyt_pending.setBackgroundColor(activity.getResources().getColor(R.color.grey_90));
                    viewHolder.img_sync.setBackgroundResource(R.drawable.ic_sync);
                    break;
                case 2:
                    viewHolder.rlyt_pending.setBackgroundColor(activity.getResources().getColor(R.color.grey_90));
                    viewHolder.img_sync.setBackgroundResource(R.drawable.ic_check);
                    break;
                case 3:
                    viewHolder.rlyt_pending.setBackgroundColor(activity.getResources().getColor(R.color.use_Red_low));
                    viewHolder.img_sync.setBackgroundResource(R.drawable.ic_error);
                    break;
            }

            viewHolder.img_sync.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null) callback.onSyncPending(i);
                }
            });

        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void insertPending(int position, Pendientes data) {
        lstPendientes.add(position, data);
        notifyItemInserted(position);
    }

    public void removePending(Pendientes data) {
        int position = lstPendientes.indexOf(data);
        lstPendientes.remove(position);
        notifyItemRemoved(position);
    }

    public void setPending(Pendientes data) {
        int position = lstPendientes.indexOf(data);
        lstPendientes.set(position,data);
        notifyDataSetChanged();
    }

    public Pendientes getPendiente(int position){
        return this.lstPendientes.get(position);
    }

    public List<Pendientes> getListPendientes(){
        return this.lstPendientes;
    }

    public List<Pendientes> getListPendientesAll(){
        return this.lstPendientesFiltered;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    lstPendientes = lstPendientesFiltered;

                } else {

                    Date Inicio;
                    Date Fin;

                    String[] parts = charString.split("::");
                    String part1 = parts[0]; //obtiene: Tipo
                    String part2 = parts[1]; //obtiene: Estado
                    String part3 = parts[2]; //obtiene: Fecha Inicio
                    String part4 = parts[3]; //obtiene: Fecha Fin

                    int tipo = Integer.parseInt(part1);
                    int estado = Integer.parseInt(part2);

                    try {
                        Inicio = dateFormat.parse(part3);
                        Fin  = dateFormat.parse(part4);
                    } catch (Exception e) {
                        Inicio = new Date();
                        Fin = new Date();
                    }

                    List<Pendientes> filteredList = new ArrayList<>();
                    for (Pendientes rowFilter : lstPendientesFiltered) {

                        if (tipo == 5) {
                            if (rowFilter.getEstadoMigracion() == estado
                                    && Inicio.compareTo(rowFilter.getFechaRegistro()) <=0
                                    && (Fin.compareTo(rowFilter.getFechaRegistro() ) >= 0 || Fin.compareTo(rowFilter.getFechaRegistro()) <= 0 ) ) {
                                filteredList.add(rowFilter);
                            }
                        } else {
                            if (rowFilter.getTypeObject() == tipo
                                    && rowFilter.getEstadoMigracion() == estado
                                    && Inicio.compareTo(rowFilter.getFechaRegistro()) <= 0
                                    && (Fin.compareTo(rowFilter.getFechaRegistro()) >= 0 || Fin.compareTo(rowFilter.getFechaRegistro()) <= 0 ) ) {
                                filteredList.add(rowFilter);
                            }
                        }

                    }

                    lstPendientes = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = lstPendientes;
                filterResults.count = lstPendientes.size();
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                lstPendientes = (List<Pendientes>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
    //endregion

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout rlyt_pending;
        private ImageView img_sync;
        private TextView tv_tipo;
        private TextView tv_descripcion;
        private TextView tv_fecha_registro;

        ViewHolder(View itemView) {
            super(itemView);
            rlyt_pending = itemView.findViewById(R.id.rlyt_pending);
            img_sync = itemView.findViewById(R.id.img_sync);
            tv_tipo = itemView.findViewById(R.id.tv_tipo);
            tv_descripcion = itemView.findViewById(R.id.tv_descripcion);
            tv_fecha_registro = itemView.findViewById(R.id.tv_fecha_registro);
        }
    }
}