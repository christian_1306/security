package com.ndp.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Factura;

public class AccountStatusSellerAdapter extends RecyclerView.Adapter<AccountStatusSellerAdapter.ViewHolder>{
    private List<Factura> listaFacturas = new ArrayList<>();

    public AccountStatusSellerAdapter(){
        listaFacturas = Factura.listAll(Factura.class);
    }

    public AccountStatusSellerAdapter(List<Factura> facturas){
        this.listaFacturas = facturas;
    }
    @NonNull
    @Override
    public AccountStatusSellerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_item_account_state_seller, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AccountStatusSellerAdapter.ViewHolder viewHolder, int i) {
        viewHolder.displayData(i);
    }

    @Override
    public int getItemCount() {
        return listaFacturas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_numero_factura,tv_cliente,tv_referencia,tv_fecha;
        private TextView tv_estado,tv_monto;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            setElements();
        }

        private void setElements(){
            tv_numero_factura = itemView.findViewById(R.id.tv_texto_numero_factura);
            tv_cliente = itemView.findViewById(R.id.tv_nombre_cliente);
            tv_referencia = itemView.findViewById(R.id.tv_nombre_referencia);
            tv_fecha = itemView.findViewById(R.id.tv_numero_fecha);
            tv_estado = itemView.findViewById(R.id.tv_nombre_estado);
            tv_monto = itemView.findViewById(R.id.tv_monto_total);
        }

        private void displayData(int i){
            Factura factura = listaFacturas.get(i);
            Date fecha = factura.getFecha();
            String fecha_string = null;
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            try{
                fecha_string = dateFormat.format(fecha);
            }
            catch(Exception ex){
            }
            tv_numero_factura.setText(factura.getNumeroFactura());
            tv_cliente.setText(factura.getCliente().getRazonSocial());
            if(factura.getReferencia()!=null) tv_referencia.setText(factura.getReferencia().getRazonSocial());
            tv_fecha.setText(fecha_string);
            tv_estado.setText(factura.getEstado());
            tv_monto.setText(String.valueOf(factura.getMontoTotal()));
        }
    }
}
