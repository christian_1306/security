package com.ndp.ui.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.plugins.annotation.Line;
import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.User;
import com.ndp.ui.activities.CurrentArrivalActivity;
import com.ndp.ui.activities.OrderActivity;
import com.ndp.ui.activities.OrderClientActivity;
import com.ndp.ui.activities.RecyclerViewDirectionsActivity;
import com.ndp.ui.activities.RouteNavActivity;
import com.ndp.ui.activities.VisitActivity;
import com.ndp.utils.Useful;

import java.lang.ref.WeakReference;
import java.util.List;

import static android.support.v4.content.ContextCompat.startActivity;
import static com.ndp.utils.Const.CODE_HOME;

public class LocationAdapter extends
        RecyclerView.Adapter<LocationAdapter.ViewHolder> {

    private List<Cliente> locationList;
    private MapboxMap map;
    private WeakReference<RouteNavActivity> weakReference;
    private Activity _self;
    private User currentUser;


    public LocationAdapter(RouteNavActivity activity,
                           List<Cliente> locationList,
                           MapboxMap mapBoxMap,
                           User currentUser){
        this.locationList = locationList;
        this.map = mapBoxMap;
        this.weakReference = new WeakReference<>(activity);
        this._self = activity;
        this.currentUser = currentUser;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.rv_directions_card, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        Cliente cliente = locationList.get(i);
        holder.name.setText(cliente.getRazonSocial());
        holder.numOfAvailableTables.setText(cliente.getCardCode());

        holder.setClickListener(new RouteNavActivity.ItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                weakReference.get()
                        .onMapClick(cliente.getCoordinates());
            }
        });

        holder.btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(_self, OrderActivity.class);
                intent.putExtra(_self.getResources().getString(R.string.objUser), currentUser);
                intent.putExtra(_self.getResources().getString(R.string.valueActivity), String.valueOf(CODE_HOME));
                intent.putExtra(_self.getResources().getString(R.string.changeCardName), true);
                _self.startActivity(intent);
            }
        });

        holder.btnVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(_self, CurrentArrivalActivity.class);
                intent.putExtra(_self.getResources().getString(R.string.objUser), currentUser);
                _self.startActivity(intent);
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    //endregion

    @Override
    public int getItemCount() {
        return locationList != null ? locationList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView name;
        ImageView btnOrder;
        ImageView btnVisit;
        TextView numOfAvailableTables;
        CardView singleCard;
        RouteNavActivity.ItemClickListener clickListener;
        LinearLayout lyt_card_customer;
        ViewHolder(View view) {
            super(view);
            btnOrder = view.findViewById(R.id.btnOrder);
            btnVisit = view.findViewById(R.id.btnVisit);
            name = view.findViewById(R.id.location_title_tv);
            numOfAvailableTables = view.findViewById(R.id.location_num_of_beds_tv);
            lyt_card_customer = view.findViewById(R.id.lyt_card_customer);
            singleCard = view.findViewById(R.id.single_location_cardview);
            lyt_card_customer.setOnClickListener(this);
        }

        public void setClickListener(RouteNavActivity.ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getLayoutPosition());
        }
    }
}
