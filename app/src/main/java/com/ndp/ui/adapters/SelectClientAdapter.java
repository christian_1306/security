package com.ndp.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.ui.fragments.OrderDetailFragment;

public class SelectClientAdapter extends RecyclerView.Adapter<SelectClientAdapter.ViewHolder> {
    private List<Cliente> allClients;
    private ArrayList<Cliente> selectedClients;
    private OrderDetailFragment orderDetailFragment;

    private boolean esParaPedido;

    private Cliente clienteSeleccionadoPedido;

    public SelectClientAdapter(List<Cliente> allClients, ArrayList<Cliente> selectedClients, boolean esParaPedido) {
        this.allClients = allClients;
        if(selectedClients != null)
            this.selectedClients = new ArrayList<>(selectedClients);
        this.esParaPedido = esParaPedido;
        this.clienteSeleccionadoPedido = null;
        this.orderDetailFragment = null;
    }

    public List<Cliente> getAllClients() {
        return allClients;
    }

    public void setAllClients(List<Cliente> allClients) {
        this.allClients = allClients;
    }

    public ArrayList<Cliente> getSelectedClients() {
        return selectedClients;
    }

    public void setSelectedClients(ArrayList<Cliente> selectedClients) {
        this.selectedClients = selectedClients;
    }

    public Cliente getClienteSeleccionadoPedido() {
        return clienteSeleccionadoPedido;
    }

    public void setOrderDetailFragment(OrderDetailFragment orderDetailFragment) {
        this.orderDetailFragment = orderDetailFragment;
    }

    @NonNull
    @Override
    public SelectClientAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_select_client, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectClientAdapter.ViewHolder viewHolder, int i) {
        viewHolder.tvIndice.setText(String.valueOf(i+1));
        viewHolder.tvNombre.setText(allClients.get(i).getRazonSocial());
        if(selectedClients == null) return;
        Cliente c = allClients.get(i);
        if(selectedClients.contains(c))
            viewHolder.chbSeleccionado.setChecked(true);
        else
            viewHolder.chbSeleccionado.setChecked(false);
    }

    @Override
    public int getItemCount() {
        return allClients.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
        private TextView tvIndice;
        private TextView tvNombre;
        private CheckBox chbSeleccionado;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            setElements();
            itemView.setOnClickListener(this);
            chbSeleccionado.setOnCheckedChangeListener(this);
        }

        private void setElements() {
            tvIndice = itemView.findViewById(R.id.tv_cliente_seleccionado_indice);
            tvNombre = itemView.findViewById(R.id.tv_cliente_seleccionado_nombre);
            chbSeleccionado = itemView.findViewById(R.id.chb_cliente_seleccionado);

            if(esParaPedido)
                chbSeleccionado.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View v) {
            if(esParaPedido) {
                clienteSeleccionadoPedido = allClients.get(getPosition());
                orderDetailFragment.mostarClienteSecundario();
            }
            else {
                boolean seleccionado = chbSeleccionado.isChecked();
                chbSeleccionado.setChecked(!seleccionado);
//                if(v.getId() == chbSeleccionado.getId())
//                    seleccionado = chbSeleccionado.isChecked();
//                else{
//                    seleccionado = !chbSeleccionado.isChecked();
//                    chbSeleccionado.setChecked(seleccionado);
//                }
//                Cliente cliente = allClients.get(getPosition());
//                if(seleccionado) {
//                    if(selectedClients.contains(cliente)) return;
//                    selectedClients.add(cliente);
//                } else {
//                    selectedClients.remove(cliente);
//                }
            }
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Cliente cliente = allClients.get(getPosition());
            if(isChecked) {
                if(selectedClients.contains(cliente)) return;
                selectedClients.add(cliente);
            }
            else {
                if(!selectedClients.contains(cliente)) return;
                selectedClients.remove(cliente);
            }
        }
    }
}
