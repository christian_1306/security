package com.ndp.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ndp.models.model.Cliente;
import com.ndp.models.model.Vendedor;
import com.ndp.ui.fragments.OrderDetailFragment;
import com.ndp.ui.fragments.OrderItemFragment;
import com.ndp.ui.fragments.OrderSummaryFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    private Cliente cliente;
    private Vendedor vendedor;
    private OrderItemFragment orderItemFragment;
    private OrderDetailFragment orderDetailFragment;
    private OrderSummaryFragment orderSummaryFragment;

//    private PlaceholderFragment placeholderFragment;


    public PagerAdapter(FragmentManager fm, int mNumOfTabs){
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
    }

    public PagerAdapter(FragmentManager fm, int mNumOfTabs, Cliente cliente, Vendedor vendedor,boolean isForClient) {
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
        this.cliente = cliente;
        this.vendedor = vendedor;

        orderItemFragment = new OrderItemFragment();
        orderItemFragment.setItemForOrder(true);
        orderItemFragment.setVendedor(vendedor);
        orderItemFragment.setCliente(this.cliente);

        orderDetailFragment = new OrderDetailFragment();
        orderDetailFragment.setCliente(this.cliente);
        orderDetailFragment.setForClient(isForClient);

        orderSummaryFragment = new OrderSummaryFragment();

        orderItemFragment.setOrderDetailFragment(orderDetailFragment);
        orderSummaryFragment.setOrderItemFragment(orderItemFragment);
        orderSummaryFragment.setOrderDetailFragment(orderDetailFragment);
//        placeholderFragment.orderItemFragment = orderItemFragment;
//        placeholderFragment.orderDetailFragment = orderDetailFragment;
//        placeholderFragment.orderSummaryFragment = orderSummaryFragment;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    @Override
    public Fragment getItem(int position){
        switch (position) {
            case 0:
                return orderItemFragment;
            case 1:
                return orderDetailFragment;
            case 2:
                return orderSummaryFragment;
            default:
                return  null;
        }
//        return placeholderFragment.newInstance(position+1);
    }

    @Override
    public int getCount(){
        return mNumOfTabs;
    }
//
//    public static class PlaceholderFragment extends Fragment {
//
//        public static Cliente cliente;
//        public static Vendedor vendedor;
//        public static OrderItemFragment orderItemFragment;
//        public static OrderDetailFragment orderDetailFragment;
//        public static OrderSummaryFragment orderSummaryFragment;
//
//        public PlaceholderFragment() {
//        }
//
//        /**
//         * Returns a new instance of this fragment for the given section
//         * number.
//         */
//        public static Fragment newInstance(int sectionNumber) {
//            Fragment fragment = null;
//
//            switch (sectionNumber) {
//                case 1:
//                    fragment = orderItemFragment;
//                    break;
//                case 2:
//                    orderDetailFragment.setListaDetalles(orderItemFragment.getListaDetalles());
//                    fragment =orderDetailFragment;
//                    break;
//                case 3:
//                    fragment = orderSummaryFragment;
//                    break;
//            }
//
//            return fragment;
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            View rootView = inflater.inflate(R.layout.fragment_order_content, container, false);
////            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
////            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
//            return rootView;
//        }
//    }
}