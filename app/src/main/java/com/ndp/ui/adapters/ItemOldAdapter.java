package com.ndp.ui.adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Almacen;
import com.ndp.models.model.Articulo;
import com.ndp.models.model.ArticuloXAlmacen;
import com.ndp.models.model.DetallePedido;
import com.ndp.models.model.TarifarioXArticulo;
import com.ndp.ui.fragments.OrderItemFragment;

public class ItemOldAdapter extends RecyclerView.Adapter<ItemOldAdapter.ViewHolder>{

    private OrderItemFragment orderItemFragment;
    private boolean isItemForOrder;
    private String query;
    private boolean esDetalles = false;

    public boolean isEsDetalles() {
        return esDetalles;
    }

    public void setEsDetalles(boolean esDetalles) {
        this.esDetalles = esDetalles;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    private boolean isForSeller = false;
    private long id_tarifario;
    private boolean isForView = false;
    private List<TarifarioXArticulo> tarifarios;
    private List<Articulo> listaArticulos;
    private ArrayList<DetallePedido> listaDetalles;
    private ArrayList<Articulo> listaArticulosSeleccionados;

    private Almacen almacen;

    public void setId_tarifario(long id_tarifario) {
        this.id_tarifario = id_tarifario;
    }

    public ArrayList<DetallePedido> getListaDetalles() {
        return listaDetalles;
    }

    public void setForSeller(boolean forSeller) {
        isForSeller = forSeller;
    }

    public void setListaDetalles(ArrayList<DetallePedido> listaDetalles) {
        this.listaDetalles = listaDetalles;
    }

    public ItemOldAdapter(List<Articulo> listaArticulos, OrderItemFragment orderItemFragment, boolean isItemForOrder,
                          Almacen almacen) {
        this.listaArticulos = listaArticulos;
        this.orderItemFragment = orderItemFragment;
        this.isItemForOrder = isItemForOrder;
        this.listaDetalles = new ArrayList<>();
        this.listaArticulosSeleccionados = new ArrayList<>();
        this.almacen = almacen;
    }

    public ItemOldAdapter(List<DetallePedido> detallePedidos, boolean isItemForOrder, Almacen almacen){
        this.listaDetalles = new ArrayList<>(detallePedidos);
        this.isItemForOrder = isItemForOrder;
        this.almacen = almacen;
    }

//    //ESTE ES PARA MOSTRAR DE LA CONSULTA
//    public ItemOldAdapter(OrderItemFragment orderItemFragment,boolean isItemForOrder, List<DetallePedido> detallePedidos){
//        this.orderItemFragment = orderItemFragment;
//        this.isItemForOrder = isItemForOrder;
//        this.listaDetalles = new ArrayList<>(detallePedidos);
//        this.listaArticulosSeleccionados = new ArrayList<>();
//        this.listaArticulos = new ArrayList<>();
//        for(DetallePedido dp : detallePedidos) this.listaArticulos.add(dp.getArticulo());
//        this.isForView = true;
//    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_item_itemold, viewGroup, false);
        cargarPrecios();
        return new ViewHolder(view/*, orderItemFragment, listaArticulos, isItemForOrder*/);
    }

    private void cargarPrecios(){
        TarifarioXArticulo tarifarioXArticulo = new TarifarioXArticulo();
        //tarifarios = tarifarioXArticulo.listarPorTarifario(id_tarifario);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.displayData(i);
    }

    @Override
    public int getItemCount(){
        if (esDetalles) return listaDetalles.size();
        else return listaArticulos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener/*, TextWatcher */{
        RelativeLayout rlNombreArticulo;
        TextView tvIndiceArticulo;
        TextView tvNombreArticulo;
        TextView tvEtiquetaStock;
        TextView tvStock;
        TextView tvMoneda;
        TextView tvPrecio;
        TextView tvEtiquetaImpuesto;
        TextView tvImpuesto;
        TextView tvEtiquetaTotal;
        TextView tvTotal;
        TextView tvEtiquetaUnidad;
        TextView tvUnidad;
        TextView tvEtiquetaConversion;
        TextView tvCantidadConversion;
        TextView tvUnidadConversion;
        ImageView btnMenos;
        EditText etCantidad;
        ImageView btnMas;

        //        List<Articulo> listaArticulos;
//        OrderItemFragment orderItemFragment;
        Articulo articuloSeleccionado = null;

//        private boolean isItemForOrder;

        public ViewHolder(@NonNull View itemView/*, OrderItemFragment orderItemFragment,List<Articulo> listaArticulos,
                          boolean isItemForOrder*/) {
            super(itemView);
//            this.listaArticulos = listaArticulos;
//            this.orderItemFragment = orderItemFragment;
//            this.isItemForOrder = isItemForOrder;
            itemView.setOnClickListener(this);
            setElements();
            blockElements();
        }

        private void setElements() {
            rlNombreArticulo = itemView.findViewById(R.id.rl_item_name);
            tvIndiceArticulo = itemView.findViewById(R.id.tv_item_indice);
            tvNombreArticulo = itemView.findViewById(R.id.tv_item_name);
            tvEtiquetaStock = itemView.findViewById(R.id.tv_etiqueta_stock);
            tvStock = itemView.findViewById(R.id.tv_stock_item);
            tvMoneda = itemView.findViewById(R.id.tv_etiqueta_moneda);
            tvPrecio = itemView.findViewById(R.id.tv_item_price);
//            tvEtiquetaImpuesto = itemView.findViewById(R.id.tv_etiqueta_impuesto);
//            tvImpuesto = itemView.findViewById(R.id.tv_item_impuesto);
            tvEtiquetaTotal = itemView.findViewById(R.id.tv_etiqueta_total);
            tvTotal = itemView.findViewById(R.id.tv_item_total);
            tvEtiquetaUnidad = itemView.findViewById(R.id.tv_etiqueta_unidad);
            tvUnidad = itemView.findViewById(R.id.tv_item_unidad);
            tvEtiquetaConversion = itemView.findViewById(R.id.tv_item_etiqueta_conversion);
            tvCantidadConversion = itemView.findViewById(R.id.tv_item_cantidad_conversion);
//            tvUnidadConversion = itemView.findViewById(R.id.tv_item_unidad_conversion);
            //btnMenos = itemView.findViewById(R.id.iv_item_menos);
            etCantidad = itemView.findViewById(R.id.et_item_cant);
//            if(isForSeller && !isForView)
//                etCantidad.setInputType(InputType.TYPE_CLASS_NUMBER);
            //btnMas = itemView.findViewById(R.id.iv_item_mas);
//            btnMenos.setOnClickListener(this);
//            btnMas.setOnClickListener(this);
//            etCantidad.addTextChangedListener(this);

//            setLabelsConversion();
        }

//        private void setLabelsConversion() {
//            if(isForSeller && !isForView) return;
//            tvEtiquetaConversion.setText("Cantidad: ");
////            tvUnidadConversion.setText(" Varillas");
//            tvUnidad.setText("Toneladas");
//        }

        private void blockElements() {
            if(!isForSeller){
                tvEtiquetaStock.setVisibility(View.GONE);
                tvStock.setVisibility(View.GONE);
            }
            if(isItemForOrder) return;
//            if(!isForView) etCantidad.setVisibility(View.GONE);
            //btnMenos.setVisibility(View.INVISIBLE);
            etCantidad.setVisibility(View.INVISIBLE);
            //btnMas.setVisibility(View.INVISIBLE);
            tvStock.setVisibility(View.INVISIBLE);
            tvEtiquetaTotal.setVisibility(View.GONE);
            tvTotal.setVisibility(View.GONE);
            tvEtiquetaStock.setVisibility(View.INVISIBLE);
            tvEtiquetaConversion.setVisibility(View.INVISIBLE);
            //            tvUnidadConversion.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View v) {
            int cantidadVendedor = 0;
            double cantidadCliente = 0;
            String cadCantidad = etCantidad.getText().toString();
            if(isForSeller && !cadCantidad.isEmpty()) cantidadVendedor = Integer.parseInt(cadCantidad);
            else if (!isForSeller && !cadCantidad.isEmpty()) cantidadCliente = Double.parseDouble(cadCantidad);
            switch (v.getId()) {
                case R.id.iv_item_menos:
                    if(isForSeller) {
                        if(cantidadVendedor == 0) return;
                        etCantidad.setText(String.valueOf(cantidadVendedor));
                    }
                    else {
                        if(cantidadCliente == 0.0) return;
                        --cantidadCliente;
                        etCantidad.setText(String.valueOf(cantidadCliente));
                    }
                    return;
                case R.id.iv_item_mas:
                    if(isForSeller) {
                        ++cantidadVendedor;
                        etCantidad.setText(String.valueOf(cantidadVendedor));
                    }
                    else {
                        ++cantidadCliente;
                        etCantidad.setText(String.valueOf(cantidadCliente));
                    }
                    return;
            }
            int i=getPosition();
            ArticuloXAlmacen articuloXAlmacen = new ArticuloXAlmacen();
            ArticuloXAlmacen articuloAlmacen = articuloXAlmacen.getArticuloAlmacen(listaArticulos.get(i).getId(),
                    almacen.getId()).get(0);
//            List<ArticuloXAlmacen> articuloXAlmacenes = articuloXAlmacen.obtenerArticulosXAlmacen(listaArticulos.get(i).getId());
            if(!isItemForOrder) orderItemFragment.openDetailItem(articuloAlmacen/*,articuloXAlmacenes.get(1)*/);
        }

        public void displayData(int i) {
            DetallePedido detallePedido = null;
            if(isForView || esDetalles) detallePedido = listaDetalles.get(i);

            //SETEAR INDICE Y NOMBRE DEL ARTICULO
            if(esDetalles){
                tvIndiceArticulo.setText(String.valueOf(i+1));
                tvNombreArticulo.setText(detallePedido.getTarifarioXArticulo().getArticulo().getNombre());
                etCantidad.setText(String.valueOf(detallePedido.getPesoTotal()));
                DecimalFormat df = new DecimalFormat("#.00");
                tvTotal.setText(df.format(detallePedido.getSubtotalFinal()));
                tvCantidadConversion.setText(String.valueOf(detallePedido.getCantidad()));
                ArticuloXAlmacen articuloXAlmacen_2 = null;
                ArticuloXAlmacen articuloXAlmacen = articuloXAlmacen_2.find(ArticuloXAlmacen.class,"articulo = ? and almacen = ?",
                        String.valueOf(detallePedido.getTarifarioXArticulo().getArticulo().getId()),
                        String.valueOf(almacen.getId())).get(0);
                int stock = 0;
                stock = articuloXAlmacen.getStock();
                tvStock.setText(String.valueOf(stock));
                articuloSeleccionado = listaDetalles.get(i).getTarifarioXArticulo().getArticulo();
            }
            else {
                articuloSeleccionado = listaArticulos.get(i);
                tvIndiceArticulo.setText(String.valueOf(i + 1));
                tvNombreArticulo.setText(articuloSeleccionado.getNombre());

                //SETEAR STOCK
                ArticuloXAlmacen articuloXAlmacen = new ArticuloXAlmacen();
                List<ArticuloXAlmacen> articuloXAlmacenes = articuloXAlmacen.obtenerArticulosXAlmacen(listaArticulos.get(i).getId());
                int stock = 0;
                for (ArticuloXAlmacen aa : articuloXAlmacenes) {
                    stock += aa.getStock();
                }
                tvStock.setText(String.valueOf(stock));
                resaltarSinStock();
            }
//            for (TarifarioXArticulo t : tarifarios){
//                if(t.getArticulo().getId_articulo() == listaArticulos.get(i).getId_articulo()){
//                    tvPrecio.setText(String.valueOf(t.getPrecio()));
//                }
//            }

            displayPrices();

//            //ACTUALIZA LA CANTIDAD SI ES QUE YA HAY UN REGISTRO EN LOS DETALLES DEL PEDIDO
//            int index = listaArticulosSeleccionados.indexOf(listaArticulos.get(i));
//            if(index == -1 && !isForView) return;
//
//            if (!isForView) {
//                if(isForSeller) {
//                    etCantidad.setText(String.valueOf(listaDetalles.get(index).getCantidad()));
//                    tvCantidadConversion.setText(String.valueOf(listaDetalles.get(index).getPesoTotal()));
//                }
//                else if(!isForSeller){
//                    etCantidad.setText(String.valueOf(listaDetalles.get(index).getPesoTotal()));
//                    tvCantidadConversion.setText(String.valueOf(listaDetalles.get(index).getCantidad()));
//                }
//            }
//            else {
//                etCantidad.setText(String.valueOf(detallePedido.getCantidad()));
//                etCantidad.setEnabled(false);
//            }


//            for(DetallePedido d: listaDetalles) {
//                if(d.getArticulo().getId_articulo() == listaArticulos.get(i).getId_articulo()) {
//                    etCantidad.setText(String.valueOf(d.getCantidad()));
//                    break;
//                }
//            }
        }

        private void displayPrices() {
            //EL CODIGO DEBAJO BUSCA EL PRECIO DEL ARTICULO CORRESPONDIENTE
//            TarifarioXArticulo tarifarioXArticulo = TarifarioXArticulo.find(TarifarioXArticulo.class,
//                    "articulo = ?", String.valueOf(listaArticulos.get(i).getId())).get(0);
//            tvPrecio.setText(String.valueOf(tarifarioXArticulo.getPrecio()));
            List<TarifarioXArticulo> tarifarioXArticulo = TarifarioXArticulo.find(TarifarioXArticulo.class,
                    "articulo = ? and tarifario = ?", String.valueOf(articuloSeleccionado.getId()),
                    String.valueOf(id_tarifario));
            DecimalFormat df = new DecimalFormat("#.00");
            if(tarifarioXArticulo.size() != 0) {
                tvPrecio.setText(df.format(tarifarioXArticulo.get(0).getPrecioDefaultFinal()));
//                tvImpuesto.setText(df.format(tarifarioXArticulo.get(0).getImpuestoArticulo()));
            }
            else {
                tvPrecio.setText("0");
//                tvImpuesto.setText("0");
            }
        }

        private void resaltarSinStock() {
            if(tvStock.getText().toString().equals("0"))
                rlNombreArticulo.setBackgroundColor(Color.parseColor("#FFCBAE"));
            else
                rlNombreArticulo.setBackgroundColor(Color.parseColor("#cecdcd"));
        }

//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//        }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            int index = listaArticulosSeleccionados.indexOf(articuloSeleccionado);
//            double cantidad = 0;
//            articuloSeleccionado = listaArticulos.get(getPosition());
//
//            if (etCantidad.getText().toString().isEmpty() ||
//                    (Double.parseDouble(etCantidad.getText().toString())) == 0) { //ELIMINAR ARTICULO SI SE ENCUENTRA REGISTADO
//                if(index != -1) {
//                    listaDetalles.remove(index);
//                    listaArticulosSeleccionados.remove(articuloSeleccionado);
//                    tvCantidadConversion.setText("");
//                }
//            }
//            else {
//                List<TarifarioXArticulo> tarifarioXArticulo = Tarifario.find(TarifarioXArticulo.class,
//                        "articulo = ? and tarifario = ?", String.valueOf(articuloSeleccionado.getId()),
//                        String.valueOf(id_tarifario));
//                cantidad = Double.parseDouble(etCantidad.getText().toString());
//                if(index == -1) {
//                    addNewDetallePedido(cantidad, tarifarioXArticulo);
//                }else {
//                    actualizarDetallePedido(index, cantidad, tarifarioXArticulo);
//                }
//            }
//            orderItemFragment.getOrderDetailFragment().ActualizarTotales(listaDetalles);
//        }
//
//        private void addNewDetallePedido(double cantidad, List<TarifarioXArticulo> tarifarioXArticulo) {
//            double subtotalNeto = tarifarioXArticulo.get(0).getPrecioDefaultNeto()*cantidad;
//            double impuestoArticulo = tarifarioXArticulo.get(0).getImpuestoArticulo() * cantidad;
//            double subtotalFinal = tarifarioXArticulo.get(0).getPrecioDefaultFinal() * cantidad;
//            DecimalFormat df = new DecimalFormat("#.00");
//
//            DetallePedido newDetallePedido = new DetallePedido();
//            newDetallePedido.setArticulo(articuloSeleccionado);
//            newDetallePedido.setIsActive(1);
//            newDetallePedido.setMoneda(null);
//            if(isForSeller) { //Es para el vendedor
//                double pesoTotal = articuloSeleccionado.getPeso()*cantidad;
//                newDetallePedido.setCantidad(((int)cantidad));
//                newDetallePedido.setPesoTotal(pesoTotal);
//                tvCantidadConversion.setText(df.format(pesoTotal));
//            }
//            else { //Es para el cliente
//                int cantidadVarillas = ((int)Math.ceil(cantidad / articuloSeleccionado.getPeso()));
//                newDetallePedido.setPesoTotal(cantidad);
//                newDetallePedido.setCantidad(cantidadVarillas);
//                tvCantidadConversion.setText(String.valueOf(cantidadVarillas));
//                subtotalNeto = tarifarioXArticulo.get(0).getPrecioDefaultNeto() * cantidadVarillas;
//                impuestoArticulo = tarifarioXArticulo.get(0).getImpuestoArticulo() * cantidadVarillas;
//                subtotalFinal = tarifarioXArticulo.get(0).getPrecioDefaultFinal() * cantidadVarillas;
//            }
//            newDetallePedido.setSubtotalNeto(subtotalNeto);
//            newDetallePedido.setImpuestoDetallePedido(impuestoArticulo);
//            newDetallePedido.setSubtotalFinal(subtotalFinal);
////                    newDetallePedido.setTarifario(.);
//            listaDetalles.add(newDetallePedido);
//            listaArticulosSeleccionados.add(articuloSeleccionado);
//            actualizarMontos(newDetallePedido);
//        }
//
//        private void actualizarDetallePedido(int index, double cantidad, List<TarifarioXArticulo> tarifarioXArticulo) {
//            double subtotalNeto = tarifarioXArticulo.get(0).getPrecioDefaultNeto()*cantidad;
//            double impuestoArticulo = tarifarioXArticulo.get(0).getImpuestoArticulo() * cantidad;
//            double subtotalFinal = tarifarioXArticulo.get(0).getPrecioDefaultFinal() * cantidad;
//            DecimalFormat df = new DecimalFormat("#.00");
//
//            if(isForSeller) {
//                double pesoTotal = articuloSeleccionado.getPeso()*cantidad;
//                listaDetalles.get(index).setCantidad(((int)cantidad));
//                listaDetalles.get(index).setPesoTotal(pesoTotal);
//                tvCantidadConversion.setText(df.format(pesoTotal));
//            } else {
//                int cantidadVarillas = ((int)Math.ceil(cantidad / articuloSeleccionado.getPeso()));
//                listaDetalles.get(index).setCantidad(cantidadVarillas);
//                listaDetalles.get(index).setPesoTotal(cantidad);
//                tvCantidadConversion.setText(String.valueOf(cantidadVarillas));
//                subtotalNeto = tarifarioXArticulo.get(0).getPrecioDefaultNeto() * cantidadVarillas;
//                impuestoArticulo = tarifarioXArticulo.get(0).getImpuestoArticulo() * cantidadVarillas;
//                subtotalFinal = tarifarioXArticulo.get(0).getPrecioDefaultFinal() * cantidadVarillas;
//            }
//            listaDetalles.get(index).setSubtotalNeto(subtotalNeto);
//            listaDetalles.get(index).setImpuestoDetallePedido(impuestoArticulo);
//            listaDetalles.get(index).setSubtotalFinal(subtotalFinal);
//            actualizarMontos(listaDetalles.get(index));
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//
//        }
//
//        private void actualizarMontos(DetallePedido detallePedido) {
//            DecimalFormat df = new DecimalFormat("#.00");
//            tvTotal.setText(df.format(detallePedido.getSubtotalNeto()));
//        }
    }
}
