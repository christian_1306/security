package com.ndp.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Visita;

public class VisitsMadeAdapter extends RecyclerView.Adapter<VisitsMadeAdapter.ViewHolder>{
    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_razon_social,tv_fecha,tv_motivo,tv_estado;
        private List<Visita> visitas;
        private boolean isPending;

        public boolean isPending() {
            return isPending;
        }

        public void setPending(boolean pending) {
            isPending = pending;
        }

        public ViewHolder(View itemView, List<Visita> visitas,boolean isPending){
            super(itemView);
            this.visitas = visitas;
            this.isPending = isPending;
            setElements(itemView);
            blockElements();
        }

        private void blockElements(){
            if(this.isPending) tv_estado.setVisibility(View.GONE);
        }

        private void setElements(View itemView) {
            tv_razon_social = itemView.findViewById(R.id.tv_razon_social);
            tv_fecha = itemView.findViewById(R.id.tv_visita_fecha);
            tv_motivo = itemView.findViewById(R.id.tv_visita_motivo);
            tv_estado = itemView.findViewById(R.id.tv_visita_estado);
        }
    }

    public List<Visita> visitas;
    private Fragment fragment;
    private boolean isPending;

    public boolean isPending() {
        return isPending;
    }

    public void setPending(boolean pending) {
        isPending = pending;
    }

    public VisitsMadeAdapter(List<Visita> visitas) {
        this.visitas = visitas;
    }

    public VisitsMadeAdapter(List<Visita> visitas,Fragment fragment,boolean isPending){
        this.visitas = visitas;
        this.fragment = fragment;
        this.isPending = isPending;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i){
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_item_visit,viewGroup,false);
        ViewHolder viewHolder = new ViewHolder(view, visitas,this.isPending);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.tv_razon_social.setText(visitas.get(i).getCliente().getRazonSocial());
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String fechaEmision = dateFormat.format(visitas.get(i).getFechaVisita());
        viewHolder.tv_fecha.setText(fechaEmision);
        viewHolder.tv_motivo.setText(visitas.get(i).getMotivo());
    }

    @Override
    public int getItemCount(){return visitas.size();}
}
