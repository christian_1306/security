package com.ndp.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.ndp.R;
import com.ndp.models.model.Cliente;

public class MainClientAdapter extends RecyclerView.Adapter<MainClientAdapter.ViewHolder> {
    private ArrayList<Cliente> listaClientes;
    private boolean isForNewClient = true;

    public MainClientAdapter(ArrayList<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }

    public void setForNewClient(boolean forNewClient) {
        isForNewClient = forNewClient;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_item_main_client, viewGroup, false);
        return new ViewHolder(view, isForNewClient);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.tvIndice.setText(String.valueOf(i+1) + ".");
        viewHolder.tvNombre.setText(listaClientes.get(i).getRazonSocial());
    }

    @Override
    public int getItemCount() {
        return listaClientes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvIndice;
        private TextView tvNombre;
        private ImageView ivBorrar;
        private boolean isForNewClient;

        public ViewHolder(@NonNull View itemView, boolean isForNewClient) {
            super(itemView);
            this.isForNewClient = isForNewClient;
            tvIndice = itemView.findViewById(R.id.tv_client_indice);
            tvNombre = itemView.findViewById(R.id.tv_client_name);
            ivBorrar = itemView.findViewById(R.id.iv_client_delete);

            ivBorrar.setOnClickListener(this);
            if(!isForNewClient) ivBorrar.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View v) {
            if(v.equals(ivBorrar)) {
                removeAt(getPosition());
            }
        }
    }

    private void removeAt(int position) {
        listaClientes.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeRemoved(position, listaClientes.size());
    }
}
