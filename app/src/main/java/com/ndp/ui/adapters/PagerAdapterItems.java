package com.ndp.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import com.ndp.models.model.Tarifario;
import com.ndp.models.model.Vendedor;
import com.ndp.ui.fragments.OrderItemFragment;

public class PagerAdapterItems extends FragmentStatePagerAdapter {
    int mNumOfTabs;
//    private Almacen almacen;
    private List<Tarifario> tarifarios;
    private Vendedor vendedor;
//    private Tarifario t1;
//    private Tarifario t2;
//    private Tarifario t3;


    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public PagerAdapterItems(FragmentManager fm, int mNumOfTabs, List<Tarifario> tarifarios){
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
//        this.almacen = almacen;
//        tarifarios = Tarifario.find(Tarifario.class, "almacen = ?", String.valueOf(almacen));
//        t1 = Tarifario.find(Tarifario.class,"id_tarifario = ?","1").get(0);
//        t2 = Tarifario.find(Tarifario.class,"id_tarifario = ?","2").get(0);
//        t3 = Tarifario.find(Tarifario.class,"id_tarifario = ?","3").get(0);
        this.tarifarios = tarifarios;
    }

    @Override
    public Fragment getItem(int position){
        OrderItemFragment orderItemFragment = new OrderItemFragment();
        orderItemFragment.setItemForOrder(false);
        orderItemFragment.setTarifario(tarifarios.get(position));
        orderItemFragment.setVendedor(vendedor);
        return orderItemFragment;
//        switch(position){
//            case 0:
//                OrderItemFragment orderItemFragment = new OrderItemFragment();
//                orderItemFragment.setItemForOrder(false);
//                orderItemFragment.setId_tarifario(t1.getId());
//                return orderItemFragment;
//            case 1:
//                OrderItemFragment orderItemFragment2 = new OrderItemFragment();
//                orderItemFragment2.setItemForOrder(false);
//                orderItemFragment2.setId_tarifario(t2.getId());
//                return orderItemFragment2;
//            case 2:
//                OrderItemFragment orderItemFragment3 = new OrderItemFragment();
//                orderItemFragment3.setItemForOrder(false);
//                orderItemFragment3.setId_tarifario(t3.getId());
//                return orderItemFragment3;
//            default:
//                return null;
//        }
    }

    @Override
    public int getCount(){
        return mNumOfTabs;
    }
}
