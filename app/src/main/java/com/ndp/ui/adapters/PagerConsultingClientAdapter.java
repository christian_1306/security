package com.ndp.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ndp.models.model.Cliente;
import com.ndp.ui.fragments.AccountStatusFragment;
import com.ndp.ui.fragments.GeneratedOrdersFragment;

public class PagerConsultingClientAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    private Cliente cliente;

    public PagerConsultingClientAdapter(FragmentManager fm, int mNumOfTabs, Cliente cliente) {
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
        this.cliente = cliente;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 0:
                GeneratedOrdersFragment generatedOrdersFragment = new GeneratedOrdersFragment();
                generatedOrdersFragment.setPending(false);
                generatedOrdersFragment.setCliente(cliente);
                return generatedOrdersFragment;
            case 1:
                AccountStatusFragment accountStatusFragment = new AccountStatusFragment();
                accountStatusFragment.setCliente(cliente);
                return accountStatusFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return this.mNumOfTabs;
    }
}
