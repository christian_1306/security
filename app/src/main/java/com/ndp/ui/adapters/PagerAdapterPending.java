package com.ndp.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ndp.models.model.Cliente;
import com.ndp.models.model.Vendedor;
import com.ndp.ui.fragments.GeneratedOrdersFragment;
import com.ndp.ui.fragments.VisitsMadeFragment;

public class PagerAdapterPending extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    private Cliente cliente = null;
    private Vendedor vendedor = null;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public PagerAdapterPending(FragmentManager fm, int mNumOfTabs){
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position){
        switch(position){
            case 0:
                GeneratedOrdersFragment generatedOrdersFragment = new GeneratedOrdersFragment();
                generatedOrdersFragment.setPending(true);
                generatedOrdersFragment.setCliente(cliente);
                return generatedOrdersFragment;
            case 1:
                VisitsMadeFragment visitsMadeFragment = new VisitsMadeFragment();
                visitsMadeFragment.setPending(true);
                visitsMadeFragment.setCliente(cliente);
                visitsMadeFragment.setVendedor(vendedor);
                return visitsMadeFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount(){
        return mNumOfTabs;
    }
}
