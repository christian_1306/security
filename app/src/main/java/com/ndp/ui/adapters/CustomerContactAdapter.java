package com.ndp.ui.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import com.ndp.R;
import com.ndp.models.appRest.apiContactoDetail;


public class CustomerContactAdapter extends RecyclerView.Adapter<CustomerContactAdapter.ViewHolder> {

    private List<apiContactoDetail> apiContactoDetailList;
    private Activity activity;
    private LinearLayout lyt_empty_contacto;
    private RecyclerView recyclerView;

    public CustomerContactAdapter(List<apiContactoDetail> apiContactoDetailList, Activity activity, LinearLayout lyt_empty_contacto, RecyclerView recyclerView) {
        this.apiContactoDetailList = apiContactoDetailList;
        this.activity = activity;
        this.lyt_empty_contacto = lyt_empty_contacto;
        this.recyclerView = recyclerView;
    }

    //region Adapter Methods

    //region Evitan que los elementos se repitan
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    //endregion

    @Override
    public int getItemCount() {
        return apiContactoDetailList != null ? apiContactoDetailList.size() : 0;
    }

    @Override
    @NonNull
    public CustomerContactAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_customer_contact, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") int i) {

        viewHolder.tv_contact_name.setText(apiContactoDetailList.get(i).getNombres());
        viewHolder.tv_contact_telefono.setText(
                (apiContactoDetailList.get(i).getTelefono() != null && !apiContactoDetailList.get(i).getTelefono().isEmpty()) ?
                        apiContactoDetailList.get(i).getTelefono() : activity.getString(R.string.not_phone));
        viewHolder.tv_contact_correo.setText(
                (apiContactoDetailList.get(i).getCorreo() != null && !apiContactoDetailList.get(i).getCorreo().isEmpty()) ?
                        apiContactoDetailList.get(i).getCorreo() : activity.getString(R.string.not_email));
        viewHolder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove(apiContactoDetailList.get(i));
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void insert(int position, apiContactoDetail data) {
        apiContactoDetailList.add(position, data);
        notifyItemInserted(position);
    }

    public void remove(apiContactoDetail data) {
        int position = apiContactoDetailList.indexOf(data);
        apiContactoDetailList.remove(position);
        notifyItemRemoved(position);
        if (apiContactoDetailList.size() == 0) {
            lyt_empty_contacto.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            apiContactoDetailList.clear();
        }
    }
    //endregion

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_delete;
        private TextView tv_contact_name;
        private TextView tv_contact_telefono;
        private TextView tv_contact_correo;

        ViewHolder(View itemView) {
            super(itemView);
            img_delete = itemView.findViewById(R.id.img_delete);
            tv_contact_name = itemView.findViewById(R.id.tv_contact_name);
            tv_contact_telefono = itemView.findViewById(R.id.tv_contact_telefono);
            tv_contact_correo = itemView.findViewById(R.id.tv_contact_correo);
        }
    }
}