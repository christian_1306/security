package com.ndp.ui.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.ui.activities.CustomerDetailActivity;

import static com.ndp.utils.Const.CODE_ORDER;
import static com.ndp.utils.Const.CODE_VISIT;
import static com.ndp.utils.Const.RESULT_CODE_SET_ALL;


public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.ViewHolder> implements Filterable {

    private List<Cliente> lstClientes;
    private List<Cliente> lstClientesFiltered;
    private Integer CODE_ACTIVITY;
    private Activity activity;
    private Boolean isTrueAction;

    public CustomerAdapter(List<Cliente> lstClientes, Integer CODE_ACTIVITY, Boolean isTrueAction, Activity activity) {
        this.lstClientes = lstClientes;
        this.lstClientesFiltered = lstClientes;
        this.CODE_ACTIVITY = CODE_ACTIVITY;
        this.isTrueAction = isTrueAction;
        this.activity = activity;
    }

    //region Adapter Methods

    //region Evitan que los elementos se repitan
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    //endregion

    @Override
    public int getItemCount() {
        return lstClientes != null ? lstClientes.size() : 0;
    }

    @Override
    @NonNull
    public CustomerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_customer, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") int i) {
            viewHolder.tv_cardname.setText(lstClientes.get(i).getRazonSocial());
            viewHolder.tv_cardcode.setText(lstClientes.get(i).getCardCode());
            viewHolder.tv_documento.setText(lstClientes.get(i).getNumeroDocIdentidad());
            viewHolder.lyt_card_customer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (CODE_ACTIVITY == CODE_ORDER && !isTrueAction) {

                        Intent intent = new Intent();
                        intent.putExtra(activity.getResources().getString(R.string.obj_customer), lstClientes.get(i));
                        intent.putExtra(activity.getResources().getString(R.string.changeCardName), false);
                        activity.setResult(RESULT_CODE_SET_ALL, intent);
                        activity.finish();

                    } else if (CODE_ACTIVITY == CODE_ORDER) {
                        Intent intent = new Intent();
                        intent.putExtra(activity.getResources().getString(R.string.obj_customer), lstClientes.get(i));
                        intent.putExtra(activity.getResources().getString(R.string.changeCardName), true);
                        activity.setResult(RESULT_CODE_SET_ALL, intent);
                        activity.finish();
                    } else if (CODE_ACTIVITY == CODE_VISIT) {

                        Intent intent = new Intent();
                        intent.putExtra(activity.getResources().getString(R.string.obj_customer), lstClientes.get(i));
                        intent.putExtra(activity.getResources().getString(R.string.changeCardName), true);
                        activity.setResult(RESULT_CODE_SET_ALL, intent);
                        activity.finish();

                    } else {
                        Intent intent = new Intent(activity, CustomerDetailActivity.class);
                        intent.putExtra(activity.getResources().getString(R.string.obj_customer), lstClientes.get(i));
                        activity.startActivity(intent);
                    }
                }
            });
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void insert(int position, Cliente data) {
        lstClientes.add(position, data);
        notifyItemInserted(position);
    }

    public void remove(Cliente data) {
        int position = lstClientes.indexOf(data);
        lstClientes.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    lstClientes = lstClientesFiltered;
                } else {
                    List<Cliente> filteredList = new ArrayList<>();
                    for (Cliente rowFilter : lstClientesFiltered) {
                        if (rowFilter.getCardCode().toLowerCase().contains(charString.toLowerCase())
                                || rowFilter.getNumeroDocIdentidad().contains(charSequence)
                                || rowFilter.getRazonSocial().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(rowFilter);
                        }
                    }

                    lstClientes = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = lstClientes;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                lstClientes = (List<Cliente>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
    //endregion

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout lyt_card_customer;
        private TextView tv_cardname;
        private TextView tv_cardcode;
        private TextView tv_documento;

        ViewHolder(View itemView) {
            super(itemView);
            lyt_card_customer = itemView.findViewById(R.id.lyt_card_customer);
            tv_cardname = itemView.findViewById(R.id.tv_cardname);
            tv_cardcode = itemView.findViewById(R.id.tv_cardcode);
            tv_documento = itemView.findViewById(R.id.tv_documento);
        }
    }
}