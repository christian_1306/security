package com.ndp.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;
import java.util.List;

import com.ndp.R;
import com.ndp.models.appRest.apiOrderDetail;
import com.ndp.models.model.Almacen;
import com.ndp.models.model.Articulo;
import com.ndp.models.model.ArticuloOrder;
import com.ndp.models.model.ArticuloXAlmacen;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.Tarifario;
import com.ndp.models.model.TarifarioXArticulo;
import com.ndp.models.model.User;
import com.ndp.ui.activities.OrderSaleActivity;
import com.ndp.ui.adapters.ItemOrderAdapter;
import com.ndp.utils.Popup.Popup;

import static com.ndp.utils.Const.CODE_ORDER;
import static com.ndp.utils.Popup.Popup.getDialogTarifario;
import static com.ndp.utils.Useful.getCol;


public class OrderDetailsFragment extends Fragment {

    private Activity _self;
    private RecyclerView rv_order_items;
    private LinearLayout lyt_set_price;
    private TextView tv_obj_list_price;
    private ImageView img_delete_all;
    private ArrayList<ArticuloOrder> articuloOrders;
    private ArrayList<ArticuloOrder> articuloOrdersMARK;
    private List<Tarifario> tarifarios;
    private ItemOrderAdapter adapter;
    private Almacen almacen;
    private Tarifario tarifario;
    private View v;
    private Paint p = new Paint();
    private OrderHeaderFragment orderHeaderFragment;
    private OrderSumFragment orderSumFragment;
    private ArrayList<apiOrderDetail> detallesPedido;
    private User currentUser;
    private ImageView img_background_price;
    private ImageView img_background_add;
    private TextView tv_background;

    public OrderDetailsFragment() {
        // ***Required empty public constructor***
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_order_details, container, false);
        setElements();
        getObjectPrice();
        setHasOptionsMenu(true);
        return v;
    }

    //region setElements
    private void setElements(){
        detallesPedido = new ArrayList<>();
        rv_order_items = v.findViewById(R.id.rv_order_items);
        lyt_set_price = v.findViewById(R.id.lyt_set_price);
        tv_obj_list_price = v.findViewById(R.id.tv_obj_list_price);
        img_delete_all = v.findViewById(R.id.img_delete_all);
        img_background_price = v.findViewById(R.id.img_background_price);
        img_background_add = v.findViewById(R.id.img_background_add);
        tv_background = v.findViewById(R.id.tv_background);


        lyt_set_price.setOnClickListener(customOnClick);
        img_delete_all.setOnClickListener(customOnClick);
        articuloOrdersMARK = new ArrayList<>();
        tv_obj_list_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                articuloOrders = new ArrayList<>();
                articuloOrdersMARK = new ArrayList<>();
            }
        });
    }
    //endregion

    //region function

    @SuppressLint("SetTextI18n")
    private void getObjectPrice() {
        List<Almacen> almacens = Almacen.find(Almacen.class, getCol("codAlmacen") + " = '" + currentUser.getAlmCodUsuario() + "' AND " + getCol("isActive") + " = 1");
        if (almacens != null && almacens.size() > 0) {
            almacen = almacens.get(0);
            if (currentUser.getTipoUsuario() == 2) {
                List<Cliente> clientes = Cliente.find(Cliente.class, getCol("cardCode")+ " = '" + currentUser.getCodUsuario() + "' AND " + getCol("isActive") + " = 1");
                if (clientes != null && clientes.size() > 0)
                {
                    tarifarios = Tarifario.find(Tarifario.class,getCol("codigoListaPrecio") + " = '" + clientes.get(0).getIdTarifario()
                            + "' AND " + getCol("isActive") + " = 1");
                    if (tarifarios != null && tarifarios.size() > 0)
                    {

                        tv_background.setText(getString(R.string.add_items_order));
                        img_delete_all.setVisibility(View.GONE);
                        img_background_price.setVisibility(View.GONE);
                        img_background_add.setVisibility(View.VISIBLE);
                        tv_obj_list_price.setText(tarifarios.get(0).getId()+"-"+tarifarios.get(0).getNombre());
                        lyt_set_price.setOnClickListener(customOnClick);
                    }
                }
            } else {
                tarifarios = Tarifario.find(Tarifario.class,getCol("warehouseCode") + " = '" + almacen.getCodAlmacen() + "' AND " + getCol("isActive") + " = 1");
            }

            if (tarifarios == null || tarifarios.size() == 0)
            {
                MDToast.makeText(_self, getString(R.string.not_list_price), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR ).show();
                lyt_set_price.setOnClickListener(null);
            }
        }
        else
        {
            MDToast.makeText(_self, getString(R.string.not_list_price), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR ).show();
            lyt_set_price.setOnClickListener(null);
        }
    }
    //endregion

    //region MENU
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_order_add, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_order_add:
                new getPriceList().execute();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //endregion

    //region onActivityResult
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null){
            articuloOrders  = data.getParcelableArrayListExtra(getResources().getString(R.string.valueActivity));
            articuloOrdersMARK = data.getParcelableArrayListExtra(getResources().getString(R.string.valueActivityI));
            if (articuloOrdersMARK != null && articuloOrdersMARK.size() > 0){
                RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
                itemAnimator.setAddDuration(300);
                itemAnimator.setRemoveDuration(300);
                rv_order_items.setItemAnimator(itemAnimator);

                rv_order_items.setHasFixedSize(true);
                rv_order_items.setLayoutManager(new LinearLayoutManager(_self));
                adapter = new ItemOrderAdapter(articuloOrdersMARK, _self,currentUser.getTipoUsuario() != null && currentUser.getTipoUsuario() == 2);
                rv_order_items.setAdapter(adapter);
                enableSwipe();
                rv_order_items.setVisibility(View.VISIBLE);
                img_delete_all.setVisibility(View.VISIBLE);
                lyt_set_price.setVisibility(View.GONE);
            } else {
                rv_order_items.setVisibility(View.GONE);
                img_delete_all.setVisibility(View.GONE);
                lyt_set_price.setVisibility(View.VISIBLE);
            }
        }
    }
    //endregion

    //region get & set
    public Activity get_self() {
        return _self;
    }

    public void set_self(Activity _self) {
        this._self = _self;
    }
    //endregion

    //region OnClickListener
    private View.OnClickListener customOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.lyt_set_price:
                    if (currentUser.getTipoUsuario() != null && currentUser.getTipoUsuario() == 2){
                        new getPriceList().execute();
                    } else {
                        getDialogTarifario(_self,tarifarios,tv_obj_list_price, getString(R.string.list_price));
                    }
                    break;
                case R.id.img_delete_all:
                    Popup.ShowDialog(_self,
                            getString(R.string.mensaje_delete_items),
                            getString(R.string.titulo_delete_items),
                            Popup.MSG_TYPE_WARNING,
                            Popup.BUTTON_TYPE_ACCEPT,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    for (ArticuloOrder articuloOrder : articuloOrdersMARK){
                                        articuloOrder.setCantidadOrder(0);
                                        articuloOrders.add(articuloOrder);
                                    }
                                    articuloOrdersMARK = new ArrayList<>();
                                    rv_order_items.setVisibility(View.GONE);
                                    img_delete_all.setVisibility(View.GONE);
                                    lyt_set_price.setVisibility(View.VISIBLE);
                                }
                            },
                            null);
                    break;
            }
        }
    };

    public OrderSumFragment getOrderSumFragment() {
        return orderSumFragment;
    }

    public void setOrderSumFragment(OrderSumFragment orderSumFragment) {
        this.orderSumFragment = orderSumFragment;
    }

    public ArrayList<ArticuloOrder> getArticuloOrdersMARK() {
        return articuloOrdersMARK;
    }

    public void setArticuloOrdersMARK(ArrayList<ArticuloOrder> articuloOrdersMARK) {
        this.articuloOrdersMARK = articuloOrdersMARK;
    }

    public ArrayList<apiOrderDetail> getDetallesPedido() {
        detallesPedido = new ArrayList<>();
        if (articuloOrdersMARK != null && articuloOrdersMARK.size() > 0){
             for (ArticuloOrder articuloOrder : articuloOrdersMARK){
                 int cantOrder = articuloOrder.getCantidadOrder();
                 int varConversion = articuloOrder.getVarillasPorTonelada();
                 double precioNeto = articuloOrder.getPrecioDefaultNeto();

                 apiOrderDetail orderDetail = new apiOrderDetail();
                 orderDetail.setCantidadVarillas(cantOrder * varConversion);
                 orderDetail.setCodAlmacen(articuloOrder.getWarehouseCode());
                 orderDetail.setCodArticulo(articuloOrder.getCodigo());
                 orderDetail.setCodImpuesto(articuloOrder.getImpuesto());
                 orderDetail.setCodListaPrecio(articuloOrder.getCodTarifario());
                 orderDetail.setImpuestoDetalle(cantOrder * varConversion * precioNeto * 0.13);
                 orderDetail.setPesoTotal(cantOrder);
                 orderDetail.setPrecioUnitario(precioNeto);
                 orderDetail.setSubtotalFinal(precioNeto * cantOrder * varConversion * 1.13);
                 orderDetail.setSubtotalNeto(precioNeto * cantOrder * varConversion);
                 orderDetail.setUnidad(articuloOrder.getUnidadMedida());
                 orderDetail.setCodCreacionApp(orderHeaderFragment.getCodCreacionApp());
                 detallesPedido.add(orderDetail);
             }
        }
        return detallesPedido;
    }

    public void setDetallesPedido(ArrayList<apiOrderDetail> detallesPedido) {
        this.detallesPedido = detallesPedido;
    }

    public OrderHeaderFragment getOrderHeaderFragment() {
        return orderHeaderFragment;
    }

    public void setOrderHeaderFragment(OrderHeaderFragment orderHeaderFragment) {
        this.orderHeaderFragment = orderHeaderFragment;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    //endregion

    //region task getPriceList
    @SuppressLint("StaticFieldLeak")
    private class getPriceList extends AsyncTask<Void, String, Void> {
        private ProgressDialog dialog;
        StringBuilder outMessage = new StringBuilder();
        String errMsg = "";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(_self);
            dialog.setTitle("Operación en curso");
            dialog.setMessage("Mensaje");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            dialog.setMessage(values[0]);
        }

        @Override
        protected Void doInBackground(Void... args) {
            try {
                if ((articuloOrders == null || articuloOrders.size() == 0) && !tv_obj_list_price.getText().equals(getString(R.string.not_price))) {
                    publishProgress(getString(R.string.async_title_price));
                    String listPrice = tv_obj_list_price.getText().toString();
                    String[] sep = listPrice.split("-");
                    int idPrice;
                    try {
                        idPrice = Integer.parseInt(sep[0]);
                    } catch (Exception e){
                        idPrice = 0;
                    }

                    Tarifario tariPrice = Tarifario.findById(Tarifario.class,idPrice);
                    if (tariPrice != null && almacen != null && tariPrice.getIsActive() == 1) {
                        publishProgress(getString(R.string.search_items));
                        List<Articulo> listArticulos = Articulo.find(Articulo.class, getCol("isActive") + " = 1 ORDER BY "+getCol("nombre"));
                        articuloOrders = new ArrayList<>();
                        if (listArticulos != null && listArticulos.size() > 0){
                            for (Articulo item : listArticulos){

                                List<ArticuloXAlmacen> stocks = ArticuloXAlmacen.find(ArticuloXAlmacen.class, getCol("itemCode") +
                                        " = '" + item.getCodigo() + "' AND " + getCol("warehouseCode") + " = '" + almacen.getCodAlmacen() + "'");

                                List<TarifarioXArticulo> precios = TarifarioXArticulo.find(TarifarioXArticulo.class, getCol("itemCode") +
                                        " = '" + item.getCodigo() + "' AND " + getCol("codTarifario") + " = '" + tariPrice.getCodigoListaPrecio() + "' AND " + getCol("precioDefaultNeto") + " > 0");

                                if (stocks != null && stocks.size() > 0 && precios != null && precios.size() > 0){
                                    ArticuloOrder articuloOrder = new ArticuloOrder();
                                    articuloOrder.setCodigo(item.getCodigo());
                                    articuloOrder.setNombre(item.getNombre());
                                    articuloOrder.setDescripcion(item.getDescripcion());
                                    articuloOrder.setUnidadMedida(item.getUnidadMedida());
                                    articuloOrder.setVarillasPorTonelada(item.getVarillasPorTonelada());
                                    articuloOrder.setCodTarifario(precios.get(0).getCodTarifario());
                                    articuloOrder.setPrecioDefaultFinal(precios.get(0).getPrecioDefaultFinal());
                                    articuloOrder.setPrecioDefaultNeto(precios.get(0).getPrecioDefaultNeto());
                                    articuloOrder.setImpuestoArticulo(precios.get(0).getImpuestoArticulo());
                                    articuloOrder.setIncluyeImpuesto(precios.get(0).getIncluyeImpuesto());
                                    articuloOrder.setImpuesto(precios.get(0).getImpuesto());
                                    articuloOrder.setMoneda(precios.get(0).getMoneda());
                                    articuloOrder.setWarehouseCode(stocks.get(0).getWarehouseCode());
                                    articuloOrder.setStock(stocks.get(0).getStock());
                                    articuloOrder.setComprometido(stocks.get(0).getComprometido());
                                    articuloOrder.setSolicitado(stocks.get(0).getSolicitado());
                                    articuloOrder.setDisponible(stocks.get(0).getDisponible());

                                    articuloOrders.add(articuloOrder);

                                }
                            }
                        }
                    }

                } else if (tv_obj_list_price.getText().equals(getString(R.string.not_price))){
                    errMsg = getString(R.string.sel_list_price);
                }

            }catch (Exception ex){
                errMsg = ex.getMessage();
            }
            return null;
        }

        protected void onPostExecute(Void result) {

            if (dialog.isShowing() && dialog!=null) {
                dialog.dismiss();
                if (!errMsg.isEmpty()) {
                    MDToast.makeText(_self, errMsg, MDToast.LENGTH_LONG, MDToast.TYPE_ERROR ).show();
                } else {
                    Intent i = new Intent(_self, OrderSaleActivity.class);
                    i.putExtra(getResources().getString(R.string.objUser), currentUser);
                    i.putParcelableArrayListExtra(getResources().getString(R.string.valueActivity),articuloOrders);
                    i.putParcelableArrayListExtra(getResources().getString(R.string.valueActivityI),articuloOrdersMARK);
                    startActivityForResult(i,CODE_ORDER);
                }
            }
        }
    }
    //endregion

    //region enableSwipe
    private void enableSwipe(){

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT ) {//| ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT){
                    final ArticuloOrder deletedModel = articuloOrdersMARK.get(position);
                    final int cantidadOrder = deletedModel.getCantidadOrder();
                    final int deletedPosition = position;
                    adapter.removeItem(position);
                    //articuloOrdersMARK.remove(deletedModel);
                    deletedModel.setCantidadOrder(0);
                    articuloOrders.add(deletedModel);
                    if (articuloOrdersMARK != null && articuloOrdersMARK.size() == 0)
                    {
                        rv_order_items.setVisibility(View.GONE);
                        img_delete_all.setVisibility(View.GONE);
                        lyt_set_price.setVisibility(View.VISIBLE);
                    }
                    View view = _self.findViewById(R.id.coordinator_details);
                    Snackbar snackbar = Snackbar.make(view, R.string.item_del, Snackbar.LENGTH_LONG);
                    snackbar.setAction(R.string.undo, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            deletedModel.setCantidadOrder(cantidadOrder);
                            adapter.restoreItem(deletedModel, deletedPosition);
                            articuloOrders.remove(deletedModel);
                            //deletedModel.setCantidadOrder(cantidadOrder);
                            //articuloOrdersMARK.add(deletedModel);
                            if (articuloOrdersMARK != null && articuloOrdersMARK.size() > 0)
                            {
                                rv_order_items.setVisibility(View.VISIBLE);
                                img_delete_all.setVisibility(View.VISIBLE);
                                lyt_set_price.setVisibility(View.GONE);
                            }
                        }
                    });
                    snackbar.setActionTextColor(_self.getResources().getColor(R.color.Pantone152C));
                    snackbar.show();
                } else {
                    final ArticuloOrder deletedModel = articuloOrdersMARK.get(position);
                    final int cantidadOrder = deletedModel.getCantidadOrder();
                    final int deletedPosition = position;
                    adapter.removeItem(position);
                    //articuloOrdersMARK.remove(deletedModel);
                    deletedModel.setCantidadOrder(0);
                    articuloOrders.add(deletedModel);
                    if (articuloOrdersMARK != null && articuloOrdersMARK.size() == 0)
                    {
                        rv_order_items.setVisibility(View.GONE);
                        img_delete_all.setVisibility(View.GONE);
                        lyt_set_price.setVisibility(View.VISIBLE);
                    }
                    Snackbar snackbar = Snackbar.make(_self.getWindow().getDecorView().getRootView(), R.string.item_del, Snackbar.LENGTH_LONG);
                    snackbar.setAction(R.string.undo, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            deletedModel.setCantidadOrder(cantidadOrder);
                            adapter.restoreItem(deletedModel, deletedPosition);
                            articuloOrders.remove(deletedModel);
                            //deletedModel.setCantidadOrder(cantidadOrder);
                            //articuloOrdersMARK.add(deletedModel);
                            if (articuloOrdersMARK != null && articuloOrdersMARK.size() > 0)
                            {
                                rv_order_items.setVisibility(View.VISIBLE);
                                img_delete_all.setVisibility(View.VISIBLE);
                                lyt_set_price.setVisibility(View.GONE);
                            }
                        }
                    });
                    snackbar.setActionTextColor(_self.getResources().getColor(R.color.Pantone152C));
                    snackbar.show();
                }
            }

            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if(dX > 0){
                        p.setColor(_self.getResources().getColor(R.color.color_add));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX,(float) itemView.getBottom());
                        c.drawRect(background,p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_menu_trash02);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width ,(float) itemView.getTop() + width,(float) itemView.getLeft()+ 2*width,(float)itemView.getBottom() - width);
                        c.drawBitmap(icon,null,icon_dest,p);
                    } else if (dX < 0)  {
                        p.setColor(_self.getResources().getColor(R.color.color_delete));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background,p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_menu_trash02);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                        c.drawBitmap(icon,null,icon_dest,p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(rv_order_items);
    }
    //endregion
}
