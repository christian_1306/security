package com.ndp.ui.fragments;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import com.ndp.models.model.Member;
import com.ndp.models.model.User;
import com.ndp.ui.activities.CurrentArrivalActivity;
import com.ndp.ui.activities.CustomersActivity;
import com.ndp.R;
import com.ndp.ui.activities.HomeActivity;
import com.ndp.ui.activities.ItemsActivity;
import com.ndp.ui.activities.LoginActivity;
import com.ndp.ui.activities.OrderActivity;
import com.ndp.ui.activities.PendingSendActivity;
import com.ndp.ui.activities.ProfileAPPActivity;
import com.ndp.ui.activities.QueriesOnlineActivity;
import com.ndp.ui.activities.ReportMainActivity;
import com.ndp.ui.activities.ReportMonthActivity;
import com.ndp.ui.activities.RouteNavActivity;
import com.ndp.utils.Popup.Popup;
import com.ndp.utils.Shared;
import com.ndp.utils.Useful;
import com.ndp.utils.methods.SessionManager;
import com.ndp.utils.methods.SessionManager2;

import static com.ndp.utils.Const.CODE_HOME;
import static com.ndp.utils.Const.CODE_QUERY;
import static com.ndp.utils.use.useAPP.CerrarSesion;

public class HomeSellerFragment extends Fragment {

    private View view;
    private Member currentUser;
    private Intent intent;
    private Activity _self;
    private SharedPreferences shrPreLogin;
    private SessionManager2 sessionManager;

    public HomeSellerFragment() {
        // ***Required empty public constructor***
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home_seller, container, false);
        setElements();
        getUserLogin();
       // HomeActivity activity=(HomeActivity)getActivity();
        //currentUser=activity.getMemmber();
        return view;
    }

    private void setElements() {
        view.findViewById(R.id.card_pedidos).setOnClickListener(tvMenu_Click);
        view.findViewById(R.id.card_visita).setOnClickListener(tvMenu_Click);
        view.findViewById(R.id.card_customers).setOnClickListener(tvMenu_Click);
        view.findViewById(R.id.card_items).setOnClickListener(tvMenu_Click);
        view.findViewById(R.id.card_consultas).setOnClickListener(tvMenu_Click);
        view.findViewById(R.id.card_pendientes).setOnClickListener(tvMenu_Click);
        view.findViewById(R.id.card_perfil).setOnClickListener(tvMenu_Click);
        view.findViewById(R.id.card_close).setOnClickListener(tvMenu_Click);
        view.findViewById(R.id.card_route).setOnClickListener(tvMenu_Click);
        view.findViewById(R.id.card_report).setOnClickListener(tvMenu_Click);
        sessionManager = new SessionManager2(_self);
    }
/*
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
       // currentUser = (Member) getActivity().getIntent().getSerializableExtra("member");
    }*/

    private void getUserLogin(){
        shrPreLogin = Shared.prf_Login(Objects.requireNonNull(_self != null ? _self : getContext()));
       currentUser = Member.find(Member.class, Useful.getCol("email")  + " = ?", shrPreLogin.getString(Shared.login_usuario, null)).get(0);
        if (currentUser == null) {
            Popup.ShowDialog(_self,
                    getString(R.string.mensaje_usuario_corrupto), Popup.MSG_TITLE_WARNING,
                    Popup.MSG_TYPE_ERROR,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            CerrarSesion(_self, sessionManager);
                        }
                    });
        }
    }

    private View.OnClickListener tvMenu_Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.card_report:
                    intent = new Intent(_self, ReportMainActivity.class);
                    startActivity(intent);
                    break;
                case R.id.card_pedidos:
                    intent = new Intent(_self, OrderActivity.class);
                    intent.putExtra(getResources().getString(R.string.objUser), currentUser);
                    intent.putExtra(getResources().getString(R.string.valueActivity), String.valueOf(CODE_HOME));
                    intent.putExtra(getResources().getString(R.string.changeCardName), true);
                    startActivity(intent);
                    break;
                case R.id.card_customers:
                    intent = new Intent(_self, CustomersActivity.class);
                    intent.putExtra(getResources().getString(R.string.objUser), currentUser);
                    intent.putExtra(getResources().getString(R.string.valueActivity), String.valueOf(CODE_HOME));
                    intent.putExtra(getResources().getString(R.string.changeCardName), true);
                    startActivity(intent);
                    break;
                case R.id.card_items:
                    intent = new Intent(_self, ItemsActivity.class);
                    intent.putExtra(getResources().getString(R.string.objUser), currentUser);
                    intent.putExtra(getResources().getString(R.string.valueActivity), String.valueOf(CODE_HOME));
                    intent.putExtra(getResources().getString(R.string.changeCardName), true);
                    startActivity(intent);
                    break;
                case R.id.card_pendientes:
                    intent = new Intent(_self, PendingSendActivity.class);
                    intent.putExtra(getResources().getString(R.string.objUser), currentUser);
                    intent.putExtra(getResources().getString(R.string.valueActivity), String.valueOf(CODE_HOME));
                    intent.putExtra(getResources().getString(R.string.changeCardName), true);
                    startActivity(intent);
                    break;
                case R.id.card_consultas:
                    intent = new Intent(_self, QueriesOnlineActivity.class);
                    intent.putExtra(getResources().getString(R.string.objUser), currentUser);
                    intent.putExtra(getResources().getString(R.string.valueActivity), String.valueOf(CODE_QUERY));
                    intent.putExtra(getResources().getString(R.string.changeCardName), true);
                    startActivity(intent);
                    break;
                case R.id.card_visita:
                    intent = new Intent(_self, CurrentArrivalActivity.class);
                    intent.putExtra(getResources().getString(R.string.objUser), currentUser);
                    startActivity(intent);
                    break;
                case R.id.card_perfil:
                    intent = new Intent(_self, ProfileAPPActivity.class);
                    intent.putExtra(getResources().getString(R.string.objUser), currentUser);
                    startActivity(intent);
                    break;
                case R.id.card_route:
                    //intent = new Intent(_self, RouteMapsActivity.class);
                    //intent = new Intent(_self, LocationComponentCameraOptionsActivity.class);
                    //intent = new Intent(_self, RecyclerViewDirectionsActivity.class);
                    intent = new Intent(_self, RouteNavActivity.class);
                    intent.putExtra(getResources().getString(R.string.objUser), currentUser);
                    startActivity(intent);
                    break;
                case R.id.card_close:
                    Popup.ShowDialog(_self,
                            getString(R.string.mensaje_cerrar_sesion), Popup.MSG_TITLE_WARNING,
                            Popup.MSG_TYPE_WARNING,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    CerrarSesion(_self, sessionManager);
                                }
                            });
                    break;
            }
        }
    };

    public Activity get_Self() {
        return _self;
    }

    public void set_Self(Activity _self) {
        this._self = _self;
    }
}
