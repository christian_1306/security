package com.ndp.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import com.ndp.R;
import com.ndp.models.model.ArticuloXAlmacen;

public class ItemDetailFragment extends Fragment {

    private ScrollView svDatos;
    private View view;
    private ArticuloXAlmacen articuloXAlmacen;
//    private ArticuloXAlmacen articuloXAlmacen_2;

//    public ArticuloXAlmacen getArticuloXAlmacen_2() {
//        return articuloXAlmacen_2;
//    }
//
//    public void setArticuloXAlmacen_2(ArticuloXAlmacen articuloXAlmacen_2) {
//        this.articuloXAlmacen_2 = articuloXAlmacen_2;
//    }

    private TextView tv_nombre_articulo,tv_nombre_codigo;
    private TextView tv_numero_stock,tv_numero_comprometido,tv_numero_disponible;
//    private TextView tv_numero_stock_2,tv_numero_comprometido_2,tv_numero_disponible_2;
    private TextView tvAlmacen;

    public ArticuloXAlmacen getArticuloXAlmacen() {
        return articuloXAlmacen;
    }

    public void setArticuloXAlmacen(ArticuloXAlmacen articuloXAlmacen) {
        this.articuloXAlmacen = articuloXAlmacen;
    }

    public ItemDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_item_detail, container, false);
        setElements();
        setNumbers();
        return view;
    }

    private void setNumbers(){
       // tvAlmacen.setText(articuloXAlmacen.getAlmacen().getNombre());

        tv_numero_stock.setText(String.valueOf(articuloXAlmacen.getStock()));
        tv_numero_comprometido.setText(String.valueOf(articuloXAlmacen.getComprometido()));
        tv_numero_disponible.setText(String.valueOf(articuloXAlmacen.getDisponible()));

//        tv_numero_stock_2.setText(String.valueOf(articuloXAlmacen_2.getStock()));
//        tv_numero_comprometido_2.setText(String.valueOf(articuloXAlmacen_2.getComprometido()));
//        tv_numero_disponible_2.setText(String.valueOf(articuloXAlmacen_2.getDisponible()));
    }

    private void setElements(){
        svDatos = view.findViewById(R.id.item_detail_svDatos);
        tv_nombre_articulo = view.findViewById(R.id.tv_nombre_articulo);
        tv_nombre_articulo.setText(articuloXAlmacen.getArticulo().getNombre());
        tv_nombre_codigo = view.findViewById(R.id.tv_nombre_codigo);
//        tv_nombre_codigo.setText(String.valueOf(articuloXAlmacen_2.getArticulo().getId_articulo()));
        tv_numero_stock = view.findViewById(R.id.tv_numero_stock);
//        tv_numero_stock_2 = view.findViewById(R.id.tv_numero_stock_2);
        tv_numero_comprometido = view.findViewById(R.id.tv_numero_comprometido);
//        tv_numero_comprometido_2 = view.findViewById(R.id.tv_numero_comprometido_2);
        tv_numero_disponible = view.findViewById(R.id.tv_numero_disponible);
//        tv_numero_disponible_2 = view.findViewById(R.id.tv_numero_disponible_2);
        tvAlmacen = view.findViewById(R.id.tv_detalle_item_almacen);
        view.findViewById(R.id.item_detail_rlvDatosArticulo).setOnClickListener(ivExpandableDatos_Click);
        view.findViewById(R.id.item_detail_rlvInventario).setOnClickListener(ivExpandableDatos_Click);
        view.findViewById(R.id.rlv_item_detail_almacen).setOnClickListener(ivExpandableDatos_Click);
//        view.findViewById(R.id.rlv_item_detail_almacen_2).setOnClickListener(ivExpandableDatos_Click);
    }

    private View.OnClickListener ivExpandableDatos_Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ExpandableRelativeLayout expandableRelative = null;
            ExpandableLinearLayout expandableLinear = null;
            ImageView ivExpandable = null;

            switch (v.getId()) {
                case R.id.item_detail_rlvDatosArticulo:
                    expandableRelative = view.findViewById(R.id.item_detail_expDatosItem);
                    ivExpandable = view.findViewById(R.id.iv_arrow_item_detail);
                    break;
                case R.id.item_detail_rlvInventario:
                    expandableRelative = view.findViewById(R.id.item_detail_expInventory);
                    ivExpandable = view.findViewById(R.id.iv_arrow_item_detail_inventario);
                    break;
                case R.id.rlv_item_detail_almacen:
                    expandableLinear = view.findViewById(R.id.expandable_lnr_almacen);
                    ivExpandable = view.findViewById(R.id.iv_arrow_item_almacen);
                    break;

//                case R.id.rlv_item_detail_almacen_2:
//                    expandableLinear = view.findViewById(R.id.expandable_lnr_almacen_2);
//                    ivExpandable = view.findViewById(R.id.iv_arrow_item_almacen_2);
//                    break;
            }

            if (expandableRelative != null) {
                expandableRelative.toggle();
                if (expandableRelative.isExpanded())
                    ivExpandable.animate().rotation(180).start();
                else {
                    ivExpandable.animate().rotation(0).start();
                    if (expandableRelative.getId() != R.id.customer_lead_expDatosLead) {
                        svDatos.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                svDatos.fullScroll(View.FOCUS_DOWN);
                            }
                        }, 400);
                    }
                }
            }
            else if(expandableLinear!=null){
                expandableLinear.toggle();
                if (expandableLinear.isExpanded())
                    ivExpandable.animate().rotation(180).start();
                else {
                    ivExpandable.animate().rotation(0).start();
                    if (expandableLinear.getId() != R.id.customer_lead_expDatosLead) {
                        svDatos.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                svDatos.fullScroll(View.FOCUS_DOWN);
                            }
                        }, 400);
                    }
                }
            }
        }
    };
}
