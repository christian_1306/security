package com.ndp.ui.fragments;


import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.DetallePedido;
import com.ndp.models.model.Direccion;
import com.ndp.models.model.Pedido;
import com.ndp.ui.adapters.SelectClientAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderDetailFragment extends Fragment {

    private View view;
    private ScrollView svDatos;
    private Cliente cliente;
    private Cliente clienteSecundario = null;
    private LinearLayout lnr_agregar_cliente;
    private boolean isForView = false;
    private boolean isForClient = false;

    private Cliente clienteSecundarioSeleccionado;
    private TextView tv_razon_social,tv_nombre_comercial,tv_correo,tv_moneda,tv_linea_credito,tv_saldo_credito,tv_telefono;
    private TextView tvNumeroDocumento,tv_condicion_pago;
    private TextView tvTotalBase;
    private TextView tvImpuesto;
    private TextView tvTotal;
    private TextView tv_agregar_cliente_secundario,tv_etiqueta_descuento;

    private EditText etDescuento;
    private EditText etNombreSecundario;
    private EditText etNumeroDocumentoSecundario;
    private EditText etTelefonoSecundario;
    private EditText etComentario;

    private Spinner spLugarEntrega;
    private Spinner spLugarFactura;

    private RecyclerView rvClientesSecundarios;

    private List<Direccion> listaDirecciones;

    private SelectClientAdapter selectClientAdapter;

    private Button btnAgregarClienteSecundario;
    private Pedido pedido;

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    private Direccion direccionEntrega;
    private Direccion direccionFactura;
    private double totalBase;
    private double totalImpuesto;
    private double total;
    private String comentario;
    private RelativeLayout rlvAgregarCliente;

    private AlertDialog dialog;
//    private ArrayList<DetallePedido> listaDetalles;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public boolean isForClient() {
        return isForClient;
    }

    public void setForClient(boolean forClient) {
        isForClient = forClient;
    }

    public Direccion getDireccionEntrega() {
        direccionEntrega = (Direccion)spLugarEntrega.getSelectedItem();
        return direccionEntrega;
    }

    public Direccion getDireccionFactura() {
        direccionFactura = (Direccion)spLugarFactura.getSelectedItem();
        return direccionFactura;
    }

    public EditText getEtDescuento() {
        return etDescuento;
    }

    public void setEtDescuento(EditText etDescuento) {
        this.etDescuento = etDescuento;
    }

    public Cliente getClienteSecundarioSeleccionado() {
        if(etNumeroDocumentoSecundario.getText().toString().isEmpty() && etNombreSecundario.getText().toString().isEmpty() && etTelefonoSecundario.getText().toString().isEmpty())
            return null;
        if(Cliente.find(Cliente.class, "numero_documento = ? and tipo_cliente = 2", etNumeroDocumentoSecundario.getText().toString()).size() == 0) {
            clienteSecundarioSeleccionado = new Cliente();
           /* clienteSecundarioSeleccionado.setNumeroDocumento(etNumeroDocumentoSecundario.getText().toString());
            clienteSecundarioSeleccionado.setRazonSocial(etNombreSecundario.getText().toString());
            clienteSecundarioSeleccionado.setTelefono(etTelefonoSecundario.getText().toString());
            int newIdCliente = ((int)Cliente.count(Cliente.class)) + 1;
            clienteSecundarioSeleccionado.setIdCliente(newIdCliente);
            clienteSecundarioSeleccionado.setTipoCliente(2);
            clienteSecundarioSeleccionado.save();*/
        }
        return clienteSecundarioSeleccionado;
    }

    public double getTotalBase() {
        return totalBase;
    }

    public double getTotalImpuesto() {
        return totalImpuesto;
    }

    public double getTotal() {
        return total;
    }

    public String getComentario() {
        return etComentario.getText().toString();
    }

    public Cliente getClienteSecundario() {
        return clienteSecundario;
    }

    public void setClienteSecundario(Cliente clienteSecundario) {
        this.clienteSecundario = clienteSecundario;
    }

    public boolean isForView() {
        return isForView;
    }

    public void setForView(boolean forView) {
        isForView = forView;
    }

    //    public ArrayList<DetallePedido> getListaDetalles() {
//        return listaDetalles;
//    }
//
//    public void setListaDetalles(ArrayList<DetallePedido> listaDetalles) {
//        this.listaDetalles = listaDetalles;
//    }

    public OrderDetailFragment() {
        totalBase = 0;
        totalImpuesto = 0;
        total = 0;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_order_detail, container, false);
        setElements();
        setDataClient();
        return view;
    }

    private void setDataClient() {
        tv_razon_social.setText(this.cliente.getRazonSocial());
        tv_correo.setText(this.cliente.getCorreo());
        tv_moneda.setText("BOLIVIANOS");
        tv_linea_credito.setText(String.valueOf(cliente.getLineaCredito()));
        tv_saldo_credito.setText(String.valueOf(cliente.getSaldoCredito()));
       // tv_condicion_pago.setText(this.cliente.getCondicionPago());
        tv_telefono.setText(cliente.getTelefono());
       // tvNumeroDocumento.setText(cliente.getNumeroDocumento());

        //Set Direcciones del cliente
        this.listaDirecciones = new ArrayList<>();
        //this.listaDirecciones = cliente.listarDirecciones();
        ArrayAdapter<Direccion> adapterDirecciones = new ArrayAdapter<>(getActivity(), R.layout.spinner_item_ubigeo, listaDirecciones);
        spLugarEntrega.setAdapter(adapterDirecciones);
        spLugarFactura.setAdapter(adapterDirecciones);
    }

    private void setElements() {
        svDatos = view.findViewById(R.id.customer_lead_svDatos);
        lnr_agregar_cliente = view.findViewById(R.id.lnr_agregar_cliente);
        view.findViewById(R.id.order_detail_cliente_rlv).setOnClickListener(ivExpandableDatos_Click);
        view.findViewById(R.id.order_detail_datos_administrativos_rlv).setOnClickListener(ivExpandableDatos_Click);
        view.findViewById(R.id.order_detail_cliente_secundario_rlv).setOnClickListener(ivExpandableDatos_Click);
        view.findViewById(R.id.order_detail_cliente_datos_despacho_rlv).setOnClickListener(ivExpandableDatos_Click);
        tv_razon_social = view.findViewById(R.id.tv_nombre_razon_social);
        tv_correo = view.findViewById(R.id.tv_texto_correo);
        tv_condicion_pago = view.findViewById(R.id.tv_nombre_condicion_pago);
        tvNumeroDocumento = view.findViewById(R.id.tv_texto_numero_documento);
        tv_etiqueta_descuento = view.findViewById(R.id.tv_etiqueta_descuento);
        tv_moneda = view.findViewById(R.id.tv_nombre_moneda);
        tv_telefono = view.findViewById(R.id.tv_texto_telefonos);
        tv_linea_credito = view.findViewById(R.id.tv_numero_linea_credito);
        tv_saldo_credito = view.findViewById(R.id.tv_numero_saldo_credito);
        tv_agregar_cliente_secundario = view.findViewById(R.id.tv_agregar_cliente_secundario);
        rlvAgregarCliente = view.findViewById(R.id.order_detail_cliente_secundario_rlv);
        spLugarEntrega = view.findViewById(R.id.sp_lugar_entrega);
        spLugarFactura = view.findViewById(R.id.sp_lugar_facturacion);

        tvTotalBase = view.findViewById(R.id.tv_texto_total_base);
        tvImpuesto = view.findViewById(R.id.tv_texto_impuesto);
        tvTotal = view.findViewById(R.id.tv_texto_total);

        etNombreSecundario = view.findViewById(R.id.et_order_razon_social_secundario);
        etNumeroDocumentoSecundario = view.findViewById(R.id.et_order_numero_documento_secundario);
        etTelefonoSecundario = view.findViewById(R.id.et_order_telefono_secundario);
        etDescuento = view.findViewById(R.id.et_texto_descuento);

        btnAgregarClienteSecundario = view.findViewById(R.id.btn_order_agregar_cliente_secundario);
        btnAgregarClienteSecundario.setOnClickListener(btnAgregarSecundario_Click);

        etDescuento.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if(s.equals("")){
                    double nuevo_total = Double.parseDouble(tvTotalBase.getText().toString())
                            + Double.parseDouble(tvImpuesto.getText().toString());
                    DecimalFormat df = new DecimalFormat("#.00");
                    tvTotal.setText(df.format(nuevo_total));
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String descuento = s.toString();
                if(!descuento.equals("")) {
                    double nuevo_total;
                    double descuento_double = Double.parseDouble(descuento);
                    nuevo_total = totalBase + totalImpuesto - descuento_double;
                    DecimalFormat df = new DecimalFormat("#.00");
                    tvTotal.setText(df.format(nuevo_total));
                }
                else if(descuento.equals("") || descuento.equals(".")){
                    double nuevo_total = Double.parseDouble(tvTotalBase.getText().toString())
                            + Double.parseDouble(tvImpuesto.getText().toString());
                    DecimalFormat df = new DecimalFormat("#.00");
                    tvTotal.setText(df.format(nuevo_total));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etComentario = view.findViewById(R.id.et_escribe_comentario);

        if(isForClient){
            etDescuento.setVisibility(View.GONE);
            tv_etiqueta_descuento.setVisibility(View.GONE);
        }
        if(isForView) {
            btnAgregarClienteSecundario.setVisibility(View.GONE);
            tv_agregar_cliente_secundario.setText("Cliente Secundario");
            etComentario.setEnabled(false);
            etDescuento.setEnabled(false);
            etComentario.setText(pedido.getComentario());
            DecimalFormat df = new DecimalFormat("#.00");
            tvTotal.setText(df.format(pedido.getTotalFinal()));
            tvImpuesto.setText(df.format(pedido.getImpuestoPedido()));
            tvTotalBase.setText(df.format(pedido.getTotalNeto()));
            etDescuento.setText(df.format(pedido.getDescuento()));
            etNombreSecundario.setEnabled(false);
            etNumeroDocumentoSecundario.setEnabled(false);
            etTelefonoSecundario.setEnabled(false);
            if(clienteSecundario!=null) {
                etNombreSecundario.setText(clienteSecundario.getRazonSocial());
               // etNumeroDocumentoSecundario.setText(clienteSecundario.getNumeroDocumento());
                etTelefonoSecundario.setText(clienteSecundario.getTelefono());
            }
            else{
                lnr_agregar_cliente.setVisibility(View.GONE);
                rlvAgregarCliente.setVisibility(View.GONE);
            }
        }
    }

    //----------EVENTOS PARA QUE SE DESPLIGUEN LOS CAMPOS DE TEXTO DEL CLIENTE Y CONTACTO----------
    private View.OnClickListener ivExpandableDatos_Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ExpandableRelativeLayout expandableRelative = null;
            ImageView ivExpandable = null;

            switch (v.getId()) {
                case R.id.order_detail_cliente_rlv:
                    expandableRelative = view.findViewById(R.id.order_detail_datos_exp_cliente);
                    ivExpandable = view.findViewById(R.id.iv_arrow_order_detail_cliente);
                    break;
                case R.id.order_detail_datos_administrativos_rlv:
                    expandableRelative = view.findViewById(R.id.order_detail_datos_exp_administrativos);
                    ivExpandable = view.findViewById(R.id.iv_arrow_order_detail_administrativos);
                    break;
                case R.id.order_detail_cliente_secundario_rlv:
                    expandableRelative = view.findViewById(R.id.order_detail_datos_exp_cliente_secundario);
                    ivExpandable = view.findViewById(R.id.iv_arrow_order_detail_add_cliente_secundario);
                    break;
                case R.id.order_detail_cliente_datos_despacho_rlv:
                    expandableRelative = view.findViewById(R.id.order_detail_datos_exp_datos_despacho);
                    ivExpandable = view.findViewById(R.id.iv_arrow_order_detail_datos_despacho);
                    break;
            }

            if (expandableRelative != null) {
                expandableRelative.toggle();
                if (expandableRelative.isExpanded())
                    ivExpandable.animate().rotation(180).start();
                else {
                    ivExpandable.animate().rotation(0).start();
                    if (expandableRelative.getId() != R.id.customer_lead_expDatosLead) {
                        svDatos.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                svDatos.fullScroll(View.FOCUS_DOWN);
                            }
                        }, 400);
                    }
                }
            }
        }
    };
    //--------------FIN EVENTOS DE DISPLIEGUE DE LOS CAMPOS DE TEXTOS DEL CLIENTE Y CONTACTO-------

    public void ActualizarTotales(ArrayList<DetallePedido> listaDetalles) {
        totalBase = 0;
        totalImpuesto = 0;
        total = 0;
        if(listaDetalles != null) {
            for(DetallePedido d: listaDetalles) {
                totalBase += d.getSubtotalNeto();
                totalImpuesto += d.getImpuestoDetallePedido();
                total += d.getSubtotalFinal();
            }
        }
        DecimalFormat df = new DecimalFormat("#.00");
        tvTotalBase.setText(df.format(totalBase));
        tvImpuesto.setText(df.format(totalImpuesto));
        tvTotal.setText(df.format(total));
    }

    private View.OnClickListener btnAgregarSecundario_Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
            View mView = getLayoutInflater().inflate(R.layout.dialog_main_clients, null);

            ((TextView)mView.findViewById(R.id.tv_titulo_lista_clientes)).setText("CLIENTES SECUNDARIOS");

            rvClientesSecundarios = mView.findViewById(R.id.rv_lista_select_clients);
            rvClientesSecundarios.setLayoutManager(new LinearLayoutManager(getContext()));
            final List<Cliente> clientesSecundarios = new ArrayList<>();
            //List<ClientePrincipalSecundario> clientePrincipalSecundarios = cliente.listarClientesSecundarios();
           /* for(ClientePrincipalSecundario cps: clientePrincipalSecundarios) {
                clientesSecundarios.add(cps.getClienteSecundario());
            }*/
            selectClientAdapter = new SelectClientAdapter(clientesSecundarios, null, true);
            selectClientAdapter.setOrderDetailFragment(OrderDetailFragment.this);
            rvClientesSecundarios.setAdapter(selectClientAdapter);

            //HABILITAR EL BOTON DE AGREGAR CLIENTE SECUNDARIO
            Button btnAgregarClienteSecundario = mView.findViewById(R.id.btn_agregar_cliente_seleccionado);
            btnAgregarClienteSecundario.setVisibility(View.GONE);
            mBuilder.setView(mView);
            dialog = mBuilder.create();
            dialog.show();
        }
    };

    public void mostarClienteSecundario() {
        clienteSecundarioSeleccionado = selectClientAdapter.getClienteSeleccionadoPedido();
        if(clienteSecundarioSeleccionado != null) {
            etNombreSecundario.setText(clienteSecundarioSeleccionado.getRazonSocial());
           // etNumeroDocumentoSecundario.setText(clienteSecundarioSeleccionado.getNumeroDocumento());
            etTelefonoSecundario.setText(clienteSecundarioSeleccionado.getTelefono());
        }
        dialog.dismiss();
    }
}
