package com.ndp.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import java.util.ArrayList;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.ClientePrincipalSecundario;
import com.ndp.models.model.Direccion;
import com.ndp.ui.adapters.AddressAdapter;
import com.ndp.ui.adapters.MainClientAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClienteDetailFragment extends Fragment {

    private View view;

    private Cliente cliente;

    private TextView tvRazonSocial;
    private TextView tvNumeroDocumento;
    private TextView tvCorreo;
    private TextView tvDeuda;
    private TextView tvLineaCredito;
    private TextView tvSaldoCredito;
    private TextView tvComentario;
    private TextView tvTituloListaClientes;
    private TextView tvNombreContacto;
    private TextView tvCargoContacto;
    private TextView tvTelefonoContacto;
    private TextView tvCorreoContacto;

    private RecyclerView rvDirecciones;
    private RecyclerView rvListaClientes;

    private ScrollView svDatos;

    private List<Direccion> listaDirecciones;

    private AddressAdapter addressAdapter;
    private MainClientAdapter mainClientAdapter;

    public ClienteDetailFragment() {
        // Required empty public constructor
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_cliente_detail, container, false);
        setElements();
        showClient();
        return this.view;
    }

    private void setElements() {
        setTextViews();
        setRecyclerViews();
        svDatos = view.findViewById(R.id.sv_detalle_cliente);
        setRelativeLayouts();
    }

    private void setTextViews() {
        tvRazonSocial = view.findViewById(R.id.tv_detalle_cliente_razon_social);
        tvNumeroDocumento = view.findViewById(R.id.tv_detalle_cliente_numero_documento);
        tvCorreo = view.findViewById(R.id.tv_detalle_cliente_correo);
        tvDeuda = view.findViewById(R.id.tv_detalle_cliente_deuda);
        tvLineaCredito = view.findViewById(R.id.tv_detalle_cliente_linea_credito);
        tvSaldoCredito = view.findViewById(R.id.tv_detalle_cliente_saldo_credito);
        tvComentario = view.findViewById(R.id.tv_detalle_cliente_comentario);
        tvTituloListaClientes = view.findViewById(R.id.tv_detalle_cliente_etiqueta_lista_clientes);
        tvNombreContacto = view.findViewById(R.id.tv_detalle_cliente_nombre_contacto);
        tvCargoContacto = view.findViewById(R.id.tv_detalle_cliente_cargo_contacto);
        tvTelefonoContacto = view.findViewById(R.id.tv_detalle_cliente_telefono_contacto);
        tvCorreoContacto = view.findViewById(R.id.tv_detalle_cliente_correo_contacto);
    }

    private void setRecyclerViews() {
        rvDirecciones = view.findViewById(R.id.rv_detalle_cliente_lista_direcciones);
        rvDirecciones.setLayoutManager(new LinearLayoutManager(getContext()));
        rvListaClientes = view.findViewById(R.id.rv_detalle_cliente_lista_clientes);
        rvListaClientes.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void setRelativeLayouts() {
        view.findViewById(R.id.rlv_detalle_cliente_datos).setOnClickListener(ivExpandableDatos_Click);
        view.findViewById(R.id.rlv_detalle_cliente_direcciones).setOnClickListener(ivExpandableDatos_Click);
        view.findViewById(R.id.rlv_detalle_cliente_lista_clientes).setOnClickListener(ivExpandableDatos_Click);
        view.findViewById(R.id.rlv_detalle_cliente_contacto).setOnClickListener(ivExpandableDatos_Click);
    }

    private void showClient() {
        if(cliente == null) return;
        tvRazonSocial.setText(cliente.getRazonSocial());
    //    tvNumeroDocumento.setText(cliente.getNumeroDocumento());
        tvCorreo.setText(cliente.getCorreo());
        tvDeuda.setText(String.valueOf(cliente.getDeuda()));
        tvLineaCredito.setText(String.valueOf(cliente.getLineaCredito()));
        tvSaldoCredito.setText(String.valueOf(cliente.getSaldoCredito()));
        tvComentario.setText(cliente.getComentario());

        listaDirecciones = new ArrayList<>();
        listaDirecciones = Direccion.find(Direccion.class, "cliente = ?",
                String.valueOf(cliente.getId()));
        ArrayList<Direccion> mostrarDirecciones = new ArrayList<>();
        for (Direccion d: listaDirecciones) {
            mostrarDirecciones.add(d);
        }
        addressAdapter = new AddressAdapter(mostrarDirecciones);
        addressAdapter.setForNewClient(false);
        rvDirecciones.setAdapter(addressAdapter);

        List<ClientePrincipalSecundario> clientePrincipalSecundarios;
      /*  if(cliente.getTipoCliente() == 1) { //Principal, listar los secundarios
            tvTituloListaClientes.setText("Clientes Secundarios");
            clientePrincipalSecundarios = ClientePrincipalSecundario.find(ClientePrincipalSecundario.class,
                    "cliente_principal = ?", String.valueOf(cliente.getId()));
        }
        else {
            clientePrincipalSecundarios = ClientePrincipalSecundario.find(ClientePrincipalSecundario.class,
                    "cliente_secundario = ?", String.valueOf(cliente.getId()));
        }*/

        ArrayList<Cliente> mostrarClientes = new ArrayList<>();
        /*for(ClientePrincipalSecundario ownClient: clientePrincipalSecundarios) {
            Cliente c = new Cliente();
            if(cliente.getTipoCliente() == 1) {
                c = ownClient.getClienteSecundario();
            } else {
                c = ownClient.getClientePrincipal();
            }
            mostrarClientes.add(c);
        }*/
        mainClientAdapter = new MainClientAdapter(mostrarClientes);
        mainClientAdapter.setForNewClient(false);
        rvListaClientes.setAdapter(mainClientAdapter);

        /*if(cliente.getPersonaContacto() != null) {
            tvNombreContacto.setText(cliente.getPersonaContacto().getNombre());
            tvCargoContacto.setText(cliente.getPersonaContacto().getCargo());
            tvTelefonoContacto.setText(cliente.getPersonaContacto().getTelefono());
            tvCorreoContacto.setText(cliente.getPersonaContacto().getCorreo());
        }*/
    }

    //----------EVENTOS PARA QUE SE DESPLIGUEN LOS CAMPOS DE TEXTO DEL CLIENTE Y CONTACTO----------
    private View.OnClickListener ivExpandableDatos_Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ExpandableRelativeLayout expandableRelative = null;
            ImageView ivExpandable = null;

            switch (v.getId()) {
                case R.id.rlv_detalle_cliente_datos:
                    expandableRelative = view.findViewById(R.id.rlv_detalle_cliente_expDatos);
                    ivExpandable = view.findViewById(R.id.iv_detalle_cliente_datos);
                    break;
                case R.id.rlv_detalle_cliente_direcciones:
                    expandableRelative = view.findViewById(R.id.rlv_detalle_cliente_expDirecciones);
                    ivExpandable = view.findViewById(R.id.iv_detalle_cliente_direcciones);
                    break;
                case R.id.rlv_detalle_cliente_lista_clientes:
                    expandableRelative = view.findViewById(R.id.rlv_detalle_cliente_expClientes);
                    ivExpandable = view.findViewById(R.id.iv_detalle_cliente_lista_clientes);
                    break;
                case R.id.rlv_detalle_cliente_contacto:
                    expandableRelative = view.findViewById(R.id.rlv_detalle_cliente_expContacto);
                    ivExpandable = view.findViewById(R.id.iv_detalle_cliente_contacto);
                    break;
            }

            if (expandableRelative != null) {
                expandableRelative.toggle();
                if (expandableRelative.isExpanded())
                    ivExpandable.animate().rotation(180).start();
                else {
                    ivExpandable.animate().rotation(0).start();
                    if (expandableRelative.getId() != R.id.customer_lead_expDatosLead) {
                        svDatos.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                svDatos.fullScroll(View.FOCUS_DOWN);
                            }
                        }, 400);
                    }
                }
            }
        }
    };
    //--------------FIN EVENTOS DE DISPLIEGUE DE LOS CAMPOS DE TEXTOS DEL CLIENTE Y CONTACTO-------
}
