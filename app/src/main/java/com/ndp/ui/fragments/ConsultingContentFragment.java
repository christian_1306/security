package com.ndp.ui.fragments;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ndp.R;
import com.ndp.models.model.Vendedor;
import com.ndp.ui.adapters.PagerAdapterConsulting;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConsultingContentFragment extends Fragment {

    private View view;
    private Vendedor vendedor;

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public ConsultingContentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_consulting_content, container, false);
        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("PEDIDOS GENERADOS"));
        tabLayout.addTab(tabLayout.newTab().setText("VISITAS REALIZADAS"));
        tabLayout.addTab(tabLayout.newTab().setText("ESTADOS DE CUENTA"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ViewPager viewPager = (ViewPager)view.findViewById(R.id.pager);
        final PagerAdapterConsulting adapter = new PagerAdapterConsulting(
                getActivity().getSupportFragmentManager(), tabLayout.getTabCount());
        adapter.setVendedor(vendedor);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return view;
    }

}
