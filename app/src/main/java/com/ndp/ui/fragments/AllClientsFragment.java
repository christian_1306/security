package com.ndp.ui.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.User;
import com.ndp.ui.activities.OrderSeller;
import com.ndp.ui.activities.VisitActivity;
import com.ndp.ui.adapters.ClienteAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllClientsFragment extends Fragment {

    private RecyclerView rvClientes;
    private ClienteAdapter clienteAdapter;
    private View view;
    private int id_fragmento;
    private OrderSeller orderSellerActivity;
    private VisitActivity visitActivity;
    private EditText et_buscar;
    private ImageView iv_buscar,ivRetroceder;

    private User usuario; //Esto es para cuando se genera un pedido

    public AllClientsFragment() {
        // ***Required empty public constructor***
    }

    public OrderSeller getOrderSellerActivity() {
        return orderSellerActivity;
    }

    public void setOrderSellerActivity(OrderSeller orderSellerActivity) {
        this.orderSellerActivity = orderSellerActivity;
    }

    public int getId_fragmento() {
        return id_fragmento;
    }

    public void setId_fragmento(int id_fragmento) {
        this.id_fragmento = id_fragmento;
    }

    public VisitActivity getVisitActivity() {
        return visitActivity;
    }

    public void setVisitActivity(VisitActivity visitActivity) {
        this.visitActivity = visitActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_all_clients, container, false);
        setElements();
        fillRecyclerView();
        //rvClientes.setHasFixedSize(true);
        return view;
    }

    //Este método llena los elementos del Recycler View con los datos de la tabla Cliente
    private void fillRecyclerView() {
        rvClientes.setLayoutManager(new LinearLayoutManager(getContext()));
        clienteAdapter = new ClienteAdapter(obtenerClientes(), this, null);
        clienteAdapter.setActivityOrderSeller(this.orderSellerActivity);
        rvClientes.setAdapter(clienteAdapter);
    }


    //Este método obtiene los clientes de la Tabla Cliente
    private List<Cliente> obtenerClientes(){
        List<Cliente> listaClientes = new ArrayList<>();
        if(id_fragmento == 1)
            listaClientes = Cliente.listAll(Cliente.class);
        else if(id_fragmento == 2)
            //listaClientes = Cliente.find(Cliente.class, "tipo_cliente = 1");
            listaClientes = Cliente.listAll(Cliente.class);
        return listaClientes;
    }

    private void setElements() {
        try {
            ivRetroceder = view.findViewById(R.id.iv_back_toolbar);
            ivRetroceder.setOnClickListener(cerrarFragment);
        }catch (Exception ex){
            ex.getMessage();
        }
        rvClientes = view.findViewById(R.id.rvClientes);
        ((TextView)getActivity().findViewById(R.id.tv_title_toolbar)).setText("CLIENTES");
        et_buscar = view.findViewById(R.id.barSearch_etBuscar);
        et_buscar.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(et_buscar,InputMethodManager.SHOW_IMPLICIT);
                et_buscar.requestFocus();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= et_buscar.getRight() - et_buscar.getTotalPaddingRight()) {
                        //CODIGO
                        initializeData(true);
                        return true;
                    }
                }
                else{
                    //et_buscar.requestFocus();
                }
                return true;
            }
        });
        //        ((ImageView)getActivity().findViewById(R.id.iv_back_toolbar)).setOnClickListener(CommonMethods.backFragment(this, ));
    }

    public void openDetailItem(Cliente cliente) {
//        ((TabLayout)view.findViewById(R.id.tabs)).setVisibility(View.INVISIBLE);
        ClienteDetailFragment clienteDetailFragment = new ClienteDetailFragment();
        clienteDetailFragment.setCliente(cliente);
        getActivity().getSupportFragmentManager().beginTransaction()
                .add(R.id.content_all_clients, clienteDetailFragment).addToBackStack(null).commit();
//        view.findViewById(R.id.tabs).setVisibility(View.INVISIBLE);
    }

    private void initializeData(boolean filtrar){
        try{
            if(filtrar){
                String buscar = et_buscar.getText().toString();
                String query = null;
                if(id_fragmento==1) query = "SELECT * FROM cliente where razon_social like '" + buscar + "%'";
                else if(id_fragmento==2) query = "SELECT * FROM cliente where razon_social like '" + buscar + "%'";
                List<Cliente> listaClientes = null;
                listaClientes = Cliente.findWithQuery(Cliente.class,query);
                clienteAdapter = new ClienteAdapter(listaClientes, this, null);
                rvClientes.setAdapter(clienteAdapter);
            }
        }catch(Exception ex){

        }
    }

    private View.OnClickListener cerrarFragment = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(id_fragmento==2){
                for(Fragment fragment: getActivity().getSupportFragmentManager().getFragments()){
                    getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                }
            }
            else getActivity().getSupportFragmentManager().popBackStackImmediate();
        }
    };

    public User getUsuario() {
        return usuario;
    }

    public void setUsuario(User usuario) {
        this.usuario = usuario;
    }
}
