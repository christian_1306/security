package com.ndp.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.Vendedor;
import com.ndp.models.model.Visita;
import com.ndp.ui.adapters.VisitsMadeAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class VisitsMadeFragment extends Fragment {

    private View view;
    private RecyclerView rvVisitsMade;
    private VisitsMadeAdapter visitsMadeAdapter;
    private boolean isPending;
    private Cliente cliente;
    private Vendedor vendedor;

    public boolean isPending() {
        return isPending;
    }

    public void setPending(boolean pending) {
        isPending = pending;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public VisitsMadeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_visits_made, container, false);
        setElements();
        fillRecyclerView();
        return view;
    }

    private void fillRecyclerView() {
        rvVisitsMade.setLayoutManager(new LinearLayoutManager(getContext()));
        List<Visita> visitas = new ArrayList<>();
        if(cliente == null) //ES PARA EL VENDEDOR
            visitas = Visita.find(Visita.class,"vendedor = ?",String.valueOf(vendedor.getId()));
        else
            visitas = Visita.find(Visita.class, "cliente = ?", String.valueOf(cliente.getId()));
        visitsMadeAdapter = new VisitsMadeAdapter(visitas, this,this.isPending);
        rvVisitsMade.setAdapter(visitsMadeAdapter);
    }

    private void setElements() {
        rvVisitsMade = view.findViewById(R.id.rvVisitsMade);
    }


}
