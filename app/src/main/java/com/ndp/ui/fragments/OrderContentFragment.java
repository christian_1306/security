package com.ndp.ui.fragments;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.Vendedor;
import com.ndp.ui.adapters.PagerAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderContentFragment extends Fragment {

    private View view;
    private Cliente cliente;
    private Vendedor vendedor = null;
    private boolean isForClient = false;
    private ImageView ivRetroceder;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public boolean isForClient() {
        return isForClient;
    }

    public void setForClient(boolean forClient) {
        isForClient = forClient;
    }

    public OrderContentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_order_content, container, false);
        setElements();
        return view;
    }

    private void setElements() {
        //SET TABS
        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        //ivRetroceder = view.findViewById(R.id.iv_back_toolbar);
        //ivRetroceder.setOnClickListener(cerrarFragment);
//        tabLayout.addTab(tabLayout.newTab().setText("Artículo"));
//        tabLayout.addTab(tabLayout.newTab().setText("Pedido"));
//        tabLayout.addTab(tabLayout.newTab().setText("Resumen"));
//        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ViewPager viewPager = (ViewPager)view.findViewById(R.id.pager);

        final PagerAdapter adapter = new PagerAdapter(
                getActivity().getSupportFragmentManager(), tabLayout.getTabCount(), this.cliente, this.vendedor, this.isForClient);
//        adapter.setCliente(this.cliente);
//        adapter.setVendedor(this.vendedor);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
//        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                viewPager.setCurrentItem(tab.getPosition());
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });
        ((TextView)getActivity().findViewById(R.id.tv_title_toolbar)).setText("PEDIDO");
//        ((ImageView)getActivity().findViewById(R.id.iv_back_toolbar)).setOnClickListener(CommonMethods.backFragment(this, 0));
    }

    private View.OnClickListener cerrarFragment = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getActivity().getSupportFragmentManager().popBackStackImmediate();
        }
    };
}
