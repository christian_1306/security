package com.ndp.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.ndp.R;
import com.ndp.data.api.SyncData;
import com.ndp.models.appRest.apiOrderDetail;
import com.ndp.models.appRest.apiOrderHeader;
import com.ndp.models.model.ArticuloOrder;
import com.ndp.models.model.Direccion;
import com.ndp.models.model.User;
import com.ndp.utils.Popup.Popup;
import com.ndp.utils.methods.MoneyValueFilter;
import com.ndp.utils.use.usePhone;

import static com.ndp.utils.Const.TYPE_WAREHOUSE_ALMACEN;
import static com.ndp.utils.Const.TYPE_WAREHOUSE_FISCAL;
import static com.ndp.utils.Useful.getCol;
import static com.ndp.utils.Useful.validarSiEsCorreo;
import static com.ndp.utils.use.useAPP.fmDecimal;
import static com.ndp.utils.use.useAPP.getDate;
import static com.ndp.utils.use.useAPP.getTodayFormat;
import static com.ndp.utils.use.usePhone.getIMEI;
import static com.ndp.utils.use.usePhone.hideKeyBoard;


public class OrderSumFragment extends Fragment {

    private RelativeLayout order_summary_dir;
    private RelativeLayout order_header_admin;
    private ExpandableRelativeLayout order_summary_admin;
    private ExpandableRelativeLayout order_detail_admin;
    private ImageView order_summary_dir_arrow;
    private ImageView order_header_dir_arrow;
    private TextView tv_dir_email;
    private TextView tv_fecha_entrega;
    private TextView tv_dir_ship;
    private TextView tv_dir_pay;
    private EditText et_sum_email;
    private EditText et_dir_ship;
    private EditText et_dir_pay;
    private TextView tv_cant_order;
    private TextView tv_texto_total_base;
    private TextView tv_texto_impuesto;
    private EditText et_texto_descuento;
    private EditText et_escribe_comentario;
    private TextView tv_texto_total;
    private TextView tv_texto_total_base_sin_descuento;
    private RelativeLayout rlt_descuento;
    private Calendar calendar;
    private View v;
    private Activity _self;
    private Menu menuSummary;
    private boolean isEditableDirShip = false;
    private boolean isEditableDirPay = false;
    private boolean isEditableMail = false;
    private OrderDetailsFragment orderDetailsFragment;
    private OrderHeaderFragment orderHeaderFragment;
    private int cantidadItemsOrder = 0;
    private double netoItemsOrderSinDscto = 0;
    private double netoItemsOrder = 0;
    private double impuestoItemsOrder = 0;
    private double totalItemsOrder = 0;
    private double descuentoItemsOrder = 0;
    private apiOrderHeader apiOrderHeader;
    private ArrayList<apiOrderDetail> detallesPedido;
    private User currentUser;

    public OrderSumFragment() {
        // ***Required empty public constructor***
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_order_sum, container, false);
        setElements();
        setHasOptionsMenu(true);
        return v;
    }

    private void setElements()
    {
        order_summary_dir = v.findViewById(R.id.order_summary_dir);
        order_header_admin = v.findViewById(R.id.order_header_admin);
        order_summary_admin = v.findViewById(R.id.order_summary_admin);
        order_detail_admin = v.findViewById(R.id.order_detail_admin);
        order_summary_dir_arrow = v.findViewById(R.id.order_summary_dir_arrow);
        order_header_dir_arrow = v.findViewById(R.id.order_header_dir_arrow);
        tv_dir_email = v.findViewById(R.id.tv_dir_email);
        tv_fecha_entrega  = v.findViewById(R.id.tv_fecha_entrega);
        et_sum_email = v.findViewById(R.id.et_sum_email);
        tv_dir_ship = v.findViewById(R.id.tv_dir_ship);
        tv_dir_pay = v.findViewById(R.id.tv_dir_pay);
        et_dir_ship = v.findViewById(R.id.et_dir_ship);
        et_dir_pay = v.findViewById(R.id.et_dir_pay);
        tv_cant_order = v.findViewById(R.id.tv_cant_order);
        tv_texto_total_base_sin_descuento = v.findViewById(R.id.tv_texto_total_base_sin_descuento);
        tv_texto_total_base = v.findViewById(R.id.tv_texto_total_base);
        tv_texto_impuesto = v.findViewById(R.id.tv_texto_impuesto);
        et_texto_descuento = v.findViewById(R.id.et_texto_descuento);
        rlt_descuento = v.findViewById(R.id.rlt_descuento);
        tv_texto_total = v.findViewById(R.id.tv_texto_total);
        et_escribe_comentario = v.findViewById(R.id.et_escribe_comentario);

        tv_fecha_entrega.setOnClickListener(customOnClick);
        tv_dir_email.setOnClickListener(customOnClick);
        order_summary_dir.setOnClickListener(customOnClick);
        order_header_admin.setOnClickListener(customOnClick);
        et_texto_descuento.addTextChangedListener(textWatcher);
        calendar = Calendar.getInstance();

        if (currentUser != null && currentUser.getTipoUsuario() == 2) {
            rlt_descuento.setVisibility(View.GONE);
        }
    }

    private View.OnClickListener customOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.order_summary_dir:
                    order_summary_admin.toggle();
                    if (order_summary_admin.isExpanded())
                        order_summary_dir_arrow.animate().rotation(180).start();
                    else {
                        order_summary_dir_arrow.animate().rotation(0).start();
                    }
                    break;
                case R.id.order_header_admin:
                    order_detail_admin.toggle();
                    if (order_detail_admin.isExpanded())
                        order_header_dir_arrow.animate().rotation(180).start();
                    else {
                        order_header_dir_arrow.animate().rotation(0).start();
                    }
                    break;
                case R.id.tv_dir_email:
                    Popup.ShowDialog(_self,
                            getString(R.string.msg_change_email),
                            getString(R.string.title_change_email),
                            Popup.MSG_TYPE_SUCCESS,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    String email = tv_dir_email.getText().toString();
                                    et_sum_email.setText(email);
                                    tv_dir_email.setVisibility(View.GONE);
                                    et_sum_email.setVisibility(View.VISIBLE);
                                    et_sum_email.requestFocus();
                                    isEditableMail = true;
                                }
                            });
                    break;
                case R.id.tv_fecha_entrega:
                    getDate(_self, tv_fecha_entrega, calendar);
                    break;
            }
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_send, menu);
        menuSummary = menu;
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_send:
                sendOrder();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //region TextWatcher Decimal Discount
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            int cursorPosition = et_texto_descuento.getSelectionEnd();
            String originalStr = et_texto_descuento.getText().toString();

            et_texto_descuento.setFilters(new InputFilter[]{new MoneyValueFilter(Integer.parseInt("2"))});

            try {

                et_texto_descuento.removeTextChangedListener(this);
                String value = et_texto_descuento.getText().toString();

                if (!value.equals("")) {
                    if (value.startsWith(".")) {
                        et_texto_descuento.setText("0.");
                    }
                    if (value.startsWith("0") && !value.startsWith("0.")) {
                        et_texto_descuento.setText("0");
                    }

                    int diff = et_texto_descuento.getText().toString().length() - originalStr.length();
                    et_texto_descuento.setSelection(cursorPosition + diff);

                } else {
                    et_texto_descuento.setText("0");
                }

                String tvdescuento = et_texto_descuento.getText().toString();
                descuentoItemsOrder = tvdescuento.isEmpty() ? 0 : Double.parseDouble(tvdescuento);

                netoItemsOrder = netoItemsOrderSinDscto - descuentoItemsOrder;
                impuestoItemsOrder = netoItemsOrder * 0.13;
                totalItemsOrder = netoItemsOrder + impuestoItemsOrder;

                tv_texto_total_base.setText(fmDecimal(netoItemsOrder));
                tv_texto_impuesto.setText(fmDecimal(impuestoItemsOrder));
                tv_texto_total.setText(fmDecimal(totalItemsOrder));

                et_texto_descuento.addTextChangedListener(this);

            } catch (Exception ex) {

                ex.printStackTrace();
                et_texto_descuento.addTextChangedListener(this);

            }
        }
    };
    //endregion

    public Activity get_self() {
        return _self;
    }

    public void set_self(Activity _self) {
        this._self = _self;
    }

    public void setText(String value){
        tv_dir_email.setText(value);
    }

    public void changeViewDireccion(Boolean isEditableDirShip, Boolean isEditableDirPay){

        if (isEditableDirPay){

            this.isEditableDirPay = isEditableDirPay;

            tv_dir_pay.setVisibility(View.GONE);
            tv_dir_pay.setText("");

            et_dir_pay.setVisibility(View.VISIBLE);
            et_dir_pay.setText("");
            et_dir_pay.setFocusable(false);
            et_dir_pay.setFocusableInTouchMode(true);

        } else {

            this.isEditableDirPay = isEditableDirPay;

            tv_dir_pay.setVisibility(View.VISIBLE);
            tv_dir_pay.setText("");

            et_dir_pay.setVisibility(View.GONE);
            et_dir_pay.setText("");

        }
    }

    public void changeDireccionPrincipal(@NonNull String cardCode){

        List<Direccion> direccionsShip = Direccion.find(Direccion.class,
                "CARD_CODE = ? AND TIPO = ?", cardCode,
                TYPE_WAREHOUSE_ALMACEN);

        if (direccionsShip != null && direccionsShip.size() > 0){
            tv_dir_ship.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Popup.showRadioButtonDialog(_self, direccionsShip, tv_dir_ship, getString(R.string.title_dir_ship));
                }
            });
            tv_dir_ship.setVisibility(View.VISIBLE);
            et_dir_ship.setVisibility(View.GONE);
            tv_dir_ship.setText("");
            et_dir_ship.setText("");
            isEditableDirShip = false;
        } else {
            isEditableDirShip = true;
            tv_dir_ship.setVisibility(View.GONE);
            et_dir_ship.setVisibility(View.VISIBLE);
            tv_dir_ship.setText("");
            et_dir_ship.setText("");
        }
    }

    public void changeDireccionSecond(@NonNull String cardCode) {

        List<Direccion> direccionsBill = Select.from(Direccion.class)
                .where(Condition.prop("tipo").eq(TYPE_WAREHOUSE_FISCAL),
                        Condition.prop(getCol("cardCode")).eq(cardCode))
                .list();

        if (direccionsBill != null && direccionsBill.size() > 0){


            tv_dir_pay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Popup.showRadioButtonDialog(_self, direccionsBill, tv_dir_pay, getString(R.string.title_dir_bill));
                }
            });
            tv_dir_pay.setVisibility(View.VISIBLE);
            et_dir_pay.setVisibility(View.GONE);
            tv_dir_pay.setText("");
            et_dir_pay.setText("");
            isEditableDirPay = false;

        } else {

            isEditableDirPay = true;
            tv_dir_pay.setVisibility(View.GONE);
            et_dir_pay.setVisibility(View.VISIBLE);
            tv_dir_pay.setText("");
            et_dir_pay.setText("");
        }

    }

    public OrderDetailsFragment getOrderDetailsFragment() {
        return orderDetailsFragment;
    }

    public void setOrderDetailsFragment(OrderDetailsFragment orderDetailsFragment) {
        this.orderDetailsFragment = orderDetailsFragment;
    }

    @SuppressLint("SetTextI18n")
    public void setDetailForPay(){
        String tvdescuento = et_texto_descuento.getText().toString();
        ArrayList<ArticuloOrder> orderArrayList = orderDetailsFragment.getArticuloOrdersMARK();

        cantidadItemsOrder = 0;
        netoItemsOrderSinDscto = 0;
        netoItemsOrder = 0;
        impuestoItemsOrder = 0;
        totalItemsOrder = 0;
        descuentoItemsOrder = tvdescuento.isEmpty() ? 0 : Double.parseDouble(tvdescuento);

        if (orderArrayList != null && orderArrayList.size() > 0) {
            for (ArticuloOrder articuloOrder : orderArrayList) {
                double neto = articuloOrder.getCantidadOrder() * articuloOrder.getVarillasPorTonelada() * articuloOrder.getPrecioDefaultNeto();
                cantidadItemsOrder += articuloOrder.getCantidadOrder();
                netoItemsOrderSinDscto += neto;
                netoItemsOrder += neto;
                impuestoItemsOrder += neto * 0.13;
                totalItemsOrder += neto * 1.13;
            }
        }

        if (descuentoItemsOrder > 0){
            netoItemsOrder = netoItemsOrderSinDscto - descuentoItemsOrder;
            impuestoItemsOrder = netoItemsOrder * 0.13;
            totalItemsOrder = netoItemsOrder + impuestoItemsOrder;
        }

        tv_texto_total_base_sin_descuento.setText(fmDecimal(netoItemsOrderSinDscto));
        tv_cant_order.setText(fmDecimal(cantidadItemsOrder) + " " + getString(R.string.TON));
        tv_texto_total_base.setText(fmDecimal(netoItemsOrder));
        tv_texto_impuesto.setText(fmDecimal(impuestoItemsOrder));
        tv_texto_total.setText(fmDecimal(totalItemsOrder));
    }


    public OrderHeaderFragment getOrderHeaderFragment() {
        return orderHeaderFragment;
    }

    public void setOrderHeaderFragment(OrderHeaderFragment orderHeaderFragment) {
        this.orderHeaderFragment = orderHeaderFragment;
    }

    public void sendOrder(){
        hideKeyBoard(_self);
        new validateSendOrder().execute();
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    @SuppressLint("StaticFieldLeak")
    private class validateSendOrder extends AsyncTask<Void, String, Void> {
        private ProgressDialog dialog;
        private String errMsg = "";
        private StringBuilder errMsgMultiple = new StringBuilder();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(_self);
            dialog.setTitle("Operación en curso");
            dialog.setMessage("Mensaje");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            dialog.setMessage(values[0]);
        }

        @Override
        protected Void doInBackground(Void... args) {
            try {

                publishProgress("Obteniendo informacion ...");
                apiOrderHeader = new apiOrderHeader();
                detallesPedido = new ArrayList<>();
                int numberError = 0;
                //TODO: Cabecera
                apiOrderHeader = orderHeaderFragment.getApiOrderHeader();
                apiOrderHeader.setNomDirEntrega(getString(R.string.ship));
                apiOrderHeader.setNomDirFacturacion(getString(R.string.pay));
                if (isEditableDirShip) apiOrderHeader.setDireccionEntrega(et_dir_ship.getText().toString());
                if (!isEditableDirShip) apiOrderHeader.setDireccionEntrega(tv_dir_ship.getText().toString());
                if (isEditableDirPay) apiOrderHeader.setDireccionFacturacion(et_dir_pay.getText().toString());
                if (!isEditableDirPay) apiOrderHeader.setDireccionFacturacion(tv_dir_pay.getText().toString());
                apiOrderHeader.setFechaDocumento(getTodayFormat(getString(R.string.format_date_app)));
                apiOrderHeader.setFechaEntrega(tv_fecha_entrega.getText().toString());
                apiOrderHeader.setComentario(et_escribe_comentario.getText().toString());
                if (isEditableMail) apiOrderHeader.setCorreoNotificacion(et_sum_email.getText().toString());
                if (!isEditableMail) apiOrderHeader.setCorreoNotificacion(tv_dir_email.getText().toString());
                apiOrderHeader.setTerminalCreacion(getIMEI(_self));
                apiOrderHeader.setMoneda(getString(R.string.currency_bolivianos));
                apiOrderHeader.setTotalNeto(netoItemsOrder);
                apiOrderHeader.setDescuento(descuentoItemsOrder);
                apiOrderHeader.setImpuestoPedido(impuestoItemsOrder);
                apiOrderHeader.setTotalFinal(totalItemsOrder);
                //TODO: Detalle
                detallesPedido = orderDetailsFragment.getDetallesPedido();
                apiOrderHeader.setDetallesPedido(detallesPedido);


                publishProgress("Validando Cliente ...");
                if (apiOrderHeader.getCardCode() == null || apiOrderHeader.getCardCode().isEmpty()){
                    numberError ++;
                    errMsgMultiple.append(numberError).append(".- Código cliente inválido.");
                }

                if (apiOrderHeader.getCardName() == null || apiOrderHeader.getCardName().isEmpty()){
                    errMsgMultiple.append("\n");
                    numberError ++;
                    errMsgMultiple.append(numberError).append(".- Razón social inválido.");
                }

                if (apiOrderHeader.getNit() == null || apiOrderHeader.getNit().isEmpty()){
                    errMsgMultiple.append("\n");
                    numberError ++;
                    errMsgMultiple.append(numberError).append(".- NIT inválido.");
                }

                publishProgress("Validando Direcciones ...");
                if (apiOrderHeader.getDireccionEntrega() == null || apiOrderHeader.getDireccionEntrega().isEmpty()){
                    errMsgMultiple.append("\n");
                    numberError ++;
                    errMsgMultiple.append(numberError).append(".- Dirección de entrega inválido.");
                }

                if (apiOrderHeader.getDireccionFacturacion() == null || apiOrderHeader.getDireccionFacturacion().isEmpty()){
                    /*errMsgMultiple.append("\n");
                    numberError ++;
                    errMsgMultiple.append(numberError).append(".- Dirección de facturación inválido.");*/
                    apiOrderHeader.setDireccionFacturacion("SDF");
                }

                publishProgress("Validando Fechas ...");
                if (apiOrderHeader.getFechaEntrega() == null || apiOrderHeader.getFechaEntrega().isEmpty()){
                    errMsgMultiple.append("\n");
                    numberError ++;
                    errMsgMultiple.append(numberError).append(".- Fecha de entrega inválido.");
                }

                publishProgress("Validando Correo ...");
                if (apiOrderHeader.getCorreoNotificacion() == null || apiOrderHeader.getCorreoNotificacion().isEmpty() ||
                        !validarSiEsCorreo(apiOrderHeader.getCorreoNotificacion())){
                    errMsgMultiple.append("\n");
                    numberError ++;
                    errMsgMultiple.append(numberError).append(".- Correo a notificar inválido.");
                }

                publishProgress("Validando Items ...");
                if (detallesPedido == null || detallesPedido.size() <= 0){
                    errMsgMultiple.append("\n");
                    numberError ++;
                    errMsgMultiple.append(numberError).append(".- Pedido sin items.");
                }

                if (apiOrderHeader.getDescuento() > apiOrderHeader.getTotalNeto()){
                    errMsgMultiple.append("\n");
                    numberError ++;
                    errMsgMultiple.append(numberError).append(".- Descuento no debe superar Total Neto.");
                }

                if (cantidadItemsOrder > 30 && currentUser.getTipoUsuario() != null && currentUser.getTipoUsuario() != 2) {
                    errMsgMultiple.append("\n");
                    numberError ++;
                    errMsgMultiple.append(numberError).append(".- Cantidad de pedido supera las 30 Toneladas.");
                }


            }catch (Exception ex){
                errMsg = (ex.getMessage() != null) ? ex.getMessage() : "-" ;
            }
            return null;
        }

        protected void onPostExecute(Void result) {

            if (dialog.isShowing() && dialog!=null) {
                dialog.dismiss();
                if (!errMsg.isEmpty() || errMsgMultiple.length() != 0) {
                    Popup.showAlert(_self, errMsg + "\n" + errMsgMultiple + "\n");
                } else {
                    new InternetCheck().execute();
                }
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class InternetCheck extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            return usePhone.isOnline();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (!result) {
                Popup.ShowDialogCustomButton(_self,
                        getString(R.string.mensaje_sin_internet_pedido),
                        getString(R.string.titulo_send_order),
                        getString(R.string.save),getString(R.string.reload),
                        Popup.MSG_TYPE_WARNING,
                        Popup.BUTTON_TYPE_ACCEPT,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                apiOrderHeader.save();
                                for (apiOrderDetail detail : detallesPedido) {
                                    detail.save();
                                }
                                _self.finish();
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                new InternetCheck().execute();
                            }
                        },null);
            } else {
                Popup.ShowDialog(_self,
                        getString(R.string.mensaje_send_order),
                        getString(R.string.titulo_send_order),
                        Popup.MSG_TYPE_SUCCESS,
                        Popup.BUTTON_TYPE_ACCEPT,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                new SendOrderService().execute();
                            }
                        },
                        null);
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    private class SendOrderService extends AsyncTask<Void, Void, Boolean> {
        private ProgressDialog dialog;
        StringBuilder outMessage = new StringBuilder();
        String errMsg = "";
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(_self, R.style.ProgressDialog);
            dialog.setTitle("Pedido");
            dialog.setMessage("Enviando ...");
            dialog.setCancelable(false);
            dialog.show();
        }

        protected Boolean doInBackground(Void... args) {
            try {
                return new SyncData(_self, 0)
                        .SyncSaveOrder(outMessage, apiOrderHeader);
            } catch (Exception e){
                errMsg = e.getMessage();
                return false;
            }
        }

        protected void onPostExecute(Boolean result) {
            String msgException = "";
            if (result) {
                try {
                    menuSummary.findItem(R.id.action_send).setEnabled(false);
                    Popup.ShowDialogHideButton(_self,
                            getString(R.string.mensaje_order_reg),
                            getString(R.string.titulo_order_reg),
                            getString(R.string.ok),"",
                            Popup.BUTTON_TYPE_ACCEPT,
                            Popup.MSG_TYPE_SUCCESS,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    _self.finish();
                                }
                            },null,null);
                } catch (Exception ex) {
                    msgException = getString(R.string.exception) + ex.getMessage();
                }
            }

            if (dialog.isShowing()) {
                dialog.dismiss();
                if (!result || !msgException.isEmpty() || !errMsg.isEmpty()) {
                    menuSummary.findItem(R.id.action_send).setEnabled(true);
                    Popup.showAlert(_self, outMessage.toString() + "\n" + msgException + "\n" + errMsg);
                }
            }

        }
    }
}
