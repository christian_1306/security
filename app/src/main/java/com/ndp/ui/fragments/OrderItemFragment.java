package com.ndp.ui.fragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.ndp.R;
import com.ndp.models.model.Articulo;
import com.ndp.models.model.ArticuloXAlmacen;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.DetallePedido;
import com.ndp.models.model.Tarifario;
import com.ndp.models.model.Vendedor;
import com.ndp.ui.adapters.ItemOldAdapter;
import com.ndp.ui.adapters.OrderItemAdapter;
import com.ndp.ui.adapters.ViewItemAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderItemFragment extends Fragment {

    private View view;
//    private ArrayList<ArticuloXAlmacen> testItems;
    private boolean isItemForOrder;
    private  boolean isForView = false;
    private EditText et_buscar;
    private List<DetallePedido> detallePedidos;
    private View layout_search;
    private Button btn_agregar,btn_cancelar;

    public List<DetallePedido> getDetallePedidos() {
        return detallePedidos;
    }

    public void setDetallePedidos(List<DetallePedido> detallePedidos) {
        this.detallePedidos = detallePedidos;
    }

    public OrderItemAdapter getOrderItemAdapter() {
        return orderItemAdapter;
    }

    public void setOrderItemAdapter(OrderItemAdapter orderItemAdapter) {
        this.orderItemAdapter = orderItemAdapter;
    }

    public boolean isForView() {
        return isForView;
    }

    public void setForView(boolean forView) {
        isForView = forView;
    }

    private boolean fragmentoIniciado = false;
    private long id_tarifario;

    private Spinner spTarifario;
    private RelativeLayout rlTarifario;
    private RelativeLayout rlv_titulo;
    private TextView tvTituloAlmacen;
    private RecyclerView rvOrderItems;

    private Tarifario tarifario;
    private Vendedor vendedor = null;
    private Cliente cliente = null;

    private OrderItemAdapter orderItemAdapter;
    private ItemOldAdapter itemOldAdapter;
    private List<Tarifario> listaTarifarios;
    private List<Articulo> articulos;
    private ArrayList<DetallePedido> listaDetalles;
    private int cantidad_toneladas = 0;
    private OrderDetailFragment orderDetailFragment;
    private AlertDialog dialog;

    private RecyclerView rvPopUp;

    public OrderItemFragment() {
        // Required empty public constructor
        this.detallePedidos = new ArrayList<>();
    }

    public long getId_tarifario() {
        return id_tarifario;
    }

    public void setId_tarifario(long id_tarifario) {
        this.id_tarifario = id_tarifario;
    }

    public boolean isItemForOrder() {
        return isItemForOrder;
    }

    public void setItemForOrder(boolean itemForOrder) {
        isItemForOrder = itemForOrder;
    }

    public Tarifario getTarifario() {
        return tarifario;
    }

    public void setTarifario(Tarifario tarifario) {
        this.tarifario = tarifario;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ArrayList<DetallePedido> getListaDetalles() {
        if(orderItemAdapter == null) return null;
        return orderItemAdapter.getListaDetalles();
    }

    public OrderDetailFragment getOrderDetailFragment() {
        return orderDetailFragment;
    }

    public void setOrderDetailFragment(OrderDetailFragment orderDetailFragment) {
        this.orderDetailFragment = orderDetailFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_order_item, container, false);
        setElements();
        return view;
    }

    public boolean isFragmentoIniciado() {
        return fragmentoIniciado;
    }

    public void setFragmentoIniciado(boolean fragmentoIniciado) {
        this.fragmentoIniciado = fragmentoIniciado;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setElements() {
        rvOrderItems = view.findViewById(R.id.rv_lista_order_items);
        rvOrderItems.setLayoutManager(new LinearLayoutManager(getActivity()));
        rlv_titulo = view.findViewById(R.id.rlv_titulo);
        et_buscar = view.findViewById(R.id.barSearch_etBuscar);
        et_buscar.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(et_buscar,InputMethodManager.SHOW_IMPLICIT);
                et_buscar.requestFocus();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= et_buscar.getRight() - et_buscar.getTotalPaddingRight()) {
                        //CODIGO
                        initializeData(true);
                        return true;
                    }
                }
                return true;
            }
        });
        tvTituloAlmacen = view.findViewById(R.id.tv_order_warehouse);
        rlTarifario = view.findViewById(R.id.rl_order_tarifario);
        spTarifario = view.findViewById(R.id.sp_order_tarifario);
        layout_search = view.findViewById(R.id.toolbar_search_item);
        articulos = Articulo.listAll(Articulo.class);
        if(!fragmentoIniciado) {
            if(vendedor != null) { //Nos encontramos en el perfil del vendedor
                orderItemAdapter = new OrderItemAdapter(articulos, this, true, vendedor.getAlmacen());
//                itemOldAdapter = new ItemOldAdapter(articulos, this, isItemForOrder, vendedor.getAlmacen());
//                itemOldAdapter.setForSeller(true);
            }
            else { //Nos encontramos en el perfil del cliente
            //    orderItemAdapter = new OrderItemAdapter(articulos, this, false, cliente.getAlmacen());
//                itemOldAdapter = new ItemOldAdapter(articulos,this, isItemForOrder, cliente.getAlmacen());
//                itemOldAdapter.setForSeller(false);
            }
            fragmentoIniciado = true;
        }
        else{
            rvOrderItems.setAdapter(itemOldAdapter);
        }

        if(!isItemForOrder && !isForView)
            setArticulosForShow(); //Esto solo muestra artículos, no es para pedidos
        else if(isForView) {
            setArticulosForView();
            layout_search.setVisibility(View.GONE);
            rlTarifario.setVisibility(View.GONE);
        }
        else
            setArticulosForOrder();
//        if(!isItemForOrder) rlv_titulo.setVisibility(View.GONE);
    }

    private void setArticulosForView(){
        ViewItemAdapter viewItemAdapter = new ViewItemAdapter(this.detallePedidos);
        rvOrderItems.setAdapter(viewItemAdapter);
    }

    private void setArticulosForShow() {
        rlTarifario.setVisibility(View.GONE); //Eliminamos el spinner de Tarifario
        itemOldAdapter = new ItemOldAdapter(articulos, this, false, vendedor.getAlmacen());
        itemOldAdapter.setId_tarifario(this.tarifario.getId());
        rvOrderItems.setAdapter(itemOldAdapter);
//        setOrderItemAdapter();
    }

    private void setOrderItemAdapter() {
        orderItemAdapter.setId_tarifario(this.tarifario.getId());
        rvOrderItems.setAdapter(orderItemAdapter);
        tvTituloAlmacen.setText(this.tarifario.getAlmacen().getNombre());
    }

    private void setArticulosForOrder() {
        if(vendedor != null) { //Mostrar la lista de tarifarios para el vendedor
            rlTarifario.setVisibility(View.VISIBLE);
            this.listaTarifarios = new ArrayList<>();
            this.listaTarifarios = Tarifario.find(Tarifario.class, "almacen = ?",
                    String.valueOf(vendedor.getAlmacen().getId()));
            ArrayAdapter<Tarifario> adapterTarifario = new ArrayAdapter<Tarifario>(getActivity(), R.layout.spinner_item_ubigeo, listaTarifarios);
            spTarifario.setAdapter(adapterTarifario);
            spTarifario.setOnItemSelectedListener(spTarifario_item_selected);
        }
        else { //Mostrar el tarifario que tiene por defecto el cliente
            //tarifario = cliente.getTarifario();
            rlTarifario.setVisibility(View.GONE);
//            setOrderItemAdapter();
        }
//        detallesPedido = new ArrayList<DetallePedido>();
    }

    private AdapterView.OnItemSelectedListener spTarifario_item_selected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            tarifario = listaTarifarios.get(position);
//            setOrderItemAdapter();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    public void openDetailItem(ArticuloXAlmacen articuloXAlmacen/*,ArticuloXAlmacen articuloXAlmacen_2*/){
        ItemDetailFragment itemDetailFragment = new ItemDetailFragment();
        itemDetailFragment.setArticuloXAlmacen(articuloXAlmacen);
//        itemDetailFragment.setArticuloXAlmacen_2(articuloXAlmacen_2);
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_items,itemDetailFragment)
                .addToBackStack(null).commit();
    }

    public void initializeData(boolean filtrar){
        try{
            if(filtrar){
                String buscar = et_buscar.getText().toString();
                String query = "select * from articulo where nombre like '" + buscar + "%'";
                articulos = Articulo.findWithQuery(Articulo.class, query);
                if(isItemForOrder){
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    View view = getActivity().getCurrentFocus();
                    imm.hideSoftInputFromWindow(view.getWindowToken(),0);
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
                    View mView = getLayoutInflater().inflate(R.layout.dialog_item_order, null);
                    rvPopUp = mView.findViewById(R.id.rv_order_items);
                    rvPopUp.setLayoutManager(new LinearLayoutManager(getContext()));
//                    orderItemAdapter = new OrderItemAdapter(articulos, this, true, vendedor.getAlmacen());
                    orderItemAdapter.setListaArticulos(articulos);
                    orderItemAdapter.setId_tarifario(this.tarifario.getId());
                    rvPopUp.setAdapter(orderItemAdapter);
                    btn_agregar = mView.findViewById(R.id.btn_agregar);
                    btn_agregar.setOnClickListener(btnAgregar_accion);
                    btn_cancelar = mView.findViewById(R.id.btn_cancelar);
                    btn_cancelar.setOnClickListener(btnCancelar_accion);
                    mBuilder.setView(mView);
                    dialog = mBuilder.create();
                    dialog.show();
                }
                else{
                    itemOldAdapter = new ItemOldAdapter(articulos, this, isItemForOrder, vendedor.getAlmacen());
                    itemOldAdapter.setQuery(query);
                    itemOldAdapter.setId_tarifario(tarifario.getId());
                    rvOrderItems.setAdapter(itemOldAdapter);
                }
            }
        }catch(Exception ex){

        }
    }

    public int getCantidad_toneladas() {
        return cantidad_toneladas;
    }

    public void setCantidad_toneladas(int cantidad_toneladas) {
        this.cantidad_toneladas = cantidad_toneladas;
    }

    public View.OnClickListener btnAgregar_accion = new View.OnClickListener() {
        @Override
        public void onClick(View v){
//            List<DetallePedido> detallesPopUp = orderItemAdapter.getListaDetalles();
//            for(DetallePedido dp : detallesPopUp){
//                boolean encontrado = false;
//                for(DetallePedido dp_1 : detallePedidos){
//                    if(dp.getArticulo().getId() == dp_1.getArticulo().getId()){
//                        dp_1.setCantidad(dp_1.getCantidad() + dp.getCantidad());
//                        dp_1.setPesoTotal(dp_1.getPesoTotal() + dp.getPesoTotal());
//                        dp_1.setSubtotalFinal(dp_1.getSubtotalFinal() + dp.getSubtotalFinal());
//                        encontrado = true;
//                        break;
//                    }
//                }
//                if(!encontrado) detallePedidos.add(dp);
//            }
            detallePedidos = orderItemAdapter.getListaDetalles();
            cantidad_toneladas = 0;
            for(int i=0;i<detallePedidos.size();i++){
                cantidad_toneladas = cantidad_toneladas + detallePedidos.get(i).getPesoTotal();
            }
            if(vendedor!=null) {
                itemOldAdapter = new ItemOldAdapter(detallePedidos,isItemForOrder,vendedor.getAlmacen());
                itemOldAdapter.setForSeller(true);
            }
            else {
             //   itemOldAdapter = new ItemOldAdapter(detallePedidos,isItemForOrder,cliente.getAlmacen());
                itemOldAdapter.setForSeller(false);
            }
            itemOldAdapter.setEsDetalles(true);
            itemOldAdapter.setId_tarifario(tarifario.getId());
            rvOrderItems.setAdapter(itemOldAdapter);
            dialog.dismiss();
        }
    };

    public View.OnClickListener btnCancelar_accion = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dialog.dismiss();
        }
    };


}
