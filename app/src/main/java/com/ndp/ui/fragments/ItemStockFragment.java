package com.ndp.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Almacen;
import com.ndp.models.model.Articulo;
import com.ndp.models.model.ArticuloXAlmacen;
import com.ndp.ui.adapters.ItemStockAdapter;

import static com.ndp.utils.Useful.getCol;

public class ItemStockFragment extends Fragment {

    private Activity _self;
    private Articulo articulo;
    private View v;
    private LinearLayout lyt_empty;
    private RecyclerView rv_itemStock;
    private List<ArticuloXAlmacen> lstArticuloXAlmacens;
    private List<Almacen> almacens;
    private ItemStockAdapter adapter;

    public ItemStockFragment() {
        // ***Required empty public constructor***
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_item_stock, container, false);
        setElements();
        getItems();
        return v;
    }


    //region setElements
    private void setElements(){
        lyt_empty = v.findViewById(R.id.lyt_empty);
        rv_itemStock = v.findViewById(R.id.rv_itemStock);
    }
    //endregion

    //region getItems
    public void getItems(){
        almacens = new ArrayList<>();

        almacens = Almacen.listAll(Almacen.class);

        lstArticuloXAlmacens = new ArrayList<>();

        lstArticuloXAlmacens = ArticuloXAlmacen.find(ArticuloXAlmacen.class,getCol("itemCode") + " = '" + articulo.getCodigo() + "'");

        if (lstArticuloXAlmacens == null || lstArticuloXAlmacens.size() <= 0){
            lyt_empty.setVisibility(View.VISIBLE);
            rv_itemStock.setVisibility(View.GONE);
        } else {
            if (almacens != null && almacens.size() > 0) {
                for (ArticuloXAlmacen aa : lstArticuloXAlmacens) {
                    if (aa.getWarehouseCode() != null  && !aa.getWarehouseCode().isEmpty()) {
                        for (Almacen alm : almacens){
                            if (aa.getWarehouseCode().equals(alm.getCodAlmacen())){
                                aa.setWarehouseCode(alm.getNombre());
                            }
                        }
                    }
                }
            }
            lyt_empty.setVisibility(View.GONE);
            rv_itemStock.setVisibility(View.VISIBLE);
            recyclerViewAllData();
        }
    }
    //endregion

    //region recyclerViewAllData
    public void recyclerViewAllData(){
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(500);
        itemAnimator.setRemoveDuration(500);
        rv_itemStock.setItemAnimator(itemAnimator);

        rv_itemStock.setHasFixedSize(true);
        rv_itemStock.setLayoutManager(new LinearLayoutManager(_self));
        adapter = new ItemStockAdapter(lstArticuloXAlmacens, 0, true, _self);
        rv_itemStock.setAdapter(adapter);
    }
    //endregion

    public Activity get_self() {
        return _self;
    }

    public void set_self(Activity _self) {
        this._self = _self;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }
}
