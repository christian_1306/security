package com.ndp.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Articulo;
import com.ndp.models.model.Tarifario;
import com.ndp.models.model.TarifarioXArticulo;
import com.ndp.ui.adapters.ItemPriceAdapter;

import static com.ndp.utils.Useful.getCol;

public class ItemPriceFragment extends Fragment {

    private Activity _self;
    private Articulo articulo;
    private View v;
    private LinearLayout lyt_empty;
    private RecyclerView rv_itemPrice;
    private List<TarifarioXArticulo> lstTarifarioXArticulos;
    private List<Tarifario> tarifarios;
    private ItemPriceAdapter adapter;

    public ItemPriceFragment() {
        // ***Required empty public constructor***
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_item_price, container, false);
        setElements();
        getItems();
        return v;
    }

    //region setElements
    private void setElements(){
        lyt_empty = v.findViewById(R.id.lyt_empty);
        rv_itemPrice = v.findViewById(R.id.rv_itemPrice);
    }
    //endregion

    //region getItems
    public void getItems(){
        tarifarios = new ArrayList<>();

        tarifarios = Tarifario.listAll(Tarifario.class);

        lstTarifarioXArticulos = new ArrayList<>();

        lstTarifarioXArticulos = TarifarioXArticulo.find(TarifarioXArticulo.class,getCol("itemCode") + " = '" + articulo.getCodigo() + "'");

        if (lstTarifarioXArticulos == null || lstTarifarioXArticulos.size() <= 0){
            lyt_empty.setVisibility(View.VISIBLE);
            rv_itemPrice.setVisibility(View.GONE);
        } else {
            if (tarifarios != null && tarifarios.size() > 0) {
                for (TarifarioXArticulo aa : lstTarifarioXArticulos) {
                    if (aa.getCodTarifario() > 0) {
                        for (Tarifario tar : tarifarios){
                            if (aa.getCodTarifario() == tar.getCodigoListaPrecio()){
                                aa.setNombreListPrice(tar.getNombre());
                            }
                        }
                    }
                }
            }
            lyt_empty.setVisibility(View.GONE);
            rv_itemPrice.setVisibility(View.VISIBLE);
            recyclerViewAllData();
        }
    }
    //endregion

    //region recyclerViewAllData
    public void recyclerViewAllData(){
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(500);
        itemAnimator.setRemoveDuration(500);
        rv_itemPrice.setItemAnimator(itemAnimator);

        rv_itemPrice.setHasFixedSize(true);
        rv_itemPrice.setLayoutManager(new LinearLayoutManager(_self));
        adapter = new ItemPriceAdapter(lstTarifarioXArticulos, 0, true, _self);
        rv_itemPrice.setAdapter(adapter);
    }
    //endregion

    public Activity get_self() {
        return _self;
    }

    public void set_self(Activity _self) {
        this._self = _self;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }
}
