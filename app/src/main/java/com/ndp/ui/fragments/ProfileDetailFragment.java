package com.ndp.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.Vendedor;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileDetailFragment extends Fragment {

    private View view;
    private ExpandableRelativeLayout expandableRelativeLayout;
    private RelativeLayout rlvDatosGenerales;
    private ImageView arrow;
    private Vendedor vendedor;
    private boolean esVendedor = false;
    private Cliente cliente;
    private ImageView ivRetroceder;
    private TextView tv_nombre,tv_numero_documento,tv_telefono,tv_correo;

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public boolean isEsVendedor() {
        return esVendedor;
    }

    public void setEsVendedor(boolean esVendedor) {
        this.esVendedor = esVendedor;
    }

    public ProfileDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view = inflater.inflate(R.layout.fragment_profile_detail, container, false);
         setElements();
         setDataClient();
         return view;
    }

    private void setDataClient(){
        if(esVendedor) {
            tv_nombre.setText(vendedor.getNombres() + " " + vendedor.getApellidoPaterno() + " " + vendedor.getApellidoMaterno());
            tv_numero_documento.setText(vendedor.getNumeroDocumentoIdentidad());
            tv_telefono.setText(vendedor.getTelefono());
            tv_correo.setText(vendedor.getCorreo());
        }
        else{
            tv_nombre.setText(cliente.getRazonSocial());
           // tv_numero_documento.setText(cliente.getNumeroDocumento());
            tv_telefono.setText(cliente.getTelefono());
            tv_correo.setText(cliente.getCorreo());
        }
    }

    private void setElements(){
        ((TextView)view.findViewById(R.id.tv_title_toolbar)).setText("PERFIL");
        expandableRelativeLayout = view.findViewById(R.id.exp_cliente);
        arrow = view.findViewById(R.id.iv_arrow_profile_datos_generales);
        rlvDatosGenerales = view.findViewById(R.id.rlv_datos_generales);
        rlvDatosGenerales.setOnClickListener(expandirDatos);
        tv_nombre = view.findViewById(R.id.tv_nombre_razon_social);
        tv_numero_documento = view.findViewById(R.id.tv_numero_documento_texto);
        tv_telefono = view.findViewById(R.id.tv_numero_telefono);
        tv_correo = view.findViewById(R.id.tv_nombre_correo);
        ivRetroceder = view.findViewById(R.id.iv_back_toolbar);
        ivRetroceder.setOnClickListener(cerrarFragment);
    }

    private View.OnClickListener expandirDatos = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (expandableRelativeLayout != null) {
                expandableRelativeLayout.toggle();
                if (expandableRelativeLayout.isExpanded())
                    arrow.animate().rotation(180).start();
                else {
                    arrow.animate().rotation(0).start();
                }
            }
        }
    };

    private View.OnClickListener cerrarFragment = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getActivity().getSupportFragmentManager().popBackStackImmediate();
        }
    };

}
