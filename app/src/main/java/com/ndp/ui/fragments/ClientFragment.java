package com.ndp.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.ui.activities.ConsultingActivity;
import com.ndp.ui.activities.LoginActivity;
import com.ndp.ui.activities.OrderClientActivity;
import com.ndp.ui.activities.PendingActivity;
import com.ndp.utils.Popup.Popup;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClientFragment extends Fragment {

    private View view;
    private Cliente cliente;

    public ClientFragment() {
        // Required empty public constructor
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_client, container, false);
        view.findViewById(R.id.home_tvPedidos).setOnClickListener(tvMenu_Click);
        view.findViewById(R.id.home_tvPendientes).setOnClickListener(tvMenu_Click);
        view.findViewById(R.id.home_tvConsultas).setOnClickListener(tvMenu_Click);
        view.findViewById(R.id.home_tvSalir).setOnClickListener(tvMenu_Click);
        return view;
    }

    private View.OnClickListener tvMenu_Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;
            switch (v.getId()) {
                case R.id.home_tvPedidos:
                    intent = new Intent(getActivity(), OrderClientActivity.class);
                    //intent.putExtra("cliente", cliente.getIdCliente());
                    startActivity(intent);
                    break;
                case R.id.home_tvPendientes:
                    intent = new Intent(getActivity(), PendingActivity.class);
                   //intent.putExtra("cliente", cliente.getIdCliente());
                    startActivity(intent);
                    break;
                case R.id.home_tvConsultas:
                    intent = new Intent(getActivity(), ConsultingActivity.class);
                   // intent.putExtra("cliente", cliente.getIdCliente());
                    startActivity(intent);
                    break;
                case R.id.home_tvSalir:
                    Popup.ShowDialog(getActivity(),
                            getString(R.string.mensaje_cerrar_sesion), Popup.MSG_TITLE_WARNING,
                            Popup.MSG_TYPE_WARNING,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(getActivity(), LoginActivity.class));
                                    getActivity().finish();
                                }
                            });
                    break;
            }
        }
    };

}
