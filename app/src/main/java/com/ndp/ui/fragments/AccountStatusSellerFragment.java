package com.ndp.ui.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.Factura;
import com.ndp.ui.adapters.AccountStatusSellerAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountStatusSellerFragment extends Fragment {

    private View view;
    private RecyclerView rvEstados;
    private EditText et_buscar;
    private AccountStatusSellerAdapter accountStatusSellerAdapter;

    public AccountStatusSellerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_account_status_seller, container, false);
        setElements();
        fillRecyclerView();
        return view;
    }

    private void fillRecyclerView(){
        accountStatusSellerAdapter = new AccountStatusSellerAdapter();
        rvEstados.setAdapter(accountStatusSellerAdapter);
    }

    private void setElements(){
        rvEstados = view.findViewById(R.id.rv_account_status);
        rvEstados.setLayoutManager(new LinearLayoutManager(getContext()));
        et_buscar = view.findViewById(R.id.barSearch_etBuscar);
        et_buscar.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(et_buscar,InputMethodManager.SHOW_IMPLICIT);
                et_buscar.requestFocus();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= et_buscar.getRight() - et_buscar.getTotalPaddingRight()) {
                        //CODIGO
                        String buscar = et_buscar.getText().toString();
                        if(buscar.isEmpty()){
                            initializeData(false);
                        }
                        else initializeData(true);
                        return true;
                    }
                }
                return true;
            }
        });
    }

    private void initializeData(boolean filtrar){
        try{
            if(filtrar){
                String buscar = et_buscar.getText().toString();
                String query = "select * from cliente where razon_social like '" + buscar + "%'";
                Cliente cliente = Cliente.findWithQuery(Cliente.class,query).get(0);

                String query2 = "select * from factura where cliente = " + cliente.getId();
                List<Factura> facturas = Factura.findWithQuery(Factura.class,query2);
                accountStatusSellerAdapter = new AccountStatusSellerAdapter(facturas);
                rvEstados.setAdapter(accountStatusSellerAdapter);
            }
            else{
                accountStatusSellerAdapter = new AccountStatusSellerAdapter();
                rvEstados.setAdapter(accountStatusSellerAdapter);
            }
        }catch(Exception ex){

        }
    }
}
