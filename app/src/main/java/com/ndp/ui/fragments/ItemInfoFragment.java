package com.ndp.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ndp.R;
import com.ndp.models.model.Articulo;
import com.ndp.utils.use.useAPP;

public class ItemInfoFragment extends Fragment {

    private Activity _self;
    private Articulo articulo;
    private View v;
    private TextView tv_itemname;
    private TextView tv_itemcode;
    private TextView tv_unidad;
    private TextView tv_varxton;
    private TextView tv_peso;
    private TextView tv_descripcion;

    public ItemInfoFragment() {
        // ***Required empty public constructor***
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_item_info, container, false);
        setElements();
        return v;
    }

    @SuppressLint("SetTextI18n")
    private void setElements(){
        tv_itemname  = v.findViewById(R.id.tv_itemname);
        tv_itemcode  = v.findViewById(R.id.tv_itemcode);
        tv_unidad  = v.findViewById(R.id.tv_unidad);
        tv_varxton  = v.findViewById(R.id.tv_varxton);
        tv_peso  = v.findViewById(R.id.tv_peso);
        tv_descripcion  = v.findViewById(R.id.tv_descripcion);
        if (articulo != null) {
            tv_itemname.setText(articulo.getNombre() != null ? articulo.getNombre() : "SN");
            tv_itemcode.setText(articulo.getCodigo() != null ? articulo.getCodigo() : "SC");
            tv_unidad.setText(articulo.getUnidadMedida() != null ? articulo.getUnidadMedida() : "SU");
            tv_varxton.setText(useAPP.fmDecimal(articulo.getVarillasPorTonelada()));
            tv_peso.setText(useAPP.fmDecimal(articulo.getPeso()));
            tv_descripcion.setText(articulo.getDescripcion() != null ? articulo.getDescripcion() : "SIN DESCRIPCIÓN");
        }
    }

    public Activity get_self() {
        return _self;
    }

    public void set_self(Activity _self) {
        this._self = _self;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }
}
