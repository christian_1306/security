package com.ndp.ui.fragments;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Almacen;
import com.ndp.models.model.Tarifario;
import com.ndp.models.model.Vendedor;
import com.ndp.ui.adapters.PagerAdapterItems;

/**
 * A simple {@link Fragment} subclass.
 */
public class ItemContentFragment extends Fragment {

    private View view;
    private Almacen almacen;
    private List<Tarifario> tarifarios;
    private Vendedor vendedor;

    public ItemContentFragment() {
        // Required empty public constructor
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_item_content, container, false);
        setElements();
        return view;
    }

    private void setElements() {
        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        tarifarios = Tarifario.find(Tarifario.class, "almacen = ?", String.valueOf(this.almacen.getId()));
        for(Tarifario t: tarifarios)
            tabLayout.addTab(tabLayout.newTab().setText(t.getNombre()));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.pager);
        final PagerAdapterItems adapter = new PagerAdapterItems(
                getActivity().getSupportFragmentManager(), tabLayout.getTabCount(), tarifarios);
        adapter.setVendedor(vendedor);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

}
