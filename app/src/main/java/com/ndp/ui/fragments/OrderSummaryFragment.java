package com.ndp.ui.fragments;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import java.sql.Time;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.ndp.R;
import com.ndp.models.model.Almacen;
import com.ndp.models.model.ArticuloXAlmacen;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.DetallePedido;
import com.ndp.models.model.Direccion;
import com.ndp.models.model.Pedido;
import com.ndp.models.model.TarifarioXArticulo;
import com.ndp.models.model.Vendedor;
import com.ndp.models.retrofit.ApiData;
import com.ndp.models.retrofit.SyncData;
import com.ndp.models.dto.PedidoView;
import com.ndp.utils.use.usePhone;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderSummaryFragment extends Fragment{

    private View view;
    private int mYear,mMonth,mDay;
    private String labelFechaEntrega = "Fecha de entrega";
    private String cadFechaEntrega = "";
    private int numeroArticulos,seleccionarFecha = 0;
    private double montoTotal;
    private SyncData syncData;
    private ApiData<PedidoView> apiData;

    public int getNumeroArticulos() {
        return numeroArticulos;
    }

    public void setNumeroArticulos(int numeroArticulos) {
        this.numeroArticulos = numeroArticulos;
    }

    public double getMontoTotal() {

        return montoTotal;
    }

    public void setMontoTotal(double montoTotal) {
        this.montoTotal = montoTotal;
    }

    private boolean isForView = false;
    private OrderItemFragment orderItemFragment;
    private OrderDetailFragment orderDetailFragment;
    private Date fecha;

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    private TextView tv_fecha_entrega;
    private TextView tvNumeroArticulos;
    private TextView tvMontoTotal;

    private Button btnRegistrarPedido;

    public void setOrderItemFragment(OrderItemFragment orderItemFragment) {
        this.orderItemFragment = orderItemFragment;
    }

    public void setOrderDetailFragment(OrderDetailFragment orderDetailFragment) {
        this.orderDetailFragment = orderDetailFragment;
    }

    public OrderSummaryFragment() {
        this.numeroArticulos = 0;
        this.montoTotal = 0;
    }


    public boolean isForView() {
        return isForView;
    }

    public void setForView(boolean forView) {
        isForView = forView;
    }

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_order_summary, container, false);
        setElements();
        setData();
        return view;
    }

    private void setElements() {
        syncData = new SyncData();
        apiData = new ApiData<PedidoView>();
        apiData.setImei(usePhone.getIMEI(getActivity()));
        tv_fecha_entrega = view.findViewById(R.id.tv_fecha_entrega);
        tv_fecha_entrega.setText(labelFechaEntrega);
        view.findViewById(R.id.order_summary_rlv).setOnClickListener(ivExpandableDatos_Click);
        tvNumeroArticulos = view.findViewById(R.id.tv_numero_articulos);
        tvMontoTotal = view.findViewById(R.id.tv_monto_total);
        btnRegistrarPedido = view.findViewById(R.id.btn_crear_pedido);
        btnRegistrarPedido.setOnClickListener(btnCrearPedido_Click);
        if(!isForView) {
            tv_fecha_entrega.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() >= tv_fecha_entrega.getRight() - tv_fecha_entrega.getTotalPaddingRight()) {
                            final Calendar c = Calendar.getInstance();
                            mYear = c.get(Calendar.YEAR);
                            mMonth = c.get(Calendar.MONTH);
                            mDay = c.get(Calendar.DAY_OF_MONTH);


                            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.ColorCalendar,
                                    new DatePickerDialog.OnDateSetListener() {

                                        @Override
                                        public void onDateSet(DatePicker view, int year,
                                                              int monthOfYear, int dayOfMonth) {
                                            cadFechaEntrega = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                            seleccionarFecha = 1;
                                            labelFechaEntrega = "Fecha de entrega                             " + cadFechaEntrega;
                                            tv_fecha_entrega.setText(labelFechaEntrega);

                                        }
                                    }, mYear, mMonth, mDay);
                            datePickerDialog.show();
                            return true;
                        }
                    }
                    return true;
                }
            });
        }
        else{
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            String strFecha = "Fecha de entrega                             " + dateFormat.format(fecha);
            DecimalFormat df = new DecimalFormat("#.00");
            tv_fecha_entrega.setText(strFecha);
            btnRegistrarPedido.setVisibility(View.GONE);
            tvMontoTotal.setText(df.format(montoTotal));
            tvNumeroArticulos.setText(String.valueOf(this.numeroArticulos));
        }
    }

    private View.OnClickListener ivExpandableDatos_Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ExpandableRelativeLayout expandableRelative = null;
            ImageView ivExpandable = null;

            expandableRelative = view.findViewById(R.id.order_summary_expDatos);
            ivExpandable = view.findViewById(R.id.iv_arrow_order_summary);

            if (expandableRelative != null) {
                expandableRelative.toggle();
                if (expandableRelative.isExpanded())
                    ivExpandable.animate().rotation(180).start();
                else {
                    ivExpandable.animate().rotation(0).start();
                }
            }
        }
    };

    private void setData() {
        if(orderItemFragment == null) return;
        ArrayList<DetallePedido> listaDetalles = orderItemFragment.getListaDetalles();
        if(listaDetalles == null || listaDetalles.size() == 0) return;
        this.numeroArticulos = 0;
        this.montoTotal = 0;
        for (DetallePedido d: listaDetalles) {
            this.numeroArticulos += d.getCantidad();
            this.montoTotal += d.getSubtotalFinal();
        }
        DecimalFormat df = new DecimalFormat("#.00");
        tvNumeroArticulos.setText(String.valueOf(numeroArticulos));
        tvMontoTotal.setText(df.format(montoTotal));
    }

    private View.OnClickListener btnCrearPedido_Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(seleccionarFecha==0){
                Toast.makeText(getActivity(), "Debe seleccionar una fecha", Toast.LENGTH_SHORT).show();
                return;
            }
            if(orderItemFragment.getListaDetalles().size()==0){
                Toast.makeText(getActivity(),"Debe seleccionar al menos un artículo del carrito",Toast.LENGTH_SHORT).show();
                return;
            }
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            DateFormat dfTime = new SimpleDateFormat("HH:mm:ss");
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat mdformatDate = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat mdformatTime = new SimpleDateFormat("HH:mm:ss");
            int newIdPedido = ((int)Pedido.count(Pedido.class)) + 1;
            Cliente cliente = orderDetailFragment.getCliente();
            Direccion direecionEntrega = orderDetailFragment.getDireccionEntrega();
            Cliente clienteSecundario = orderDetailFragment.getClienteSecundarioSeleccionado();
            if(clienteSecundario!=null){
                boolean clienteSecundarioValido = validarClienteSecundario(clienteSecundario);
                if(!clienteSecundarioValido) return;
            }
            Vendedor vendedor = orderItemFragment.getVendedor();
            double totalFinal = orderDetailFragment.getTotal();
            double impuestoPedido = orderDetailFragment.getTotalImpuesto();
            double totalNeto = orderDetailFragment.getTotalBase();
            String comentario = orderDetailFragment.getComentario();
            String cadFechaEmision = mdformatDate.format(calendar.getTime());
            String cadHora = mdformatTime.format(calendar.getTime());
            Date fechaEmision = null;
            Date fechaEntrega = null;
            Time hora = null;
            try {
                fechaEmision = df.parse(cadFechaEmision);
                fechaEntrega = df.parse(cadFechaEntrega);
                hora = new Time(dfTime.parse(cadHora).getTime());
            } catch (Exception ex) {

            }
            Direccion direccionFacturacion = orderDetailFragment.getDireccionFactura();
            double descuento;
            if(orderDetailFragment.getEtDescuento().getText().toString().isEmpty()) descuento = 0;
            else descuento = Double.parseDouble(orderDetailFragment.getEtDescuento().getText().toString());
            Almacen almacen = null;
            if(orderItemFragment.getVendedor() != null) { //Lo está registrando un vendedor
                almacen = orderItemFragment.getVendedor().getAlmacen();
            }
            else{}
                //almacen = cliente.getAlmacen();

            Pedido pedido = new Pedido(newIdPedido, cliente, direecionEntrega, clienteSecundario,
                    vendedor, totalFinal, impuestoPedido, totalNeto, comentario, fechaEmision,
                    1, hora, null, direccionFacturacion, fechaEntrega, descuento,
                    almacen);

            ArrayList<DetallePedido> listaDetalles = orderItemFragment.getListaDetalles();
            int i=0;
            for(DetallePedido d: listaDetalles) {
                TarifarioXArticulo tarifarioXArticulo = orderItemFragment.getOrderItemAdapter().getListaTarifarioXArticulo().get(i);
                d.setTarifarioXArticulo(tarifarioXArticulo);
//                d.setTarifario(orderItemFragment.getTarifario());
                ArticuloXAlmacen articuloXAlmacen = ArticuloXAlmacen.find(ArticuloXAlmacen.class,
                        "almacen = ? and articulo = ?",
                        String.valueOf(almacen.getId()),
                        String.valueOf(d.getTarifarioXArticulo().getArticulo().getId())).get(0);
                int cantidadSolicitada = d.getCantidad();
                int Stock = articuloXAlmacen.getStock();
                if(cantidadSolicitada>Stock) d.setEsPerdida(true);
                else d.setEsPerdida(false);
                if(d.isEsPerdida()) {
                    double nuevo_total_neto, nuevo_total_final;
                    nuevo_total_neto = pedido.getTotalNeto() - d.getSubtotalNeto();
                    nuevo_total_final = pedido.getTotalFinal() - d.getSubtotalFinal();
                    pedido.setTotalNeto(nuevo_total_neto);
                    pedido.setTotalFinal(nuevo_total_final);
                }
                ++i;
            }
            //pedido.save();
            for(DetallePedido d: listaDetalles) {
                d.setPedido(pedido);
               // d.save();
            }
            syncData.RegistrarPedidoEnDb(pedido, listaDetalles,getActivity(),apiData);
//            Toast.makeText(getActivity(), "Pedido registrado", Toast.LENGTH_SHORT).show();
//            getActivity().finish();
        }
    };


    public boolean validarClienteSecundario(Cliente clienteSecundario){
        String numeroDocSecundario,nombreSecundario,telefonoSecundario;
        //numeroDocSecundario = clienteSecundario.getNumeroDocumento();
        nombreSecundario = clienteSecundario.getRazonSocial();
        telefonoSecundario = clienteSecundario.getTelefono();

        if(nombreSecundario.isEmpty()){
            Toast.makeText(getActivity(),"Debe ingresar un nombre para el cliente secundario",Toast.LENGTH_SHORT).show();
            return false;
        }

        if(nombreSecundario.length() < 3){
            Toast.makeText(getActivity(),"El nombre del cliente secundario debe tener al menos 3 caracteres",Toast.LENGTH_SHORT).show();
            return false;
        }

        /*if(numeroDocSecundario.isEmpty()){
            Toast.makeText(getActivity(),"Debe ingresar el número de documento del cliente secundario",Toast.LENGTH_SHORT).show();
            return false;
        }*/

        try {
           // long numero = Long.parseLong(numeroDocSecundario);
        }catch(Exception ex){
            ex.getMessage();
            Toast.makeText(getActivity(),"El número de documento del cliente secundario ingresado no es númerico",Toast.LENGTH_SHORT).show();
            return false;
        }

        switch(0){
            case 9:
                break;
            case 7:
                break;
            default:
                Toast.makeText(getActivity(), "El tamaño del número de documento debe ser de 7 o 9 dígitos", Toast.LENGTH_SHORT).show();
                return false;
        }

        if (telefonoSecundario.isEmpty()) {
            Toast.makeText(getActivity(), "Ingrese un número telefónico para el cliente secundario", Toast.LENGTH_SHORT).show();
            return false;
        }

        try{
            long numero = Long.parseLong(telefonoSecundario);
        }catch(Exception ex){
            Toast.makeText(getActivity(),"El teléfono ingresado para el cliente secundario debe ser numérico",Toast.LENGTH_SHORT).show();
            return false;
        }

        if(telefonoSecundario.length() < 7) {
            Toast.makeText(getActivity(), "El tamaño del número telefónico ingresado para el cliente secundario debe ser de 7 u 8 dígitos", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(telefonoSecundario.length() > 8){
            Toast.makeText(getActivity(), "El tamaño del número telefónico ingresado para el cliente secundario no puede ser mayor a 8 dígitos", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(telefonoSecundario.length() == 7){
            int dig_1 = Character.getNumericValue(telefonoSecundario.charAt(0));
            switch(dig_1){
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                default:
                    Toast.makeText(getActivity(), "El número telefónico del cliente secundario debe empezar con 2, 3 o 4", Toast.LENGTH_SHORT).show();
                    return false;
            }
        }
        if(telefonoSecundario.length() == 8) {
            int car_ini = Character.getNumericValue(telefonoSecundario.charAt(0));
            switch (car_ini){
                case 6:
                    break;
                case 7:
                    break;
                default :
                    Toast.makeText(getActivity(), "El número telefónico del cliente secundario debe empezar con 6 o 7", Toast.LENGTH_SHORT).show();
                    return false;
            }
        }

        return true;
    }
}
