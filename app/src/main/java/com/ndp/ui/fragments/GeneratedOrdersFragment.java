package com.ndp.ui.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import  com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.Pedido;
import com.ndp.ui.activities.OrderViewActivity;
import com.ndp.ui.adapters.OrderAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class GeneratedOrdersFragment extends Fragment {

    private View view;
    private RecyclerView rvGeneratedOrders;
    private OrderAdapter orderAdapter;
    private EditText et_buscar;
    private boolean isPending;

    private Cliente cliente;

    public boolean isPending() {
        return isPending;
    }

    public void setPending(boolean pending) {
        isPending = pending;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public GeneratedOrdersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_generated_orders, container, false);
        setElements();
        fillRecyclerView();
        return view;
    }

    private void fillRecyclerView() {
        rvGeneratedOrders.setLayoutManager(new LinearLayoutManager(getContext()));

        List<Pedido> pedidos = new ArrayList<Pedido>();
        if(cliente == null) //LISTA PARA EL VENDEDOR
            pedidos = Pedido.listAll(Pedido.class);
        else
            pedidos = Pedido.find(Pedido.class, "cliente_principal = ?", String.valueOf(cliente.getId()));

//        Pedido pedido = new Pedido();
//
//        Cliente cliente = new Cliente();
//        cliente.setRazonSocial("PUCP");
//        String fecha;
//        Date date = null;
//        try {
//            fecha = "1998-04-17";
//            date = new SimpleDateFormat("yyyy-mm-dd").parse(fecha);
//        }catch(Exception ex){
//
//        }
//        pedido.setFechaEmision(date);
//        pedido.setClientePrincipal(cliente);
//        pedido.setTotalFinal(12312.50);
//        pedidos.add(pedido);
        orderAdapter = new OrderAdapter(pedidos, this,this.isPending);
        rvGeneratedOrders.setAdapter(orderAdapter);
    }

    private void setElements() {

        rvGeneratedOrders = view.findViewById(R.id.rvGeneratedOrders);
        et_buscar = view.findViewById(R.id.barSearch_etBuscar);
        et_buscar.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(et_buscar,InputMethodManager.SHOW_IMPLICIT);
                et_buscar.requestFocus();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= et_buscar.getRight() - et_buscar.getTotalPaddingRight()) {
                        //CODIGO
                        initializeData(true);
                        return true;
                    }
                }
                return true;
            }
        });
    }

    public void openDetail(Pedido pedido){
        Intent intent = new Intent(getActivity(),OrderViewActivity.class);
        intent.putExtra("pedido",String.valueOf(pedido.getIdPedido()));
        startActivity(intent);
    }

    private void initializeData(boolean filtrar){
        if(filtrar){
            String buscar = et_buscar.getText().toString();
            String query = null,query2 = null;
            if(cliente==null){//LISTAR POR PRINCIPALES, ESTAMOS EN PERFIL DE VENDEDOR
                query = "select * from cliente where razon_social like '" + buscar + "%'";
                List<Cliente> clientes = Cliente.findWithQuery(Cliente.class,query);
                if(clientes.size()>0) {
                    Cliente cliente = clientes.get(0);
                    query2 = "select * from pedido where cliente_principal = " + cliente.getId();
                    List<Pedido> pedidos = Pedido.findWithQuery(Pedido.class, query2);
                    orderAdapter = new OrderAdapter(pedidos, this, this.isPending);
                    rvGeneratedOrders.setAdapter(orderAdapter);
                }
            }
            else{//LISTAR POR SECUNDARIOS, ESTAMOS EN PERFIL DE CLIENTE
                query = "select * from cliente where razon_social like '" + buscar + "%'";
                Cliente cliente = Cliente.findWithQuery(Cliente.class,query).get(0);
                query2 = "select * from pedido where cliente_principal = " + this.cliente.getId() +
                        " and cliente_secundario = " + cliente.getId();
                List<Pedido> pedidos = Pedido.findWithQuery(Pedido.class,query2);
                orderAdapter = new OrderAdapter(pedidos,this,this.isPending);
                rvGeneratedOrders.setAdapter(orderAdapter);
            }
        }
        else{
            List<Pedido> pedidos = Pedido.listAll(Pedido.class);
            orderAdapter = new OrderAdapter(pedidos,this,this.isPending);
            return;
        }
    }
}
