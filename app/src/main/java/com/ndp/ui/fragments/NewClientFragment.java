package com.ndp.ui.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ndp.models.model.Almacen;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.Departamento;
import com.ndp.models.model.Direccion;
import com.ndp.models.model.Municipio;
import com.ndp.models.model.PersonaContacto;
import com.ndp.models.model.Provincia;
import com.ndp.R;
import com.ndp.models.model.Tarifario;
import com.ndp.models.retrofit.ApiData;
import com.ndp.models.retrofit.SyncData;
import com.ndp.models.dto.ClienteView;
import com.ndp.ui.adapters.AddressAdapter;
import com.ndp.ui.adapters.MainClientAdapter;
import com.ndp.ui.adapters.SelectClientAdapter;
import com.ndp.utils.use.usePhone;

public class NewClientFragment extends Fragment {

    private View view;
    private ScrollView svDatos;
    private Button btnAgregarClientesPrincipales;
    private Button btnAgregarDirecciones;
    private Button btnRegistrarCliente;
    private EditText etRazonSocial;
    private EditText etNumeroDocumento;
    private EditText etTelefono;
    private EditText etCorreo;
    private EditText etDireccion;
    private EditText etComentario;
    private EditText etNombrePersonaContacto;
    private EditText etCargoPersonaContacto;
    private EditText etTelefoonoPersonaContacto;
    private EditText etCorreoPersonaContacto;
    private Spinner spDepartamento;
    private Spinner spProvincia;
    private Spinner spMunicipio;
    private Spinner spAlmacen;
    private Spinner spTarifario;
    private RadioGroup rgPrincipalSecundario;
    private RadioButton rbPrincipal;
    private RadioButton rbSecundario;
    private TextView tvTituloClientes;
    private AlertDialog dialog;
    private TextView tvAlmacen;
    private TextView tvTarifario;
    private RecyclerView rvDirecciones;
    private RecyclerView rvClientes;
    private RecyclerView rvClientesSeleccionados;

    private List<Cliente> allClients;
    private ArrayList<Cliente> selectedClients = new ArrayList<>();

    //Para la lista de departamentos, provincias y municipios
    private List<Departamento> listaDepartamentos;
    private List<Provincia> listaProvincias;
    private List<Municipio> listaMunicipios;
    private List<Almacen> listaAlmacenes;
    private List<Tarifario> listaTarifarios;
    private ArrayAdapter<Departamento> adapterDepartamento;
    private ArrayAdapter<Provincia> adapterProvincia;
    private ArrayAdapter<Municipio> adapterMunicipio;
    private ArrayAdapter<Almacen> adapterAlmacen;
    private ArrayAdapter<Tarifario> adapterTarifario;
    private SyncData syncData = new SyncData();
    private ApiData<ClienteView> apidata = new ApiData<ClienteView>();

    //Para guardar las direcciones
    private ArrayList<Direccion> addresses = new ArrayList<Direccion>();
    private ArrayAdapter<Direccion> adapterDireccion;

    private SelectClientAdapter  selectClientAdapter;

    private Cliente clienteSeleccionado;

    private String razonSocial, numeroDocumento, telefono, correo, comentario, nombreContacto, cargoContacto,
    telefonoContacto, correoContacto;

    private String emailPattern = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@" +
            "[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
    Pattern pattern = Pattern.compile(emailPattern);
    private int id_vendedor;

    public NewClientFragment() {

    }



    public int getId_vendedor() {
        return id_vendedor;
    }

    public void setId_vendedor(int id_vendedor) {
        this.id_vendedor = id_vendedor;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_new_client, container, false);
        setElements();
//        showClient();
        return this.view;
    }

    private void setElements() {
        apidata.setImei(usePhone.getIMEI(getActivity()));
        svDatos = view.findViewById(R.id.customer_lead_svDatos);

        view.findViewById(R.id.customer_lead_rlvDatosLead).setOnClickListener(ivExpandableDatos_Click);
        view.findViewById(R.id.customer_lead_rlvDatosContacto).setOnClickListener(ivExpandableDatos_Click);
        view.findViewById(R.id.customer_lead_rlv_clientes_principales).setOnClickListener(ivExpandableDatos_Click);

        btnAgregarDirecciones = view.findViewById(R.id.btn_agregar_direcciones);
        btnAgregarDirecciones.setOnClickListener(btnAgregarDirecciones_Click);

        tvTarifario = view.findViewById(R.id.tv_tarifario);
        tvAlmacen = view.findViewById(R.id.tv_almacen);
//        tvClientesPrincipales = view.findViewById(R.id.tv_clientes_principales);
//        tvDireccionesCliente = view.findViewById(R.id.tv_direcciones_cliente);
        btnAgregarClientesPrincipales = view.findViewById(R.id.btn_agregar_clientes);
        btnAgregarClientesPrincipales.setOnClickListener(btnAgregarClientesPrincipales_Click);

        btnRegistrarCliente = view.findViewById(R.id.btn_crear_cliente);
        btnRegistrarCliente.setOnClickListener(btnCrearCliente_Click);

        rvDirecciones = view.findViewById(R.id.rv_lista_direcciones);
        rvDirecciones.setLayoutManager(new LinearLayoutManager(getContext()));

        rvClientes = view.findViewById(R.id.rv_main_client_list);
        rvClientes.setLayoutManager(new LinearLayoutManager(getContext()));

        rgPrincipalSecundario = view.findViewById(R.id.rg_principal_secundario);
        rbPrincipal = view.findViewById(R.id.rb_principal);
        rbSecundario = view.findViewById(R.id.rb_secundario);

        spAlmacen = view.findViewById(R.id.sp_almacen);
        spTarifario = view.findViewById(R.id.sp_tarifario);

//        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
//        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

//        lvDirecciones = view.findViewById(R.id.lv_direcciones);

        //Se iniciará por defecto listar los clientes principales ya que se iniciará con la opción
        //CLIENTE PRINCIPAL marcada
        allClients = Cliente.find(Cliente.class, "tipo_cliente = ?", "2");

        this.listaDepartamentos = new ArrayList<>();
        this.listaDepartamentos = Departamento.listAll(Departamento.class);

        this.listaProvincias = new ArrayList<>();
        this.listaProvincias = Provincia.listAll(Provincia.class);

        this.listaMunicipios = new ArrayList<>();
        this.listaMunicipios = Municipio.listAll(Municipio.class);

        this.listaAlmacenes = new ArrayList<>();
        this.listaAlmacenes = Almacen.listAll(Almacen.class);
        adapterAlmacen = new ArrayAdapter<Almacen>(getActivity(), R.layout.spinner_item_ubigeo, listaAlmacenes);
        spAlmacen.setAdapter(adapterAlmacen);
        spAlmacen.setOnItemSelectedListener(spAlmacen_selected);

//        this.listaTarifarios = new ArrayList<>();
//        this.listaTarifarios = Tarifario.listAll(Tarifario.class);
//        adapterTarifario = new ArrayAdapter<Tarifario>(getActivity(), R.layout.spinner_item_ubigeo, listaTarifarios);
//        spTarifario.setAdapter(adapterTarifario);

        etRazonSocial = view.findViewById(R.id.et_razon_social_cliente);
        etNumeroDocumento = view.findViewById(R.id.et_numero_documento_cliente);
        etTelefono = view.findViewById(R.id.et_telefono_cliente);
        etCorreo = view.findViewById(R.id.et_correo_cliente);
        etComentario = view.findViewById(R.id.customer_lead_etComentario);
        etNombrePersonaContacto = view.findViewById(R.id.et_persona_contacto);
        etCargoPersonaContacto = view.findViewById(R.id.et_cargo_contacto);
        etTelefoonoPersonaContacto = view.findViewById(R.id.et_telefono_contacto);
        etCorreoPersonaContacto = view.findViewById(R.id.et_correo_contacto);

        tvTituloClientes = view.findViewById(R.id.tv_etiqueta_clientes_principales);
        tvTituloClientes.setText("Clientes Secundarios");
        rgPrincipalSecundario.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_principal:
                        tvTituloClientes.setText("Clientes Secundarios");
                        allClients = Cliente.find(Cliente.class, "tipo_cliente = ?", "2");
                        etCargoPersonaContacto.getText().clear();
                        etCorreoPersonaContacto.getText().clear();
                        etNombrePersonaContacto.getText().clear();
                        etTelefoonoPersonaContacto.getText().clear();
                        selectedClients = new ArrayList<>();
                        spAlmacen.setVisibility(View.VISIBLE);
                        spTarifario.setVisibility(View.VISIBLE);
                        tvAlmacen.setVisibility(View.VISIBLE);
                        tvTarifario.setVisibility(View.VISIBLE);
                        addresses = new ArrayList<>();
                        AddressAdapter addressAdapter = new AddressAdapter(addresses);
                        rvDirecciones.setAdapter(addressAdapter);
                        selectedClients = new ArrayList<>();
                        selectClientAdapter = new SelectClientAdapter(allClients, selectedClients, false);
                        break;
                    case R.id.rb_secundario:
                        tvTituloClientes.setText("Clientes Principales");
                        spAlmacen.setVisibility(View.GONE);
                        spTarifario.setVisibility(View.GONE);
                        tvAlmacen.setVisibility(View.GONE);
                        tvTarifario.setVisibility(View.GONE);

                        tvTituloClientes.setText("Clientes Principales");
                        allClients = Cliente.find(Cliente.class, "tipo_cliente = ?", "1");
                        etCargoPersonaContacto.getText().clear();
                        etCorreoPersonaContacto.getText().clear();
                        etNombrePersonaContacto.getText().clear();
                        etTelefoonoPersonaContacto.getText().clear();
                        addresses = new ArrayList<>();
                        AddressAdapter addressAdapter_2 = new AddressAdapter(addresses);
                        rvDirecciones.setAdapter(addressAdapter_2);
                        selectedClients = new ArrayList<>();
                        selectClientAdapter = new SelectClientAdapter(allClients, selectedClients, false);
                        break;
                }
                MainClientAdapter adapter = new MainClientAdapter(selectedClients);
                rvClientes.setAdapter(adapter);
            }
        });
    }

    //----------EVENTOS PARA QUE SE DESPLIGUEN LOS CAMPOS DE TEXTO DEL CLIENTE Y CONTACTO----------
    private View.OnClickListener ivExpandableDatos_Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ExpandableRelativeLayout expandableRelative = null;
            ImageView ivExpandable = null;

            switch (v.getId()) {
                case R.id.customer_lead_rlvDatosLead:
                    expandableRelative = view.findViewById(R.id.customer_lead_expDatosLead);
                    ivExpandable = view.findViewById(R.id.customer_lead_ivDatosLead);
                    break;
                case R.id.customer_lead_rlvDatosContacto:
                    expandableRelative = view.findViewById(R.id.customer_lead_expDatosContacto);
                    ivExpandable = view.findViewById(R.id.customer_lead_ivDatosContacto);
                    break;
                case R.id.customer_lead_rlv_clientes_principales:
                    expandableRelative = view.findViewById(R.id.customer_lead_expClientesPrincipales);
                    ivExpandable = view.findViewById(R.id.customer_lead_ivClientesPrincipales);
                    break;
            }

            if (expandableRelative != null) {
                expandableRelative.toggle();
                if (expandableRelative.isExpanded())
                    ivExpandable.animate().rotation(180).start();
                else {
                    ivExpandable.animate().rotation(0).start();
                    if (expandableRelative.getId() != R.id.customer_lead_expDatosLead) {
                        svDatos.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                svDatos.fullScroll(View.FOCUS_DOWN);
                            }
                        }, 400);
                    }
                }
            }
        }
    };
    //--------------FIN EVENTOS DE DISPLIEGUE DE LOS CAMPOS DE TEXTOS DEL CLIENTE Y CONTACTO-------

    //--------------EVENTO PARA AGREGAR DIRECCIONES-----------------------
    private View.OnClickListener btnAgregarDirecciones_Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
            View mView = getLayoutInflater().inflate(R.layout.address_dialog, null);

            //SETEAR LOS ELEMENTOS DE DIRECCION
            etDireccion = mView.findViewById(R.id.et_direccion_cliente);
            spDepartamento = mView.findViewById(R.id.sp_departamento);
            spProvincia = mView.findViewById(R.id.sp_provincia);
            spMunicipio = mView.findViewById(R.id.sp_municipio);
            spDepartamento.setOnItemSelectedListener(spinner_item_selected);
            spProvincia.setOnItemSelectedListener(spinner_item_selected);

            //AGREGAMOS LOS DEPARTAMENTOS, LAS PROVINCIAS Y LOS MUNICPIOS AL SPINNER
            adapterDepartamento = new ArrayAdapter<Departamento>(getActivity(), R.layout.spinner_item_ubigeo, listaDepartamentos);
            adapterProvincia = new ArrayAdapter<Provincia>(getActivity(), R.layout.spinner_item_ubigeo, listaProvincias);
            adapterMunicipio = new ArrayAdapter<Municipio>(getActivity(), R.layout.spinner_item_ubigeo, listaMunicipios);
            spDepartamento.setAdapter(adapterDepartamento);
            spProvincia.setAdapter(adapterProvincia);
            spMunicipio.setAdapter(adapterMunicipio);

            //HABILITAR EL BOTON DE REGISTRAR DIRECCIONES
            Button btnRegistrar = mView.findViewById(R.id.btn_registrar_direccion);

            btnRegistrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String newAddress = etDireccion.getText().toString().trim();
                    if(!newAddress.isEmpty()){
                        //OBTENEMOS EL INDEX DEL SPINNER DEL DEPARTAMENTO
                        int index;
//                        index = spDepartamento.getSelectedItemPosition();
//                        Departamento departamento = listaDepartamentos.get(index);
////
////                        //OBTENEMOS EL INDEX DE PROVINCIA PARA OBTENER LA PROVINCIA SELECCIONADA
//                        index = spProvincia.getSelectedItemPosition();
//                        Provincia provincia = listaProvincias.get(index);
//
//                        //INDEX DE MUNICPIO
                        index = spMunicipio.getSelectedItemPosition();
                        Municipio municipio = listaMunicipios.get(index);

                        //EN REALIDAD SOLO INTERESA EL ID MUNICIPIO PORQUE ESE TIENE EL ID PROVINCIA
                        //Y A SU VEZ ESE TIENE EL ID DEPARTAMENTO

                      /*  Direccion direccion = new Direccion(0, newAddress, municipio, null);
                        addresses.add(direccion);*/
                        AddressAdapter addressAdapter = new AddressAdapter(addresses);
                        rvDirecciones.setAdapter(addressAdapter);
                        Toast.makeText(getActivity(), "Dirección agregada", Toast.LENGTH_SHORT).show();
                        etDireccion.setText("");
                        dialog.dismiss();
                    }
                    else {
                        Toast.makeText(getActivity(), "Debe ingresar una dirección", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            mBuilder.setView(mView);
            dialog = mBuilder.create();
            dialog.show();
        }
    };
    //----------------------FIN EVENTO AGREGAR DIRECCIONES--------------------------

    //------------FILTRAR CUANDO SE SELECCIONA UN ALMACEN
    AdapterView.OnItemSelectedListener spAlmacen_selected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            try{
                if(parent.getId() == spAlmacen.getId()) {
                    Almacen almacen = (Almacen) parent.getSelectedItem();
                    listaTarifarios = Tarifario.find(Tarifario.class,
                            "almacen = ?", String.valueOf(almacen.getId()));
                    adapterTarifario = new ArrayAdapter<Tarifario>(getActivity(), R.layout.spinner_item_ubigeo, listaTarifarios);
                    spTarifario.setAdapter(adapterTarifario);
                }
            } catch (Exception ex) {

            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    //---------------------FILTRAR CUANDO SE SELECCIONA DEPARTAMENTO Y PROVINCIA
    AdapterView.OnItemSelectedListener spinner_item_selected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            try {
                if(parent.getId() == spDepartamento.getId()) {
                    Departamento departamento = (Departamento) parent.getSelectedItem();
                    listaProvincias = Provincia.find(Provincia.class, "departamento = ?",
                            String.valueOf(departamento.getId()));
                    adapterProvincia = new ArrayAdapter<Provincia>(getActivity(), R.layout.spinner_item_ubigeo, listaProvincias);
                    spProvincia.setAdapter(adapterProvincia);
                }
                else if(parent.getId() == spProvincia.getId()) {
                    Provincia provincia = (Provincia) parent.getSelectedItem();
                listaMunicipios = Municipio.find(Municipio.class, "provincia = ?",
                        String.valueOf(provincia.getId()));
                    adapterMunicipio = new ArrayAdapter<Municipio>(getActivity(), R.layout.spinner_item_ubigeo, listaMunicipios);
                    spMunicipio.setAdapter(adapterMunicipio);
                }
            } catch (Exception ex) {
                Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };
    //-----------------FIN FILTRAR CUANDO SE SELECCIONA DEPARTAMENTO Y PROVINCIA


    //--------------EVENTO PARA ASIGNAR LOS CLIENTES PRINCIPALES AL LEAD-----------------------
    private View.OnClickListener btnAgregarClientesPrincipales_Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
            View mView = getLayoutInflater().inflate(R.layout.dialog_main_clients, null);

            if(rbPrincipal.isChecked())
                ((TextView)mView.findViewById(R.id.tv_titulo_lista_clientes)).setText("CLIENTES SECUNDARIOS");
            else
                ((TextView)mView.findViewById(R.id.tv_titulo_lista_clientes)).setText("CLIENTES PRINCIPALES");
            rvClientesSeleccionados = mView.findViewById(R.id.rv_lista_select_clients);
            rvClientesSeleccionados.setLayoutManager(new LinearLayoutManager(getContext()));
            selectClientAdapter = new SelectClientAdapter(allClients, selectedClients, false);
            rvClientesSeleccionados.setAdapter(selectClientAdapter);

            //HABILITAR EL BOTON DE AGREGAR CLIENTES
            Button btnAgregarClientes = mView.findViewById(R.id.btn_agregar_cliente_seleccionado);
            mBuilder.setView(mView);
            final AlertDialog dialog = mBuilder.create();
            dialog.show();

            btnAgregarClientes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedClients = new ArrayList<>(selectClientAdapter.getSelectedClients());
                    MainClientAdapter adapter = new MainClientAdapter(selectedClients);
                    rvClientes.setAdapter(adapter);
                    dialog.dismiss();
                }
            });
        }
    };
    //----------------------FIN EVENTO ASIGNAR CLIENTES PRINCIPALES AL LEAD--------------------------

    //------------------REGISTRAR CLIENTE SECUNDARIO--------------------------
    private View.OnClickListener btnCrearCliente_Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //PRIMERO SE VERIFICARÁ SI SE QUIERE REGISTRAR UN CLIENTE PRINCIPAL O SECUNDARIO
            int newIdCliente = (int)Cliente.count(Cliente.class) + 1;
            if(rbPrincipal.isChecked()) { //REGISTRAR CLIENTE PRINCIPAL
                RegistrarClientePrincipal(newIdCliente);
            }
            else if(rbSecundario.isChecked()) { //REGISTRAR CLIENTE SECUNDARIO
                RegistrarClienteSecundario(newIdCliente);
            }
        }
    };

    private void RegistrarClientePrincipal(int newIdCliente) {
        if(!datosValidosClientePrincipal()) return;
        //INICIO DE REGISTRO
        PersonaContacto p = null;
        Tarifario tarifario = (Tarifario)spTarifario.getSelectedItem();
        Almacen almacen = (Almacen)spAlmacen.getSelectedItem();

        int idPersonaContacto = (int)PersonaContacto.count(PersonaContacto.class)+1;
     /*   p = new PersonaContacto(idPersonaContacto,
                etNombrePersonaContacto.getText().toString().trim(),
                etCargoPersonaContacto.getText().toString().trim(),
                etTelefoonoPersonaContacto.getText().toString().trim(),
                etCorreoPersonaContacto.getText().toString().trim());*/
//        p.save();

      /*  Cliente cliente = new Cliente(newIdCliente, p, almacen, "",
                etNumeroDocumento.getText().toString().trim(),
                etRazonSocial.getText().toString().trim(),
                etTelefono.getText().toString().trim(),
                etCorreo.getText().toString().trim(),
                etComentario.getText().toString().trim(),
                "123456789", 1, 0, 0, 0,
                1, tarifario,"Efectivo");*/
       /* syncData.registrarClienteEnDb(cliente,p,tarifario.getCodigoListaPrecio(),id_vendedor,
                getActivity(),selectedClients,addresses,apidata);*/
       // cliente.save();

        for (Direccion direccion: addresses) {
           /* int newIdDireccion = (int)Direccion.count(Direccion.class) + 1;
            direccion.setIdDireccion(newIdDireccion);
            direccion.setCliente(cliente);
            direccion.save();*/
        }

        for (Cliente clienteSecundario: selectedClients) {
           // ClientePrincipalSecundario relacionClientes = new ClientePrincipalSecundario(cliente, clienteSecundario);
           // relacionClientes.save();
        }


    }

    private boolean datosValidosClientePrincipal() {
        //VALIDACIONES
        eliminarEspaciosBlanco();
        if(razonSocial.isEmpty()) {
            Toast.makeText(getActivity(), "Ingrese la razón social del cliente", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(razonSocial.length() < 3) {
            Toast.makeText(getActivity(), "El tamaño del texto de la razón social debe ser mayor o igual a 3 caracteres", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(numeroDocumento.isEmpty()) {
            Toast.makeText(getActivity(), "Ingrese el número de documento", Toast.LENGTH_SHORT).show();
            return false;
        }

        try{
            long numero = Long.parseLong(numeroDocumento);
        }
        catch (Exception ex){
            Toast.makeText(getActivity(), "El número de documento ingresado no es numérico", Toast.LENGTH_SHORT).show();
            return false;
        }

        switch(numeroDocumento.length()){
            case 9:
                break;
            case 7:
                break;
            default:
                Toast.makeText(getActivity(), "EL tamaño del número de documento debe ser de 7 o 9 dígitos", Toast.LENGTH_SHORT).show();
                return false;
        }


        if (telefono.isEmpty()) {
            Toast.makeText(getActivity(), "Ingrese un número telefónico", Toast.LENGTH_SHORT).show();
            return false;
        }

        try{
            long numero = Long.parseLong(telefono);
        }catch(Exception ex){
            Toast.makeText(getActivity(),"El teléfono ingresado debe ser numérico",Toast.LENGTH_SHORT).show();
            return false;
        }

        if(telefono.length() < 7 || telefono.length() > 8) {
            Toast.makeText(getActivity(), "El tamaño del número telefónico ingresado debe ser de 7 u 8 dígitos", Toast.LENGTH_SHORT).show();
            return false;
        }


        if(telefono.length() == 7){
            int dig_1 = Character.getNumericValue(telefono.charAt(0));
            switch(dig_1){
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                default:
                    Toast.makeText(getActivity(), "El número telefónico debe empezar con 2, 3 o 4", Toast.LENGTH_SHORT).show();
                    return false;
            }
        }
        if(telefono.length() == 8) {
            int car_ini = Character.getNumericValue(telefono.charAt(0));
            switch (car_ini){
                case 6:
                    break;
                case 7:
                    break;
                default :
                    Toast.makeText(getActivity(), "El número telefónico debe empezar con 6 o 7", Toast.LENGTH_SHORT).show();
                    return false;
            }
        }

        //Las validaciones de si es un correo se hará luego
        if (correo.isEmpty()) {
            Toast.makeText(getActivity(), "Ingrese un correo electrónico", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(correo.length() < 10) {
            Toast.makeText(getActivity(), "El tamaño del correo electrónico debe ser mayor a 10 caracteres", Toast.LENGTH_SHORT).show();
            return false;
        }

        boolean esCorreo = validarSiEsCorreo(correo);
        if(!esCorreo){
            Toast.makeText(getActivity(),"El correo electrónico ingresado no es válido",Toast.LENGTH_SHORT).show();
            return false;
        }

        if(!comentario.isEmpty() && comentario.length() < 10) {
            Toast.makeText(getActivity(), "Si va a ingregar un comentario, el tamaño del texto debe ser mayor o igual a 10 caracteres. Caso contrario dejelo vacío.", Toast.LENGTH_LONG).show();
            return false;
        }

        if(listaAlmacenes.size() == 0) {
            Toast.makeText(getActivity(), "No hay almacenes registrados", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(listaTarifarios.size() == 0) {
            Toast.makeText(getActivity(), "No hay tarifarios registrados con el almacén seleccionado", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(addresses.size() == 0) {
            Toast.makeText(getActivity(), "Debe ingresar al menos una dirección", Toast.LENGTH_SHORT).show();
            return false;
        }

        //DATOS DE PERSONA DE CONTACTO
        if (nombreContacto.isEmpty()) {
            Toast.makeText(getActivity(), "Debe ingresar el nombre de la persona contacto", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(nombreContacto.length() < 3) {
            Toast.makeText(getActivity(), "El tamaño de texto del nombre del contacto debe ser mayor o igual a 3", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (cargoContacto.isEmpty()) {
            Toast.makeText(getActivity(), "Debe ingresar el cargo de la persona de contacto", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(cargoContacto.length() < 3) {
            Toast.makeText(getActivity(), "El tamaño de texto del cargo del contacto debe ser mayor o igual a 3", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (telefonoContacto.isEmpty()) {
            Toast.makeText(getActivity(), "Debe ingresar el telefono de la persona contacto", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(telefonoContacto.length() < 7) {
            Toast.makeText(getActivity(), "El tamaño del número telefónico del contacto debe ser mayor o igual a 7", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(telefonoContacto.length() > 8) {
            Toast.makeText(getActivity(), "El tamaño del número telefónico del contacto no puede ser mayor a 8 dígitos", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(telefonoContacto.length() == 7){
            int dig_1 = Character.getNumericValue(telefonoContacto.charAt(0));
            switch(dig_1){
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                default:
                    Toast.makeText(getActivity(), "El número telefónico del contacto debe empezar con 2, 3 o 4", Toast.LENGTH_SHORT).show();
                    return false;
            }
        }
        if(telefonoContacto.length() == 8) {
            int car_ini = Character.getNumericValue(telefonoContacto.charAt(0));
            switch (car_ini){
                case 6:
                    break;
                case 7:
                    break;
                default :
                    Toast.makeText(getActivity(), "El número telefónico del contacto debe empezar con 6 o 7", Toast.LENGTH_SHORT).show();
                    return false;
            }
        }

        if (correoContacto.isEmpty()) {
            Toast.makeText(getActivity(), "Debe ingresar el correo de la persona contacto", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(correoContacto.length() < 10) {
            Toast.makeText(getActivity(), "El tamaño del correo electrónico debe ser mayor a 10 caracteres", Toast.LENGTH_SHORT).show();
            return false;
        }

        boolean validarCorreoContacto = validarSiEsCorreo(correoContacto);
        if(!validarCorreoContacto){
            Toast.makeText(getActivity(),"El correo electrónico del contacto ingresado no es válido",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void eliminarEspaciosBlanco() {
        //Este método elimina los espacios en blanco de todos los campos de texto
        razonSocial = etRazonSocial.getText().toString().trim();
        numeroDocumento = etNumeroDocumento.getText().toString().trim();
        telefono = etTelefono.getText().toString().trim();
        correo = etCorreo.getText().toString().trim();
        comentario = etComentario.getText().toString().trim();
        nombreContacto = etNombrePersonaContacto.getText().toString().trim();
        cargoContacto = etCargoPersonaContacto.getText().toString().trim();
        telefonoContacto = etTelefoonoPersonaContacto.getText().toString().trim();
        correoContacto = etCorreoPersonaContacto.getText().toString().trim();

        //Una vez quitado, se vuelve a colocar los textos extraídos sin espacios en los campos de texto
        etRazonSocial.setText(razonSocial);
        etNumeroDocumento.setText(numeroDocumento);
        etTelefono.setText(telefono);
        etCorreo.setText(correo);
        etComentario.setText(comentario);
        etNombrePersonaContacto.setText(nombreContacto);
        etCargoPersonaContacto.setText(cargoContacto);
        etTelefoonoPersonaContacto.setText(telefonoContacto);
        etCorreoPersonaContacto.setText(correoContacto);
    }

    private void RegistrarClienteSecundario(int newIdCliente) {
        if(!datosValidosClienteSecundario()) return;
        //INICIO DE REGISTRO
        PersonaContacto p = null;

        int idPersonaContacto = (int)PersonaContacto.count(PersonaContacto.class)+1;
        if(!etNombrePersonaContacto.getText().toString().isEmpty()) {
           /* p = new PersonaContacto(idPersonaContacto,
                    etNombrePersonaContacto.getText().toString().trim(),
                    etCargoPersonaContacto.getText().toString().trim(),
                    etTelefoonoPersonaContacto.getText().toString().trim(),
                    etCorreoPersonaContacto.getText().toString().trim());*/
        }

       /* Cliente cliente = new Cliente(newIdCliente, p, null, "",
                etNumeroDocumento.getText().toString().trim(),
                etRazonSocial.getText().toString().trim(),
                etTelefono.getText().toString().trim(),
                etCorreo.getText().toString().trim(),
                etComentario.getText().toString().trim(),
                "", 2, 0, 0, 0,
                1, null,"tarjeta");*/
       // cliente.save();

        for (Direccion direccion: addresses) {
            int newIdDireccion = (int)Direccion.count(Direccion.class) + 1;
          //  direccion.setIdDireccion(newIdDireccion);
           // direccion.setCliente(cliente);
            direccion.save();
        }

        for (Cliente clientePrincipal: selectedClients) {
           // ClientePrincipalSecundario relacionClientes = new ClientePrincipalSecundario(clientePrincipal, cliente);
           // relacionClientes.save();
        }

       /// syncData.registrarClienteEnDb(cliente,p,0,this.id_vendedor,getActivity(),selectedClients,addresses,apidata);
//        Toast.makeText(getActivity(), "Cliente registrado", Toast.LENGTH_SHORT).show();
//        getActivity().finish();
    }

    private boolean datosValidosClienteSecundario() {
        //VALIDACIONES
        eliminarEspaciosBlanco();
        String razonSocial,numeroDocumento,telefono,correo;

        razonSocial = etRazonSocial.getText().toString();
        numeroDocumento = etNumeroDocumento.getText().toString();
        telefono = etTelefono.getText().toString();
        correo = etCorreo.getText().toString();

        if(razonSocial.isEmpty()) {
            Toast.makeText(getActivity(), "Ingrese el nombre del cliente", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(razonSocial.length()<3){
            Toast.makeText(getActivity(),"El nombre del cliente debe tener al menos 3 caracteres",Toast.LENGTH_SHORT).show();
            return false;
        }

        if(numeroDocumento.isEmpty()) {
            Toast.makeText(getActivity(), "Ingrese el número de documento", Toast.LENGTH_SHORT).show();
            return false;
        }

        try{
            long numero = Long.parseLong(numeroDocumento);
        }
        catch (Exception ex){
            Toast.makeText(getActivity(), "El número de documento ingresado no es numérico", Toast.LENGTH_SHORT).show();
            return false;
        }

        switch(numeroDocumento.length()){
            case 9:
                break;
            case 7:
                break;
            default:
                Toast.makeText(getActivity(), "EL tamaño del número de documento debe ser de 7 o 9 dígitos", Toast.LENGTH_SHORT).show();
                return false;
        }

        if (telefono.isEmpty()) {
            Toast.makeText(getActivity(), "Ingrese un número telefónico", Toast.LENGTH_SHORT).show();
            return false;
        }

        try{
            long numero = Long.parseLong(telefono);
        }catch(Exception ex){
            Toast.makeText(getActivity(),"El teléfono ingresado debe ser numérico",Toast.LENGTH_SHORT).show();
            return false;
        }

        if(telefono.length() < 7 || telefono.length() > 8) {
            Toast.makeText(getActivity(), "El tamaño del número telefónico ingresado debe ser de 7 u 8 dígitos", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(telefono.length() == 7){
            int dig_1 = Character.getNumericValue(telefono.charAt(0));
            switch(dig_1){
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                default:
                    Toast.makeText(getActivity(), "El número telefónico debe empezar con 2, 3 o 4", Toast.LENGTH_SHORT).show();
                    return false;
            }
        }
        if(telefono.length() == 8) {
            int car_ini = Character.getNumericValue(telefono.charAt(0));
            switch (car_ini){
                case 6:
                    break;
                case 7:
                    break;
                default :
                    Toast.makeText(getActivity(), "El número telefónico debe empezar con 6 o 7", Toast.LENGTH_SHORT).show();
                    return false;
            }
        }



        if(!etCorreo.getText().toString().isEmpty()) {
            boolean esCorreo = validarSiEsCorreo(correo);
            if (!esCorreo) {
                Toast.makeText(getActivity(), "El correo electrónico ingresado no es válido", Toast.LENGTH_SHORT).show();
                return false;
            }
            if(correo.length() < 10) {
                Toast.makeText(getActivity(), "El tamaño del correo electrónico debe ser mayor a 10 caracteres", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        if(!comentario.isEmpty() && comentario.length() < 10) {
            Toast.makeText(getActivity(), "Si va a ingregar un comentario, el tamaño del texto debe ser mayor o igual a 10 caracteres. Caso contrario dejelo vacío.", Toast.LENGTH_LONG).show();
            return false;
        }

        if (selectedClients.size() == 0) {
            Toast.makeText(getActivity(), "Debe seleccionar al menos un cliente principal", Toast.LENGTH_SHORT).show();
            return false;
        }


        if(!nombreContacto.isEmpty() && nombreContacto.length() < 3) {
            Toast.makeText(getActivity(), "El nombre de la persona de contacto debe ser mayor o igual a 3", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!cargoContacto.isEmpty() && cargoContacto.length() < 3) {
            Toast.makeText(getActivity(), "El cargo de la persona de contacto debe tener al menos 3 caracteres", Toast.LENGTH_SHORT).show();
            return false;
        }


        if (!telefonoContacto.isEmpty() && telefono.length() < 7) {
            Toast.makeText(getActivity(), "El número telefónico del contacto debe tener al menos 7 dígitos", Toast.LENGTH_SHORT).show();
            return false;
        }


        if(!telefonoContacto.isEmpty() && telefonoContacto.length() > 8) {
            Toast.makeText(getActivity(), "El número telefónico del contacto no puede ser mayor a 8 dígitos", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(!telefonoContacto.isEmpty() && telefonoContacto.length() == 7){
            int dig_1 = Character.getNumericValue(telefonoContacto.charAt(0));
            switch(dig_1){
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                default:
                    Toast.makeText(getActivity(), "El número telefónico del contacto debe empezar con 2, 3 o 4", Toast.LENGTH_SHORT).show();
                    return false;
            }
        }
        if(!telefonoContacto.isEmpty() && telefonoContacto.length() == 8) {
            int car_ini = Character.getNumericValue(telefonoContacto.charAt(0));
            switch (car_ini){
                case 6:
                    break;
                case 7:
                    break;
                default :
                    Toast.makeText(getActivity(), "El número telefónico del contacto debe empezar con 6 o 7", Toast.LENGTH_SHORT).show();
                    return false;
            }
        }

        if(!correoContacto.isEmpty() && correoContacto.length() < 10) {
            Toast.makeText(getActivity(), "El tamaño del correo electrónico debe ser mayor a 10 caracteres", Toast.LENGTH_SHORT).show();
            return false;
        }

        boolean validarCorreoContacto = validarSiEsCorreo(correoContacto);
        if(!correoContacto.isEmpty() && !validarCorreoContacto){
            Toast.makeText(getActivity(),"El correo electrónico del contacto ingresado no es válido",Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;

    }

    private boolean validarSiEsCorreo(String usuario){
        Matcher matcher = pattern.matcher(usuario);
        if(matcher.matches()) return true;
        else return false;
    }
    //----------------------------------FIN REGISTRAR CLIENTE SECUNDARIO-----------------------------
}
