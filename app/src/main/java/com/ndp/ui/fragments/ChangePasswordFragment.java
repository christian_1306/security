package com.ndp.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.Vendedor;
import com.ndp.models.retrofit.ApiData;
import com.ndp.models.retrofit.SyncData;
import com.ndp.utils.use.usePhone;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordFragment extends Fragment {

    private Vendedor vendedor;
    private Cliente cliente;
    private Button btnActualizarContrasenia;
    private View view;
    private SyncData syncData;
    private ApiData apiData;
    private ImageView ivRetroceder;
    private boolean esVendedor = false;
    private EditText et_password_actual,et_password_nuevo_1,et_password_nuevo_2;

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public boolean isEsVendedor() {
        return esVendedor;
    }

    public void setEsVendedor(boolean esVendedor) {
        this.esVendedor = esVendedor;
    }

    public ChangePasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_change_password, container, false);
        setElements();
        return  view;
    }

    private void setElements(){
        btnActualizarContrasenia = view.findViewById(R.id.btn_actualizar_password);
        btnActualizarContrasenia.setOnClickListener(cambiar_password);
        et_password_actual = view.findViewById(R.id.etPassword_actual);
        et_password_nuevo_1 = view.findViewById(R.id.etPassword_nuevo_1);
        et_password_nuevo_2 = view.findViewById(R.id.etPassword_nuevo_2);
        ivRetroceder = view.findViewById(R.id.iv_back_toolbar);
        ivRetroceder.setOnClickListener(cerrarFragment);
        ((TextView)view.findViewById(R.id.tv_title_toolbar)).setText("CONTRASEÑA");
        syncData = new SyncData();
        apiData = new ApiData();
        apiData.setImei(usePhone.getIMEI(getActivity()));
    }

    private View.OnClickListener cambiar_password = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String password_actual,password_nuevo_1,password_nuevo_2;
            password_actual = et_password_actual.getText().toString();
            if(esVendedor) {
                if (!password_actual.equals(vendedor.getPassword())) {
                    Toast.makeText(getActivity(), "La contraseña actual ingresada es incorrecta", Toast.LENGTH_SHORT).show();
                    return;
                }
                password_nuevo_1 = et_password_nuevo_1.getText().toString();
                password_nuevo_2 = et_password_nuevo_2.getText().toString();
                if (!password_nuevo_1.equals(password_nuevo_2)) {
                    Toast.makeText(getActivity(), "Las contraseñas nuevas son diferentes", Toast.LENGTH_SHORT).show();
                    return;
                }
                syncData.changePasswordInDB(password_nuevo_1,vendedor.getIdVendedor(),2,getActivity(),apiData);
            }
            else{
                /*if(!password_actual.equals(cliente.getPassword())){
                    Toast.makeText(getActivity(), "La contraseña actual ingresada es incorrecta", Toast.LENGTH_SHORT).show();
                    return;
                }*/
                password_nuevo_1 = et_password_nuevo_1.getText().toString();
                password_nuevo_2 = et_password_nuevo_2.getText().toString();

                int cantidad_numeros = 0;
                if (!password_nuevo_1.equals(password_nuevo_2)) {
                    Toast.makeText(getActivity(), "Las contraseñas nuevas son diferentes", Toast.LENGTH_SHORT).show();
                    return;
                }

                for(int i=0;i<password_nuevo_1.length();i++){
                    if(Character.isDigit(password_nuevo_1.charAt(i))) cantidad_numeros++;
                }

                if(cantidad_numeros<3){
                    Toast.makeText(getActivity(), "La contraseña nueva debe contener al menos 3 números", Toast.LENGTH_SHORT).show();
                    return;
                }
                //syncData.changePasswordInDB(password_nuevo_1,cliente.getIdCliente(),1,getActivity(),apiData);
            }
        }
    };


    private View.OnClickListener cerrarFragment = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getActivity().getSupportFragmentManager().popBackStackImmediate();
        }
    };
}
