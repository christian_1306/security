package com.ndp.ui.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.Factura;
import com.ndp.ui.adapters.AccountStatusAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountStatusFragment extends Fragment {

    private View view;
    private RecyclerView rvEstados;
    private EditText et_buscar;
    private Cliente cliente;
    private AccountStatusAdapter accountStatusAdapter;


    public AccountStatusFragment() {
        // Required empty public constructor
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_account_status, container, false);
        setElemetns();
        fillRecyclerView();
        return view;
    }

    private void setElemetns() {
        rvEstados = view.findViewById(R.id.rv_account_status);
        rvEstados.setLayoutManager(new LinearLayoutManager(getContext()));
        et_buscar = view.findViewById(R.id.barSearch_etBuscar);
        et_buscar.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(et_buscar,InputMethodManager.SHOW_IMPLICIT);
                et_buscar.requestFocus();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= et_buscar.getRight() - et_buscar.getTotalPaddingRight()) {
                        //CODIGO
                        String buscar = et_buscar.getText().toString();
                        if(buscar.isEmpty()) initializeData(false);
                        else initializeData(true);
                    }
                }
                return true;
            }
        });
    }

    private void fillRecyclerView() {
        AccountStatusAdapter adapter = new AccountStatusAdapter(cliente);
        rvEstados.setAdapter(adapter);
    }

    private void initializeData(boolean filtrar){
        try{
            if(filtrar){

                String buscar = et_buscar.getText().toString();
                String query = "select * from cliente where razon_social like '" + buscar + "%' and tipo_cliente = 2";
                List<Cliente> listaClientes = Cliente.findWithQuery(Cliente.class,query);
                if(listaClientes.size()!=0) {
                    Cliente cliente = Cliente.findWithQuery(Cliente.class, query).get(0);
                    String query2 = "select * from factura where referencia = " + cliente.getId() + " and cliente = " + this.cliente.getId();
                    List<Factura> facturas = Factura.findWithQuery(Factura.class, query2);
                    accountStatusAdapter = new AccountStatusAdapter(facturas);
                    rvEstados.setAdapter(accountStatusAdapter);
                }
                else{
                    Toast.makeText(getContext(),"No se encontraron resultados",Toast.LENGTH_SHORT).show();
                }
            }
            else{
                accountStatusAdapter = new AccountStatusAdapter(this.cliente);
                rvEstados.setAdapter(accountStatusAdapter);
            }
        }catch(Exception ex){
            ex.getMessage();
        }
    }

}
