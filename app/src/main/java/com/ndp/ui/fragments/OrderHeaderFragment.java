package com.ndp.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.Date;
import java.util.List;

import com.ndp.R;
import com.ndp.models.appRest.apiOrderHeader;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.CondicionPago;
import com.ndp.models.model.User;
import com.ndp.ui.activities.CustomersActivity;
import com.ndp.utils.Popup.Popup;

import static com.ndp.utils.Const.CODE_ORDER;
import static com.ndp.utils.Useful.getCol;
import static com.ndp.utils.use.useAPP.isNullOrEmpty;
import static com.ndp.utils.use.usePhone.getcodCreacionApp;

public class OrderHeaderFragment extends Fragment {

    private RelativeLayout order_header_customer;
    private RelativeLayout order_header_admin;
    private RelativeLayout order_header_second;
    private ExpandableRelativeLayout order_detail_customer;
    private ExpandableRelativeLayout order_detail_admin;
    private ExpandableRelativeLayout order_detail_second;
    private ImageView order_header_customer_arrow;
    private ImageView order_header_admin_arrow;
    private ImageView order_header_second_arrow;
    private TextView tv_cardcode;
    private TextView tv_cardname;
    private TextView tv_numero_documento;
    private TextView tv_telefonos;
    private TextView tv_correo;
    private TextView tv_cardcode_second;
    private TextView tv_cardname_second;
    private TextView tv_numero_documento_second;
    private TextView tv_telefonos_second;
    private TextView tv_correo_second;
    private TextView tv_linea_credito;
    private TextView tv_saldo_credito;
    private TextView tv_condition;
    private EditText et_cardname_second;
    private EditText et_numero_documento_second;
    private Cliente cliente;
    private OrderSumFragment orderSumFragment;
    private View v;
    private Activity _self;
    private Intent i;
    private apiOrderHeader apiOrderHeader;
    private String codCreacionApp;
    private User currentUser;
    private SharedPreferences shrPreLogin;
    private boolean isEditText = false;

    public OrderHeaderFragment() {
        // ***Required empty public constructor***
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_order_header, container, false);
        setElements();
        return v;
    }

    //region setElements
    @SuppressLint("ClickableViewAccessibility")
    private void setElements()
    {
        order_header_customer = v.findViewById(R.id.order_header_customer);
        order_header_admin = v.findViewById(R.id.order_header_admin);
        order_header_second = v.findViewById(R.id.order_header_second);
        order_detail_customer = v.findViewById(R.id.order_detail_customer);
        order_detail_admin = v.findViewById(R.id.order_detail_admin);
        order_detail_second = v.findViewById(R.id.order_detail_second);
        order_header_customer_arrow = v.findViewById(R.id.order_header_customer_arrow);
        order_header_admin_arrow = v.findViewById(R.id.order_header_admin_arrow);
        order_header_second_arrow = v.findViewById(R.id.order_header_second_arrow);

        tv_cardcode = v.findViewById(R.id.tv_cardcode);
        tv_cardname = v.findViewById(R.id.tv_cardname);
        tv_numero_documento = v.findViewById(R.id.tv_numero_documento);
        tv_telefonos = v.findViewById(R.id.tv_telefonos);
        tv_correo = v.findViewById(R.id.tv_correo);
        tv_linea_credito = v.findViewById(R.id.tv_linea_credito);
        tv_saldo_credito = v.findViewById(R.id.tv_saldo_credito);
        tv_condition = v.findViewById(R.id.tv_condition);
        tv_cardcode_second = v.findViewById(R.id.tv_cardcode_second);
        tv_cardname_second = v.findViewById(R.id.tv_cardname_second);
        et_cardname_second  = v.findViewById(R.id.et_cardname_second);
        tv_numero_documento_second = v.findViewById(R.id.tv_numero_documento_second);
        et_numero_documento_second = v.findViewById(R.id.et_numero_documento_second);
        tv_telefonos_second = v.findViewById(R.id.tv_telefonos_second);
        tv_correo_second = v.findViewById(R.id.tv_correo_second);

        order_header_customer.setOnClickListener(customOnClick);
        order_header_admin.setOnClickListener(customOnClick);
        order_header_second.setOnClickListener(customOnClick);

        tv_cardcode.setOnClickListener(customOnClick);
        tv_cardname.setOnClickListener(customOnClick);
        tv_correo.setOnClickListener(customOnClick);

        tv_cardcode_second.setOnClickListener(customOnClick);

        codCreacionApp = getString(R.string.type_orden_venta) + getcodCreacionApp(_self);

        if (currentUser == null){
            MDToast mdToast = MDToast.makeText(_self, getString(R.string.msg_error_function), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR );
            mdToast.show();
            _self.finish();
        }


    }
    //endregion

    //region OnClickListener
    private View.OnClickListener customOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.order_header_customer:
                    order_detail_customer.toggle();
                    if (order_detail_customer.isExpanded())
                        order_header_customer_arrow.animate().rotation(180).start();
                    else {
                        order_header_customer_arrow.animate().rotation(0).start();
                    }
                    break;
                case R.id.order_header_admin:
                    order_detail_admin.toggle();
                    if (order_detail_admin.isExpanded())
                        order_header_admin_arrow.animate().rotation(180).start();
                    else {
                        order_header_admin_arrow.animate().rotation(0).start();
                    }
                    break;
                case R.id.order_header_second:
                    order_detail_second.toggle();
                    if (order_detail_second.isExpanded())
                        order_header_second_arrow.animate().rotation(180).start();
                    else {
                        order_header_second_arrow.animate().rotation(0).start();
                    }
                    break;
                case R.id.tv_cardcode:
                    i = new Intent(_self, CustomersActivity.class);
                    i.putExtra(getResources().getString(R.string.objUser), currentUser);
                    i.putExtra(getResources().getString(R.string.valueActivity), String.valueOf(CODE_ORDER));
                    i.putExtra(getResources().getString(R.string.changeCardName), false);
                    startActivityForResult(i,CODE_ORDER);
                    break;
                case R.id.tv_cardcode_second:
                    setCardName();
                    break;
            }
        }
    };
    //endregion

    private void setCardName(){

            Popup.ShowDialogCustomButton(_self,
                    getString(R.string.order_header_msg_cardname),
                    getString(R.string.order_header_title_cardname),
                    getString(R.string.button_text_yes),getString(R.string.button_text_no),
                    Popup.MSG_TYPE_WARNING,
                    Popup.BUTTON_TYPE_SETTINGS,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isEditText = true;
                            orderSumFragment.changeViewDireccion(false,isEditText);
                            tv_cardname_second.setVisibility(View.GONE);
                            tv_numero_documento_second.setVisibility(View.GONE);

                            tv_cardcode_second.setText("");
                            tv_cardname_second.setText("");
                            tv_numero_documento_second.setText("");
                            tv_telefonos_second.setText("");
                            tv_correo_second.setText("");

                            et_cardname_second.setVisibility(View.VISIBLE);
                            et_cardname_second.setText("");
                            et_cardname_second.setEnabled(true);
                            et_cardname_second.setError("Ingresar Nombre");

                            et_numero_documento_second.setVisibility(View.VISIBLE);
                            et_numero_documento_second.setText("");
                            et_numero_documento_second.setEnabled(true);
                            et_numero_documento_second.setError("Ingresar NIT");
                        }
                    },
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                                isEditText = false;
                                orderSumFragment.changeViewDireccion(false,isEditText);
                                et_cardname_second.setEnabled(false);
                                et_cardname_second.setVisibility(View.GONE);
                                tv_cardname_second.setVisibility(View.VISIBLE);

                                et_numero_documento_second.setEnabled(false);
                                et_numero_documento_second.setVisibility(View.GONE);
                                tv_numero_documento_second.setVisibility(View.VISIBLE);

                            if (currentUser != null && currentUser.getTipoUsuario() == 1) {

                                i = new Intent(_self, CustomersActivity.class);
                                i.putExtra(getResources().getString(R.string.objUser), currentUser);
                                i.putExtra(getResources().getString(R.string.valueActivity), String.valueOf(CODE_ORDER));
                                i.putExtra(getResources().getString(R.string.changeCardName), true);
                                startActivityForResult(i,CODE_ORDER);

                            } else if (currentUser != null && currentUser.getTipoUsuario() == 2) {
                                if (cliente != null) {
                                    tv_cardcode_second.setText(cliente.getCardCode());
                                    tv_cardname_second.setText(isNullOrEmpty(cliente.getRazonSocial()));
                                    tv_numero_documento_second.setText(isNullOrEmpty(cliente.getNumeroDocIdentidad()));
                                    tv_telefonos_second.setText(isNullOrEmpty(cliente.getTelefono()));
                                    tv_correo_second.setText(isNullOrEmpty(cliente.getCorreo()));
                                    orderSumFragment.changeDireccionSecond(cliente.getCardCode());
                                }
                            }
                        }
                    }, null
            );
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            cliente = (Cliente) data.getSerializableExtra(getResources().getString(R.string.obj_customer));
            boolean isChangeAll = data.getBooleanExtra(getResources().getString(R.string.changeCardName),false);
            if (cliente != null) {

                String CardCode = isNullOrEmpty(cliente.getCardCode());
                String CardName = isNullOrEmpty(cliente.getRazonSocial());
                String NumDoc = isNullOrEmpty(cliente.getNumeroDocIdentidad());
                String Telefono = isNullOrEmpty(cliente.getTelefono());
                String Correo = isNullOrEmpty(cliente.getCorreo());

                if (!isChangeAll) {

                    tv_cardcode.setText(CardCode);
                    tv_cardname.setText(CardName);
                    tv_numero_documento.setText(NumDoc);
                    tv_telefonos.setText(Telefono);
                    tv_correo.setText(Correo);
                    tv_linea_credito.setText(String.valueOf(cliente.getLineaCredito()));
                    tv_saldo_credito.setText(String.valueOf(cliente.getSaldoCredito()));

                    if (cliente.getCondicionDePago() != null) {
                        List<CondicionPago> condicionPagos = CondicionPago.find(CondicionPago.class,
                                getCol("codigo") + " = '" + cliente.getCondicionDePago() + "'" );
                        if ( condicionPagos != null && condicionPagos.size() > 0) {
                            tv_condition.setText(condicionPagos.get(0).getNombre());
                        } else {
                            tv_condition.setText(isNullOrEmpty(cliente.getCondicionDePago()));
                        }

                    } else {
                        tv_condition.setText(isNullOrEmpty(cliente.getCondicionDePago()));
                    }


                    tv_cardcode_second.setText(CardCode);
                    tv_cardname_second.setText(CardName);
                    tv_numero_documento_second.setText(NumDoc);
                    tv_telefonos_second.setText(Telefono);
                    tv_correo_second.setText(Correo);
                    orderSumFragment.setText(isNullOrEmpty(Correo));
                    orderSumFragment.changeDireccionPrincipal(CardCode);
                    orderSumFragment.changeDireccionSecond(CardCode);

                } else {
                    tv_cardcode_second.setText(CardCode);
                    tv_cardname_second.setText(CardName);
                    tv_numero_documento_second.setText(NumDoc);
                    tv_telefonos_second.setText(Telefono);
                    tv_correo_second.setText(Correo);
                    orderSumFragment.changeDireccionSecond(CardCode);
                }
            } else  {
                MDToast.makeText(_self, getString(R.string.error_ins_customer), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR ).show();
            }
        } else {
            MDToast.makeText(_self, getString(R.string.error_ins_customer), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR ).show();
        }
    }

    public OrderSumFragment getOrderSumFragment() {
        return orderSumFragment;
    }

    public void setOrderSumFragment(OrderSumFragment orderSumFragment) {
        this.orderSumFragment = orderSumFragment;
    }



    public apiOrderHeader getApiOrderHeader() {
        apiOrderHeader = new apiOrderHeader();

        //region Control de migración
        apiOrderHeader.setUserAccount(currentUser.getCorreoUsuario());
        apiOrderHeader.setTypeObject(1);
        apiOrderHeader.setFechaRegistro(new Date());
        apiOrderHeader.setEstadoMigracion(1);
        apiOrderHeader.setMensajeMigracion("");
        //endregion

        apiOrderHeader.setCardCode(tv_cardcode.getText().toString());
        if (isEditText) apiOrderHeader.setCardName(et_cardname_second.getText().toString());
        if (isEditText) apiOrderHeader.setNit(et_numero_documento_second.getText().toString());
        if (!isEditText) apiOrderHeader.setCardName(tv_cardname_second.getText().toString());
        if (!isEditText) apiOrderHeader.setNit(tv_numero_documento_second.getText().toString());
        apiOrderHeader.setCodCreacionApp(codCreacionApp);
        apiOrderHeader.setUsuarioCreacion(currentUser.getCorreoUsuario());

        return apiOrderHeader;
    }

    public void setApiOrderHeader(apiOrderHeader apiOrderHeader) {
        this.apiOrderHeader = apiOrderHeader;
    }


    public Activity get_self() {
        return _self;
    }

    public void set_self(Activity _self) {
        this._self = _self;
    }


    public String getCodCreacionApp() {
        return codCreacionApp;
    }

    public void setCodCreacionApp(String codCreacionApp) {
        this.codCreacionApp = codCreacionApp;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void setCustomer (Cliente customer) {


           try  {

                   tv_cardcode.setText(customer.getCardCode());
                   tv_cardname.setText(isNullOrEmpty(customer.getRazonSocial()));
                   tv_numero_documento.setText(isNullOrEmpty(customer.getNumeroDocIdentidad()));
                   tv_telefonos.setText(isNullOrEmpty(customer.getTelefono()));
                   tv_correo.setText(isNullOrEmpty(customer.getCorreo()));
                   tv_linea_credito.setText(isNullOrEmpty(String.valueOf(customer.getLineaCredito())));
                   tv_saldo_credito.setText(isNullOrEmpty(String.valueOf(customer.getSaldoCredito())));
                   //tv_condition.setText(isNullOrEmpty(customer.getCondicionDePago()));

                   if (customer.getCondicionDePago() != null) {
                       List<CondicionPago> condicionPagos = CondicionPago.find(CondicionPago.class,
                               getCol("codigo") + " = '" + customer.getCondicionDePago() + "'" );
                       if ( condicionPagos != null && condicionPagos.size() > 0) {
                           tv_condition.setText(condicionPagos.get(0).getNombre());
                       } else {
                           tv_condition.setText(isNullOrEmpty(customer.getCondicionDePago()));
                       }

                   } else {
                       tv_condition.setText(isNullOrEmpty(customer.getCondicionDePago()));
                   }

                   tv_cardcode_second.setText(customer.getCardCode());
                   tv_cardname_second.setText(isNullOrEmpty(customer.getRazonSocial()));
                   tv_numero_documento_second.setText(isNullOrEmpty(customer.getNumeroDocIdentidad()));
                   tv_telefonos_second.setText(isNullOrEmpty(customer.getTelefono()));
                   tv_correo_second.setText(isNullOrEmpty(customer.getCorreo()));
                   orderSumFragment.setText(isNullOrEmpty(isNullOrEmpty(customer.getCorreo())));
                   orderSumFragment.changeDireccionPrincipal(customer.getCardCode());
                   orderSumFragment.changeDireccionSecond(customer.getCardCode());
                   tv_cardcode.setOnClickListener(null);
                   tv_cardcode_second.setOnClickListener(customOnClick);
                   cliente = customer;
           } catch (Exception e) {
               MDToast.makeText(_self, e.getMessage(), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR ).show();
               _self.finish();
           }

    }
}
