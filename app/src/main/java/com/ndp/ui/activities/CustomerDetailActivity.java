package com.ndp.ui.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.User;
import com.ndp.ui.fragments.OrderSumFragment;

public class CustomerDetailActivity extends AppCompatActivity {

    private RelativeLayout order_header_customer;
    private RelativeLayout order_header_admin;
    private RelativeLayout order_header_second;
    private ExpandableRelativeLayout order_detail_customer;
    private ExpandableRelativeLayout order_detail_admin;
    private ExpandableRelativeLayout order_detail_second;
    private ImageView order_header_customer_arrow;
    private ImageView order_header_admin_arrow;
    private ImageView order_header_second_arrow;
    private TextView tv_cardcode;
    private TextView tv_cardname;
    private TextView tv_numero_documento;
    private TextView tv_telefonos;
    private TextView tv_correo;
    private TextView tv_cardcode_second;
    private TextView tv_cardname_second;
    private TextView tv_numero_documento_second;
    private TextView tv_telefonos_second;
    private TextView tv_correo_second;
    private TextView tv_linea_credito;
    private TextView tv_saldo_credito;
    private TextView tv_condition;
    private EditText et_cardname_second;
    private EditText et_numero_documento_second;
    private Cliente cliente;
    private OrderSumFragment orderSumFragment;
    private Activity _self;
    private Intent i;
    private com.ndp.models.appRest.apiOrderHeader apiOrderHeader;
    private String codCreacionApp;
    private User currentUser;
    private SharedPreferences shrPreLogin;
    private boolean isEditText = false;
    private TextView tv_title_toolbar;
    private ImageView ivRetroceder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_detail);
        setElements();
    }



    @SuppressLint("ClickableViewAccessibility")
    private void setElements()
    {
        _self = this;
        Toolbar toolbar = findViewById(R.id.tb_view);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null ) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setTitle("");
        } else {
            this.finish();
        }
        tv_title_toolbar = findViewById(R.id.tv_title_toolbar);
        tv_title_toolbar.setText("Cliente");
        ivRetroceder = findViewById(R.id.iv_back_toolbar);
        ivRetroceder.setOnClickListener(closeItems);

        order_header_customer = findViewById(R.id.order_header_customer);
        order_header_admin = findViewById(R.id.order_header_admin);
        order_header_second = findViewById(R.id.order_header_second);
        order_detail_customer = findViewById(R.id.order_detail_customer);
        order_detail_admin = findViewById(R.id.order_detail_admin);
        order_detail_second = findViewById(R.id.order_detail_second);
        order_header_customer_arrow = findViewById(R.id.order_header_customer_arrow);
        order_header_admin_arrow = findViewById(R.id.order_header_admin_arrow);
        order_header_second_arrow = findViewById(R.id.order_header_second_arrow);

        tv_cardcode = findViewById(R.id.tv_cardcode);
        tv_cardname = findViewById(R.id.tv_cardname);
        tv_numero_documento = findViewById(R.id.tv_numero_documento);
        tv_telefonos = findViewById(R.id.tv_telefonos);
        tv_correo = findViewById(R.id.tv_correo);
        tv_linea_credito = findViewById(R.id.tv_linea_credito);
        tv_saldo_credito = findViewById(R.id.tv_saldo_credito);
        tv_condition = findViewById(R.id.tv_condition);
        tv_cardcode_second = findViewById(R.id.tv_cardcode_second);
        tv_cardname_second = findViewById(R.id.tv_cardname_second);
        et_cardname_second  = findViewById(R.id.et_cardname_second);
        tv_numero_documento_second = findViewById(R.id.tv_numero_documento_second);
        et_numero_documento_second = findViewById(R.id.et_numero_documento_second);
        tv_telefonos_second = findViewById(R.id.tv_telefonos_second);
        tv_correo_second = findViewById(R.id.tv_correo_second);

        order_header_customer.setOnClickListener(customOnClick);
        order_header_admin.setOnClickListener(customOnClick);
        order_header_second.setOnClickListener(customOnClick);

        //tv_cardcode.setOnClickListener(customOnClick);
        //tv_cardname.setOnClickListener(customOnClick);
        tv_correo.setOnClickListener(customOnClick);

        //tv_cardcode_second.setOnClickListener(customOnClick);

        //codCreacionApp = getString(R.string.type_orden_venta) + getcodCreacionApp(_self);

       /* if (currentUser == null){
            MDToast mdToast = MDToast.makeText(_self, getString(R.string.msg_error_function), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR );
            mdToast.show();
            this.finish();
        }*/

        cliente = (Cliente) getIntent().getSerializableExtra(getResources().getString(R.string.obj_customer));
        tv_cardcode.setText(cliente.getCardCode());
        tv_cardname.setText(cliente.getRazonSocial());
        tv_numero_documento.setText(cliente.getNumeroDocIdentidad());
        tv_telefonos.setText(cliente.getTelefono());
        tv_correo.setText(cliente.getCorreo());
        tv_linea_credito.setText("SOL 0.00");
        tv_saldo_credito.setText("SOL 0.00");
        tv_condition.setText("Contado");

        tv_telefonos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + cliente.getTelefono()));
                startActivity(intent);
            }
        });

        tv_correo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL, cliente.getCorreo());
                startActivity(Intent.createChooser(intent, "Enviar correo"));
            }
        });
    }

    private View.OnClickListener closeItems = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    private View.OnClickListener customOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.order_header_customer:
                    order_detail_customer.toggle();
                    if (order_detail_customer.isExpanded())
                        order_header_customer_arrow.animate().rotation(180).start();
                    else {
                        order_header_customer_arrow.animate().rotation(0).start();
                    }
                    break;
                case R.id.order_header_admin:
                    order_detail_admin.toggle();
                    if (order_detail_admin.isExpanded())
                        order_header_admin_arrow.animate().rotation(180).start();
                    else {
                        order_header_admin_arrow.animate().rotation(0).start();
                    }
                    break;
            }
        }
    };
}
