package com.ndp.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.valdesekamdem.library.mdtoast.MDToast;

import com.ndp.R;
import com.ndp.data.api.SyncData;
import com.ndp.utils.Popup.Popup;
import com.ndp.utils.Useful;
import com.ndp.utils.use.usePhone;

import static com.ndp.utils.Const.PUT_TIMEOUT;
import static com.ndp.utils.Const.PUT_USERNAME;

public class SendEmailActivity extends AppCompatActivity {

    ImageView imgBack;
    EditText etUsuario;
    Button btnEnviar;
    String email;
    String pin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_email);
        initActivity();
    }

    //region initActivity
    public void initActivity() {
        setElements();
        OnClickListener();
    }
    //endregion

    //region setElements
    private void setElements() {
        etUsuario = findViewById(R.id.etUsuario);
        btnEnviar = findViewById(R.id.btnEnviar);
        imgBack = findViewById(R.id.imgBack);
    }
    //endregion

    //region OnClickListener
    public void OnClickListener()
    {
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarDatos()) {
                    btnEnviar.setEnabled(false);
                    usePhone.hideKeyBoard(SendEmailActivity.this);
                    email = etUsuario.getText().toString();
                    new InternetCheck().execute();
                }
            }
        });
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
    }
    //endregion

    //region validarDatos
    private boolean validarDatos() {

        if(etUsuario.getText().toString().isEmpty()) {
            Toast.makeText(this,R.string.empty_correo,Toast.LENGTH_SHORT).show();
            return false;
        }

        if(!Useful.validarSiEsCorreo(etUsuario.getText().toString())) {
            Toast.makeText(this,R.string.invalid_correo,Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }
    //endregion

    //region onBackPressed
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    //endregion

    //region Call Web Services
    @SuppressLint("StaticFieldLeak")
    private class InternetCheck extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            return usePhone.isOnline();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (!result) {
                btnEnviar.setEnabled(true);
                Popup.showAlert(SendEmailActivity.this, getString(R.string.titulo_sin_conexion_internet));
            } else
                new ConnectWithService().execute();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class ConnectWithService extends AsyncTask<Void, Void, Boolean> {
        private ProgressDialog dialog;
        StringBuilder outMessage = new StringBuilder();
        StringBuilder timeOut = new StringBuilder();
        String errMsg = "";

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(SendEmailActivity.this, R.style.ProgressDialog);
            dialog.setTitle(R.string.titulo_autenticacion_email);
            dialog.setMessage(getString(R.string.mensaje_validando_email));
            dialog.setCancelable(false);
            dialog.show();
        }

        protected Boolean doInBackground(Void... args) {
            try {
                return new SyncData(SendEmailActivity.this, 0)
                        .SyncAuthenticationEmail(email,pin,timeOut,outMessage);
            } catch (Exception e) {
                errMsg = e.getMessage();
                return false;
            }
        }

        protected void onPostExecute(Boolean result) {
            String msgException = "";
            if (result) {
                try {
                    MDToast.makeText(SendEmailActivity.this, getString(R.string.email_validate), MDToast.LENGTH_SHORT, MDToast.TYPE_SUCCESS ).show();
                    Intent intent = new Intent(SendEmailActivity.this, ConfirmPinActivity.class);
                    intent.putExtra(PUT_USERNAME, email);
                    intent.putExtra(PUT_TIMEOUT, timeOut.toString());
                    btnEnviar.setEnabled(true);
                    startActivity(intent);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                } catch (Exception ex) {
                    msgException = getString(R.string.exception) + ex.getMessage();
                }
            }

            if (dialog.isShowing()) {
                dialog.dismiss();
                if (!result || !msgException.isEmpty() || !errMsg.isEmpty()) {
                    Popup.showAlert(SendEmailActivity.this, outMessage.toString() + "\n" + msgException+ "\n" + errMsg);
                    btnEnviar.setEnabled(true);
                }
            }

        }
    }
    //endregion


}
