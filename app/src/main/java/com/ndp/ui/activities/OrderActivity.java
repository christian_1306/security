package com.ndp.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.List;

import com.ndp.R;
import com.ndp.data.api.SyncData;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.User;
import com.ndp.ui.adapters.ViewPagerAdapter;
import com.ndp.ui.fragments.OrderDetailsFragment;
import com.ndp.ui.fragments.OrderHeaderFragment;
import com.ndp.ui.fragments.OrderSumFragment;
import com.ndp.utils.Popup.Popup;
import com.ndp.utils.use.usePhone;

import static com.ndp.utils.Useful.getCol;

public class OrderActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private BottomNavigationView navView;
    private MenuItem prevMenuItem;
    private OrderHeaderFragment orderHeaderFragment;
    private OrderDetailsFragment orderDetailsFragment;
    private OrderSumFragment orderSumFragment;
    private Toolbar toolbar;
    private SyncData syncData;
    private Boolean isConnectedREST = true;
    private User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        setElements();
    }

    private void setElements(){
        toolbar = findViewById(R.id.tb_view);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        currentUser = (User) getIntent().getSerializableExtra(getResources().getString(R.string.objUser));
        if (currentUser == null) this.finish();
        ((TextView)findViewById(R.id.tv_title_toolbar)).setText(getResources().getString(R.string.order_sale));
        findViewById(R.id.iv_back_toolbar).setOnClickListener(closeOrder);
        viewPager = findViewById(R.id.viewpager);
        navView = findViewById(R.id.nav_order);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        viewPager.addOnPageChangeListener(pageChangeListener);
        setupViewPager(viewPager);
        //new InternetCheck().execute();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.nav_header:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.nav_detail:
                    viewPager.setCurrentItem(1);
                    return true;
                case R.id.nav_sumary:
                    viewPager.setCurrentItem(2);
                    return true;
            }
            return false;
        }
    };

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (prevMenuItem != null) {
                prevMenuItem.setChecked(false);
            }
            else
            {
                navView.getMenu().getItem(0).setChecked(false);
            }
            navView.getMenu().getItem(position).setChecked(true);
            prevMenuItem = navView.getMenu().getItem(position);
            if (position == 2){
                orderSumFragment.setDetailForPay();
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private void setupViewPager(ViewPager viewPager) {
        viewPager.setCurrentItem(3);
        viewPager.setOffscreenPageLimit(3);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        orderHeaderFragment = new OrderHeaderFragment();
        orderDetailsFragment = new OrderDetailsFragment();
        orderSumFragment = new OrderSumFragment();

        orderHeaderFragment.setOrderSumFragment(orderSumFragment);
        orderHeaderFragment.setCurrentUser(currentUser);
        orderDetailsFragment.setOrderHeaderFragment(orderHeaderFragment);
        orderDetailsFragment.setOrderSumFragment(orderSumFragment);
        orderDetailsFragment.setCurrentUser(currentUser);
        orderSumFragment.setOrderDetailsFragment(orderDetailsFragment);
        orderSumFragment.setOrderHeaderFragment(orderHeaderFragment);
        orderSumFragment.setCurrentUser(currentUser);

        orderDetailsFragment.set_self(this);
        orderHeaderFragment.set_self(this);
        orderSumFragment.set_self(this);

        adapter.addFragment(orderHeaderFragment);
        adapter.addFragment(orderDetailsFragment);
        adapter.addFragment(orderSumFragment);
        viewPager.setAdapter(adapter);
    }

    private View.OnClickListener closeOrder = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            closeOrder();
        }
    };

    @Override
    public void onBackPressed() {
        closeOrder();
    }

    private void closeOrder(){
        Popup.ShowDialog(this,
                getString(R.string.mensaje_exit),
                getString(R.string.modelo_mensaje_titulo_advertencia),
                Popup.MSG_TYPE_WARNING,
                Popup.BUTTON_TYPE_ACCEPT,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                },
                null);
    }

    //region Call Web Services
    @SuppressLint("StaticFieldLeak")
    private class InternetCheck extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            return usePhone.isOnline();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (!result) {
                Popup.ShowDialog(OrderActivity.this,
                        getString(R.string.mensaje_autenticacion_sin_conexión),
                        getString(R.string.titulo_sin_conexion_internet),
                        Popup.MSG_TYPE_SUCCESS,
                        Popup.BUTTON_TYPE_ACCEPT,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                setCustomer();
                                MDToast mdToast = MDToast.makeText(OrderActivity.this,
                                        "Trabajando sin recibir actualizaciones!", MDToast.LENGTH_SHORT, MDToast.TYPE_ERROR );
                                mdToast.show();
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new InternetCheck().execute();
                            }
                        });

            } else {
                new ConnectWithService().execute();
            }
        }

    }


    @SuppressLint("StaticFieldLeak")
    private class ConnectWithService extends AsyncTask<Void, String, Void> {
        private ProgressDialog dialog;
        StringBuilder outMessage = new StringBuilder();
        String errMsg = "";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(OrderActivity.this);
            dialog.setTitle("Operación en curso");
            dialog.setMessage("Mensaje");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            dialog.setMessage(values[0]);
        }

        @Override
        protected Void doInBackground(Void... args) {
            try {

                syncData = new SyncData(OrderActivity.this,1);

                if (isConnectedREST){
                    publishProgress(getResources().getString(R.string.sync_warehouse));
                    isConnectedREST = syncData.SyncAlmacenes(outMessage);
                }

                if (isConnectedREST){
                    publishProgress(getResources().getString(R.string.sync_items));
                    isConnectedREST = syncData.SyncItems(outMessage);
                }

            }catch (Exception ex) {
                errMsg = ex.getMessage();
            }
            return null;
        }

        protected void onPostExecute(Void result) {

            if (dialog.isShowing() && dialog!=null) {
                dialog.dismiss();

                setCustomer();
                if (!isConnectedREST || !errMsg.isEmpty()) {
                    Popup.ShowAlertDialog(OrderActivity.this
                            , getResources().getString(R.string.titulo_error)
                            , errMsg.isEmpty() ? outMessage.toString() : errMsg
                            , new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    new InternetCheck().execute();
                                }
                            }, true);
                } else {
                    Popup.ShowAlertDialog(OrderActivity.this
                            ,getResources().getString(R.string.ready_go_order)
                            , getResources().getString(R.string.update_end)
                            , new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                }
            }
        }
    }

    private void setCustomer(){
        if (currentUser.getTipoUsuario() != null && currentUser.getTipoUsuario() == 2) {
            List<Cliente> clienteList = Cliente.find(Cliente.class, getCol("cardCode") + "='" + currentUser.getCodUsuario()+ "'" );
            if (clienteList != null && clienteList.size() > 0) {
                orderHeaderFragment.setCustomer(clienteList.get(0));
            }
        }
    }
    //endregion
}

