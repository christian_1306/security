package com.ndp.ui.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.leinardi.android.speeddial.SpeedDialActionItem;
import com.leinardi.android.speeddial.SpeedDialView;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.Lead;
import com.ndp.models.model.User;
import com.ndp.ui.adapters.CustomerAdapter;
import com.ndp.utils.Popup.Popup;

import static com.ndp.utils.Shared.getStatusCheckCustomer;
import static com.ndp.utils.Shared.getStatusCheckLead;
import static com.ndp.utils.Shared.storeCustomerFilterStatus;
import static com.ndp.utils.Useful.getCol;


public class CustomersActivity extends AppCompatActivity {

    private SpeedDialView speedDialView;
    private RecyclerView rv_customers;
    private Integer codeActivity;
    private List<Cliente> lstCustomer;
    private List<Cliente> lstCustomerLead;
    private List<Cliente> lstCustomerFilter;
    private Boolean isTrueAction;
    private TextView tv_title_toolbar;
    private ImageView ivRetroceder;
    private Button button;
    private CheckBox checkBox_customer;
    private CheckBox checkBox_lead;
    private Intent intent;
    private SearchView searchView;
    private CustomerAdapter adapter;
    private LinearLayout lyt_empty;
    private User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customers);
        setElements();
        addItems();
        /* ACTIVAR SIN ASYNCTASK
        getCustomers();
        recyclerViewAllData();
        */
        new LoadCustomers().execute();
    }

    //region setElements
    private void setElements() {
        Toolbar toolbar = findViewById(R.id.tb_view);
        setSupportActionBar(toolbar);
        currentUser = (User) getIntent().getSerializableExtra(getResources().getString(R.string.objUser));
        if (currentUser == null || currentUser.getTipoUsuario() == null) this.finish();

        if (getSupportActionBar() != null ) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setTitle("");
        } else {
            this.finish();
        }
        String intKey = getIntent().getStringExtra(getResources().getString(R.string.valueActivity));
        codeActivity = Integer.parseInt((intKey != null) ? intKey : getResources().getString(R.string.zero) );
        isTrueAction = getIntent().getBooleanExtra(getResources().getString(R.string.changeCardName),false);
        tv_title_toolbar = findViewById(R.id.tv_title_toolbar);
        tv_title_toolbar.setText(getResources().getString(R.string.title_customer));
        speedDialView = findViewById(R.id.fab_customers);
        rv_customers = findViewById(R.id.rv_customers);
        ivRetroceder = findViewById(R.id.iv_back_toolbar);
        lyt_empty = findViewById(R.id.lyt_empty);
        ivRetroceder.setOnClickListener(closeCustomer);
    }
    //endregion

    //region addItems
    private void addItems() {
        if (currentUser.getTipoUsuario() != 2) {
            speedDialView.addActionItem(
                    new SpeedDialActionItem.Builder(R.id.add_lead, R.drawable.ic_menu_add_lead)
                            .setFabBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.Tahiti_Gold, getTheme()))
                            .setLabel(getString(R.string.add_lead))
                            .setLabelBackgroundColor(ResourcesCompat.getColor(getResources(),R.color.Gray_DimGray, getTheme()))
                            .setLabelColor(ResourcesCompat.getColor(getResources(),R.color.white, getTheme()))
                            .setLabelClickable(false)
                            .setTheme(R.style.Style_Text_Icon_Fab)
                            .create()
            );

            speedDialView.addActionItem(
                    new SpeedDialActionItem.Builder(R.id.add_customer, R.drawable.ic_menu_add_business)
                            .setFabBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.Tahiti_Gold, getTheme()))
                            .setLabel(getString(R.string.add_customer))
                            .setLabelBackgroundColor(ResourcesCompat.getColor(getResources(),R.color.Gray_DimGray, getTheme()))
                            .setLabelColor(ResourcesCompat.getColor(getResources(),R.color.white, getTheme()))
                            .setLabelClickable(false)
                            .setTheme(R.style.Style_Text_Icon_Fab)
                            .create()
            );
        }

        if (isTrueAction) {
            speedDialView.addActionItem(
                    new SpeedDialActionItem.Builder(R.id.customer_filter, R.drawable.ic_menu_filter)
                            .setFabBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.Tahiti_Gold, getTheme()))
                            .setLabel(getString(R.string.filter_customer))
                            .setLabelBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.Gray_DimGray, getTheme()))
                            .setLabelColor(ResourcesCompat.getColor(getResources(), R.color.white, getTheme()))
                            .setLabelClickable(false)
                            .setTheme(R.style.Style_Text_Icon_Fab)
                            .create()
            );
        }

        speedDialView.setOnChangeListener(new SpeedDialView.OnChangeListener() {
            @Override
            public boolean onMainActionSelected() {
                return false;
            }

            @Override
            public void onToggleChanged(boolean isOpen) {
                if (isOpen){
                    searchView.setIconified(true);
                    searchView.setIconified(true);
                }
            }
        });

        speedDialView.setOnActionSelectedListener(new SpeedDialView.OnActionSelectedListener() {

            @Override
            public boolean onActionSelected(SpeedDialActionItem speedDialActionItem) {
                switch (speedDialActionItem.getId()) {
                    case R.id.customer_filter:
                            getDialogFilter();
                        return false;
                    case R.id.add_customer:
                        intent = new Intent(CustomersActivity.this, CreateBusinessActivity.class);
                        intent.putExtra(getResources().getString(R.string.valueAction), true);
                        startActivity(intent);
                        return false;
                    case R.id.add_lead:
                            intent = new Intent(CustomersActivity.this, CreateBusinessActivity.class);
                            intent.putExtra(getResources().getString(R.string.valueAction), false);
                            startActivity(intent);
                        return false;
                    default:
                        return false;
                }
            }
        });
    }
    //endregion

    //region getCustomers
    public void getCustomers(){
        lstCustomerFilter = new ArrayList<>();
        lstCustomerLead = new ArrayList<>();
        lstCustomer = new ArrayList<>();
        lstCustomer = Cliente.find(Cliente.class,getCol("isLead")+" = 0 AND " + getCol("isActive") + " = 1 AND CARD_CODE NOT NULL AND "
                        + getCol("razonSocial") + " NOT NULL AND " + getCol("numeroDocIdentidad") + " NOT NULL ORDER BY " + getCol("razonSocial"));

        if (isTrueAction) {
            List<Lead> leads = Lead.find(Lead.class, getCol("isActive") + " = 1 AND CARD_CODE NOT NULL AND "
                    + getCol("razonSocial") + " NOT NULL AND " + getCol("numeroDocIdentidad") + " NOT NULL ORDER BY " + getCol("razonSocial"));
            if (leads != null && leads.size() > 0){
                for (Lead lead : leads){
                    Cliente cliente = new Cliente();
                    cliente.setCardCode(lead.getCardCode());
                    cliente.setRazonSocial(lead.getRazonSocial());
                    cliente.setTelefono(lead.getTelefono());
                    cliente.setNumeroDocIdentidad(lead.getNumeroDocIdentidad());
                    cliente.setCorreo(lead.getCorreo());
                    cliente.setIsLead(1);
                    lstCustomerLead.add(cliente);
                    lstCustomer.add(cliente);
                }
            }
            storeCustomerFilterStatus(CustomersActivity.this,true,true);
        }


        if (lstCustomer == null || lstCustomer.size() <= 0){
            MDToast mdToast = MDToast.makeText(this, getString(R.string.not_customers), MDToast.LENGTH_LONG, MDToast.TYPE_INFO );
            mdToast.show();
            this.finish();
        } else {
            lstCustomerFilter.addAll(lstCustomer);
        }
    }
    //endregion

    //region recyclerViewAllData
    public void recyclerViewAllData(){
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(500);
        itemAnimator.setRemoveDuration(500);
        rv_customers.setItemAnimator(itemAnimator);

        rv_customers.setHasFixedSize(true);
        rv_customers.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CustomerAdapter(lstCustomer, codeActivity, isTrueAction, this);
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                checkEmpty();
                super.onChanged();
            }
        });
        rv_customers.setAdapter(adapter);
    }
    //endregion

    private void checkEmpty() {
        if (adapter.getItemCount() <= 0) {
            lyt_empty.setVisibility(View.VISIBLE);
            rv_customers.setVisibility(View.GONE);
        } else {
            lyt_empty.setVisibility(View.GONE);
            rv_customers.setVisibility(View.VISIBLE);
        }
    }

    private void getDialogFilter(){

        Dialog dialog = new Dialog(this, R.style.Dialog_AppTheme_Transparent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_customer_check);

        button = dialog.findViewById(R.id.dialog_alert_btnAceptar);
        checkBox_customer = dialog.findViewById(R.id.checkBox_customer);
        checkBox_lead = dialog.findViewById(R.id.checkBox_lead);

        checkBox_customer.setText(getString(R.string.title_customer));
        checkBox_lead.setText(getString(R.string.title_lead));
        checkBox_customer.setChecked(getStatusCheckCustomer(CustomersActivity.this));
        checkBox_lead.setChecked(getStatusCheckLead(CustomersActivity.this));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkBox_customer.isChecked() && !checkBox_lead.isChecked()) {
                    MDToast mdToast = MDToast.makeText(CustomersActivity.this, getString(R.string.msg_customer_error_filter), MDToast.LENGTH_SHORT, MDToast.TYPE_ERROR );
                    mdToast.show();
                } else {
                    if (checkBox_customer.isChecked() && checkBox_lead.isChecked()){
                        lstCustomer.clear();
                        lstCustomer.addAll(lstCustomerFilter);
                        adapter.notifyDataSetChanged();
                    } else if (checkBox_customer.isChecked()) {
                        lstCustomer.clear();
                        lstCustomer.addAll(lstCustomerFilter);
                        lstCustomer.removeAll(lstCustomerLead);
                        adapter.notifyDataSetChanged();
                    } else if (checkBox_lead.isChecked()) {
                        lstCustomer.clear();
                        lstCustomer.addAll(lstCustomerLead);
                        adapter.notifyDataSetChanged();
                    }

                    storeCustomerFilterStatus(CustomersActivity.this,checkBox_customer.isChecked(),checkBox_lead.isChecked());
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }


    private View.OnClickListener closeCustomer = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();

        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));

        searchView.setOnSearchClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    tv_title_toolbar.setVisibility(View.GONE);
                                                    ivRetroceder.setVisibility(View.GONE);
                                                }
                                            });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                tv_title_toolbar.setVisibility(View.VISIBLE);
                ivRetroceder.setVisibility(View.VISIBLE);
                return false;
            }
        });

        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_search:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }


    @SuppressLint("StaticFieldLeak")
    private class LoadCustomers extends AsyncTask<Void, Void, Boolean> {
        private ProgressDialog dialog;
        StringBuilder outMessage = new StringBuilder();
        String errMsg = "";
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(CustomersActivity.this, R.style.ProgressDialog);
            dialog.setTitle("Operación en curso");
            dialog.setMessage("Obteniendo clientes ...");
            dialog.setCancelable(false);
            dialog.show();
        }

        protected Boolean doInBackground(Void... args) {
            try {
                lstCustomerFilter = new ArrayList<>();
                lstCustomerLead = new ArrayList<>();
                lstCustomer = new ArrayList<>();
                lstCustomer = Cliente.find(Cliente.class,getCol("isLead")+" = 0 AND " + getCol("isActive") + " = 1 AND CARD_CODE NOT NULL AND "
                        + getCol("razonSocial") + " NOT NULL AND " + getCol("numeroDocIdentidad") + " NOT NULL ORDER BY " + getCol("razonSocial"));

                if (isTrueAction) {
                    List<Lead> leads = Lead.find(Lead.class, getCol("isActive") + " = 1 AND CARD_CODE NOT NULL AND "
                            + getCol("razonSocial") + " NOT NULL AND " + getCol("numeroDocIdentidad") + " NOT NULL ORDER BY " + getCol("razonSocial"));
                    if (leads != null && leads.size() > 0){
                        for (Lead lead : leads){
                            Cliente cliente = new Cliente();
                            cliente.setCardCode(lead.getCardCode());
                            cliente.setRazonSocial(lead.getRazonSocial());
                            cliente.setTelefono(lead.getTelefono());
                            cliente.setNumeroDocIdentidad(lead.getNumeroDocIdentidad());
                            cliente.setCorreo(lead.getCorreo());
                            cliente.setIsLead(1);
                            lstCustomerLead.add(cliente);
                            lstCustomer.add(cliente);
                        }
                    }
                    storeCustomerFilterStatus(CustomersActivity.this,true,true);
                }
                return true;
            } catch (Exception e){
                errMsg = e.getMessage();
                return false;
            }
        }

        protected void onPostExecute(Boolean result) {
            String msgException = "";

            if (dialog.isShowing()) {
                dialog.dismiss();
                if (!result || !msgException.isEmpty() || !errMsg.isEmpty()) {
                    Popup.showAlert(CustomersActivity.this, outMessage.toString() + "\n" + msgException + "\n" + errMsg);
                }
            }

            if (result) {
                try {
                    if (lstCustomer == null || lstCustomer.size() <= 0){
                        MDToast mdToast = MDToast.makeText(CustomersActivity.this, getString(R.string.not_customers), MDToast.LENGTH_LONG, MDToast.TYPE_INFO );
                        mdToast.show();
                        finish();
                    } else {
                        lstCustomerFilter.addAll(lstCustomer);
                        recyclerViewAllData();
                    }
                } catch (Exception ex) {
                    msgException = getString(R.string.exception) + ex.getMessage();
                }
            }

        }
    }
}
