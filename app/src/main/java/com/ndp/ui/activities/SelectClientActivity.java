package com.ndp.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.ui.adapters.ClienteAdapter;
import com.ndp.utils.methods.CommonMethods;

public class SelectClientActivity extends AppCompatActivity {

    private ClienteAdapter clienteAdapter;
    private RecyclerView rvClientes;
    private ImageView ivRetroceder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_client);
        setElements();
        fillRecyclerView();
    }

    private void fillRecyclerView(){
        clienteAdapter = new ClienteAdapter(Cliente.find(Cliente.class,"tipo_cliente = ?","1"),this);
        clienteAdapter.setEsVisita(true);
        rvClientes.setLayoutManager(new LinearLayoutManager(this));
        rvClientes.setAdapter(clienteAdapter);
    }

    private void setElements(){
        rvClientes = findViewById(R.id.rvClientes);
        ((TextView)findViewById(R.id.tv_title_toolbar)).setText("CLIENTES");
        ivRetroceder = findViewById(R.id.iv_back_toolbar);
        ivRetroceder.setOnClickListener(CommonMethods.backActivity(this));
    }

    public void seleccionarCliente(Cliente cliente){
        Intent resultIntent = new Intent();
        //resultIntent.putExtra("idCliente",String.valueOf(cliente.getIdCliente()));
        setResult(RESULT_OK,resultIntent);
        finish();
    }
}
