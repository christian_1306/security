package com.ndp.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.chivorn.smartmaterialspinner.SmartMaterialSpinner;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.ndp.R;
import com.ndp.data.api.SyncData;
import com.ndp.models.appRest.apiContactoDetail;
import com.ndp.models.appRest.apiCreateBusiness;
import com.ndp.models.appRest.apiCreateLead;
import com.ndp.models.appRest.apiCreateVisit;
import com.ndp.models.appRest.apiDireccionDetail;
import com.ndp.models.appRest.apiOrderDetail;
import com.ndp.models.appRest.apiOrderHeader;
import com.ndp.models.model.Pendientes;
import com.ndp.models.model.User;
import com.ndp.ui.adapters.PendingAdapter;
import com.ndp.utils.Popup.Popup;
import com.ndp.utils.use.usePhone;

import static com.ndp.utils.Const.CODE_PENDING;
import static com.ndp.utils.Useful.getCol;
import static com.ndp.utils.use.useAPP.getDate;
import static com.ndp.utils.use.useAPP.getDescripcionPorTipo;
import static com.ndp.utils.use.useAPP.getEstadoPending;
import static com.ndp.utils.use.useAPP.getTipoPending;

public class PendingSendActivity extends AppCompatActivity implements PendingAdapter.Listener {

    private Toolbar tb_view;
    private TextView tv_title_toolbar;
    private SmartMaterialSpinner sp_type;
    private SmartMaterialSpinner sp_estado;
    private TextView consult_history_etFechaInicio;
    private TextView consult_history_etFechaFin;
    private FloatingActionButton fab_filter;
    private RecyclerView rv_slopes;
    private LinearLayout lyt_empty;
    private List<String> typePending;
    private List<String> estadoPending;
    private Calendar calendarIni;
    private Calendar calendarFin;
    private List<apiOrderHeader> apiOrderHeaders;
    private List<apiCreateLead> apiCreateLeads;
    private List<apiCreateBusiness> apiCreateBusinesses;
    private List<apiCreateVisit> apiCreateVisits;
    private Integer syncListaPendientes = 0;
    private Pendientes objectPendiente;
    private List<Pendientes> listObjectPendiente;
    private List<Pendientes> pendientesList;
    private SimpleDateFormat dateFormat;
    private PendingAdapter adapter;
    private String tipoPendiente = "";
    private String estadoPendiente = "";
    private Boolean isConnectedREST = true;
    private SyncData syncData;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_send);
        setElements();
        new LoadPending().execute();
    }

    @SuppressLint("SimpleDateFormat")
    private void setElements() {

        user = (User) getIntent().getSerializableExtra(getResources().getString(R.string.objUser));
        if (user == null) this.finish();

        calendarIni = Calendar.getInstance();
        calendarFin = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        pendientesList = new ArrayList<>();
        listObjectPendiente = new ArrayList<>();
        tb_view = findViewById(R.id.tb_view);
        tv_title_toolbar = findViewById(R.id.tv_title_toolbar);
        sp_type = findViewById(R.id.sp_type);
        sp_estado = findViewById(R.id.sp_estado);
        consult_history_etFechaInicio = findViewById(R.id.consult_history_etFechaInicio);
        consult_history_etFechaFin = findViewById(R.id.consult_history_etFechaFin);
        fab_filter = findViewById(R.id.fab_filter);
        rv_slopes = findViewById(R.id.rv_slopes);
        lyt_empty = findViewById(R.id.lyt_empty);

        setSupportActionBar(tb_view);
        if (getSupportActionBar() != null ) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        } else {
            this.finish();
        }

        typePending = getTipoPending();
        estadoPending = getEstadoPending();

        ArrayAdapter<String> adapType = new ArrayAdapter<String>(this, R.layout.spinner_item,typePending);
        ArrayAdapter<String> adapEstado = new ArrayAdapter<String>(this, R.layout.spinner_item,estadoPending);

        sp_type.setAdapter(adapType);
        sp_estado.setAdapter(adapEstado);

        sp_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                String namePosition = typePending.get(position);
                if (namePosition != null && !namePosition.isEmpty()) {
                    tipoPendiente = namePosition.substring(0,1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        sp_estado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                String namePosition = estadoPending.get(position);
                if (namePosition != null && !namePosition.isEmpty()) {
                    estadoPendiente = namePosition.substring(0,1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        consult_history_etFechaInicio.setOnClickListener(customOnClick);
        consult_history_etFechaFin.setOnClickListener(customOnClick);
        fab_filter.setOnClickListener(customOnClick);

    }


    private View.OnClickListener customOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.consult_history_etFechaInicio:
                    getDate(PendingSendActivity.this, consult_history_etFechaInicio, calendarIni,false);
                    break;
                case R.id.consult_history_etFechaFin:
                    getDate(PendingSendActivity.this, consult_history_etFechaFin, calendarFin, false);
                    break;
                case R.id.fab_filter:
                    validateFilter();
                    break;
            }
        }
    };

    private void validateFilter() {

        String tipoP = sp_type.getSelectedItem() != null  ? sp_type.getSelectedItem().toString() : "";
        String estadoP = sp_estado.getSelectedItem() != null  ? sp_estado.getSelectedItem().toString() : "";
        String fecIni = consult_history_etFechaInicio.getText().toString();
        String fecFin = consult_history_etFechaFin.getText().toString();

        if (tipoPendiente.isEmpty() || tipoP.equals(getString(R.string.type_pending))) {
            MDToast.makeText(PendingSendActivity.this, "Por favor, seleccionar tipo pendiente", MDToast.LENGTH_SHORT, MDToast.TYPE_ERROR ).show();
            return;
        }

        if (estadoPendiente.isEmpty() || estadoP.equals(getString(R.string.estado_envio))) {
            MDToast.makeText(PendingSendActivity.this, "Por favor, seleccionar estado envio", MDToast.LENGTH_SHORT, MDToast.TYPE_ERROR ).show();
            return;
        }

        if (fecIni.isEmpty()){
            MDToast.makeText(PendingSendActivity.this, "Por favor, ingresar fecha desde", MDToast.LENGTH_SHORT, MDToast.TYPE_ERROR ).show();
            return;
        }

        if (fecFin.isEmpty()){
            MDToast.makeText(PendingSendActivity.this, "Por favor, ingresar fecha fin", MDToast.LENGTH_SHORT, MDToast.TYPE_ERROR ).show();
            return;
        }

        String filterSend = tipoPendiente + "::" + estadoPendiente + "::" + fecIni + "::" + fecFin + "::";
        //adapter.getFilter().filter("");
        adapter.getFilter().filter(filterSend);

    }

    @Override
    public void onSyncPending(int position) {

        objectPendiente = adapter.getPendiente(position);
        if (objectPendiente != null) {
            switch (objectPendiente.getEstadoMigracion()) {
                case 1:
                    syncListaPendientes = 0;
                    new InternetCheck().execute();
                    break;
                case 2:
                    syncListaPendientes = -1;
                    Popup.ShowAlertDialog(PendingSendActivity.this,
                            getString(R.string.pending)
                            , getDescripcionPorTipo(objectPendiente.getTypeObject()) + getString(R.string.sync_registred_ok)
                            , new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }, true);
                    break;
                case 3:
                    syncListaPendientes = -1;
                    Popup.ShowAlertDialog(PendingSendActivity.this,
                            getString(R.string.pending)
                            , (!objectPendiente.getMensajeMigracion().isEmpty() || objectPendiente.getMensajeMigracion() != null)
                                    ? objectPendiente.getMensajeMigracion()
                                    : getDescripcionPorTipo(objectPendiente.getTypeObject()) + getString(R.string.sync_registred_error)
                            , new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    syncListaPendientes = 0;
                                    new InternetCheck().execute();
                                }
                            }, true);
                    break;
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    private class LoadPending extends AsyncTask<Void, Void, Boolean> {
        private ProgressDialog dialog;
        StringBuilder outMessage = new StringBuilder();
        String errMsg = "";
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(PendingSendActivity.this, R.style.ProgressDialog);
            dialog.setTitle("Operación en curso");
            dialog.setMessage("Obteniendo pendientes ...");
            dialog.setCancelable(false);
            dialog.show();
        }

        protected Boolean doInBackground(Void... args) {
            try {

                apiOrderHeaders = apiOrderHeader.find(apiOrderHeader.class, getCol("userAccount") + " = '"+user.getCorreoUsuario()+"'");
                apiCreateLeads = apiCreateLead.find(apiCreateLead.class, getCol("userAccount") + " = '"+user.getCorreoUsuario()+"'");
                apiCreateBusinesses = apiCreateBusiness.find(apiCreateBusiness.class, getCol("userAccount") + " = '"+user.getCorreoUsuario()+"'");
                apiCreateVisits = apiCreateVisit.find(apiCreateVisit.class, getCol("userAccount") + " = '"+user.getCorreoUsuario()+"'");

                for (apiOrderHeader api : apiOrderHeaders) {
                    Pendientes p = new Pendientes();
                    p.setCodCreacionApp(api.getCodCreacionApp());
                    p.setFechaRegistro(api.getFechaRegistro());
                    p.setTypeObject(api.getTypeObject());
                    p.setDescripcionObject(api.getCardName());
                    p.setEstadoMigracion(api.getEstadoMigracion());
                    p.setMensajeMigracion(api.getMensajeMigracion());
                    pendientesList.add(p);
                }

                for (apiCreateLead api : apiCreateLeads) {
                    Pendientes p = new Pendientes();
                    p.setCodCreacionApp(api.getCodCreacionApp());
                    p.setFechaRegistro(api.getFechaRegistro());
                    p.setTypeObject(api.getTypeObject());
                    p.setDescripcionObject(api.getCardName());
                    p.setEstadoMigracion(api.getEstadoMigracion());
                    p.setMensajeMigracion(api.getMensajeMigracion());
                    pendientesList.add(p);
                }

                for (apiCreateBusiness api : apiCreateBusinesses) {
                    Pendientes p = new Pendientes();
                    p.setCodCreacionApp(api.getCodCreacionApp());
                    p.setFechaRegistro(api.getFechaRegistro());
                    p.setTypeObject(api.getTypeObject());
                    p.setDescripcionObject(api.getRazonSocial());
                    p.setEstadoMigracion(api.getEstadoMigracion());
                    p.setMensajeMigracion(api.getMensajeMigracion());
                    pendientesList.add(p);
                }

                for (apiCreateVisit api : apiCreateVisits) {
                    Pendientes p = new Pendientes();
                    p.setCodCreacionApp(api.getCodCreacionApp());
                    p.setFechaRegistro(api.getFechaRegistro());
                    p.setTypeObject(api.getTypeObject());
                    p.setDescripcionObject(api.getCardName());
                    p.setEstadoMigracion(api.getEstadoMigracion());
                    p.setMensajeMigracion(api.getMensajeMigracion());
                    pendientesList.add(p);
                }

                return true;
            } catch (Exception e){
                errMsg = e.getMessage();
                return false;
            }
        }

        protected void onPostExecute(Boolean result) {
            String msgException = "";

            if (result) {
                try {
                    if (pendientesList == null || pendientesList.size() <= 0){
                        lyt_empty.setVisibility(View.VISIBLE);
                        rv_slopes.setVisibility(View.GONE);
                        MDToast mdToast = MDToast.makeText(PendingSendActivity.this, getString(R.string.not_pending), MDToast.LENGTH_SHORT, MDToast.TYPE_SUCCESS );
                        mdToast.show();
                    } else {
                        lyt_empty.setVisibility(View.GONE);
                        rv_slopes.setVisibility(View.VISIBLE);
                    }
                    recyclerViewAllData();
                } catch (Exception ex) {
                    msgException = getString(R.string.exception) + ex.getMessage();
                }
            }

            if (dialog.isShowing()) {
                dialog.dismiss();
                if (!result || !msgException.isEmpty() || !errMsg.isEmpty()) {
                    Popup.showAlert(PendingSendActivity.this, outMessage.toString() + "\n" + msgException + "\n" + errMsg);
                }
            }

        }
    }

    //region recyclerViewAllData
    private void recyclerViewAllData(){
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(500);
        itemAnimator.setRemoveDuration(500);
        rv_slopes.setItemAnimator(itemAnimator);

        rv_slopes.setHasFixedSize(true);
        rv_slopes.setLayoutManager(new LinearLayoutManager(this));
        Collections.sort(pendientesList,Collections.reverseOrder());
        if  (pendientesList == null) pendientesList = new ArrayList<>();
        adapter = new PendingAdapter(pendientesList, CODE_PENDING, false, this, this);
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                checkEmpty();
                super.onChanged();
            }
        });
        rv_slopes.setAdapter(adapter);
        Date InicioFec = new Date();

        Calendar calRecycler = Calendar.getInstance();
        calRecycler.add(Calendar.DATE, -7);
        Date FinFec = calRecycler.getTime();
        String filterSend = "5::1::" + dateFormat.format(FinFec) + "::" + dateFormat.format(InicioFec) + "::";
        adapter.getFilter().filter(filterSend);
    }
    //endregion

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_send, menu);
        return true;
    }

    private void checkEmpty() {
        if (adapter.getItemCount() <= 0) {
            lyt_empty.setVisibility(View.VISIBLE);
            rv_slopes.setVisibility(View.GONE);
        } else {
            lyt_empty.setVisibility(View.GONE);
            rv_slopes.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_send:
                syncListaPendientes = 1;
                listObjectPendiente = new ArrayList<>();
                listObjectPendiente = adapter.getListPendientes();
                if (listObjectPendiente != null && listObjectPendiente.size() > 0) {
                    new InternetCheck().execute();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    //region Call Web Services
    @SuppressLint("StaticFieldLeak")
    private class InternetCheck extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            return usePhone.isOnline();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (!result) {
                Popup.ShowAlertDialog(PendingSendActivity.this,
                        getString(R.string.titulo_error)
                        , getString(R.string.mensaje_perdida_conexion_internet)
                        , new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new InternetCheck().execute();
                            }
                        }, true);
            } else {
                new SyncPendingService().execute();
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    private class SyncPendingService extends AsyncTask<Void, String, Void> {
        private ProgressDialog dialog;
        StringBuilder outMessage = new StringBuilder();
        String errMsg = "";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(PendingSendActivity.this);
            dialog.setTitle("Operación en curso");
            dialog.setMessage("Mensaje");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            dialog.setMessage(values[0]);
        }

        private boolean syncObject (@NonNull Pendientes pen) {
            try {

                switch (pen.getTypeObject()) {
                    case 1:
                        List<apiOrderHeader> listOrder =
                                apiOrderHeader.find(apiOrderHeader.class, getCol("codCreacionApp") + " = '" + pen.getCodCreacionApp() + "'");
                        if (listOrder != null && listOrder.size() > 0) {
                            List<apiOrderDetail> detalleOrder =
                                    apiOrderDetail.find(apiOrderDetail.class, getCol("codCreacionApp") + " = '" + pen.getCodCreacionApp() + "'");
                            if (detalleOrder != null && detalleOrder.size() > 0)
                                listOrder.get(0).setDetallesPedido(new ArrayList<apiOrderDetail>(detalleOrder));
                            return  syncData.SyncSaveOrder(outMessage, listOrder.get(0));
                        }
                    case 2:
                        List<apiCreateBusiness> listBusiness =
                                apiCreateBusiness.find(apiCreateBusiness.class, getCol("codCreacionApp") + " = '" + pen.getCodCreacionApp() + "'");
                        if (listBusiness != null && listBusiness.size() > 0) {
                            List<apiDireccionDetail> direcciones =
                                    apiDireccionDetail.find(apiDireccionDetail.class, getCol("codCreacionBusiness") + " = '" + pen.getCodCreacionApp() + "'");
                            if (direcciones != null && direcciones.size() > 0)
                                listBusiness.get(0).setListaDirecciones(new ArrayList<apiDireccionDetail>(direcciones));
                            List<apiContactoDetail> contactos =
                                    apiContactoDetail.find(apiContactoDetail.class, getCol("codCreacionBusiness") + " = '" + pen.getCodCreacionApp() + "'");
                            if (contactos != null && contactos.size() > 0)
                                listBusiness.get(0).setListContactos(new ArrayList<apiContactoDetail>(contactos));
                            return syncData.SyncSaveBusiness(outMessage, listBusiness.get(0));
                        }
                    case 3:
                        List<apiCreateLead> listLead =
                                apiCreateLead.find(apiCreateLead.class, getCol("codCreacionApp") + " = '" + pen.getCodCreacionApp() + "'");
                        if (listLead != null && listLead.size() > 0) {
                            return syncData.SyncSaveLead(outMessage, listLead.get(0));
                        }
                    case 4:
                        List<apiCreateVisit> listVisit =
                                apiCreateVisit.find(apiCreateVisit.class, getCol("codCreacionApp") + " = '" + pen.getCodCreacionApp() + "'");
                        if (listVisit != null && listVisit.size() > 0) {
                            return syncData.SyncSaveVisit(outMessage, listVisit.get(0));
                        }
                    default:
                        return true;
                }
            } catch (Exception ex) {
                errMsg = ex.getMessage();
                return false;
            }
        }
        private void setEstadoMigracion(Pendientes objPen) {
            if (isConnectedREST)
                objPen.setEstadoMigracion(2);
            else
                objPen.setEstadoMigracion(3);
        }

        @Override
        protected Void doInBackground(Void... args) {
            try {

                publishProgress(getString(R.string.sync_pending));
                syncData = new SyncData(PendingSendActivity.this,0);
                switch (syncListaPendientes){
                    case 0:
                        if (objectPendiente != null) {
                            publishProgress(getString(R.string.sync_pending)+ "\n" + getDescripcionPorTipo(objectPendiente.getTypeObject()));
                            isConnectedREST = syncObject(objectPendiente);
                            setEstadoMigracion(objectPendiente);
                        }
                        break;
                    case 1:
                        for (Pendientes objpen : listObjectPendiente) {
                            if (objpen.getEstadoMigracion() == 1) {
                                publishProgress(getString(R.string.sync_pending) + "\n" + getDescripcionPorTipo(objpen.getTypeObject()));
                                isConnectedREST = syncObject(objpen);
                                setEstadoMigracion(objpen);
                            }
                        }
                        break;
                }

            } catch (Exception ex) {

                errMsg = ex.getMessage();
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            if (dialog!=null && dialog.isShowing()) {
                dialog.dismiss();
                adapter.notifyDataSetChanged();
                if (!isConnectedREST || !errMsg.isEmpty()) {
                    Popup.showAlert(PendingSendActivity.this, outMessage.toString() + "\n" + errMsg);
                } else {
                    Popup.ShowAlertDialog(PendingSendActivity.this
                            , getString(R.string.pending)
                            , getString(R.string.oper_end)
                            , new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                }
            }
        }
    }
    //endregion

}
