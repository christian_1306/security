package com.ndp.ui.activities;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.ui.adapters.ViewPagerAdapter;
import com.ndp.ui.fragments.CustomerContactoFragment;
import com.ndp.ui.fragments.CustomerDireccionFragment;
import com.ndp.ui.fragments.CustomerInfoFragment;
import com.ndp.utils.Popup.Popup;

public class CustomerInfoActivity extends AppCompatActivity {


    private ViewPager viewPager;
    private BottomNavigationView navView;
    private MenuItem prevMenuItem;
    private Toolbar toolbar;
    private Cliente cliente;
    private CustomerInfoFragment customerInfoFragment;
    private CustomerDireccionFragment customerDireccionFragment;
    private CustomerContactoFragment customerContactoFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_info);
        setElements();
    }


    private void setElements(){
        cliente = (Cliente) getIntent().getSerializableExtra(getResources().getString(R.string.obj_customer));
        if (cliente != null && cliente.getCardCode() != null && !cliente.getCardCode().isEmpty()) {
            toolbar = findViewById(R.id.tb_view);
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            ((TextView) findViewById(R.id.tv_title_toolbar)).setText(getResources().getString(R.string.title_customer_info));
            findViewById(R.id.iv_back_toolbar).setOnClickListener(closeCustomer);
            viewPager = findViewById(R.id.viewpager_customer);
            navView = findViewById(R.id.navview_customer);
            navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
            viewPager.addOnPageChangeListener(pageChangeListener);
            setupViewPager(viewPager);
        } else {
            MDToast mdToast = MDToast.makeText(this, getString(R.string.error_ins_customer), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR );
            mdToast.show();
            this.finish();
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.nav_info_customer:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.nav_direccion:
                    viewPager.setCurrentItem(1);
                    return true;
                case R.id.nav_contacto:
                    viewPager.setCurrentItem(2);
                    return true;
            }
            return false;
        }
    };

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (prevMenuItem != null) {
                prevMenuItem.setChecked(false);
            }
            else
            {
                navView.getMenu().getItem(0).setChecked(false);
            }
            navView.getMenu().getItem(position).setChecked(true);
            prevMenuItem = navView.getMenu().getItem(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    private void setupViewPager(ViewPager viewPager) {
        viewPager.setCurrentItem(3);
        viewPager.setOffscreenPageLimit(3);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        customerInfoFragment = new CustomerInfoFragment();
        customerDireccionFragment = new CustomerDireccionFragment();
        customerContactoFragment = new CustomerContactoFragment();

        customerInfoFragment.set_self(this);
        customerDireccionFragment.set_self(this);
        customerContactoFragment.set_self(this);

        customerInfoFragment.setCliente(cliente);
        customerDireccionFragment.setCliente(cliente);
        customerContactoFragment.setCliente(cliente);

        adapter.addFragment(customerInfoFragment);
        adapter.addFragment(customerDireccionFragment);
        adapter.addFragment(customerContactoFragment);
        viewPager.setAdapter(adapter);
    }

    private View.OnClickListener closeCustomer = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            closeCustomer();
        }
    };

    @Override
    public void onBackPressed() {
        closeCustomer();
    }

    private void closeCustomer(){
        Popup.ShowDialog(this,
                getString(R.string.mensaje_exit),
                getString(R.string.modelo_mensaje_titulo_advertencia),
                Popup.MSG_TYPE_WARNING,
                Popup.BUTTON_TYPE_ACCEPT,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                },
                null);
    }
}
