package com.ndp.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ndp.R;

public class ReportMainActivity extends AppCompatActivity {

    private TextView tv_title_toolbar;
    private ImageView ivRetroceder;
    private CardView card_sale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_main);

        Toolbar toolbar = findViewById(R.id.tb_view);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null ) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setTitle("");
        } else {
            this.finish();
        }
        tv_title_toolbar = findViewById(R.id.tv_title_toolbar);
        tv_title_toolbar.setText("Reportes");
        ivRetroceder = findViewById(R.id.iv_back_toolbar);
        card_sale  = findViewById(R.id.card_sale);
        card_sale.setOnClickListener(cardOnClick);
        ivRetroceder.setOnClickListener(closeItems);
    }

    private View.OnClickListener closeItems = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    private View.OnClickListener cardOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ReportMainActivity.this, ReportMonthActivity.class);
            startActivity(intent);
        }
    };
}
