package com.ndp.ui.activities;


import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ndp.models.dao.DaoMaster;
import com.ndp.models.dao.MemberDao;
import com.ndp.models.dao.MemberDaoDao;
import com.ndp.models.model.Member;
import com.ndp.models.dao.DaoSession;
import com.ndp.utils.methods.SessionManager2;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.HashMap;
import java.util.List;

import com.ndp.R;
import com.ndp.utils.methods.SessionManager;

import org.greenrobot.greendao.database.Database;

import static com.ndp.utils.Useful.getCol;
import static com.ndp.utils.use.useAPP.CerrarSesion;


public class SplashScreenActivity extends AppCompatActivity {

    private MemberDaoDao memberDao;
    private SessionManager2 sessionManager;
    private DaoMaster daoMaster;
    private static DaoSession daoSession;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        initDB();
        initAPP();

        runSplash();
    }


    //region initAPP
    private void initAPP(){

        sessionManager = new SessionManager2(this);
     // daoSession=((App) getApplication()).getDaoSession();
    }

    //region initDB
    private void initDB(){
      //  SugarContext.init(this);

      //  Schema schema = new Schema(1000, "de.greenrobot.daoexample");


        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this,"takeorder-db");
        Database db = helper.getWritableDb();
        daoMaster=new DaoMaster(db);
        daoSession = daoMaster.newSession();


        /*SugarContext.init(this);
        SugarContext.terminate();
        SchemaGenerator schemaGenerator = new SchemaGenerator(getApplicationContext());
        SugarContext.init(getApplicationContext());
        schemaGenerator.createDatabase(new SugarDb(getApplicationContext()).getDB());*/
    }
    //endregion

    //region runSplash
    private void runSplash() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

               if (!sessionManager.checkLogin()) {
                   CerrarSesion(SplashScreenActivity.this, sessionManager);
               } else {
                   HashMap<String, String> user = sessionManager.getUserDetails();
                   memberDao=daoSession.getMemberDaoDao();

                   List<MemberDao> currentUser =memberDao.queryBuilder().where(MemberDaoDao.Properties.Email.eq(SessionManager.KEY_EMAIL)).list();
                   if (currentUser != null && currentUser.size() > 0) {
                       MDToast.makeText(SplashScreenActivity.this, getString(R.string.login_successful) + " "
                               + currentUser.get(0).getName() +"!", MDToast.LENGTH_LONG, MDToast.TYPE_SUCCESS ).show();
                       Intent i = new Intent(SplashScreenActivity.this, HomeActivity.class);
                       i.putExtra(getString(R.string.intent_tipUser),currentUser.get(0).getEmail());
                       startActivity(i);
                       overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                       finish();
                   } else {
                       CerrarSesion(SplashScreenActivity.this, sessionManager);
                   }

               }
            }
        }, 500);
    }
    //endregion

    public static DaoSession getDaoSession() {
        return daoSession;
    }
    //region runSplash
    private void runSplash2() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (!sessionManager.checkLogin()) {
                    CerrarSesion(SplashScreenActivity.this, sessionManager);
                } else {
                    HashMap<String, String> user = sessionManager.getUserDetails();
                    List<Member> currentUser = Member.find(Member.class,getCol("email")+ " = ?",user.get(SessionManager.KEY_EMAIL));
                    if (currentUser != null && currentUser.size() > 0) {
                        MDToast.makeText(SplashScreenActivity.this, getString(R.string.login_successful) + " "
                                + currentUser.get(0).getName() +"!", MDToast.LENGTH_LONG, MDToast.TYPE_SUCCESS ).show();
                        Intent i = new Intent(SplashScreenActivity.this, HomeActivity.class);
                        i.putExtra(getString(R.string.intent_tipUser),currentUser.get(0).getEmail());
                        startActivity(i);
                        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                        finish();
                    } else {
                        CerrarSesion(SplashScreenActivity.this, sessionManager);
                    }

                }
            }
        }, 500);
    }






}
