package com.ndp.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.Vendedor;
import com.ndp.ui.fragments.ChangePasswordFragment;
import com.ndp.ui.fragments.ProfileDetailFragment;
import com.ndp.utils.methods.CommonMethods;

public class ProfileOldActivity extends AppCompatActivity {

    private RelativeLayout rlv_profile,rlv_change_password;
    private Intent intent;
    private Vendedor vendedor;
    private ImageView ivRetroceder;
    private boolean esVendedor = false;
    private Cliente cliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        setElements();
    }

    private void setElements() {
        rlv_profile = findViewById(R.id.rlv_ver_perfil);
        rlv_profile.setOnClickListener(verPerfil);
        rlv_change_password = findViewById(R.id.rlv_change_password);
        rlv_change_password.setOnClickListener(cambiarContrasenia);
        ivRetroceder = findViewById(R.id.iv_back_toolbar);
        ivRetroceder.setOnClickListener(CommonMethods.backActivity(this));

        intent = getIntent();
        String id_vendedor,id_cliente;
        esVendedor = intent.getBooleanExtra("esVendedor",false);
        if(esVendedor) {
            id_vendedor = intent.getStringExtra("vendedor");
            vendedor = Vendedor.find(Vendedor.class, "id_vendedor = ?", id_vendedor).get(0);
        }
        else{
            id_cliente = intent.getStringExtra("cliente");
            cliente = Cliente.find(Cliente.class,"id_cliente = ?",id_cliente).get(0);
        }
        ((TextView)findViewById(R.id.tv_title_toolbar)).setText("CONFIGURACIÓN");
    }

    private View.OnClickListener verPerfil = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(esVendedor) {
                ProfileDetailFragment profileDetailFragment = new ProfileDetailFragment();
                profileDetailFragment.setVendedor(vendedor);
                profileDetailFragment.setEsVendedor(true);
                getSupportFragmentManager().beginTransaction().add(R.id.content_profile, profileDetailFragment).addToBackStack(null).commit();
            }
            else{
                ProfileDetailFragment profileDetailFragment = new ProfileDetailFragment();
                profileDetailFragment.setCliente(cliente);
                profileDetailFragment.setEsVendedor(false);
                getSupportFragmentManager().beginTransaction().add(R.id.content_profile,profileDetailFragment).addToBackStack(null).commit();
            }
        }
    };

    private View.OnClickListener cambiarContrasenia = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            if(esVendedor) {
                ChangePasswordFragment changePasswordFragment = new ChangePasswordFragment();
                changePasswordFragment.setEsVendedor(true);
                changePasswordFragment.setVendedor(vendedor);
                getSupportFragmentManager().beginTransaction().add(R.id.content_profile, changePasswordFragment).addToBackStack(null).commit();
            }
            else{
                ChangePasswordFragment changePasswordFragment = new ChangePasswordFragment();
                changePasswordFragment.setEsVendedor(false);
                changePasswordFragment.setCliente(cliente);
                getSupportFragmentManager().beginTransaction().add(R.id.content_profile,changePasswordFragment).addToBackStack(null).commit();
            }
        }
    };
}
