package com.ndp.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chivorn.smartmaterialspinner.SmartMaterialSpinner;
import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.ndp.R;
import com.ndp.data.api.SyncData;
import com.ndp.models.appRest.apiQuery;
import com.ndp.models.dto.QueryFacturasView;
import com.ndp.models.dto.QueryPedidoView;
import com.ndp.models.dto.QueryVisitaView;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.User;
import com.ndp.ui.adapters.QueryAdapter;
import com.ndp.utils.Popup.Popup;
import com.ndp.utils.use.usePhone;

import static com.ndp.utils.Const.CODE_ORDER;
import static com.ndp.utils.Const.CODE_QUERY;
import static com.ndp.utils.Useful.getCol;
import static com.ndp.utils.secure.AESCrypt.decrypt;
import static com.ndp.utils.use.useAPP.fmDecimal;
import static com.ndp.utils.use.useAPP.getDate;
import static com.ndp.utils.use.useAPP.getEstadoQuery;
import static com.ndp.utils.use.useAPP.getTipoQuery;
import static com.ndp.utils.use.useAPP.isNullOrEmpty;

public class QueriesOnlineActivity extends AppCompatActivity {

    private Toolbar tb_view;
    private TextView tv_title_toolbar;
    private SmartMaterialSpinner sp_type;
    private TextView tv_cardname;
    private TextView consult_history_etFechaInicio;
    private TextView consult_history_etFechaFin;
    private FloatingActionButton fab_filter;
    private LinearLayout lyt_empty;
    private List<String> typeQuery;
    private List<String> estadoQuery;
    private Integer codTipoQuery = -1;
    private String tipoQuery = "";
    private String estadQuery = "";
    private Cliente objCliente;
    private User currentUser;
    private Calendar calendarIni;
    private Calendar calendarFin;
    private SimpleDateFormat dateFormat;
    private Intent i;
    private RecyclerView rv_query;
    private String nomQuery = "";
    private String nomCustomer = "";
    private String fecIni = "";
    private String fecFin = "" ;
    private SyncData syncData;
    private Boolean isConnectedREST = true;
    private List<QueryVisitaView> queryVisitaViews;
    private List<QueryPedidoView> queryPedidoViews;
    private List<QueryFacturasView> queryFacturasViews;
    private apiQuery apiQueryData;
    private QueryAdapter adapter;
    private String jsonParse = "";
    private View line_cardname;
    private SmartMaterialSpinner sp_estado;
    private View line_estado;
    private MenuItem itemToHide;
    private String totalPagar = "BS 0.00";
    private String monedaPrimaria = "BS";
    private String monedaSecundaria = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_queries_online);
        setElements();
    }

    @SuppressLint("SimpleDateFormat")
    private void setElements() {
        currentUser = (User) getIntent().getSerializableExtra(getResources().getString(R.string.objUser));
        if (currentUser == null) this.finish();

        tb_view = findViewById(R.id.tb_view);
        tv_title_toolbar = findViewById(R.id.tv_title_toolbar);
        sp_type = findViewById(R.id.sp_type);
        tv_cardname = findViewById(R.id.tv_cardname);
        consult_history_etFechaInicio = findViewById(R.id.consult_history_etFechaInicio);
        consult_history_etFechaFin = findViewById(R.id.consult_history_etFechaFin);
        fab_filter = findViewById(R.id.fab_filter);
        rv_query = findViewById(R.id.rv_query);
        lyt_empty = findViewById(R.id.lyt_empty);
        line_cardname = findViewById(R.id.line_cardname);
        sp_estado = findViewById(R.id.sp_estado);
        line_estado = findViewById(R.id.line_estado);


        setSupportActionBar(tb_view);
        if (getSupportActionBar() != null ) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        } else {
            this.finish();
        }

        calendarIni = Calendar.getInstance();
        calendarFin = Calendar.getInstance();
        queryVisitaViews = new ArrayList<>();
        queryPedidoViews = new ArrayList<>();
        queryFacturasViews = new ArrayList<>();
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        typeQuery = getTipoQuery();
        ArrayAdapter<String> adapType = new ArrayAdapter<String>(this, R.layout.spinner_item,typeQuery);
        sp_type.setAdapter(adapType);

        sp_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                String namePosition = typeQuery.get(position);
                if (namePosition != null && !namePosition.isEmpty()) {
                    tipoQuery = namePosition.substring(0,1);
                    if (tipoQuery.equals("2")){
                        itemToHide.setVisible(true);
                        sp_estado.setVisibility(View.VISIBLE);
                        line_estado.setVisibility(View.VISIBLE);
                    } else {
                        itemToHide.setVisible(false);
                        sp_estado.setVisibility(View.GONE);
                        line_estado.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        estadoQuery = getEstadoQuery();
        ArrayAdapter<String> adapEstado = new ArrayAdapter<String>(this, R.layout.spinner_item,estadoQuery);
        sp_estado.setAdapter(adapEstado);

        sp_estado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                String namePosition = estadoQuery.get(position);
                if (namePosition != null && !namePosition.isEmpty()) {
                    estadQuery = namePosition.substring(0,1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });




        tv_cardname.setOnClickListener(customOnClick);
        consult_history_etFechaInicio.setOnClickListener(customOnClick);
        consult_history_etFechaFin.setOnClickListener(customOnClick);
        fab_filter.setOnClickListener(customOnClick);

        if (currentUser.getTipoUsuario() != null && currentUser.getTipoUsuario() == 2) {
            line_cardname.setVisibility(View.GONE);
            tv_cardname.setVisibility(View.GONE);

            List<Cliente> clienteList = Cliente.find(Cliente.class, getCol("cardCode") + "='" + currentUser.getCodUsuario()+ "'" );
            if (clienteList != null && clienteList.size() > 0) {
                objCliente = clienteList.get(0);
                tv_cardname.setText(objCliente.getRazonSocial());
            } else {
                MDToast.makeText(QueriesOnlineActivity.this, getString(R.string.error_ins_customer), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR ).show();
                finish();
            }
        }

    }


    private View.OnClickListener customOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.tv_cardname:
                    i = new Intent(QueriesOnlineActivity.this, CustomersActivity.class);
                    i.putExtra(getResources().getString(R.string.objUser), currentUser);
                    i.putExtra(getResources().getString(R.string.valueActivity), String.valueOf(CODE_ORDER));
                    i.putExtra(getResources().getString(R.string.changeCardName), true);
                    startActivityForResult(i,CODE_QUERY);
                    break;
                case R.id.consult_history_etFechaInicio:
                    getDate(QueriesOnlineActivity.this, consult_history_etFechaInicio, calendarIni,false);
                    break;
                case R.id.consult_history_etFechaFin:
                    getDate(QueriesOnlineActivity.this, consult_history_etFechaFin, calendarFin, false);
                    break;
                case R.id.fab_filter:
                    validateFilter();
                    break;
            }
        }
    };





    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            objCliente = (Cliente) data.getSerializableExtra(getResources().getString(R.string.obj_customer));
            boolean isChangeAll = data.getBooleanExtra(getResources().getString(R.string.changeCardName),false);
            if (objCliente != null) {

                String CardCode = isNullOrEmpty(objCliente.getCardCode());
                String CardName = isNullOrEmpty(objCliente.getRazonSocial());

                tv_cardname.setText(CardName);

            } else  {
                MDToast.makeText(QueriesOnlineActivity.this, getString(R.string.error_ins_customer), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR ).show();
            }
        } else {
            MDToast.makeText(QueriesOnlineActivity.this, getString(R.string.error_ins_customer), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR ).show();
        }
    }

    private void validateFilter() {

        nomQuery = sp_type.getSelectedItem() != null  ? sp_type.getSelectedItem().toString() : "";
        nomCustomer = tv_cardname.getText().toString();
        fecIni = consult_history_etFechaInicio.getText().toString();
        fecFin = consult_history_etFechaFin.getText().toString();

        if (tipoQuery.isEmpty() || nomQuery.equals(getString(R.string.type_query))) {
            MDToast.makeText(QueriesOnlineActivity.this, "Por favor, seleccionar tipo de consulta", MDToast.LENGTH_SHORT, MDToast.TYPE_ERROR ).show();
            return;
        }

        if (nomCustomer.isEmpty() || objCliente == null || objCliente.getCardCode() == null || objCliente.getCardCode().isEmpty()) {
            MDToast.makeText(QueriesOnlineActivity.this, "Por favor, seleccionar cliente", MDToast.LENGTH_SHORT, MDToast.TYPE_ERROR ).show();
            return;
        }

        if (tipoQuery.equals("2") && estadQuery.equals("E")) {
            MDToast.makeText(QueriesOnlineActivity.this, "Por favor, seleccionar estado", MDToast.LENGTH_SHORT, MDToast.TYPE_ERROR ).show();
            return;
        }


        if (fecIni.isEmpty()){
            MDToast.makeText(QueriesOnlineActivity.this, "Por favor, ingresar fecha desde", MDToast.LENGTH_SHORT, MDToast.TYPE_ERROR ).show();
            return;
        }

        if (fecFin.isEmpty()){
            MDToast.makeText(QueriesOnlineActivity.this, "Por favor, ingresar fecha fin", MDToast.LENGTH_SHORT, MDToast.TYPE_ERROR ).show();
            return;
        }

        totalPagar = "BS 0.00";
        codTipoQuery = Integer.parseInt(tipoQuery);

        apiQueryData = new apiQuery();
        apiQueryData.setCardCode(objCliente.getCardCode());
        apiQueryData.setFechaInicio(fecIni);
        apiQueryData.setFechaFin(fecFin);
        apiQueryData.setIdMunicipio(0);
        apiQueryData.setCodCreacionApp("");
        apiQueryData.setEstadoMigracion(-1);
        apiQueryData.setUsuarioCreacion(currentUser.getCorreoUsuario());
        if (tipoQuery.equals("2") && !estadQuery.equals("E")) {
            apiQueryData.setEstadoFactura(Integer.parseInt(estadQuery));
        } else {
            apiQueryData.setEstadoFactura(-1);
        }



        new InternetCheck().execute();

    }


    //region Call Web Services
    @SuppressLint("StaticFieldLeak")
    private class InternetCheck extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            return usePhone.isOnline();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (!result) {
                Popup.ShowAlertDialog(QueriesOnlineActivity.this,
                        getString(R.string.titulo_error)
                        , getString(R.string.mensaje_perdida_conexion_internet)
                        , new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new InternetCheck().execute();
                            }
                        }, true);
            } else {
                new QueriesOnlineService().execute();
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    private class QueriesOnlineService extends AsyncTask<Integer, String, Integer> {
        private ProgressDialog dialog;
        StringBuilder outMessage = new StringBuilder();
        String errMsg = "";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(QueriesOnlineActivity.this);
            dialog.setTitle("Operación en curso");
            dialog.setMessage("Mensaje");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            dialog.setMessage(values[0]);
        }



        @Override
        protected Integer doInBackground(Integer... integers) {
            try {

                publishProgress("Validando Servicio ...");
                syncData = new SyncData(QueriesOnlineActivity.this,0);
                switch (codTipoQuery) {
                    case 1:


                        publishProgress("Consultando Pedidos ...");
                        isConnectedREST = true;
                        queryPedidoViews = new ArrayList<>();

                        jsonParse = syncData.SyncListDrafts(outMessage,apiQueryData);
                        if (jsonParse != null) {
                            jsonParse = decrypt(jsonParse);
                            JSONArray objArray = new JSONArray(jsonParse);
                            Gson gson = new Gson();
                            for (int i = 0; i < objArray.length(); i++) {
                                JSONObject object = objArray.getJSONObject(i);
                                QueryPedidoView queryPedidoView = gson.fromJson(object.toString(), QueryPedidoView.class);
                                queryPedidoView.setIsOrder(0);
                                queryPedidoViews.add(queryPedidoView);
                            }
                        }

                        jsonParse = syncData.SyncListOrders(outMessage,apiQueryData);
                        if (jsonParse != null) {
                            jsonParse = decrypt(jsonParse);
                            JSONArray objArray = new JSONArray(jsonParse);
                            Gson gson = new Gson();
                            for (int i = 0; i < objArray.length(); i++) {
                                JSONObject object = objArray.getJSONObject(i);
                                QueryPedidoView queryPedidoView = gson.fromJson(object.toString(), QueryPedidoView.class);
                                queryPedidoView.setIsOrder(1);
                                queryPedidoViews.add(queryPedidoView);
                            }
                        }


                        if (queryPedidoViews != null && queryPedidoViews.size() > 0)
                            return 1;
                        else
                            return -1;

                    case 2:
                        publishProgress("Consultando Facturas ...");
                        isConnectedREST = true;
                        queryFacturasViews = new ArrayList<>();
                        jsonParse = syncData.SyncListInvoces(outMessage,apiQueryData);
                        if (jsonParse != null) {
                            jsonParse = decrypt(jsonParse);
                            JSONArray objArray = new JSONArray(jsonParse);
                            Gson gson = new Gson();
                            double pagarPending = 0;
                            for (int i = 0; i < objArray.length(); i++) {
                                JSONObject object = objArray.getJSONObject(i);
                                QueryFacturasView queryFac = gson.fromJson(object.toString(), QueryFacturasView.class);

                                if (monedaPrimaria.equals(queryFac.getDocCurrency() != null ? queryFac.getDocCurrency() : "BS"))
                                    pagarPending += ( (queryFac.getDocTotal() != null ? queryFac.getDocTotal() : 0 ) - (queryFac.getPaidToDate() != null ? queryFac.getPaidToDate() : 0d ) );

                                queryFacturasViews.add(queryFac);

                            }
                            totalPagar = monedaPrimaria + " " + fmDecimal(pagarPending);
                        }
                        if (queryFacturasViews != null && queryFacturasViews.size() > 0){

                            return 2;
                        }
                        else
                            return -1;
                    case 3:
                        publishProgress("Consultando Visitas ...");
                        isConnectedREST = true;
                        queryVisitaViews = new ArrayList<>();
                        queryVisitaViews = syncData.SyncListVisit(outMessage,apiQueryData);
                        if (queryVisitaViews != null && queryVisitaViews.size() > 0)
                            return 3;
                        else
                            return -1;
                }

            } catch (Exception ex) {

                isConnectedREST = false;
                errMsg = ex.getMessage();

            }
            return -1;
        }

        protected void onPostExecute(Integer result) {
            if (dialog!=null && dialog.isShowing()) {
                dialog.dismiss();
                setData(result);
                if (!isConnectedREST || !errMsg.isEmpty()) {
                    Popup.showAlert(QueriesOnlineActivity.this, outMessage.toString() + "\n" + errMsg);
                }
            }
        }

        private void setData (Integer result) {


            RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
            itemAnimator.setAddDuration(500);
            itemAnimator.setRemoveDuration(500);
            rv_query.setItemAnimator(itemAnimator);
            rv_query.setHasFixedSize(true);
            rv_query.setLayoutManager(new LinearLayoutManager(QueriesOnlineActivity.this));
            switch (result) {
                case -1:
                    rv_query.setVisibility(View.GONE);
                    lyt_empty.setVisibility(View.VISIBLE);
                    break;
                case 1:
                    adapter = new QueryAdapter<QueryPedidoView>(queryPedidoViews,codTipoQuery,QueriesOnlineActivity.this);
                    rv_query.setAdapter(adapter);
                    rv_query.setVisibility(View.VISIBLE);
                    lyt_empty.setVisibility(View.GONE);
                    break;
                case 2:

                    adapter = new QueryAdapter<QueryFacturasView>(queryFacturasViews,codTipoQuery,QueriesOnlineActivity.this);
                    rv_query.setAdapter(adapter);
                    rv_query.setVisibility(View.VISIBLE);
                    lyt_empty.setVisibility(View.GONE);
                    break;
                case 3:
                    adapter = new QueryAdapter<QueryVisitaView>(queryVisitaViews,codTipoQuery,QueriesOnlineActivity.this);
                    rv_query.setAdapter(adapter);
                    rv_query.setVisibility(View.VISIBLE);
                    lyt_empty.setVisibility(View.GONE);
                    break;
            }


        }
    }
    //endregion

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_query_invoice, menu);
        itemToHide = menu.findItem(R.id.total_pagar);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.total_pagar:
                showTotalaPagar();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showTotalaPagar() {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AppTheme_AppBarOverlay));
        builder.setTitle("TOTAL PENDIENTE A PAGAR");
        builder.setIcon(R.drawable.ic_moneda);
        builder.setMessage(totalPagar);
        builder.setPositiveButton("OK", null);

        builder.show();
    }
}
