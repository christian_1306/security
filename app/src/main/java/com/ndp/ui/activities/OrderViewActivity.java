package com.ndp.ui.activities;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;

import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com.ndp.R;
import com.ndp.models.model.DetallePedido;
import com.ndp.models.model.Pedido;
import com.ndp.ui.fragments.OrderDetailFragment;
import com.ndp.ui.fragments.OrderItemFragment;
import com.ndp.ui.fragments.OrderSummaryFragment;
import com.ndp.utils.methods.CommonMethods;

public class OrderViewActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private Pedido pedido;
    private ImageView iv_Retroceder;
    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_view);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        String id = getIntent().getStringExtra("pedido");
        this.pedido = Pedido.find(Pedido.class,"id_pedido = ?",id).get(0);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),this.pedido);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        ((TextView)findViewById(R.id.tv_title_toolbar)).setText("DETALLE PEDIDO");

        iv_Retroceder = findViewById(R.id.iv_back_toolbar);
        iv_Retroceder.setOnClickListener(CommonMethods.backActivity(this));
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_order_view, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

//    /**
//     * A placeholder fragment containing a simple view.
//     */
//    public static class PlaceholderFragment extends Fragment {
//        /**
//         * The fragment argument representing the section number for this
//         * fragment.
//         */
//        private static final String ARG_SECTION_NUMBER = "section_number";
//
//        public PlaceholderFragment() {
//        }
//
//        /**
//         * Returns a new instance of this fragment for the given section
//         * number.
//         */
//        public static PlaceholderFragment newInstance(int sectionNumber) {
//            PlaceholderFragment fragment = new PlaceholderFragment();
//            Bundle args = new Bundle();
//            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//            fragment.setArguments(args);
//            return fragment;
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            View rootView = inflater.inflate(R.layout, container, false);
//            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
//            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
//            return rootView;
//        }
//    }
//
//    /**
//     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
//     * one of the sections/tabs/pages.
//     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private Pedido pedido;
        private OrderItemFragment orderItemFragment;
        private OrderDetailFragment orderDetailFragment;
        private OrderSummaryFragment orderSummaryFragment;

        public Pedido getPedido() {
            return pedido;
        }

        public void setPedido(Pedido pedido) {
            this.pedido = pedido;
        }

        public SectionsPagerAdapter(FragmentManager fm, Pedido pedido) {
             super(fm);
             this.pedido = pedido;

             orderItemFragment = new OrderItemFragment();
             orderItemFragment.setItemForOrder(false);
             orderItemFragment.setForView(true);
             orderItemFragment.setFragmentoIniciado(true);

             List<DetallePedido> detallePedidos = pedido.listarDetalles();

             orderItemFragment.setDetallePedidos(detallePedidos);
            // Tarifario tarifario = detallePedidos.get(0).getTarifarioXArticulo().getTarifario();

             //orderItemFragment.setTarifario(pedido.listarDetalles().get(0).getTarifarioXArticulo().getTarifario());

             orderDetailFragment = new OrderDetailFragment();
             orderDetailFragment.setCliente(pedido.getClientePrincipal());
             orderDetailFragment.setForView(true);
             orderDetailFragment.setPedido(this.pedido);
             if(pedido.getClienteSecundario()!=null) orderDetailFragment.setClienteSecundario(pedido.getClienteSecundario());


             orderSummaryFragment = new OrderSummaryFragment();
             orderSummaryFragment.setForView(true);
             orderSummaryFragment.setFecha(pedido.getFechaEntrega());
             orderSummaryFragment.setMontoTotal(pedido.getTotalFinal());

             int numero_articulos = 0;
             for(DetallePedido detallePedido : pedido.listarDetalles()){
                 numero_articulos+=detallePedido.getCantidad();
             }
             orderSummaryFragment.setNumeroArticulos(numero_articulos);

             orderItemFragment.setOrderDetailFragment(orderDetailFragment);
             orderSummaryFragment.setOrderItemFragment(orderItemFragment);
             orderSummaryFragment.setOrderDetailFragment(orderDetailFragment);
         }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return orderItemFragment;
                case 1:
                    return orderDetailFragment;
                case 2:
                    return orderSummaryFragment;
                default:
                    return  null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }
}
