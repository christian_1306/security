package com.ndp.ui.activities;

import android.content.Intent;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.leinardi.android.speeddial.SpeedDialActionItem;
import com.leinardi.android.speeddial.SpeedDialOverlayLayout;
import com.leinardi.android.speeddial.SpeedDialView;

import com.ndp.R;
import com.ndp.models.model.User;
import com.ndp.utils.Popup.Popup;

import static com.ndp.utils.Const.PUT_USERNAME;

public class ProfileAPPActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageView ivRetroceder;
    private TextView tv_title_toolbar;
    private RelativeLayout order_header_profile;
    private ImageView order_header_profile_arrow;
    private ExpandableRelativeLayout order_detail_profile;
    private TextView tv_code;
    private TextView tv_name;
    private TextView tv_documento;
    private TextView tv_email;
    private TextView tv_type;
    private TextView tv_warehouse;
    private SpeedDialOverlayLayout overlay;
    private SpeedDialView speedDialView;
    private User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_app);
        setElements();
        addItems();
    }


    private void setElements() {
        toolbar = findViewById(R.id.tb_view);
        tv_title_toolbar = findViewById(R.id.tv_title_toolbar);
        ivRetroceder = findViewById(R.id.iv_back_toolbar);
        order_header_profile = findViewById(R.id.order_header_profile);
        order_header_profile_arrow = findViewById(R.id.order_header_profile_arrow);
        order_detail_profile = findViewById(R.id.order_detail_profile);
        tv_code = findViewById(R.id.tv_code);
        tv_name = findViewById(R.id.tv_name);
        tv_documento = findViewById(R.id.tv_documento);
        tv_email = findViewById(R.id.tv_email);
        tv_type = findViewById(R.id.tv_type);
        tv_warehouse = findViewById(R.id.tv_warehouse);
        overlay = findViewById(R.id.overlay);
        speedDialView = findViewById(R.id.fab_profile);


        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null ) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setTitle("");

        } else {

            this.finish();

        }

        currentUser = (User) getIntent().getSerializableExtra(getResources().getString(R.string.objUser));
        if (currentUser == null) this.finish();
        tv_title_toolbar.setText(getString(R.string.account_user));
        tv_code.setText(currentUser.getCodUsuario());
        tv_name.setText(currentUser.getNomUsuario());
        tv_documento.setText(currentUser.getDocUsuario());
        tv_email.setText(currentUser.getCorreoUsuario());
        tv_type.setText(currentUser.getTipoUsuario() == 1 ? "VENDEDOR" : (currentUser.getTipoUsuario() == 2 ? "CLIENTE" : "OTROS"));
        tv_warehouse.setText(currentUser.getAlmNomUsuario());

        order_header_profile.setOnClickListener(customOnClick);
        ivRetroceder.setOnClickListener(customOnClick);
    }


    //region OnClickListener
    private View.OnClickListener customOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.order_header_profile:
                    order_detail_profile.toggle();
                    if (order_detail_profile.isExpanded())
                        order_header_profile_arrow.animate().rotation(180).start();
                    else {
                        order_header_profile_arrow.animate().rotation(0).start();
                    }
                    break;
                case R.id.iv_back_toolbar:
                    close();
                    break;
            }
        }
    };
    //endregion


    //region addItems
    private void addItems() {

            speedDialView.addActionItem(
                    new SpeedDialActionItem.Builder(R.id.profile_password, R.drawable.ic_key_master)
                            .setFabBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.Tahiti_Gold, getTheme()))
                            .setLabel(getString(R.string.Cambiar_contraseña))
                            .setLabelBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.Gray_DimGray, getTheme()))
                            .setLabelColor(ResourcesCompat.getColor(getResources(), R.color.white, getTheme()))
                            .setLabelClickable(false)
                            .setTheme(R.style.Style_Text_Icon_Fab)
                            .create()
            );


        speedDialView.setOnActionSelectedListener(new SpeedDialView.OnActionSelectedListener() {

            @Override
            public boolean onActionSelected(SpeedDialActionItem speedDialActionItem) {
                switch (speedDialActionItem.getId()) {
                    case R.id.profile_password:
                        Intent intent = new Intent(ProfileAPPActivity.this, ConfirmPassActivity.class);
                        intent.putExtra(PUT_USERNAME, currentUser.getCorreoUsuario());
                        intent.putExtra(getResources().getString(R.string.valueAction), true);
                        startActivity(intent);
                        return false;
                    default:
                        return false;
                }
            }
        });
    }
    //endregion


    //region onBack
    private void close(){
        Popup.ShowDialog(this,
                getString(R.string.mensaje_exit),
                getString(R.string.modelo_mensaje_titulo_advertencia),
                Popup.MSG_TYPE_WARNING,
                Popup.BUTTON_TYPE_ACCEPT,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                },
                null);
    }

    @Override
    public void onBackPressed() {
        close();
    }
    //endregion
    //endregion
}
