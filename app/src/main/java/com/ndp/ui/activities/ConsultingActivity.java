package com.ndp.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.Vendedor;
import com.ndp.ui.fragments.ConsultingContentClientFragment;
import com.ndp.ui.fragments.ConsultingContentFragment;
import com.ndp.utils.methods.CommonMethods;

public class ConsultingActivity extends AppCompatActivity {

    private Cliente cliente = null;
    private ImageView ivRetroceder;
    private Vendedor vendedor = null;

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulting);
        getData();
        ((TextView)findViewById(R.id.tv_title_toolbar)).setText("CONSULTAS");
        if(cliente == null) //ES PARA EL VENDEDOR
        {
            ConsultingContentFragment consultingContentFragment = new ConsultingContentFragment();
            consultingContentFragment.setVendedor(this.vendedor);
            getSupportFragmentManager().beginTransaction().add(R.id.content_consulting,consultingContentFragment).addToBackStack(null).commit();
        }
        else { //PARA EL CLIENTE
            ConsultingContentClientFragment consultingContentClientFragment = new ConsultingContentClientFragment();
            consultingContentClientFragment.setCliente(cliente);
            getSupportFragmentManager().beginTransaction().add(R.id.content_consulting, consultingContentClientFragment).addToBackStack(null).commit();
        }
        ivRetroceder = findViewById(R.id.iv_back_toolbar);
        ivRetroceder.setOnClickListener(CommonMethods.backActivity(this));
    }

    private void getData() {
        Intent intent = getIntent();
        int idCliente = intent.getIntExtra("cliente", 0);
        if(idCliente == 0) {
            int idVendedor = intent.getIntExtra("vendedor",0);
            vendedor = Vendedor.find(Vendedor.class,"id_vendedor = ?",String.valueOf(idVendedor)).get(0);
            return;
        }
        cliente = Cliente.find(Cliente.class, "id_cliente = ?", String.valueOf(idCliente)).get(0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
