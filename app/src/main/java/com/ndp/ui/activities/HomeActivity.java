package com.ndp.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Objects;

import com.mapbox.mapboxsdk.log.Logger;
import com.ndp.R;
import com.ndp.data.api.SyncData;
import com.ndp.models.model.Almacen;
import com.ndp.models.model.Articulo;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.CondicionPago;
import com.ndp.models.model.CoreMotivo;
import com.ndp.models.model.CoreRuta;
import com.ndp.models.model.Departamento;
import com.ndp.models.model.Impuesto;
import com.ndp.models.model.Lead;
import com.ndp.models.model.Member;
import com.ndp.models.model.Moneda;
import com.ndp.models.model.User;
import com.ndp.ui.fragments.HomeCustomerFragment;
import com.ndp.ui.fragments.HomeSellerFragment;
import com.ndp.utils.Popup.Popup;
import com.ndp.utils.Useful;
import com.ndp.utils.methods.SessionManager;
import com.ndp.utils.methods.SessionManager2;
import com.ndp.utils.use.usePhone;

import static com.ndp.utils.Const.CODE_HOME;
import static com.ndp.utils.Const.CODE_QUERY;
import static com.ndp.utils.use.useAPP.CerrarSesion;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String nombreUsuario;
    private String numeroDocumentoUsuario;
    private NavigationView navUsuario;
    private long timeBack;
    private Member currentUser;
    private Boolean isConnectedREST = true;
    private Boolean isDataValidate = true;
    private ImageView ivcerrarsesion;
    private ImageView ivsettings;
    private TextView tv_nombreUsuario;
    private TextView tv_dniUsuario;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private SyncData syncData;
    private ImageView iv_sync_home;
    private Integer intForzar = -1;
    private SessionManager2 sessionManager;
    private Integer typeUser;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setElements();
        obtenerUsuario();
    }

    //region setElements
    private void setElements() {
        toolbar = findViewById(R.id.toolbar);
        iv_sync_home = findViewById(R.id.iv_sync_home);
        ivsettings = findViewById(R.id.iv_nav_settings);
        ivcerrarsesion = findViewById(R.id.iv_nav_exit);
        drawer = findViewById(R.id.drawer_layout);
        navUsuario = findViewById(R.id.nav_view);
        View view = navUsuario.getHeaderView(0);
        tv_nombreUsuario = view.findViewById(R.id.tv_nombreUsuario);
        tv_dniUsuario = view.findViewById(R.id.tv_dniUsuario);

        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        sessionManager = new SessionManager2(HomeActivity.this);
        navUsuario.setNavigationItemSelectedListener(this);
        navUsuario.setItemIconTintList(null);
        iv_sync_home.setOnClickListener(onClickCustom);
        ivcerrarsesion.setOnClickListener(onClickCustom);
        ivsettings.setOnClickListener(onClickCustom);

    }
    //endregion

    //region obtenerUsuario
    private void obtenerUsuario() {
        Intent intent = getIntent();
        String correoLogin = intent.getStringExtra(getString(R.string.intent_tipUser));
        // currentUser = (Member)intent.getSerializableExtra("member");
       currentUser = Member.find(Member.class, Useful.getCol("email")  + " = ?", correoLogin).get(0);
       String typousuario=currentUser.getMemberType().toString();
       if(typousuario=="usr")
       {
           typeUser=2;//cliente
       }else {
           typeUser=1;//seller
       }
        if (currentUser != null) {
            nombreUsuario = currentUser.getName();
            numeroDocumentoUsuario = currentUser.getDocumentNumber();
            tv_nombreUsuario.setText(nombreUsuario);
            tv_dniUsuario.setText(numeroDocumentoUsuario);
            if (currentUser.getMemberType().equals("usr")) bloquearOpciones();
            abrirFragmento();
            new InternetCheck().execute();
        } else {
            CerrarSesion(HomeActivity.this,sessionManager);
        }


    }
    //endregion

    //region bloquearOpciones
    private void bloquearOpciones() {
        navUsuario.getMenu().findItem(R.id.nav_clientes).setVisible(false);
        navUsuario.getMenu().findItem(R.id.nav_articulos).setVisible(false);
        navUsuario.getMenu().findItem(R.id.nav_visitas).setVisible(false);
    }
    //endregion

    //region abrirFragmento
    private void abrirFragmento() {
        //Intent i = new Intent(HomeActivity.this, HomeSellerFragment.class);
        //i.putExtra("member",currentUser);
        if(typeUser == 1) { //Home Seller
            HomeSellerFragment homeSellerFragment = new HomeSellerFragment();
            homeSellerFragment.set_Self(this);
            getSupportFragmentManager().beginTransaction().add(R.id.content_home, homeSellerFragment).addToBackStack(null).commit();
        }
        else if (typeUser == 2){ //Home Customer
            HomeCustomerFragment homeCustomerFragment = new HomeCustomerFragment();
            homeCustomerFragment.set_Self(this);
            getSupportFragmentManager().beginTransaction().add(R.id.content_home, homeCustomerFragment).addToBackStack(null).commit();
        } else {
            CerrarSesion(HomeActivity.this,sessionManager);
        }
    }

    private void abrirFragmento2(String tipoUser) {

        if(tipoUser == "sync") { //Home Seller
            HomeSellerFragment homeSellerFragment = new HomeSellerFragment();
            homeSellerFragment.set_Self(this);
            getSupportFragmentManager().beginTransaction().add(R.id.content_home, homeSellerFragment).addToBackStack(null).commit();
        }
        else if (tipoUser == "usr"){ //Home Customer
            HomeCustomerFragment homeCustomerFragment = new HomeCustomerFragment();
            homeCustomerFragment.set_Self(this);
            getSupportFragmentManager().beginTransaction().add(R.id.content_home, homeCustomerFragment).addToBackStack(null).commit();
        } else {
            CerrarSesion(HomeActivity.this,sessionManager);
        }
    }
    //endregion

    //region onNavigationItemSelected
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch (id) {
            case R.id.nav_inicio:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                break;
            case R.id.nav_rutas:
                intent = new Intent(this, RouteNavActivity.class);
                intent.putExtra(getResources().getString(R.string.objUser),currentUser);
                startActivity(intent);
                break;
            case R.id.nav_reportes:
                intent = new Intent(this, ReportMainActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_pedidos:
                intent = new Intent(this, OrderActivity.class);
                intent.putExtra(getResources().getString(R.string.objUser), currentUser);
                startActivity(intent);
                break;
            case R.id.nav_clientes:
                intent = new Intent(this, CustomersActivity.class);
                intent.putExtra(getResources().getString(R.string.objUser), currentUser);
                intent.putExtra(getResources().getString(R.string.valueActivity), String.valueOf(CODE_HOME));
                intent.putExtra(getResources().getString(R.string.changeCardName), true);
                startActivity(intent);
                break;
            case R.id.nav_articulos:
                intent = new Intent(this, ItemsActivity.class);
                intent.putExtra(getResources().getString(R.string.objUser), currentUser);
                intent.putExtra(getResources().getString(R.string.valueActivity), String.valueOf(CODE_HOME));
                intent.putExtra(getResources().getString(R.string.changeCardName), true);
                startActivity(intent);
                break;
            case R.id.nav_pendientes:
                intent = new Intent(this, PendingSendActivity.class);
                intent.putExtra(getResources().getString(R.string.objUser), currentUser);
                intent.putExtra(getResources().getString(R.string.valueActivity), String.valueOf(CODE_HOME));
                intent.putExtra(getResources().getString(R.string.changeCardName), true);
                startActivity(intent);
                break;
            case R.id.nav_consultas:
                intent = new Intent(this, QueriesOnlineActivity.class);
                intent.putExtra(getResources().getString(R.string.objUser), currentUser);
                intent.putExtra(getResources().getString(R.string.valueActivity), String.valueOf(CODE_QUERY));
                intent.putExtra(getResources().getString(R.string.changeCardName), true);
                startActivity(intent);
                break;
            case R.id.nav_visitas:
                intent = new Intent(this, CurrentArrivalActivity.class);
                intent.putExtra(getResources().getString(R.string.objUser), currentUser);
                startActivity(intent);
                break;
            case R.id.iv_nav_settings:
                break;
            case R.id.iv_nav_exit:
                closeSession();
                break;
        }
        return true;
    }
    //endregion


    private View.OnClickListener onClickCustom = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            switch (v.getId()){
                case R.id.iv_sync_home:
                    syncDataProcess();
                    break;
                case R.id.iv_nav_exit:
                    closeSession();
                    break;
                case R.id.iv_nav_settings:
                    break;
            }
        }
    };

    private void syncDataProcess() {
        Popup.ShowDialogCustomButton(HomeActivity.this,
                getString(R.string.mensaje_home_sync),
                getString(R.string.titulo_home_sync),
                "SYNC   TOTAL","SYNC PARCIAL",
                Popup.MSG_TYPE_SUCCESS,
                Popup.BUTTON_TYPE_SYNC,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        intForzar = 0;
                        new InternetCheck().execute();
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        intForzar = 1;
                        new InternetCheck().execute();
                    }
                },null);
    }

    //region Call Web Services
    @SuppressLint("StaticFieldLeak")
    private class InternetCheck extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            return usePhone.isOnline();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (!result) {
                Popup.ShowAlertDialog(HomeActivity.this,
                        getString(R.string.titulo_error)
                        , getString(R.string.mensaje_perdida_conexion_internet_continue)
                        , new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new InternetCheck().execute();
                            }
                        }
                        , new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new ValidateObjectService().execute();
                            }
                        }
                        , false
                        );
            } else {
                new ConnectWithService().execute();
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    private class ConnectWithService extends AsyncTask<Void, String, Void> {
        private ProgressDialog dialog;
        StringBuilder outMessage = new StringBuilder();
        String errMsg = "";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(HomeActivity.this);
            dialog.setTitle("Operación en curso");
            dialog.setMessage("Mensaje");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            dialog.setMessage(values[0]);
        }

        @Override
        protected Void doInBackground(Void... args) {
            try {
                isConnectedREST = true;
                int rowsCondPago = intForzar == 0 ? 0 : (int) CondicionPago.count(CondicionPago.class);
                int rowsMoneda = intForzar == 0 ? 0 :  (int) Moneda.count(Moneda.class);
                int rowsImpuesto = intForzar == 0 ? 0 :  (int) Impuesto.count(Impuesto.class);
                int rowsCliente = intForzar == 0 ? 0 : (int) Cliente.count(Cliente.class);
                int rowsAlmacen  = intForzar == 0 ? 0 : (int) Almacen.count(Almacen.class);
                int rowsArticulo = intForzar == 0 ? 0 : (int) Articulo.count(Articulo.class);
                int rowsLead = intForzar == 0 ? 0 : (int) Lead.count(Lead.class);
                int rowsDepartamentos = intForzar == 0 ? 0 : (int) Departamento.count(Departamento.class);
                int rowsMotivos = intForzar == 0 ? 0 : (int) CoreMotivo.count(CoreMotivo.class);
                int rowsRutas = intForzar == 0 ? 0 : (int) CoreRuta.count(CoreRuta.class);

                syncData = new SyncData(HomeActivity.this,0);

                if (rowsDepartamentos == 0){
                    publishProgress(getResources().getString(R.string.sync_ubigeo));
                    isConnectedREST = syncData.SyncDepartments(outMessage);
                }

                    if (isConnectedREST && rowsMotivos == 0 && typeUser == 1 ){
                    publishProgress(getResources().getString(R.string.sync_motivos));
                    isConnectedREST = syncData.SyncListMotivos(outMessage);
                }

                if (isConnectedREST && rowsRutas == 0 && typeUser == 1 ){
                    publishProgress(getResources().getString(R.string.sync_rutas));
                    isConnectedREST = syncData.SyncListRutas(outMessage);
                }

                if (isConnectedREST && rowsImpuesto == 0){
                    publishProgress(getResources().getString(R.string.sync_taxs));
                    isConnectedREST = syncData.SyncTaxs(outMessage);
                }

                if (isConnectedREST && rowsMoneda == 0){
                    publishProgress(getResources().getString(R.string.sync_coins));
                    isConnectedREST = syncData.SyncCoins(outMessage);
                }

                if (isConnectedREST && rowsCondPago == 0){
                    publishProgress(getResources().getString(R.string.sync_payment_condition));
                    isConnectedREST = syncData.SyncPaymentConditions(outMessage);
                }

                if (isConnectedREST){
                    publishProgress(getResources().getString(R.string.sync_warehouse));
                    syncData.setForzar(0);
                    if (rowsAlmacen > 0) syncData.setForzar(1);
                    isConnectedREST = syncData.SyncAlmacenes(outMessage);
                }

                if (isConnectedREST){
                    publishProgress(getResources().getString(R.string.sync_items));
                    syncData.setForzar(0);
                    if (rowsArticulo > 0) syncData.setForzar(1);
                    isConnectedREST = syncData.SyncItems(outMessage);
                }

                if (isConnectedREST && typeUser == 2 ){
                    publishProgress(getResources().getString(R.string.sync_customer));
                    isConnectedREST = syncData.SyncCustomersAlone(outMessage,currentUser.getIdMember());
                }

                if (isConnectedREST && typeUser == 1 ){
                    publishProgress(getResources().getString(R.string.sync_customers));
                    syncData.setForzar(0);
                    if (rowsCliente > 0) syncData.setForzar(1);
                    isConnectedREST = syncData.SyncCustomers(outMessage);
                }

                if (isConnectedREST && typeUser == 1 ){
                    publishProgress(getResources().getString(R.string.sync_leads));
                    syncData.setForzar(0);
                    if (rowsLead > 0) syncData.setForzar(1);
                    isConnectedREST = syncData.SyncLeads(outMessage);
                }

                if (isConnectedREST && intForzar < 1){
                    publishProgress(getResources().getString(R.string.sync_updateLast));
                    isConnectedREST = syncData.SyncUpdateSyncLast(outMessage);
                }

            }catch (Exception ex) {
                errMsg = ex.getMessage();
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            if (dialog.isShowing() && dialog!=null) {
                dialog.dismiss();
                if (!isConnectedREST || !errMsg.isEmpty()) {
                    Popup.ShowAlertDialog(HomeActivity.this
                            , getResources().getString(R.string.titulo_error)
                            , errMsg.isEmpty() ? outMessage.toString() : errMsg
                            //, outMessage.toString()+"-" + errMsg
                            , new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    new InternetCheck().execute();
                                }
                            }, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (intForzar == -1) {
                                        CerrarSesion(HomeActivity.this, sessionManager);
                                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                                        finish();
                                    }
                                }
                            });
                } else {
                    Popup.ShowAlertDialog(HomeActivity.this
                            ,getResources().getString(R.string.ready_go)
                            , getResources().getString(R.string.update_end)
                            , new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                }
            }
        }
    }


    @SuppressLint("StaticFieldLeak")
    private class ValidateObjectService extends AsyncTask<Void, String, Void> {
        private ProgressDialog dialog;
        //String outMessage = "";
        String errMsg = "";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(HomeActivity.this);
            dialog.setTitle("Operación en curso");
            dialog.setMessage("Mensaje");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            dialog.setMessage(values[0]);
        }

        @Override
        protected Void doInBackground(Void... args) {
            try {
                isDataValidate = true;
                int rowsCondPago = (int) CondicionPago.count(CondicionPago.class);
                int rowsMoneda = (int) Moneda.count(Moneda.class);
                int rowsImpuesto = (int) Impuesto.count(Impuesto.class);
                int rowsCliente = (int) Cliente.count(Cliente.class);
                int rowsAlmacen  = (int) Almacen.count(Almacen.class);
                int rowsArticulo = (int) Articulo.count(Articulo.class);
                //int rowsLead = (int) Lead.count(Lead.class);
                int rowsDepartamentos = (int) Departamento.count(Departamento.class);
                int rowsMotivos = (int) CoreMotivo.count(CoreMotivo.class);
                int rowsRutas = (int) CoreRuta.count(CoreRuta.class);


                if (rowsDepartamentos == 0){
                    publishProgress(getResources().getString(R.string.sync_ubigeo_val));
                    isDataValidate = false;
                }

                if (isDataValidate && rowsMotivos == 0 && typeUser == 1 ){
                    publishProgress(getResources().getString(R.string.sync_motivos_val));
                    isDataValidate = false;
                }

                if (isDataValidate && rowsRutas == 0 && typeUser == 1 ){
                    publishProgress(getResources().getString(R.string.sync_rutas_val));
                    isDataValidate = false;
                }

                if (isDataValidate && rowsImpuesto == 0){
                    publishProgress(getResources().getString(R.string.sync_taxs_val));
                    isDataValidate = false;
                }

                if (isDataValidate && rowsMoneda == 0){
                    publishProgress(getResources().getString(R.string.sync_coins_val));
                    isDataValidate = false;
                }

                if (isDataValidate && rowsCondPago == 0){
                    publishProgress(getResources().getString(R.string.sync_payment_condition_val));
                    isDataValidate = false;
                }

                if (isDataValidate && rowsAlmacen == 0){
                    publishProgress(getResources().getString(R.string.sync_warehouse_val));
                    isDataValidate = false;
                }

                if (isDataValidate && rowsArticulo == 0){
                    publishProgress(getResources().getString(R.string.sync_items_val));
                    isDataValidate = false;
                }

                if (isDataValidate && rowsCliente == 0){
                    publishProgress(getResources().getString(R.string.sync_customer_val));
                    isDataValidate = false;
                }


            }catch (Exception ex) {
                errMsg = ex.getMessage();
            }
            return null;
        }



        protected void onPostExecute(Void result) {
            if (dialog.isShowing() && dialog!=null) {
                dialog.dismiss();
                if (!isDataValidate || !errMsg.isEmpty()) {
                    Popup.ShowAlertDialog(HomeActivity.this
                            , getResources().getString(R.string.titulo_error)
                            , errMsg.isEmpty() ? getResources().getString(R.string.error_data_corrupt) : errMsg
                            , new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    new InternetCheck().execute();
                                }
                            }, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                        CerrarSesion(HomeActivity.this, sessionManager);
                                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                                        finish();
                                }
                            });
                } else {
                    Popup.ShowAlertDialog(HomeActivity.this
                            ,getResources().getString(R.string.ready_go)
                            , getResources().getString(R.string.validate_end)
                            , new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                }
            }
        }
    }
    //endregion

    //region onBackPressed
    private void closeSession(){
        Popup.ShowDialog(HomeActivity.this,
                getString(R.string.mensaje_cerrar_sesion),
                Popup.MSG_TITLE_WARNING,
                Popup.MSG_TYPE_WARNING,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CerrarSesion(HomeActivity.this, sessionManager);
                    }
                });
    }
    public Member getMemmber() {
        return currentUser;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            timeBack = Useful.timeBack(this, timeBack);
            if (timeBack == -1) {
                finishAffinity();
                System.exit(0);
            }
        }
    }
    //endregion
}
