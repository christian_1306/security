package com.ndp.ui.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;
import java.util.List;

import com.ndp.R;
import com.ndp.models.model.Articulo;
import com.ndp.ui.adapters.ItemAdapter;

import static com.ndp.utils.Useful.getCol;

public class ItemsActivity extends AppCompatActivity {

    private TextView tv_title_toolbar;
    private ImageView ivRetroceder;
    private List<Articulo> lstItem;
    private List<Articulo> lstItemFilter;
    private SearchView searchView;
    private LinearLayout lyt_empty;
    private RecyclerView rv_items;
    private Integer codeActivity;
    private Intent intent;
    private ItemAdapter adapter;
    private Boolean isTrueAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);
        setElements();
        getItems();
        recyclerViewAllData();
    }

    //region setElements
    private void setElements() {
        Toolbar toolbar = findViewById(R.id.tb_view);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null ) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setTitle("");
        } else {
            this.finish();
        }
        String intKey = getIntent().getStringExtra(getResources().getString(R.string.valueActivity));
        codeActivity = Integer.parseInt((intKey != null) ? intKey : getResources().getString(R.string.zero) );
        isTrueAction = getIntent().getBooleanExtra(getResources().getString(R.string.changeCardName),false);
        tv_title_toolbar = findViewById(R.id.tv_title_toolbar);
        tv_title_toolbar.setText(getResources().getString(R.string.title_item));
        rv_items = findViewById(R.id.rv_items);
        ivRetroceder = findViewById(R.id.iv_back_toolbar);
        lyt_empty = findViewById(R.id.lyt_empty);
        ivRetroceder.setOnClickListener(closeItems);
    }
    //endregion


    //region getItems
    public void getItems(){
        lstItemFilter = new ArrayList<>();
        lstItem = new ArrayList<>();
        lstItem = Articulo.find(Articulo.class,getCol("isActive") + " = 1  ORDER BY " + getCol("nombre"));

        if (lstItem == null || lstItem.size() <= 0){
            MDToast mdToast = MDToast.makeText(this, getString(R.string.not_items), MDToast.LENGTH_LONG, MDToast.TYPE_INFO );
            mdToast.show();
            this.finish();
        } else {
            lstItemFilter.addAll(lstItem);
        }
    }
    //endregion

    //region recyclerViewAllData
    public void recyclerViewAllData(){
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(500);
        itemAnimator.setRemoveDuration(500);
        rv_items.setItemAnimator(itemAnimator);

        rv_items.setHasFixedSize(true);
        rv_items.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ItemAdapter(lstItem, codeActivity, isTrueAction, this);
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                checkEmpty();
                super.onChanged();
            }
        });
        rv_items.setAdapter(adapter);
    }
    //endregion

    private void checkEmpty() {
        if (adapter.getItemCount() <= 0) {
            lyt_empty.setVisibility(View.VISIBLE);
            rv_items.setVisibility(View.GONE);
        } else {
            lyt_empty.setVisibility(View.GONE);
            rv_items.setVisibility(View.VISIBLE);
        }
    }

    private View.OnClickListener closeItems = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();

        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_title_toolbar.setVisibility(View.GONE);
                ivRetroceder.setVisibility(View.GONE);
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                tv_title_toolbar.setVisibility(View.VISIBLE);
                ivRetroceder.setVisibility(View.VISIBLE);
                return false;
            }
        });

        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_search:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }
}
