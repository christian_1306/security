package com.ndp.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;

import com.valdesekamdem.library.mdtoast.MDToast;

import com.ndp.R;
import com.ndp.data.api.SyncData;
import com.ndp.utils.Popup.Popup;
import com.ndp.utils.Useful;
import com.ndp.utils.secure.AESCrypt;
import com.ndp.utils.use.usePhone;

import static com.ndp.utils.Const.PUT_USERNAME;

public class ConfirmPassActivity extends AppCompatActivity  {

    Button btnValidar;
    ImageView imgBack;
    EditText etOldPassword;
    EditText etNewPassword;
    EditText etConfirmPassword;
    View linea1,linea2,linea3;
    Switch swPasssword;
    String currentHash,newHash,appUser,appToken;
    Boolean isChangeInApp = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_pass);
        initActivity();
    }

    //region initActivity
    public void initActivity() {
        setElements();
        method();
    }
    //endregion

    //region setElements
    public void setElements() {
        btnValidar = findViewById(R.id.btnValidar);
        imgBack = findViewById(R.id.imgBack);
        etOldPassword = findViewById(R.id.etOldPassword);
        etNewPassword = findViewById(R.id.etNewPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        linea1 = findViewById(R.id.linea1);
        linea2 = findViewById(R.id.linea2);
        linea3 = findViewById(R.id.linea3);
        swPasssword = findViewById(R.id.swPasssword);
        Bundle extras = getIntent().getExtras();
        appUser = extras != null ? extras.getString(PUT_USERNAME) : null;
        isChangeInApp = extras != null && extras.getBoolean(getResources().getString(R.string.valueAction));
        appToken = "";
    }
    //endregion

    //region OnClickListener
    public void method()
    {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });

        btnValidar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Validate();
            }
        });
        swPasssword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    swPasssword.setThumbDrawable(getResources().getDrawable(R.drawable.icons_eye));
                    etOldPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    etNewPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    etConfirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                else {
                    swPasssword.setThumbDrawable(getResources().getDrawable(R.drawable.icons_hide));
                    etOldPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    etNewPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    etConfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });
    }
    //endregion

    //region Validate
    public void Validate(){
        usePhone.hideKeyBoard(this);
        String s1,s2;

        s1 = etNewPassword.getText().toString();
        s2 = etConfirmPassword.getText().toString();

        if (TextUtils.isEmpty(s1))
        {
            etNewPassword.setError(getString(R.string.add_new_pass));
            etNewPassword.requestFocus();
        }
        else if (TextUtils.isEmpty(s2))
        {
            etConfirmPassword.setError(getString(R.string.confirm_new_pass));
            etConfirmPassword.requestFocus();
        }
        else if (!TextUtils.isEmpty(s1) &&  !TextUtils.isEmpty(s2) && !Useful.isEqualsString(s1,s2))
        {
            etConfirmPassword.setError(getString(R.string.pass_not_match));
            etConfirmPassword.requestFocus();
        }
        else if(Useful.isValidPassword(s1))
        {

            try {
                btnValidar.setEnabled(false);
                currentHash = "";
                newHash = s1;
                newHash = AESCrypt.encrypt(newHash);
                new InternetCheck().execute();
            } catch (Exception e) {
                Popup.showAlert(ConfirmPassActivity.this, e.getMessage());
                btnValidar.setEnabled(true);
            }

        }
        else
        {
            etNewPassword.setError(getString(R.string.pass_invalid));
            etNewPassword.requestFocus();
        }

    }
    //endregion

    //region onBackPressed
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    //endregion

    //region Call Web Services
    @SuppressLint("StaticFieldLeak")
    private class InternetCheck extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            return usePhone.isOnline();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (!result) {
                btnValidar.setEnabled(true);
                Popup.showAlert(ConfirmPassActivity.this, getString(R.string.titulo_sin_conexion_internet));
            } else
                new ConnectWithService().execute();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class ConnectWithService extends AsyncTask<Void, Void, Boolean> {
        private ProgressDialog dialog;
        StringBuilder outMessage = new StringBuilder();
        String errMsg = "";

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(ConfirmPassActivity.this, R.style.ProgressDialog);
            dialog.setTitle(R.string.titulo_check_service);
            dialog.setMessage(getString(R.string.mensaje_validando_email));
            dialog.setCancelable(false);
            dialog.show();
        }

        protected Boolean doInBackground(Void... args) {
            try {
                return new SyncData(ConfirmPassActivity.this, 0)
                        .SyncChangePasswordEmail(currentHash,newHash,appUser,appToken,outMessage);
            } catch (Exception e) {
                errMsg = e.getMessage();
                return false;
            }
        }

        protected void onPostExecute(Boolean result) {
            String msgException = "";
            if (result) {
                try {
                    MDToast.makeText(ConfirmPassActivity.this, getString(R.string.change_successful), MDToast.LENGTH_SHORT, MDToast.TYPE_SUCCESS ).show();
                    if (!isChangeInApp) {
                        Intent i = new Intent(ConfirmPassActivity.this, LoginActivity.class);
                        startActivity(i);
                        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                        finish();
                    } else {
                        finish();
                    }
                } catch (Exception ex) {
                    msgException = getString(R.string.exception) + ex.getMessage();
                }
            }

            if (dialog.isShowing()) {
                dialog.dismiss();
                if (!result || !msgException.isEmpty() || !errMsg.isEmpty()) {
                    Popup.showAlert(ConfirmPassActivity.this, outMessage.toString() + "\n" + msgException+ "\n" + errMsg);
                    btnValidar.setEnabled(true);
                }
            }

        }
    }
    //endregion
}
