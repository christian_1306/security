package com.ndp.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.ui.fragments.OrderContentFragment;
import com.ndp.utils.methods.CommonMethods;

public class OrderClientActivity extends AppCompatActivity {

    private Cliente cliente;
    private ImageView ivRetroceder;

    public Cliente getCliente() {
        return cliente;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_client);
        ivRetroceder = findViewById(R.id.iv_back_toolbar);
        ivRetroceder.setOnClickListener(CommonMethods.backActivity(this));
        getData();
        openOrderContentFragment();
    }

    private void getData() {
        Intent intent = getIntent();
        int idCliente = intent.getIntExtra("cliente", 0);
        if(idCliente == 0) finish();
        this.cliente = Cliente.find(Cliente.class, "id_cliente = ?", String.valueOf(idCliente)).get(0);
    }

    private void openOrderContentFragment() {
        OrderContentFragment orderContentFragment = new OrderContentFragment();
        orderContentFragment.setForClient(true);
        orderContentFragment.setCliente(cliente);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_order_client, orderContentFragment).commit();
    }
}
