package com.ndp.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mukesh.OtpView;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import com.ndp.R;
import com.ndp.data.api.SyncData;
import com.ndp.utils.Popup.Popup;
import com.ndp.utils.secure.AESCrypt;
import com.ndp.utils.use.usePhone;

import static com.ndp.utils.Const.PUT_TIMEOUT;
import static com.ndp.utils.Const.PUT_USERNAME;

public class ConfirmPinActivity extends AppCompatActivity {

    TextView tvTimer;
    Integer intTimeMin;
    long milliSeconds;
    OtpView otp_view;
    Button btnValidar;
    ImageView imgBack;
    String appUser, appPIN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_pin);
        initApp(savedInstanceState);
    }

    //region initApp
    private void initApp(Bundle sInst){
        setElements();
        OnClickListener();
        Bundle extras = getIntent().getExtras();
        appUser = extras != null ? extras.getString(PUT_USERNAME) : null;
        String timeOut = extras != null ? extras.getString(PUT_TIMEOUT) : null;
        if (appUser != null && timeOut != null){
            intTimeMin = Integer.parseInt(timeOut);
        } else {
            Toast.makeText(this, getString(R.string.not_value), Toast.LENGTH_SHORT).show();
            intTimeMin = 1;
        }
        milliSeconds = TimeUnit.MINUTES.toMillis(intTimeMin);
        countDownTimer(milliSeconds);
    }
    //endregion

    //region setElements
    private void setElements() {
        tvTimer = findViewById(R.id.tvTimer);
        otp_view = findViewById(R.id.otp_view);
        btnValidar = findViewById(R.id.btnValidar);
        imgBack = findViewById(R.id.imgBack);
    }
    //endregion

    //region countDownTimer
    private void countDownTimer(long milliSeconds){
        if (tvTimer != null) {
            new CountDownTimer(milliSeconds - 30000, 1000) {

                @SuppressLint("SetTextI18n")
                public void onTick(long millisUntilFinished) {
                    @SuppressLint("DefaultLocale")
                    String time = String.format("%02d:%02d:%02d",
                            TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) -
                                    TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)), // The change is in this line
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                    tvTimer.setText("CÓDIGO EXPIRA EN: " + time);
                }

                public void onFinish() {
                    finish();
                }

            }.start();
        }
    }
    //endregion

    //region OnClickListener
    public void OnClickListener()
    {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });

        btnValidar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (validar()){
                        btnValidar.setEnabled(false);
                        appPIN = Objects.requireNonNull(otp_view.getText()).toString();
                        appPIN = AESCrypt.encrypt(appPIN);
                        new InternetCheck().execute();
                    }
                } catch (Exception e) {
                    Popup.showAlert(ConfirmPinActivity.this, e.getMessage());
                    btnValidar.setEnabled(true);
                }
            }
        });
    }
    //endregion

    //region Validar
    public boolean validar(){
        usePhone.hideKeyBoard(ConfirmPinActivity.this);
        if (Objects.requireNonNull(otp_view.getText()).toString().isEmpty()){
            Toast.makeText(this, getString(R.string.pin_valido), Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (otp_view.getText().length()<6) {
            Toast.makeText(this, getString(R.string.ing_pin_complete), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    //endregion

    //region onPause
    @Override
    protected void onPause() {
        super.onPause();
    }
    //endregion

    //region onBackPressed
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    //endregion

    //region Call Web Services
    @SuppressLint("StaticFieldLeak")
    private class InternetCheck extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            return usePhone.isOnline();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (!result) {
                btnValidar.setEnabled(true);
                Popup.showAlert(ConfirmPinActivity.this, getString(R.string.titulo_sin_conexion_internet));
            } else
                new ConnectWithService().execute();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class ConnectWithService extends AsyncTask<Void, Void, Boolean> {
        private ProgressDialog dialog;
        StringBuilder outMessage = new StringBuilder();
        String errMsg = "";

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(ConfirmPinActivity.this, R.style.ProgressDialog);
            dialog.setTitle(R.string.titulo_check_pin);
            dialog.setMessage(getString(R.string.mensaje_validando_email));
            dialog.setCancelable(false);
            dialog.show();
        }

        protected Boolean doInBackground(Void... args) {
            try {
                return new SyncData(ConfirmPinActivity.this, 0)
                        .SyncCheckPIN(appUser, appPIN, outMessage);
            } catch (Exception e) {
                errMsg = e.getMessage();
                return false;
            }
        }

        protected void onPostExecute(Boolean result) {
            String msgException = "";
            if (result) {
                try {
                    MDToast mdToast = MDToast.makeText(ConfirmPinActivity.this, getString(R.string.pin_validate), MDToast.LENGTH_SHORT, MDToast.TYPE_SUCCESS );
                    mdToast.show();
                    Intent intent = new Intent(ConfirmPinActivity.this, ConfirmPassActivity.class);
                    intent.putExtra(PUT_USERNAME, appUser);
                    startActivity(intent);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    finish();
                } catch (Exception ex) {
                    msgException = getString(R.string.exception) + ex.getMessage();
                }
            }

            if (dialog.isShowing()) {
                dialog.dismiss();
                if (!result || !msgException.isEmpty() || !errMsg.isEmpty()) {
                    Objects.requireNonNull(otp_view.getText()).clear();
                    Popup.showAlert(ConfirmPinActivity.this, outMessage.toString() + "\n" + msgException+ "\n" + errMsg);
                    btnValidar.setEnabled(true);
                }
            }
        }
    }
    //endregion
}
