package com.ndp.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ndp.R;
import com.ndp.models.model.User;
import com.ndp.ui.fragments.AllClientsFragment;

public class OrderSeller extends AppCompatActivity {

    private TextView tv_seleccionar_cliente;
    private View linea;
    private ImageView ivRetroceder;
    private boolean estoyEnPedido = false;
    private AllClientsFragment allClientsFragment;
    private User usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_seller);
        setElements();
        getData();
    }

    private void setElements() {
        tv_seleccionar_cliente = findViewById(R.id.tv_seleccionar_cliente);
        tv_seleccionar_cliente.setOnClickListener(seleccionarCliente);
        linea = findViewById(R.id.linea1);
        ((TextView)findViewById(R.id.tv_title_toolbar)).setText(getResources().getString(R.string.order_title));
        ivRetroceder = findViewById(R.id.iv_back_toolbar);
        ivRetroceder.setOnClickListener(cerrarVentana);
    }

    private View.OnClickListener seleccionarCliente = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            allClientsFragment = new AllClientsFragment();
            allClientsFragment.setId_fragmento(2);
            allClientsFragment.setUsuario(usuario);
            allClientsFragment.setOrderSellerActivity(OrderSeller.this);
            tv_seleccionar_cliente.setVisibility(View.GONE);
            linea.setVisibility(View.GONE);
            //v.setVisibility(View.GONE);
            estoyEnPedido = true;
            getSupportFragmentManager().beginTransaction().add(R.id.content_order_seller,allClientsFragment).addToBackStack(null).commit();
        }
    };

    private View.OnClickListener cerrarVentana = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(estoyEnPedido){
                getSupportFragmentManager().popBackStackImmediate();
                estoyEnPedido = false;
            }
            else{
                finish();
            }
        }
    };

    private void getData() {
        usuario = (User) getIntent().getSerializableExtra(getResources().getString(R.string.objUser));
    }

    public User getUsuario() {
        return usuario;
    }

    public void setUsuario(User usuario) {
        this.usuario = usuario;
    }

    public boolean isEstoyEnPedido() {
        return estoyEnPedido;
    }

    public void setEstoyEnPedido(boolean estoyEnPedido) {
        this.estoyEnPedido = estoyEnPedido;
    }
}


