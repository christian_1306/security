package com.ndp.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.Objects;

import com.ndp.R;
import com.ndp.models.appRest.apiContactoDetail;
import com.ndp.utils.Popup.Popup;

import static com.ndp.utils.Const.CODE_REQ_OK;
import static com.ndp.utils.Useful.validarSiEsCorreo;
import static com.ndp.utils.use.usePhone.hideKeyBoard;

public class AddContactActivity extends AppCompatActivity {


    private MaterialEditText et_contacto_nombre;
    private MaterialEditText et_contacto_cargo;
    private MaterialEditText et_contacto_correo;
    private MaterialEditText et_contacto_telefono;
    private RelativeLayout header_admin;
    private ImageView header_admin_arrow;
    private ExpandableRelativeLayout card_info;
    private Button btn_create;
    private apiContactoDetail apiContacto;
    private ImageView iv_back_toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        setElements();
    }

    private  void setElements() {
        ((TextView)findViewById(R.id.tv_title_toolbar)).setText(getResources().getString(R.string.add_con));
        iv_back_toolbar = findViewById(R.id.iv_back_toolbar);
        et_contacto_nombre = findViewById(R.id.et_contacto_nombre);
        et_contacto_cargo = findViewById(R.id.et_contacto_cargo);
        et_contacto_correo = findViewById(R.id.et_contacto_correo);
        et_contacto_telefono = findViewById(R.id.et_contacto_telefono);
        header_admin = findViewById(R.id.header_admin);
        header_admin_arrow = findViewById(R.id.header_admin_arrow);
        card_info = findViewById(R.id.card_info);
        btn_create = findViewById(R.id.btn_create);

        header_admin.setOnClickListener(customOnClick);
        iv_back_toolbar.setOnClickListener(customOnClick);
        btn_create.setOnClickListener(customOnClick);

    }


    //region OnClickListener
    private View.OnClickListener customOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.header_admin:
                    card_info.toggle();
                    if (card_info.isExpanded())
                        header_admin_arrow.animate().rotation(180).start();
                    else {
                        header_admin_arrow.animate().rotation(0).start();
                    }
                    break;
                case R.id.btn_create:
                    agregarContacto();
                    break;
                case R.id.iv_back_toolbar:
                    close();
                    break;
            }
        }
    };
    //endregion

    private void agregarContacto() {

        String nombreContacto = Objects.requireNonNull(et_contacto_nombre.getText()).toString();
        String cargoContacto = Objects.requireNonNull(et_contacto_cargo.getText()).toString();
        String correoContacto = Objects.requireNonNull(et_contacto_correo.getText()).toString();
        String telfContacto = Objects.requireNonNull(et_contacto_telefono.getText()).toString();

        if (nombreContacto.isEmpty()){
            et_contacto_nombre.requestFocus();
            et_contacto_nombre.setError(getString(R.string.request_name));
            return;
        }

        if (!et_contacto_nombre.isCharactersCountValid()){
            et_contacto_nombre.requestFocus();
            et_contacto_nombre.setError(getString(R.string.error_supera_maxcaracter));
            return;
        }

        if (!et_contacto_cargo.isCharactersCountValid()){
            et_contacto_cargo.requestFocus();
            et_contacto_cargo.setError(getString(R.string.error_supera_maxcaracter));
            return;
        }

        if (!et_contacto_correo.isCharactersCountValid()){
            et_contacto_correo.requestFocus();
            et_contacto_correo.setError(getString(R.string.error_supera_maxcaracter));
            return;
        }

        if (!et_contacto_telefono.isCharactersCountValid()){
            et_contacto_telefono.requestFocus();
            et_contacto_telefono.setError(getString(R.string.error_supera_maxcaracter));
            return;
        }

        if (!correoContacto.isEmpty()) {
            if (!validarSiEsCorreo(correoContacto)) {
                et_contacto_correo.requestFocus();
                et_contacto_correo.setError(getString(R.string.request_email_validate));
                return;
            }
        }

        apiContacto = new apiContactoDetail();
        apiContacto.setIdPersonaContacto(0);
        apiContacto.setNombres(nombreContacto);
        apiContacto.setCargo(cargoContacto);
        apiContacto.setCorreo(correoContacto);
        apiContacto.setTelefono(telfContacto);

        Intent intent = new Intent();
        intent.putExtra(getResources().getString(R.string.obj_customer_contacto), apiContacto);
        setResult(CODE_REQ_OK, intent);
        hideKeyBoard(this);
        finish();

    }


    @Override
    public void onBackPressed() {
        close();
    }

    private void close(){
        Popup.ShowDialog(this,
                getString(R.string.mensaje_exit),
                getString(R.string.modelo_mensaje_titulo_advertencia),
                Popup.MSG_TYPE_WARNING,
                Popup.BUTTON_TYPE_ACCEPT,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                },
                null);
    }
}
