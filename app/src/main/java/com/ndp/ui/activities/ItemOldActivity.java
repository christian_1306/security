package com.ndp.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ndp.R;
import com.ndp.models.model.Vendedor;
import com.ndp.ui.fragments.ItemContentFragment;
import com.ndp.ui.fragments.ItemDetailFragment;

public class ItemOldActivity extends AppCompatActivity {

//    private ArrayList<String> testItems;
//    private RecyclerView rvListaItems;
//    private Almacen almacen;
    private Vendedor vendedor;
    private ImageView ivRetroceder;
    private boolean estoyEnDetalle = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itemold);
        ((TextView)findViewById(R.id.tv_title_toolbar)).setText("ARTÍCULOS");
        //setElements();
        getData();
        ItemContentFragment itemContentFragment = new ItemContentFragment();
        itemContentFragment.setAlmacen(vendedor.getAlmacen());
        itemContentFragment.setVendedor(vendedor);
        getSupportFragmentManager().beginTransaction().add(R.id.content_items,itemContentFragment).addToBackStack(null).commit();
        ivRetroceder = findViewById(R.id.iv_back_toolbar);
        ivRetroceder.setOnClickListener(retroceder);
    }

    private void getData() {
        Intent intent = getIntent();
        int idVendedor = intent.getIntExtra("vendedor", 0);
        vendedor = Vendedor.find(Vendedor.class, "id_vendedor = ?", String.valueOf(idVendedor)).get(0);
    }

    private void setElements() {
//        rvListaItems = findViewById(R.id.rv_lista_items);
//        rvListaItems.setLayoutManager(new LinearLayoutManager(this));
//
//        //ARTICULOS DE PRUEBA, SOLO SERVIRÁN PARA LA PRESENTACION
//        testItems = new ArrayList<>();
//        testItems.add("Fierro Corrugado ASTM A615 - NTP 341.03/ Grado 60");
//        testItems.add("Corrugado 4.7mm");
//        testItems.add("Clavos de Acero");
//        testItems.add("Estribos Corrugados");
//        testItems.add("Alambre Negro Recocido");
//
//        testItems.add("Mallas Electrosoldadas");
//        testItems.add("Angulos Estructurales de Calidad Dual");
//        testItems.add("Barras Cuadradas Ornamentales");
//        testItems.add("Barras Redondas Lisas");
//        testItems.add("Barras Cuadradas");
//        ItemOldAdapter itemAdapter = new ItemOldAdapter(testItems,this, false);
//        rvListaItems.setAdapter(itemAdapter);
//
//        ((TextView)findViewById(R.id.tv_title_toolbar)).setText("ARTÍCULOS");
//        ((ImageView)findViewById(R.id.iv_back_toolbar)).setOnClickListener(CommonMethods.backActivity(this));
    }

    public void openDetailItem(){
        getSupportFragmentManager().beginTransaction().add(R.id.content_items, new ItemDetailFragment()).addToBackStack(null).commit();
        estoyEnDetalle = true;
    }

    private View.OnClickListener retroceder = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(estoyEnDetalle){
                getSupportFragmentManager().popBackStackImmediate();
                estoyEnDetalle = false;
                ivRetroceder.setOnClickListener(retroceder);
            }
            else{
                finish();
            }
        }
    };
}
