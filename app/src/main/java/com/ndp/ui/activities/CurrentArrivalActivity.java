package com.ndp.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.chivorn.smartmaterialspinner.SmartMaterialSpinner;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.valdesekamdem.library.mdtoast.MDToast;
import com.yayandroid.locationmanager.LocationManager;
import com.yayandroid.locationmanager.base.LocationBaseActivity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import com.ndp.R;
import com.ndp.data.api.SyncData;
import com.ndp.models.appRest.apiCreateVisit;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.CoreMotivo;
import com.ndp.models.model.CoreRuta;
import com.ndp.models.model.Departamento;
import com.ndp.models.model.Direccion;
import com.ndp.models.model.Municipio;
import com.ndp.models.model.Provincia;
import com.ndp.models.model.User;
import com.ndp.utils.Popup.Popup;
import com.ndp.utils.use.usePhone;


import static com.ndp.utils.Const.CODE_VISIT;
import static com.ndp.utils.Useful.getCol;
import static com.ndp.utils.use.useAPP.isNullOrEmpty;
import static com.ndp.utils.use.useFormat.DateTime24;
import static com.ndp.utils.use.usePhone.getcodCreacionApp;
import static com.ndp.utils.use.usePhone.hideKeyBoard;

import com.yayandroid.locationmanager.configuration.Configurations;
import com.yayandroid.locationmanager.configuration.LocationConfiguration;
import com.yayandroid.locationmanager.constants.FailType;
import com.yayandroid.locationmanager.constants.ProcessType;

public class CurrentArrivalActivity extends LocationBaseActivity  {

    private Toolbar toolbar;
    private ImageView ivRetroceder;
    private TextView tv_title_toolbar;
    private RelativeLayout order_header_admin;
    private RelativeLayout order_header_ubigeo;
    private RelativeLayout order_header_estado;
    private ImageView order_header_admin_arrow;
    private ImageView order_header_ubigeo_arrow;
    private ImageView order_header_estado_arrow;
    private ExpandableRelativeLayout order_detail_admin;
    private ExpandableRelativeLayout order_detail_ubigeo;
    private ExpandableRelativeLayout order_detail_estado;
    private TextView tv_cardcode;
    private TextView tv_cardname;
    private EditText et_escribe_direccion;
    private TextView tv_fecha;
    private EditText et_escribe_motivo;
    private EditText et_escribe_comentario;
    private SmartMaterialSpinner sp_departamento;
    private SmartMaterialSpinner sp_provincia;
    private SmartMaterialSpinner sp_municipio;
    private Intent i;
    private List<String> depa;
    private List<String> prov;
    private List<String> muni;
    private int codMunicipio = 0;
    private String municipio = "";
    private Cliente cliente;
    private Button btn_create;
    private apiCreateVisit apiVisit;
    private String codCreacionApp;
    private User user;
    private Boolean isConnectedREST = true;
    private boolean requireFineGranularity = true;
    private boolean passiveMode = false;
    private long updateIntervalInMilliseconds = 2 * 1000;
    private ProgressDialog progressDialog;
    private double latitudLoad = 0d;
    private double longitudLoad = 0d;
    private LinearLayout rly_motivo;
    private Spinner spinner_motivoPersona;
    private Switch sw_efectiva;
    private List<CoreMotivo> coreMotivos;
    private List<String> listMotivos;
    private EditText et_escribe_ruta;
    private Spinner spinner_rutas;
    private List<CoreRuta> coreRutas;
    private List<String> listRutas;
    private Integer isEfectiva = 0;

    //region activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_arrival);
        setElements();
    }

    //region setElements
    private void setElements() {

        depa = new ArrayList<>();
        prov = new ArrayList<>();
        muni = new ArrayList<>();
        codCreacionApp = getString(R.string.type_visita) + getcodCreacionApp(this);

        toolbar = findViewById(R.id.tb_view);
        tv_title_toolbar = findViewById(R.id.tv_title_toolbar);
        ivRetroceder = findViewById(R.id.iv_back_toolbar);
        order_header_admin = findViewById(R.id.order_header_admin);
        order_header_ubigeo = findViewById(R.id.order_header_ubigeo);
        order_header_admin_arrow = findViewById(R.id.order_header_admin_arrow);
        order_header_ubigeo_arrow = findViewById(R.id.order_header_ubigeo_arrow);
        order_detail_admin = findViewById(R.id.order_detail_admin);
        order_detail_ubigeo = findViewById(R.id.order_detail_ubigeo);
        tv_cardcode = findViewById(R.id.tv_cardcode);
        tv_cardname = findViewById(R.id.tv_cardname);
        et_escribe_direccion = findViewById(R.id.et_escribe_direccion);
        tv_fecha = findViewById(R.id.tv_fecha);
        et_escribe_motivo = findViewById(R.id.et_escribe_motivo);
        et_escribe_comentario = findViewById(R.id.et_escribe_comentario);
        sp_departamento = findViewById(R.id.sp_departamento);
        sp_provincia = findViewById(R.id.sp_provincia);
        sp_municipio = findViewById(R.id.sp_municipio);

        et_escribe_ruta = findViewById(R.id.et_escribe_ruta);
        spinner_rutas = findViewById(R.id.spinner_rutas);
        order_header_estado  = findViewById(R.id.order_header_estado);
        order_header_estado_arrow  = findViewById(R.id.order_header_estado_arrow);
        order_detail_estado = findViewById(R.id.order_detail_estado);
        rly_motivo = findViewById(R.id.rly_motivo);
        spinner_motivoPersona = findViewById(R.id.spinner_motivoPersona);
        sw_efectiva = findViewById(R.id.sw_efectiva);

        btn_create = findViewById(R.id.btn_create);

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null ) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setTitle("");

        } else {

            this.finish();

        }


        tv_title_toolbar.setText(getResources().getString(R.string.title_CurrentArrival));
        tv_fecha.setText(DateTime24(null));
        tv_cardcode.setOnClickListener(customOnClick);
        order_header_admin.setOnClickListener(customOnClick);
        order_header_ubigeo.setOnClickListener(customOnClick);
        order_header_estado.setOnClickListener(customOnClick);
        et_escribe_motivo.setOnClickListener(customOnClick);
        et_escribe_ruta.setOnClickListener(customOnClick);
        btn_create.setOnClickListener(customOnClick);
        ivRetroceder.setOnClickListener(customOnClick);

        List<Departamento> departamentos = Departamento.listAll(Departamento.class);

        if (departamentos != null && departamentos.size() > 0) {
            for (Departamento dep : departamentos) {
                depa.add(dep.getNombre());
            }
        }

        sp_departamento.setItems(depa);

        sp_departamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                String nomDepartameno = depa.get(position);

                List<Departamento> findep = Departamento.find(Departamento.class, getCol("nombre") + " = '" + nomDepartameno + "'");
                if (findep != null && findep.size() > 0) {
                    prov = new ArrayList<>();
                    List<Provincia> provincias = Provincia.find(Provincia.class, getCol("idDepartamento")
                            + " = " + findep.get(0).getIdDepartamento());
                    if (provincias != null && provincias.size() > 0) {

                        for (Provincia pro : provincias) {
                            prov.add(pro.getNombre());
                        }
                    }

                    sp_provincia.setItems(prov);

                    muni = new ArrayList<>();
                    sp_municipio.setItems(muni);
                    codMunicipio = 0;
                    municipio = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        sp_provincia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                String nomProvincia = prov.get(position);

                List<Provincia> finprov = Provincia.find(Provincia.class, getCol("nombre") + " = '" + nomProvincia + "'");
                if (finprov != null && finprov.size() > 0) {
                    muni = new ArrayList<>();
                    List<Municipio> municipios = Municipio.find(Municipio.class, getCol("idProvincia")
                            + " = " + finprov.get(0).getIdProvincia());
                    if (municipios != null && municipios.size() > 0) {

                        for (Municipio mun : municipios) {
                            muni.add(mun.getNombre());
                        }
                    }

                    sp_municipio.setItems(muni);
                }
                codMunicipio = 0;
                municipio = "";
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        sp_municipio.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                String nomMunicipio = muni.get(position);

                List<Municipio> municipios = Municipio.find(Municipio.class, getCol("nombre")
                        + " = '" + nomMunicipio + "'");

                if (municipios != null && municipios.size() > 0) {
                    codMunicipio = municipios.get(0).getIdMunicipio();
                    municipio = municipios.get(0).getNombre();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        listRutas = new ArrayList<>();
        coreRutas = CoreRuta.find(CoreRuta.class, getCol("estado") + " = 1" );
        if (coreRutas != null && coreRutas.size() > 0) {
            for (CoreRuta coreRuta : coreRutas) {
                listRutas.add(coreRuta.getDescripcion());
            }
        }

        ArrayAdapter<String> adapterRuta = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, listRutas);
        spinner_rutas.setAdapter(adapterRuta);

        spinner_rutas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String auxDescription = listRutas.get(position);
                et_escribe_ruta.setText(auxDescription);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listMotivos = new ArrayList<>();
        listMotivos.add("VISITA EFECTIVA");
        coreMotivos = CoreMotivo.find(CoreMotivo.class, getCol("estado") + " = 1" );
        if (coreMotivos != null && coreMotivos.size() > 0) {
            for (CoreMotivo coreMotivo : coreMotivos) {
                listMotivos.add(coreMotivo.getDescripcion());
            }
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, listMotivos);
        spinner_motivoPersona.setAdapter(adapter);

        spinner_motivoPersona.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String auxDescription = listMotivos.get(position);
                et_escribe_motivo.setText(auxDescription);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        sw_efectiva.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    rly_motivo.setVisibility(View.GONE);
                    isEfectiva = 1;
                    try {
                        if (listMotivos.size() > 0)
                            spinner_motivoPersona.setSelection(0);
                    } catch (Exception e) {
                        Log.i("CurrentArrivalActivity", e.getMessage());
                    }
                }
                else {
                    isEfectiva = 0;
                    rly_motivo.setVisibility(View.VISIBLE);
                }
            }
        });

        user = (User) getIntent().getSerializableExtra(getResources().getString(R.string.objUser));
        if (user == null) this.finish();

        LocationManager location = getLocationManager();
        location.get();
    }
    //endregion

    //region OnClickListener
    private View.OnClickListener customOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.order_header_admin:
                    order_detail_admin.toggle();
                    if (order_detail_admin.isExpanded())
                        order_header_admin_arrow.animate().rotation(180).start();
                    else {
                        order_header_admin_arrow.animate().rotation(0).start();
                    }
                    break;
                case R.id.order_header_ubigeo:
                    order_detail_ubigeo.toggle();
                    if (order_detail_ubigeo.isExpanded())
                        order_header_ubigeo_arrow.animate().rotation(180).start();
                    else {
                        order_header_ubigeo_arrow.animate().rotation(0).start();
                    }
                    break;
                case R.id.order_header_estado:
                    order_detail_estado.toggle();
                    if (order_detail_estado.isExpanded())
                        order_header_estado_arrow.animate().rotation(180).start();
                    else {
                        order_header_estado_arrow.animate().rotation(0).start();
                    }
                    break;
                case R.id.tv_cardcode:
                    i = new Intent(CurrentArrivalActivity.this, CustomersActivity.class);
                    i.putExtra(getResources().getString(R.string.objUser), user);
                    i.putExtra(getResources().getString(R.string.valueActivity), String.valueOf(CODE_VISIT));
                    i.putExtra(getResources().getString(R.string.changeCardName), true);
                    startActivityForResult(i,CODE_VISIT);
                    break;
                case R.id.btn_create:
                    sendVisit();
                    break;
                case R.id.iv_back_toolbar:
                    close();
                    break;
                case R.id.et_escribe_motivo:
                    spinner_motivoPersona.performClick();
                    break;
                case R.id.et_escribe_ruta:
                    spinner_rutas.performClick();
                    break;
            }
        }
    };
    //endregion

    //region onActivityResult
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {

            cliente = (Cliente) data.getSerializableExtra(getResources().getString(R.string.obj_customer));

            if (cliente != null) {

                String CardCode = isNullOrEmpty(cliente.getCardCode());
                String CardName = isNullOrEmpty(cliente.getRazonSocial());

                tv_cardcode.setText(CardCode);
                tv_cardname.setText(CardName);
                changeDireccion(CardCode);

            } else  {

                MDToast mdToast = MDToast.makeText(this, getString(R.string.error_ins_customer), MDToast.LENGTH_SHORT, MDToast.TYPE_ERROR );
                mdToast.show();

            }

        } else {

            MDToast mdToast = MDToast.makeText(this, getString(R.string.error_ins_customer), MDToast.LENGTH_SHORT, MDToast.TYPE_ERROR );
            mdToast.show();

        }
    }
    //endregion

    public void changeDireccion(@NonNull String cardCode){

        List<Direccion> direccions = Direccion.find(Direccion.class,
                "CARD_CODE = ? ", cardCode);

        if (direccions != null && direccions.size() > 0){
            et_escribe_direccion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Popup.showRadioButtonDialog(CurrentArrivalActivity.this, direccions, et_escribe_direccion, getString(R.string.title_dir_ship));
                }
            });

            et_escribe_direccion.setText("");
            et_escribe_direccion.setFocusableInTouchMode(false);
            et_escribe_direccion.setFocusable(false);
            et_escribe_direccion.setCursorVisible(false);

        } else {

            et_escribe_direccion.setOnClickListener(null);
            et_escribe_direccion.setText("");
            et_escribe_direccion.setFocusableInTouchMode(true);
            et_escribe_direccion.setFocusable(true);
            et_escribe_direccion.setCursorVisible(true);
        }
    }

    private void sendVisit() {
        hideKeyBoard(this);
        new validateSendVisit().execute();
    }

    @SuppressLint("StaticFieldLeak")
    private class validateSendVisit extends AsyncTask<Void, String, Void> {
        private ProgressDialog dialog;
        private String errMsg = "";
        private StringBuilder errMsgMultiple = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(CurrentArrivalActivity.this);
            dialog.setTitle("Operación en curso");
            dialog.setMessage("Mensaje");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            dialog.setMessage(values[0]);
        }

        @Override
        protected Void doInBackground(Void... args) {
            try {

                publishProgress("Obteniendo informacion ...");

                String cardcode = tv_cardcode.getText().toString();
                String cardName = tv_cardname.getText().toString();
                String direccion = et_escribe_direccion.getText().toString();
                String fecha = tv_fecha.getText().toString();
                String motivo = et_escribe_motivo.getText().toString();
                String ruta = et_escribe_ruta.getText().toString();
                String comentario = et_escribe_comentario.getText().toString();
                String codMuni = String.valueOf(codMunicipio);
                String latitude = String.valueOf(latitudLoad);
                String longitude = String.valueOf(longitudLoad);

                int numberError = 0;

                publishProgress("Validando Visita ...");
                if (cardcode.isEmpty()){
                    numberError ++;
                    errMsgMultiple.append(numberError).append(".- Código cliente inválido.");
                }

                if (direccion.isEmpty()){
                    errMsgMultiple.append("\n");
                    numberError ++;
                    errMsgMultiple.append(numberError).append(".- Dirección inválida.");
                }

                if (fecha.isEmpty()){
                    errMsgMultiple.append("\n");
                    numberError ++;
                    errMsgMultiple.append(numberError).append(".- Fecha inválida.");
                }

                if (motivo.isEmpty()){
                    errMsgMultiple.append("\n");
                    numberError ++;
                    errMsgMultiple.append(numberError).append(".- Motivo inválido.");
                }

                if (motivo.equals("VISITA EFECTIVA") && isEfectiva == 0) {
                    errMsgMultiple.append("\n");
                    numberError ++;
                    errMsgMultiple.append(numberError).append(".- Si la visita no fue efectiva, cambiar el motivo.");
                }

                if (codMunicipio == 0){
                    errMsgMultiple.append("\n");
                    numberError ++;
                    errMsgMultiple.append(numberError).append(".- Municipio inválido.");
                }

                apiVisit = new apiCreateVisit();
                apiVisit.setCardCode(cardcode);
                apiVisit.setCardName(cardName);
                apiVisit.setCodCreacionApp(codCreacionApp);
                apiVisit.setComentario(comentario);
                apiVisit.setDireccion(direccion);
                apiVisit.setFechaVisita(fecha);
                apiVisit.setLatitud(latitude);
                apiVisit.setLongitud(longitude);
                apiVisit.setMotivo(motivo);
                apiVisit.setMunicipio(codMunicipio);
                apiVisit.setUsuario(user.getCorreoUsuario());
                apiVisit.setRuta(ruta);
                apiVisit.setEstadoVisita(isEfectiva);

                apiVisit.setUserAccount(user.getCorreoUsuario());
                apiVisit.setTypeObject(4);
                apiVisit.setFechaRegistro(new Date());
                apiVisit.setEstadoMigracion(1);
                apiVisit.setMensajeMigracion("");

            } catch (Exception ex){
                errMsg = (ex.getMessage() != null) ? ex.getMessage() : "-" ;
            }
            return null;
        }

        protected void onPostExecute(Void result) {

            if (dialog.isShowing() && dialog!=null) {
                dialog.dismiss();
                if (!errMsg.isEmpty() || errMsgMultiple.length() != 0) {
                    Popup.showAlert(CurrentArrivalActivity.this, errMsg + "\n" + errMsgMultiple + "\n");
                } else {
                    new InternetCheck().execute();
                }
            }
        }
    }

    //region Call Web Services
    @SuppressLint("StaticFieldLeak")
    private class InternetCheck extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            return usePhone.isOnline();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (!result) {
                Popup.ShowDialogCustomButton(CurrentArrivalActivity.this,
                        getString(R.string.mensaje_sin_internet_visit),
                        getString(R.string.btn_registrar_visita),
                        getString(R.string.save),getString(R.string.reload),
                        Popup.MSG_TYPE_WARNING,
                        Popup.BUTTON_TYPE_ACCEPT,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                apiVisit.save();
                                finish();
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                new InternetCheck().execute();
                            }
                        },null);
            } else {
                new ConnectWithService().execute();
            }
        }
    }


    @SuppressLint("StaticFieldLeak")
    private class ConnectWithService extends AsyncTask<Void, String, Void> {
        private ProgressDialog dialog;
        StringBuilder outMessage = new StringBuilder();
        String errMsg = "";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(CurrentArrivalActivity.this);
            dialog.setTitle("Operación en curso");
            dialog.setMessage("Mensaje");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            dialog.setMessage(values[0]);
        }

        @Override
        protected Void doInBackground(Void... args) {
            try {

                SyncData syncData = new SyncData(CurrentArrivalActivity.this,0);

                publishProgress(getResources().getString(R.string.save_visita));
                isConnectedREST = syncData.SyncSaveVisit(outMessage, apiVisit);

            } catch (Exception ex) {
                errMsg = ex.getMessage();
            }
            return null;
        }

        protected void onPostExecute(Void result) {

            if (dialog.isShowing() && dialog!=null) {
                dialog.dismiss();
                if (!isConnectedREST || !errMsg.isEmpty()) {
                    Popup.ShowAlertDialog(CurrentArrivalActivity.this
                            , getResources().getString(R.string.titulo_error)
                            , errMsg.isEmpty() ? outMessage.toString() : errMsg
                            , new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    new InternetCheck().execute();
                                }
                            }, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                } else {
                    Popup.ShowDialogHideButton(CurrentArrivalActivity.this,
                            getString(R.string.mensaje_ok_process),
                            getString(R.string.title_CurrentArrival),
                            getString(R.string.ok),"",
                            Popup.BUTTON_TYPE_ACCEPT,
                            Popup.MSG_TYPE_SUCCESS,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    finish();
                                }
                            },null,null);
                }
            }
        }
    }
    //endregion

    //region onBack
    private void close(){
        Popup.ShowDialog(this,
                getString(R.string.mensaje_exit),
                getString(R.string.modelo_mensaje_titulo_advertencia),
                Popup.MSG_TYPE_WARNING,
                Popup.BUTTON_TYPE_ACCEPT,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                },
                null);
    }

    @Override
    public void onBackPressed() {
        close();
    }
    //endregion
    //endregion

    //region ciclo de vida
    @Override
    protected void onResume() {
        super.onResume();
        if (getLocationManager().isWaitingForLocation()
                && !getLocationManager().isAnyDialogShowing()) {
            displayProgress();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissProgress();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    //endregion

    //region Location Manager
    private void displayProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            Objects.requireNonNull(progressDialog.getWindow()).addFlags(Window.FEATURE_NO_TITLE);
            progressDialog.setMessage(getString(R.string.GET_LOCATION));
        }

        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void updateProgress(String text) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.setMessage(text);
        }
    }

    @Override
    public LocationConfiguration getLocationConfiguration() {
        return Configurations.defaultConfiguration(getString(R.string.ENABLED_PERMISSION), getString(R.string.QUERY_GPS));
    }

    @Override
    public void onLocationChanged(Location location) {
        dismissProgress();
        latitudLoad = location.getLatitude();
        longitudLoad = location.getLongitude();
    }

    @Override
    public void onProcessTypeChanged(@ProcessType int processType) {
        switch (processType) {
            case ProcessType.GETTING_LOCATION_FROM_GOOGLE_PLAY_SERVICES: {
                updateProgress(getString(R.string.ProcessType_GETTING_LOCATION_FROM_GOOGLE_PLAY_SERVICES));
                break;
            }
            case ProcessType.GETTING_LOCATION_FROM_GPS_PROVIDER: {
                updateProgress(getString(R.string.ProcessType_GETTING_LOCATION_FROM_GPS_PROVIDER));
                break;
            }
            case ProcessType.GETTING_LOCATION_FROM_NETWORK_PROVIDER: {
                updateProgress(getString(R.string.ProcessType_GETTING_LOCATION_FROM_NETWORK_PROVIDER));
                break;
            }
            case ProcessType.ASKING_PERMISSIONS:
            case ProcessType.GETTING_LOCATION_FROM_CUSTOM_PROVIDER:
                // Ignored
                break;
        }
    }

    @Override
    public void onLocationFailed(@FailType int failType) {
        dismissProgress();
        switch (failType) {
            case FailType.TIMEOUT: {
                Toast.makeText(this, getString(R.string.FailType_TIMEOUT), Toast.LENGTH_SHORT).show();
                break;
            }
            case FailType.PERMISSION_DENIED: {
                Toast.makeText(this,getString(R.string.FailType_PERMISSION_DENIED), Toast.LENGTH_SHORT).show();
                break;
            }
            case FailType.NETWORK_NOT_AVAILABLE: {
                Toast.makeText(this,getString(R.string.FailType_NETWORK_NOT_AVAILABLE), Toast.LENGTH_SHORT).show();
                break;
            }
            case FailType.GOOGLE_PLAY_SERVICES_NOT_AVAILABLE: {
                Toast.makeText(this,getString(R.string.FailType_GOOGLE_PLAY_SERVICES_NOT_AVAILABLE), Toast.LENGTH_SHORT).show();
                break;
            }
            case FailType.GOOGLE_PLAY_SERVICES_CONNECTION_FAIL: {
                Toast.makeText(this,getString(R.string.FailType_GOOGLE_PLAY_SERVICES_CONNECTION_FAIL), Toast.LENGTH_SHORT).show();
                break;
            }
            case FailType.GOOGLE_PLAY_SERVICES_SETTINGS_DIALOG: {
                Toast.makeText(this,getString(R.string.FailType_GOOGLE_PLAY_SERVICES_SETTINGS_DIALOG), Toast.LENGTH_SHORT).show();
                break;
            }
            case FailType.GOOGLE_PLAY_SERVICES_SETTINGS_DENIED: {
                Toast.makeText(this,getString(R.string.FailType_GOOGLE_PLAY_SERVICES_SETTINGS_DENIED), Toast.LENGTH_SHORT).show();
                break;
            }
            case FailType.VIEW_DETACHED: {
                Toast.makeText(this,getString(R.string.FailType_VIEW_DETACHED), Toast.LENGTH_SHORT).show();
                break;
            }
            case FailType.VIEW_NOT_REQUIRED_TYPE: {
                Toast.makeText(this,getString(R.string.FailType_VIEW_NOT_REQUIRED_TYPE), Toast.LENGTH_SHORT).show();
                break;
            }
            case FailType.UNKNOWN: {
                Toast.makeText(this,getString(R.string.FailType_UNKNOWN), Toast.LENGTH_SHORT).show();
                break;
            }
        }
    }
    //endregion
}
