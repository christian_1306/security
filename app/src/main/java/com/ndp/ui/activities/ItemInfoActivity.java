package com.ndp.ui.activities;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import com.ndp.R;
import com.ndp.models.model.Articulo;
import com.ndp.ui.adapters.ViewPagerAdapter;
import com.ndp.ui.fragments.ItemInfoFragment;
import com.ndp.ui.fragments.ItemPriceFragment;
import com.ndp.ui.fragments.ItemStockFragment;
import com.ndp.utils.Popup.Popup;

public class ItemInfoActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private BottomNavigationView navView;
    private MenuItem prevMenuItem;
    private Toolbar toolbar;
    private ItemInfoFragment itemInfoFragment;
    private ItemStockFragment itemStockFragment;
    private ItemPriceFragment itemPriceFragment;
    private Articulo articulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_info);
        setElements();
    }


    private void setElements(){
        articulo = (Articulo) getIntent().getSerializableExtra(getResources().getString(R.string.obj_item));
        if (articulo != null && articulo.getCodigo() != null && !articulo.getCodigo().isEmpty()) {
            toolbar = findViewById(R.id.tb_view);
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            ((TextView) findViewById(R.id.tv_title_toolbar)).setText(getResources().getString(R.string.title_iteminfo));
            findViewById(R.id.iv_back_toolbar).setOnClickListener(closeItem);
            viewPager = findViewById(R.id.viewpager_item);
            navView = findViewById(R.id.navview_item);
            navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
            viewPager.addOnPageChangeListener(pageChangeListener);
            setupViewPager(viewPager);
        } else {
            MDToast mdToast = MDToast.makeText(this, getString(R.string.error_ins_item), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR );
            mdToast.show();
            this.finish();
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.nav_info:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.nav_stock:
                    viewPager.setCurrentItem(1);
                    return true;
                case R.id.nav_price:
                    viewPager.setCurrentItem(2);
                    return true;
            }
            return false;
        }
    };

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (prevMenuItem != null) {
                prevMenuItem.setChecked(false);
            }
            else
            {
                navView.getMenu().getItem(0).setChecked(false);
            }
            navView.getMenu().getItem(position).setChecked(true);
            prevMenuItem = navView.getMenu().getItem(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    private void setupViewPager(ViewPager viewPager) {
        viewPager.setCurrentItem(3);
        viewPager.setOffscreenPageLimit(3);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        itemInfoFragment = new ItemInfoFragment();
        itemStockFragment = new ItemStockFragment();
        itemPriceFragment = new ItemPriceFragment();

        itemInfoFragment.set_self(this);
        itemStockFragment.set_self(this);
        itemPriceFragment.set_self(this);

        itemInfoFragment.setArticulo(articulo);
        itemStockFragment.setArticulo(articulo);
        itemPriceFragment.setArticulo(articulo);

        adapter.addFragment(itemInfoFragment);
        adapter.addFragment(itemStockFragment);
        adapter.addFragment(itemPriceFragment);
        viewPager.setAdapter(adapter);
    }

    private View.OnClickListener closeItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            closeItem();
        }
    };

    @Override
    public void onBackPressed() {
        closeItem();
    }

    private void closeItem(){
        Popup.ShowDialog(this,
                getString(R.string.mensaje_exit),
                getString(R.string.modelo_mensaje_titulo_advertencia),
                Popup.MSG_TYPE_WARNING,
                Popup.BUTTON_TYPE_ACCEPT,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                },
                null);
    }
}
