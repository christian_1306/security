package com.ndp.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chivorn.smartmaterialspinner.SmartMaterialSpinner;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.List;

import com.ndp.R;
import com.ndp.models.appRest.apiDireccionDetail;
import com.ndp.models.model.Departamento;
import com.ndp.models.model.Municipio;
import com.ndp.models.model.Provincia;
import com.ndp.utils.Popup.Popup;

import static com.ndp.utils.Const.CODE_REQ_OK;
import static com.ndp.utils.Const.TYPE_WAREHOUSE_ALMACEN;
import static com.ndp.utils.Const.TYPE_WAREHOUSE_FISCAL;
import static com.ndp.utils.Useful.getCol;
import static com.ndp.utils.use.usePhone.hideKeyBoard;

public class AddDirectionActivity extends AppCompatActivity {

    private SmartMaterialSpinner sp_departamento;
    private SmartMaterialSpinner sp_provincia;
    private SmartMaterialSpinner sp_municipio;
    private SmartMaterialSpinner sp_tipo_direccion;
    private List<String> type;
    private List<String> depa;
    private List<String> prov;
    private List<String> muni;
    private ImageView iv_back_toolbar;
    private Button btn_create;
    private String typeDireccion = "";
    private int codMunicipio = 0;
    private String municipio = "";
    private String nombreDireccion;
    private String descripdireccion;
    private MaterialEditText et_nombre_direccion;
    private MaterialEditText et_descripcion_direccion;
    private apiDireccionDetail apiDireccion;
    private RelativeLayout header_admin;
    private ImageView header_admin_arrow;
    private ExpandableRelativeLayout card_info;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_direction);
        setElements();
    }

    private void setElements(){
        ((TextView)findViewById(R.id.tv_title_toolbar)).setText(getResources().getString(R.string.add_dir));
        btn_create  = findViewById(R.id.btn_create);
        iv_back_toolbar = findViewById(R.id.iv_back_toolbar);
        sp_tipo_direccion = findViewById(R.id.sp_tipo_direccion);
        sp_departamento = findViewById(R.id.sp_departamento);
        sp_provincia = findViewById(R.id.sp_provincia);
        sp_municipio = findViewById(R.id.sp_municipio);
        et_nombre_direccion = findViewById(R.id.et_nombre_direccion);
        et_descripcion_direccion = findViewById(R.id.et_descripcion_direccion);
        header_admin = findViewById(R.id.header_admin);
        header_admin_arrow = findViewById(R.id.header_admin_arrow);
        card_info = findViewById(R.id.card_info);

        type = new ArrayList<>();
        type.add("FISCAL");
        type.add("ALMACEN");
        sp_tipo_direccion.setItems(type);

        depa = new ArrayList<>();
        prov = new ArrayList<>();
        muni = new ArrayList<>();


        List<Departamento> departamentos = Departamento.listAll(Departamento.class);
        if (departamentos != null && departamentos.size() > 0) {
            for (Departamento dep : departamentos) {
                depa.add(dep.getNombre());
            }
        }

        sp_departamento.setItems(depa);
        sp_departamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                String nomDepartameno = depa.get(position);

                List<Departamento> findep = Departamento.find(Departamento.class, getCol("nombre") + " = '" + nomDepartameno + "'");
                if (findep != null && findep.size() > 0) {
                    prov = new ArrayList<>();
                    List<Provincia> provincias = Provincia.find(Provincia.class, getCol("idDepartamento")
                            + " = " + findep.get(0).getIdDepartamento());
                    if (provincias != null && provincias.size() > 0) {

                        for (Provincia pro : provincias) {
                            prov.add(pro.getNombre());
                        }
                    }

                    sp_provincia.setItems(prov);

                    muni = new ArrayList<>();
                    sp_municipio.setItems(muni);
                    codMunicipio = 0;
                    municipio = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        sp_provincia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                String nomProvincia = prov.get(position);

                List<Provincia> finprov = Provincia.find(Provincia.class, getCol("nombre") + " = '" + nomProvincia + "'");
                if (finprov != null && finprov.size() > 0) {
                    muni = new ArrayList<>();
                    List<Municipio> municipios = Municipio.find(Municipio.class, getCol("idProvincia")
                            + " = " + finprov.get(0).getIdProvincia());
                    if (municipios != null && municipios.size() > 0) {

                        for (Municipio mun : municipios) {
                            muni.add(mun.getNombre());
                        }
                    }

                    sp_municipio.setItems(muni);
                }
                codMunicipio = 0;
                municipio = "";
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        sp_municipio.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                String nomMunicipio = muni.get(position);

                List<Municipio> municipios = Municipio.find(Municipio.class, getCol("nombre")
                            + " = '" + nomMunicipio + "'");

                if (municipios != null && municipios.size() > 0) {
                    codMunicipio = municipios.get(0).getIdMunicipio();
                    municipio = municipios.get(0).getNombre();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        sp_tipo_direccion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                typeDireccion = type.get(position);
                if (typeDireccion.equals("ALMACEN"))
                    typeDireccion = TYPE_WAREHOUSE_ALMACEN;
                else
                    typeDireccion = TYPE_WAREHOUSE_FISCAL;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        header_admin.setOnClickListener(customOnClick);
        iv_back_toolbar.setOnClickListener(customOnClick);
        btn_create.setOnClickListener(customOnClick);

    }

    //region OnClickListener
    private View.OnClickListener customOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.header_admin:
                    card_info.toggle();
                    if (card_info.isExpanded())
                        header_admin_arrow.animate().rotation(180).start();
                    else {
                        header_admin_arrow.animate().rotation(0).start();
                    }
                    break;
                case R.id.btn_create:
                    agregarDireccion();
                    break;
                case R.id.iv_back_toolbar:
                    close();
                    break;
            }
        }
    };
    //endregion

    @Override
    public void onBackPressed() {
        close();
    }

    private void agregarDireccion(){

        nombreDireccion = et_nombre_direccion.getText() != null ? et_nombre_direccion.getText().toString() : "";
        descripdireccion = et_descripcion_direccion.getText() != null ? et_descripcion_direccion.getText().toString() : "";

        if (typeDireccion.isEmpty()){
            return;
        }

        if (municipio.isEmpty()){
            return;
        }

        if (nombreDireccion.isEmpty()){
            et_nombre_direccion.requestFocus();
            et_nombre_direccion.setError(getString(R.string.into_value));
            return;
        }

        if (descripdireccion.isEmpty()){
            et_descripcion_direccion.requestFocus();
            et_descripcion_direccion.setError(getString(R.string.into_value));
            return;
        }

        if (!et_nombre_direccion.isCharactersCountValid()){
            et_nombre_direccion.requestFocus();
            return;
        }

        if (!et_descripcion_direccion.isCharactersCountValid()){
            et_descripcion_direccion.requestFocus();
            return;
        }



        apiDireccion = new apiDireccionDetail();
        apiDireccion.setIdDireccion(0);
        apiDireccion.setNombreDireccion(nombreDireccion);
        apiDireccion.setDireccion(descripdireccion);
        apiDireccion.setIdMunicipio(codMunicipio);
        apiDireccion.setNombMunicipio(municipio);
        apiDireccion.setTipo(typeDireccion);

        Intent intent = new Intent();
        intent.putExtra(getResources().getString(R.string.obj_customer_direction), apiDireccion);
        setResult(CODE_REQ_OK, intent);
        hideKeyBoard(this);
        finish();
    }

    private void close(){
        Popup.ShowDialog(this,
                getString(R.string.mensaje_exit),
                getString(R.string.modelo_mensaje_titulo_advertencia),
                Popup.MSG_TYPE_WARNING,
                Popup.BUTTON_TYPE_ACCEPT,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                },
                null);
    }
}
