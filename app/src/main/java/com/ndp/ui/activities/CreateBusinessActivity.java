package com.ndp.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import com.ndp.R;
import com.ndp.data.api.SyncData;
import com.ndp.models.appRest.apiContactoDetail;
import com.ndp.models.appRest.apiCreateBusiness;
import com.ndp.models.appRest.apiCreateLead;
import com.ndp.models.appRest.apiDireccionDetail;
import com.ndp.models.model.CondicionPago;
import com.ndp.models.model.Tarifario;
import com.ndp.models.model.User;
import com.ndp.ui.adapters.CustomerAddressAdapter;
import com.ndp.ui.adapters.CustomerContactAdapter;
import com.ndp.utils.Popup.Popup;
import com.ndp.utils.Shared;
import com.ndp.utils.Useful;
import com.ndp.utils.use.usePhone;

import static com.ndp.utils.Const.CODE_CREATE_BUSINESS_ADD_CONTACT;
import static com.ndp.utils.Const.CODE_CREATE_BUSINESS_ADD_DIRECTION;
import static com.ndp.utils.Useful.getCol;
import static com.ndp.utils.Useful.validarSiEsCorreo;
import static com.ndp.utils.use.useAPP.getTipoDocumento;
import static com.ndp.utils.use.useAPP.getTipoPersona;
import static com.ndp.utils.use.usePhone.getcodCreacionApp;

public class CreateBusinessActivity extends AppCompatActivity {

    private TextView tv_title_toolbar;
    private ImageView ivRetroceder;
    private MaterialEditText et_lead_tipo_persona;
    private MaterialEditText et_lead_razon_social;
    private MaterialEditText et_lead_nombre;
    private MaterialEditText et_lead_apepat;
    private MaterialEditText et_lead_apemat;
    private MaterialEditText et_lead_tipo_documento;
    private MaterialEditText et_lead_numero_documento;
    private MaterialEditText et_lead_correo;
    private MaterialEditText et_lead_telefono;
    private MaterialEditText et_lead_condicion_pago;
    private MaterialEditText et_lead_lista_precio;
    private Button btn_create_lead;
    private Boolean isConnectedREST = true;
    private apiCreateLead apiCreateLead;
    private String codCreacionApp;
    private Spinner spinner_tipoPersona;
    private Spinner spinner_tipoDocumento;
    private Spinner spinner_condicion_pago;
    private Spinner spinner_lista_precio;
    private Boolean isCreateBusiness = false;
    private LinearLayout lyt_condicion_pago;
    private LinearLayout lyt_lista_precio;

    private RelativeLayout order_header_direccion;
    private ImageView order_header_direccion_arrow;
    private ExpandableRelativeLayout order_detail_direccion;
    private LinearLayout lyt_empty_direccion;
    private RecyclerView rv_direcciones;

    private RelativeLayout order_header_contactos;
    private ImageView order_header_contactos_arrow;
    private ExpandableRelativeLayout order_detail_contactos;
    private LinearLayout lyt_empty_contacto;
    private RecyclerView rv_contactos;

    private ArrayList<apiDireccionDetail>  direccionDetails;
    private ArrayList<apiContactoDetail>  contactoDetails;
    private CustomerAddressAdapter addressAdapter;
    private CustomerContactAdapter contactAdapter;

    private RelativeLayout header_admin;
    private ImageView header_admin_arrow;
    private ExpandableRelativeLayout card_info;

    private Intent i;
    private apiCreateBusiness apiBusiness;
    private Integer codPriceList = 1;
    private String nomPriceList = "";
    private Integer codCondicion = -1;
    private SharedPreferences shrPreLogin;
    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_business);
        setElements();
    }


    //region setElements
    private void setElements() {

        Toolbar toolbar = findViewById(R.id.tb_view);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null ) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setTitle("");
        } else {
            this.finish();
        }

        isCreateBusiness = getIntent().getBooleanExtra(getResources().getString(R.string.valueAction),false);
        tv_title_toolbar = findViewById(R.id.tv_title_toolbar);
        if (!isCreateBusiness) tv_title_toolbar.setText(getResources().getString(R.string.title_create_lead));
        if (isCreateBusiness) tv_title_toolbar.setText(getResources().getString(R.string.title_create_business));
        et_lead_tipo_persona = findViewById(R.id.et_lead_tipo_persona);
        spinner_tipoPersona = findViewById(R.id.spinner_tipoPersona);
        et_lead_razon_social = findViewById(R.id.et_lead_razon_social);
        et_lead_nombre = findViewById(R.id.et_lead_nombre);
        et_lead_apepat = findViewById(R.id.et_lead_apepat);
        et_lead_apemat = findViewById(R.id.et_lead_apemat);
        et_lead_tipo_documento = findViewById(R.id.et_lead_tipo_documento);
        spinner_tipoDocumento = findViewById(R.id.spinner_tipoDocumento);
        et_lead_numero_documento = findViewById(R.id.et_lead_numero_documento);
        et_lead_numero_documento.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_lead_correo = findViewById(R.id.et_lead_correo);
        et_lead_telefono = findViewById(R.id.et_lead_telefono);
        et_lead_condicion_pago = findViewById(R.id.et_lead_condicion_pago);
        spinner_condicion_pago = findViewById(R.id.spinner_condicion_pago);
        et_lead_lista_precio = findViewById(R.id.et_lead_lista_precio);
        spinner_lista_precio = findViewById(R.id.spinner_lista_precio);
        lyt_condicion_pago = findViewById(R.id.lyt_condicion_pago);
        lyt_lista_precio = findViewById(R.id.lyt_lista_precio);
        btn_create_lead = findViewById(R.id.btn_create_lead);
        ivRetroceder = findViewById(R.id.iv_back_toolbar);
        header_admin = findViewById(R.id.header_admin);
        header_admin_arrow = findViewById(R.id.header_admin_arrow);
        card_info = findViewById(R.id.card_info);
        header_admin.setOnClickListener(onClickCustom);
        order_header_direccion = findViewById(R.id.order_header_direccion);
        order_header_direccion_arrow = findViewById(R.id.order_header_direccion_arrow);
        order_detail_direccion = findViewById(R.id.order_detail_direccion);
        lyt_empty_direccion = findViewById(R.id.lyt_empty_direccion);
        rv_direcciones = findViewById(R.id.rv_direcciones);
        order_header_contactos = findViewById(R.id.order_header_contactos);
        order_header_contactos_arrow = findViewById(R.id.order_header_contactos_arrow);
        order_detail_contactos = findViewById(R.id.order_detail_contactos);
        lyt_empty_contacto = findViewById(R.id.lyt_empty_contacto);
        rv_contactos = findViewById(R.id.rv_contactos);
        shrPreLogin = Shared.prf_Login(this);
        List<User> userList = User.find(User.class, Useful.getCol("correoUsuario")  + " = ?", shrPreLogin.getString(Shared.login_usuario, null));
        if (userList != null && userList.size() > 0) {
            user = userList.get(0);
        } else {
            MDToast mdToast = MDToast.makeText(this, "No se puede abrir función, por favor loguearse nuevamente.", MDToast.LENGTH_LONG, MDToast.TYPE_ERROR );
            mdToast.show();
            this.finish();
        }

        if (!isCreateBusiness)
            codCreacionApp = getString(R.string.type_create_lead) + getcodCreacionApp(this);

        if (isCreateBusiness) {

            codCreacionApp = getString(R.string.type_create_business) + getcodCreacionApp(this);
            order_header_direccion.setVisibility(View.VISIBLE);
            order_detail_direccion.setVisibility(View.VISIBLE);
            order_header_contactos.setVisibility(View.VISIBLE);
            order_detail_contactos.setVisibility(View.VISIBLE);
            lyt_condicion_pago.setVisibility(View.VISIBLE);
            lyt_lista_precio.setVisibility(View.VISIBLE);

            List<String> listCondicionPago = new ArrayList<String>();
            List<CondicionPago> condicionPagos =  CondicionPago.listAll(CondicionPago.class);
            if (condicionPagos != null && condicionPagos.size() > 0) {
                for (CondicionPago c : condicionPagos) {
                    listCondicionPago.add(c.getNombre());
                }
            } else
                listCondicionPago.add("CONTADO");

            ArrayAdapter<String> adapter = new ArrayAdapter<String>
                    (this, android.R.layout.simple_dropdown_item_1line, listCondicionPago);
            adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            spinner_condicion_pago.setAdapter(adapter);
            spinner_condicion_pago.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String type = parent.getItemAtPosition(position).toString();
                    et_lead_condicion_pago.setText(type);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            List<String> listPrice = new ArrayList<String>();
            List<Tarifario> tarifarios =  Tarifario.listAll(Tarifario.class);
            if (tarifarios != null && tarifarios.size() > 0) {
                for (Tarifario t : tarifarios) {
                    listPrice.add(t.getNombre());
                }
            } else
                listCondicionPago.add("SIN PRECIOS");

            ArrayAdapter<String> adapPrice = new ArrayAdapter<String>
                    (this, android.R.layout.simple_dropdown_item_1line, listPrice);
            adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            spinner_lista_precio.setAdapter(adapPrice);
            spinner_lista_precio.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String type = parent.getItemAtPosition(position).toString();
                    et_lead_lista_precio.setText(type);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }


        List<String> listTipoPersona = getTipoPersona();
        ArrayAdapter<String> adp = new ArrayAdapter<String>
                (this, android.R.layout.simple_dropdown_item_1line, listTipoPersona);
        adp.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner_tipoPersona.setAdapter(adp);
        spinner_tipoPersona.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String type = parent.getItemAtPosition(position).toString();
                if (type.equals(getString(R.string.type_person_tpn))){
                    et_lead_razon_social.setVisibility(View.GONE);
                    et_lead_nombre.setVisibility(View.VISIBLE);
                    et_lead_apepat.setVisibility(View.VISIBLE);
                    et_lead_apemat.setVisibility(View.VISIBLE);
                } else {
                    et_lead_razon_social.setVisibility(View.VISIBLE);
                    et_lead_nombre.setVisibility(View.GONE);
                    et_lead_apepat.setVisibility(View.GONE);
                    et_lead_apemat.setVisibility(View.GONE);
                }

                if (type.equals(getString(R.string.type_person_tpj))){
                    spinner_tipoDocumento.setSelection(3);
                }

                if (type.equals(getString(R.string.type_person_snd))){
                    spinner_tipoDocumento.setSelection(0);
                }

                et_lead_tipo_persona.setText(type);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<String> listTipoDocumento = getTipoDocumento();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_dropdown_item_1line, listTipoDocumento);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner_tipoDocumento.setAdapter(adapter);
        spinner_tipoDocumento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String type = parent.getItemAtPosition(position).toString();
                et_lead_tipo_documento.setText(type);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        et_lead_tipo_persona.setOnClickListener(onClickCustom);
        et_lead_tipo_documento.setOnClickListener(onClickCustom);
        et_lead_condicion_pago.setOnClickListener(onClickCustom);
        et_lead_lista_precio.setOnClickListener(onClickCustom);
        order_header_direccion.setOnClickListener(onClickCustom);
        order_header_contactos.setOnClickListener(onClickCustom);
        ivRetroceder.setOnClickListener(onClickCustom);
        btn_create_lead.setOnClickListener(onClickCustom);
        direccionDetails = new ArrayList<>();
        contactoDetails = new ArrayList<>();

    }
    //endregion

    //region OnClickListener onClickCustom
    private View.OnClickListener onClickCustom = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.header_admin:
                    card_info.toggle();
                    if (card_info.isExpanded())
                        header_admin_arrow.animate().rotation(180).start();
                    else {
                        header_admin_arrow.animate().rotation(0).start();
                    }
                    break;
                case R.id.iv_back_toolbar:
                    closeCreateBusiness();
                    break;
                case R.id.btn_create_lead:
                    validateAndSendCreateBusiness();
                    break;
                case R.id.et_lead_tipo_persona:
                    spinner_tipoPersona.performClick();
                    break;
                case R.id.et_lead_tipo_documento:
                    spinner_tipoDocumento.performClick();
                    break;
                case R.id.et_lead_condicion_pago:
                    spinner_condicion_pago.performClick();
                    break;
                case R.id.et_lead_lista_precio:
                    spinner_lista_precio.performClick();
                    break;
                case R.id.order_header_direccion:
                    order_detail_direccion.toggle();
                    if (order_detail_direccion.isExpanded())
                        order_header_direccion_arrow.animate().rotation(180).start();
                    else {
                        order_header_direccion_arrow.animate().rotation(0).start();
                    }
                    break;
                case R.id.order_header_contactos:
                    order_detail_contactos.toggle();
                    if (order_detail_contactos.isExpanded())
                        order_header_contactos_arrow.animate().rotation(180).start();
                    else {
                        order_header_contactos_arrow.animate().rotation(0).start();
                    }
                    break;
            }
        }
    };
    //endregion

    private void validateAndSendCreateBusiness(){

        String tipoPersona = Objects.requireNonNull(et_lead_tipo_persona.getText()).toString();
        String tipoDocumento = Objects.requireNonNull(et_lead_tipo_documento.getText()).toString();
        String razonSocial = Objects.requireNonNull(et_lead_razon_social.getText()).toString();
        String nombre = Objects.requireNonNull(et_lead_nombre.getText()).toString();
        String apellidoPaterno = Objects.requireNonNull(et_lead_apepat.getText()).toString();
        String apellidoMaterno = Objects.requireNonNull(et_lead_apemat.getText()).toString();
        String numeroDocumento = Objects.requireNonNull(et_lead_numero_documento.getText()).toString();
        String email = Objects.requireNonNull(et_lead_correo.getText()).toString();
        String telefono = Objects.requireNonNull(et_lead_telefono.getText()).toString();
        String nomCond = Objects.requireNonNull(et_lead_condicion_pago.getText()).toString();
        String nomPrec = Objects.requireNonNull(et_lead_lista_precio.getText()).toString();

        List<Tarifario> tarifarios = Tarifario.find(Tarifario.class, getCol("nombre") + " = '" + nomPrec + "'");
        if (tarifarios != null && tarifarios.size() > 0) {
             codPriceList = tarifarios.get(0).getCodigoListaPrecio();
             nomPriceList = tarifarios.get(0).getNombre();
        }

        List<CondicionPago> condicionPagos = CondicionPago.find(CondicionPago.class,getCol("nombre") + " = '" + nomCond + "'");
        if (condicionPagos != null && condicionPagos.size() > 0) {
            codCondicion = condicionPagos.get(0).getCodigo();
        }

        if (tipoPersona.isEmpty()){
            et_lead_tipo_persona.requestFocus();
            et_lead_tipo_persona.setError(getString(R.string.request_type_person));
            return;
        }

        if (tipoDocumento.isEmpty()){
            et_lead_tipo_documento.requestFocus();
            et_lead_tipo_documento.setError(getString(R.string.request_type_document));
            return;
        }

        if (tipoPersona.equals(getString(R.string.type_person_tpn)) && !tipoDocumento.equals(getString(R.string.type_document_ci))){
            et_lead_tipo_documento.requestFocus();
            et_lead_tipo_documento.setError(getString(R.string.request_type_document_tpn));
            return;
        }

        if (tipoPersona.equals(getString(R.string.type_person_tpj)) && !tipoDocumento.equals(getString(R.string.type_document_nit))){
            et_lead_tipo_documento.requestFocus();
            et_lead_tipo_documento.setError(getString(R.string.request_type_document_tpj));
            return;
        }

        if (tipoPersona.equals(getString(R.string.type_person_snd)) && !tipoDocumento.equals("0 - Otros Tipos de Documentos") && !tipoDocumento.equals("4 - Pasaporte")){
            et_lead_tipo_documento.requestFocus();
            et_lead_tipo_documento.setError(getString(R.string.request_type_document_snd));
            return;
        }

        if (tipoPersona.equals(getString(R.string.type_person_tpn)) && nombre.isEmpty()){
            et_lead_nombre.requestFocus();
            et_lead_nombre.setError(getString(R.string.request_name));
            return;
        }

        if (!et_lead_nombre.isCharactersCountValid()){
            et_lead_nombre.requestFocus();
            et_lead_nombre.setError(getString(R.string.error_supera_maxcaracter));
            return;
        }

        if (tipoPersona.equals(getString(R.string.type_person_tpn)) && apellidoPaterno.isEmpty()){
            et_lead_apepat.requestFocus();
            et_lead_apepat.setError(getString(R.string.request_ape_pat));
            return;
        }

        if (!et_lead_apepat.isCharactersCountValid()){
            et_lead_apepat.requestFocus();
            et_lead_apepat.setError(getString(R.string.error_supera_maxcaracter));
            return;
        }


        if (!et_lead_apemat.isCharactersCountValid()){
            et_lead_apemat.requestFocus();
            et_lead_apemat.setError(getString(R.string.error_supera_maxcaracter));
            return;
        }

        if (!tipoPersona.equals(getString(R.string.type_person_tpn)) && razonSocial.isEmpty()){
            et_lead_razon_social.requestFocus();
            et_lead_razon_social.setError(getString(R.string.request_razon_social));
            return;
        }

        if (!et_lead_razon_social.isCharactersCountValid()){
            et_lead_razon_social.requestFocus();
            et_lead_razon_social.setError(getString(R.string.error_supera_maxcaracter));
            return;
        }

        if (numeroDocumento.isEmpty()){
            et_lead_numero_documento.requestFocus();
            et_lead_numero_documento.setError(getString(R.string.request_documento));
            return;
        }

        if (!et_lead_numero_documento.isCharactersCountValid()){
            et_lead_numero_documento.requestFocus();
            et_lead_numero_documento.setError(getString(R.string.error_supera_maxcaracter));
            return;
        }

        if (email.isEmpty() && isCreateBusiness){
            et_lead_correo.requestFocus();
            et_lead_correo.setError(getString(R.string.request_email));
            return;
        }

        if (!validarSiEsCorreo(email) && isCreateBusiness){
            et_lead_correo.requestFocus();
            et_lead_correo.setError(getString(R.string.request_email_validate));
            return;
        }

        if (!et_lead_correo.isCharactersCountValid()){
            et_lead_correo.requestFocus();
            et_lead_correo.setError(getString(R.string.error_supera_maxcaracter));
            return;
        }

        if (telefono.isEmpty() && isCreateBusiness){
            et_lead_telefono.requestFocus();
            et_lead_telefono.setError(getString(R.string.request_telefono));
            return;
        }

        if (!et_lead_telefono.isCharactersCountValid()){
            et_lead_telefono.requestFocus();
            et_lead_telefono.setError(getString(R.string.error_supera_maxcaracter));
            return;
        }

        if (!isCreateBusiness) {

            apiCreateLead = new apiCreateLead();
            apiCreateLead.setCardCode(getString(R.string.symbol_lead) + numeroDocumento);
            apiCreateLead.setCardName(razonSocial);
            apiCreateLead.setNombre("");
            apiCreateLead.setApellidoPaterno("");
            apiCreateLead.setApellidoMaterno("");

            if (tipoPersona.equals(getString(R.string.type_person_tpn))) {
                apiCreateLead.setNombre(nombre);
                apiCreateLead.setApellidoPaterno(apellidoPaterno);
                apiCreateLead.setApellidoMaterno(apellidoMaterno);
                apiCreateLead.setCardName(nombre + " " + apellidoPaterno);
            }

            apiCreateLead.setNumeroDocIdentidad(numeroDocumento);
            apiCreateLead.setCorreo(email.isEmpty() ? "-" : email);
            apiCreateLead.setTelefono(telefono.isEmpty() ? "-" : telefono);
            apiCreateLead.setTipoDocumento(Integer.parseInt(tipoDocumento.substring(0, 1)));
            apiCreateLead.setTipoPersona(tipoPersona.substring(0, 3));
            apiCreateLead.setCodCreacionApp(codCreacionApp);
            apiCreateLead.setUserAccount(user.getCorreoUsuario());
            apiCreateLead.setTypeObject(3);
            apiCreateLead.setFechaRegistro(new Date());
            apiCreateLead.setEstadoMigracion(1);
            apiCreateLead.setMensajeMigracion("");

        } else {

            if (direccionDetails == null || direccionDetails.size() <= 0) {
                MDToast mdToast = MDToast.makeText(this, getString(R.string.in_list_address), MDToast.LENGTH_SHORT, MDToast.TYPE_INFO );
                mdToast.show();
                return;
            }

            if (contactoDetails == null || contactoDetails.size() <= 0) {
                MDToast mdToast = MDToast.makeText(this, getString(R.string.in_list_contact), MDToast.LENGTH_SHORT, MDToast.TYPE_INFO );
                mdToast.show();
                return;
            }

            apiBusiness = new apiCreateBusiness();
            apiBusiness.setIdCliente(0);
            apiBusiness.setCardCode(getString(R.string.symbol_business) + numeroDocumento);
            apiBusiness.setRazonSocial(razonSocial);
            apiBusiness.setNombre("");
            apiBusiness.setApellidoPaterno("");
            apiBusiness.setApellidoMaterno("");
            if (tipoPersona.equals(getString(R.string.type_person_tpn))) {
                apiBusiness.setNombre(nombre);
                apiBusiness.setApellidoPaterno(apellidoPaterno);
                apiBusiness.setApellidoMaterno(apellidoMaterno);
                apiBusiness.setRazonSocial(nombre + " " + apellidoPaterno);
            }

            apiBusiness.setNumeroDocIdentidad(numeroDocumento);
            apiBusiness.setCorreo(email);
            apiBusiness.setTelefono(telefono);
            apiBusiness.setTipoDocumento(Integer.parseInt(tipoDocumento.substring(0, 1)));
            apiBusiness.setTipoPersona(tipoPersona.substring(0, 3));
            apiBusiness.setCodCreacionApp(codCreacionApp);
            apiBusiness.setDeuda(0d);
            apiBusiness.setLineaCredito(0d);
            apiBusiness.setSaldoCredito(0d);
            apiBusiness.setComentario("Created by app");
            apiBusiness.setEstadoLogico(1);
            apiBusiness.setCodListaPrecio(codPriceList);
            apiBusiness.setNombListaPrecio(nomPriceList);
            apiBusiness.setCondicionDePago(codCondicion);
            apiBusiness.setListContactos(contactoDetails);
            apiBusiness.setListaDirecciones(direccionDetails);
            apiBusiness.setUserAccount(user.getCorreoUsuario());
            apiBusiness.setTypeObject(2);
            apiBusiness.setFechaRegistro(new Date());
            apiBusiness.setEstadoMigracion(1);
            apiBusiness.setMensajeMigracion("");
        }

        new InternetCheck().execute();
    }

    //region salir Activity
    @Override
    public void onBackPressed() {
        closeCreateBusiness();
    }
    //endregion

    //region Call Web Services
    @SuppressLint("StaticFieldLeak")
    private class InternetCheck extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            return usePhone.isOnline();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (!result) {
                Popup.ShowDialogCustomButton(CreateBusinessActivity.this,
                        getString(R.string.mensaje_sin_internet_business),
                        getString(R.string.titulo_send_customer),
                        getString(R.string.save),getString(R.string.reload),
                        Popup.MSG_TYPE_WARNING,
                        Popup.BUTTON_TYPE_ACCEPT,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (!isCreateBusiness) {
                                    apiCreateLead.save();
                                } else {
                                    apiBusiness.save();
                                    for (apiContactoDetail detailContact : contactoDetails) {
                                        detailContact.save();
                                    }
                                    for (apiDireccionDetail detailAddress : direccionDetails) {
                                        detailAddress.save();
                                    }
                                }
                                finish();
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                new InternetCheck().execute();
                            }
                        },null);
            } else {
                Popup.ShowDialog(CreateBusinessActivity.this,
                         getString(R.string.mensaje_send_customer),
                         getString(R.string.titulo_send_customer),
                         Popup.MSG_TYPE_SUCCESS,
                         Popup.BUTTON_TYPE_ACCEPT,
                         new View.OnClickListener() {
                           @Override
                           public void onClick(View view) {
                               new ConnectWithService().execute();
                           }
                         },
                         null);
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    private class ConnectWithService extends AsyncTask<Void, String, Void> {
        private ProgressDialog dialog;
        StringBuilder outMessage = new StringBuilder();
        String errMsg = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(CreateBusinessActivity.this);
            dialog.setTitle("Operación en curso");
            dialog.setMessage("Mensaje");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            dialog.setMessage(values[0]);
        }

        @Override
        protected Void doInBackground(Void... args) {
            try {
                if (!isCreateBusiness) {
                    publishProgress(getResources().getString(R.string.sync_create_lead));
                    SyncData syncData = new SyncData(CreateBusinessActivity.this, 0);
                    isConnectedREST = syncData.SyncSaveLead(outMessage, apiCreateLead);
                } else {
                    publishProgress(getResources().getString(R.string.sync_create_business));
                    SyncData syncData = new SyncData(CreateBusinessActivity.this, 0);
                    isConnectedREST = syncData.SyncSaveBusiness(outMessage, apiBusiness);
                }

            }catch (Exception ex) {
                errMsg = ex.getMessage();
            }
            return null;
        }

        protected void onPostExecute(Void result) {

            if (dialog.isShowing() && dialog!=null) {
                dialog.dismiss();
                if (!isConnectedREST || !errMsg.isEmpty()) {
                    Popup.ShowAlertDialog(CreateBusinessActivity.this
                            , getResources().getString(R.string.titulo_error)
                            , errMsg.isEmpty() ? outMessage.toString() : errMsg
                            , new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    new InternetCheck().execute();
                                }
                            }, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                } else {
                    Popup.ShowDialogHideButton(CreateBusinessActivity.this,
                            getString(R.string.mensaje_ok_process),
                            getString(R.string.btn_create),
                            getString(R.string.ok),"",
                            Popup.BUTTON_TYPE_ACCEPT,
                            Popup.MSG_TYPE_SUCCESS,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    finish();
                                }
                            },null,null);
                }
            }
        }
    }
    //endregion

    private void closeCreateBusiness(){
        Popup.ShowDialog(this,
                getString(R.string.mensaje_exit),
                getString(R.string.modelo_mensaje_titulo_advertencia),
                Popup.MSG_TYPE_WARNING,
                Popup.BUTTON_TYPE_ACCEPT,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                },
                null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        if (isCreateBusiness)
            getMenuInflater().inflate(R.menu.menu_create_business, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_add_direccion:
                i = new Intent(CreateBusinessActivity.this, AddDirectionActivity.class);
                i.putExtra(getResources().getString(R.string.valueActivity), String.valueOf(CODE_CREATE_BUSINESS_ADD_DIRECTION));
                startActivityForResult(i,CODE_CREATE_BUSINESS_ADD_DIRECTION);
                return true;
            case R.id.action_add_contacto:
                i = new Intent(CreateBusinessActivity.this, AddContactActivity.class);
                i.putExtra(getResources().getString(R.string.valueActivity), String.valueOf(CODE_CREATE_BUSINESS_ADD_CONTACT));
                startActivityForResult(i,CODE_CREATE_BUSINESS_ADD_CONTACT);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    //region onActivityResult
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null){
            if (requestCode == CODE_CREATE_BUSINESS_ADD_DIRECTION) {
                apiDireccionDetail apiDireccion = (apiDireccionDetail) data.getSerializableExtra(getResources().getString(R.string.obj_customer_direction));
                if (apiDireccion != null) {
                    apiDireccion.setCodCreacionBusiness(codCreacionApp);
                    direccionDetails.add(apiDireccion);
                }

                RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
                itemAnimator.setAddDuration(300);
                itemAnimator.setRemoveDuration(300);
                rv_direcciones.setItemAnimator(itemAnimator);

                rv_direcciones.setHasFixedSize(true);
                rv_direcciones.setLayoutManager(new LinearLayoutManager(this));
                addressAdapter = new CustomerAddressAdapter(direccionDetails, this, lyt_empty_direccion, rv_direcciones);
                rv_direcciones.setAdapter(addressAdapter);

                rv_direcciones.setVisibility(View.VISIBLE);
                lyt_empty_direccion.setVisibility(View.GONE);
            }

            if (requestCode == CODE_CREATE_BUSINESS_ADD_CONTACT) {
                apiContactoDetail apiContacto = (apiContactoDetail) data.getSerializableExtra(getResources().getString(R.string.obj_customer_contacto));
                if (apiContacto != null) {
                    apiContacto.setCodCreacionBusiness(codCreacionApp);
                    contactoDetails.add(apiContacto);
                }

                RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
                itemAnimator.setAddDuration(300);
                itemAnimator.setRemoveDuration(300);
                rv_contactos.setItemAnimator(itemAnimator);

                rv_contactos.setHasFixedSize(true);
                rv_contactos.setLayoutManager(new LinearLayoutManager(this));
                contactAdapter = new CustomerContactAdapter(contactoDetails, this, lyt_empty_contacto, rv_contactos);
                rv_contactos.setAdapter(contactAdapter);

                rv_contactos.setVisibility(View.VISIBLE);
                lyt_empty_contacto.setVisibility(View.GONE);

            }

        }
    }
    //endregion




}
