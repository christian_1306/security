package com.ndp.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.ndp.R;
import com.ndp.models.model.Cliente;
import com.ndp.models.model.Direccion;
import com.ndp.models.model.Visita;
import com.ndp.models.retrofit.ApiData;
import com.ndp.models.retrofit.SyncData;
import com.ndp.models.dto.VisitaView;
import com.ndp.utils.use.usePhone;

public class VisitActivity extends AppCompatActivity {

    private Spinner spMotivos;
    private Spinner spDirecciones;
    private TextView tv_seleccionar_cliente,tv_fecha_visita,tv_hora_visita;
    private int mYear,mMonth,mDay,mHour,mMinute;
    private String string_fechaEntrega;
    private Date fecha_Entrega;
    private Time time_entrega;
    private ImageView ivRetroceder;
    private int id_vendedor;
    private SyncData syncData;
    private ApiData<VisitaView> apiData;
    private EditText etComentario;

    private Cliente cliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.cliente = null;
        setElements();
    }

    private void setElements() {
        syncData = new SyncData();
        apiData = new ApiData<VisitaView>();
        apiData.setImei(usePhone.getIMEI(this));
        Intent intent = getIntent();
        id_vendedor = intent.getIntExtra("id_vendedor",0);
        setContentView(R.layout.activity_visit);
        setSpinnerMotivos();
        ((TextView)findViewById(R.id.tv_title_toolbar)).setText("REGISTRAR VISITA");
        tv_seleccionar_cliente = findViewById(R.id.tv_seleccionar_cliente_visita);
        tv_seleccionar_cliente.setOnClickListener(btnSeleccionarCliente_Click);
        etComentario = findViewById(R.id.et_comentario_visita);
        spDirecciones = findViewById(R.id.sp_direccion_visita);
        tv_hora_visita = findViewById(R.id.tv_hora_visita);
        tv_fecha_visita = findViewById(R.id.tv_fecha_visita);
        Calendar calendar = Calendar.getInstance();
        this.fecha_Entrega = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat dateFormat_2 = new SimpleDateFormat("hh:mm:ss");
        String fecha_actual = dateFormat.format(fecha_Entrega);
        String hora_actual = dateFormat_2.format(fecha_Entrega);
        tv_fecha_visita.setText(fecha_actual);
        tv_hora_visita.setText(hora_actual);
        ivRetroceder = findViewById(R.id.iv_back_toolbar);
        ivRetroceder.setOnClickListener(cerrarActivity);

    }

    private void setSpinnerDirecciones(){
       // List<Direccion> direcciones = this.cliente.listarDirecciones();
      //  ArrayAdapter<Direccion> adapterDirecciones = new ArrayAdapter<>(this,R.layout.spinner_item_ubigeo,direcciones);
       // spDirecciones.setAdapter(adapterDirecciones);
    }

    private View.OnClickListener btnSeleccionarCliente_Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(VisitActivity.this,SelectClientActivity.class);
            startActivityForResult(intent,1);
        }
    };

    private void setSpinnerMotivos() {
        spMotivos = findViewById(R.id.sp_motivo_visita);
        String[] listaMotivos = new String[]{"Venta", "Cobranza", "Apertura de cliente"};
        ArrayAdapter<String> adapterMotivo = new ArrayAdapter<String>(this, R.layout.spinner_item_ubigeo, listaMotivos);
        spMotivos.setAdapter(adapterMotivo);
    }

    public void mostarCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data){
        if(requestCode==1){
            if(resultCode==RESULT_OK){
                this.cliente = Cliente.find(Cliente.class,"id_cliente = ?",data.getStringExtra("idCliente")).get(0);
                tv_seleccionar_cliente.setText(cliente.getRazonSocial());
                setSpinnerDirecciones();
            }
        }
    }

    public void registrarVisita(View view){
        if(this.cliente==null){
            Toast.makeText(this,"Debe seleccionar un cliente",Toast.LENGTH_SHORT).show();
            return;
        }
        switch(etComentario.length()){
            case 0:
                creacionDeVisita();
                break;
            default :
                if(etComentario.length()<3){
                    Toast.makeText(this,"El comentario debe tener al menos 3 caracteres",Toast.LENGTH_SHORT).show();
                    return;
                }
                creacionDeVisita();

        }
    }

    private void creacionDeVisita(){
        Visita visita = new Visita();
        visita.setCliente(this.cliente);
        visita.setEstado(1);
        visita.setComentario(((EditText)findViewById(R.id.et_comentario_visita)).getText().toString());
        visita.setFechaVisita(this.fecha_Entrega);
        visita.setMotivo(((Spinner)findViewById(R.id.sp_motivo_visita)).getSelectedItem().toString());
        //visita.setHoraVisita(this.time_entrega);
        visita.setDireccion((Direccion)spDirecciones.getSelectedItem());
        int newIdVisita = (int)Visita.count(Visita.class) + 1;
        visita.setIdVisita(newIdVisita);
        syncData.registrarVisitaEnDb(visita,apiData,this,id_vendedor);
       // visita.save();
//        Toast.makeText(VisitActivity.this,"Visita registrado exitosamente",Toast.LENGTH_SHORT).show();
//        finish();
    }


    private View.OnClickListener cerrarActivity = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };


}
