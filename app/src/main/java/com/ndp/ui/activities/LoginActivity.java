package com.ndp.ui.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ndp.models.dao.DaoSession;
import com.ndp.models.dao.MemberDao;
import com.ndp.models.dao.MemberDaoDao;
import com.ndp.models.model.Member;
import com.ndp.utils.methods.SessionManager;
import com.orm.SugarContext;
import com.valdesekamdem.library.mdtoast.MDToast;

import com.ndp.R;


import com.ndp.data.api.SyncData;
import com.ndp.models.model.User;
import com.ndp.utils.secure.AESCrypt;
import com.ndp.utils.Popup.Popup;
import com.ndp.utils.Shared;
import com.ndp.utils.Useful;
import com.ndp.utils.use.useFormat;
import com.ndp.utils.use.usePhone;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import static com.ndp.utils.Useful.getCol;

public class LoginActivity extends AppCompatActivity {

    private DaoSession daoSession;
    private MemberDaoDao memberDao;
    private TextInputEditText etUsuario;
    private TextInputEditText etPassword;
    private TextView tvRegistrarse;
    private Button btnIngresar;
    private long timeBack;
    private String usuario;
    private Member currentMember;
    private String password;
    private String[] appPermissions = new String[]{
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.INTERNET,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_SYNC_SETTINGS,
            Manifest.permission.READ_SYNC_SETTINGS,
            Manifest.permission.READ_SYNC_STATS,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.CALL_PHONE
    };
    private int PERMISSIONS_REQUEST_CODE = 1248;
    private boolean recordar = true;
    private String imei,hash;
    private boolean isHidePass = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if(Useful.checkAndRequestPermissions(appPermissions,this,PERMISSIONS_REQUEST_CODE))
        {
            initApp();
        }
        daoSession= SplashScreenActivity.getDaoSession();
    }

    //region initApp
    public void initApp(){
        setElements();
        imei = usePhone.getIMEI(this);
        tvRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SendEmailActivity.class));
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
    }
    //endregion

    //region setElements
    @SuppressLint("ClickableViewAccessibility")
    private void setElements() {
        etUsuario = findViewById(R.id.etUsuario);
        etPassword = findViewById(R.id.etPassword);
        btnIngresar = findViewById(R.id.btnIngresar);
        tvRegistrarse = findViewById(R.id.tvRegistrarse);

        etPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    if(event.getRawX() >= (etPassword.getRight() - etPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (isHidePass) {
                            isHidePass = false;
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icons_hide, 0);
                            etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        } else {
                            isHidePass = true;
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icons_eye, 0);
                            etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        }

                        return true;
                    }
                }
                return false;
            }
        });
        //SugarContext.init(getApplicationContext());//Parte de SUGAR
    }
    //endregion

    //region IngresarSistema
    public void IngresarSistema(View view) {
        btnIngresar.setEnabled(false);
        usePhone.hideKeyBoard(LoginActivity.this);
        this.usuario = etUsuario.getText().toString().trim();
        this.password = etPassword.getText().toString();

        if(!validarDatos()){
            btnIngresar.setEnabled(true);
        } else {
            if (!usePhone.useWifiOrMobileData(LoginActivity.this)) {
                PopupOffLine();
                btnIngresar.setEnabled(true);
            } else {
                ConnectApp(true);
            }
        }
    }
    //endregion

    //region validarDatos
    private boolean validarDatos() {

        if(usuario.isEmpty()) {
            Toast.makeText(this,R.string.empty_correo,Toast.LENGTH_SHORT).show();
            return false;
        }

        if(!Useful.validarSiEsCorreo(this.usuario)) {
            Toast.makeText(this,R.string.invalid_correo,Toast.LENGTH_SHORT).show();
            return false;
        }

        if(password.isEmpty()) {
            Toast.makeText(this,R.string.empty_pass,Toast.LENGTH_SHORT).show();
            return false;
        }

        if(password.length() < 8){
            Toast.makeText(this,R.string.full_pass,Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }
    //endregion

    //region salir
    @Override
    public void onBackPressed() {
        timeBack = Useful.timeBack(this, timeBack);
        if (timeBack == -1) {
            finishAffinity();
            System.exit(0);
        }
    }
    //endregion

    //region onRequestPermissionsResult
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE)
        {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;

            for (int i=0;i<grantResults.length;i++)
            {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED)
                {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0)
            {
                initApp();
            }
            else
            {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet())
                {
                    String permName = entry.getKey();
                    int permResult = entry.getValue();

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName))
                    {
                        Popup.ShowDialog(LoginActivity.this,
                                getString(R.string.mensaje_sin_permisos),
                                getString(R.string.titulo_sin_permisos),
                                Popup.MSG_TYPE_WARNING,
                                Popup.BUTTON_TYPE_PERMISSION,
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Useful.checkAndRequestPermissions(appPermissions, LoginActivity.this, PERMISSIONS_REQUEST_CODE);
                                    }
                                },
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        finish();
                                    }
                                }
                                );
                    }
                    else
                    {
                        Popup.ShowDialog(LoginActivity.this,
                                getString(R.string.mensaje_sin_permisos),
                                getString(R.string.titulo_sin_permisos),
                                Popup.MSG_TYPE_WARNING,
                                Popup.BUTTON_TYPE_SETTINGS,
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                },
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        finish();
                                    }
                                }
                        );
                        break;
                    }
                }
            }
        }

    }
    //endregion

    //region PopupOffLine
    private void PopupOffLine() {
        Popup.ShowDialog(LoginActivity.this,
                getString(R.string.mensaje_autenticacion_sin_conexión),
                getString(R.string.titulo_sin_conexion_internet),
                Popup.MSG_TYPE_ERROR,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                            ConnectApp(false);
                    }
                });
    }
    //endregion

    //region ConnectApp
    private void ConnectApp(boolean connectedNetwork) {
        try {
            Shared.edit_Login(LoginActivity.this, imei, usuario, recordar, useFormat.hora24(null), null);
            hash = AESCrypt.encrypt(password);

            if (connectedNetwork) {
                new InternetCheck().execute();
            }
            else {
                Toast.makeText(this, "Debe iniciar sesión en línea!", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Popup.showAlert(LoginActivity.this, e.getMessage());
            btnIngresar.setEnabled(true);
        }
    }
    //endregion

    //region Call Web Services
    @SuppressLint("StaticFieldLeak")
    private class InternetCheck extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            return usePhone.isOnline();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (!result) {
                PopupOffLine();
                btnIngresar.setEnabled(true);
            } else
                new ConnectWithService2().execute();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class ConnectWithService extends AsyncTask<Void, Void,Member> {
        private ProgressDialog dialog;
        StringBuilder outMessage = new StringBuilder();
        String errMsg = "";

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(LoginActivity.this, R.style.ProgressDialog);
            dialog.setTitle(R.string.titulo_autenticacion);
            dialog.setMessage(getString(R.string.mensaje_autenticacion_validando));
            dialog.setCancelable(false);
            dialog.show();
        }

        protected Member doInBackground(Void... args) {

            try {

                currentMember=new SyncData(LoginActivity.this, 0)
                        .SyncLogin3(password, outMessage);
            } catch (Exception e) {
                errMsg = e.getMessage();
            }
            return currentMember;
        }

        protected void onPostExecute(Member currentMember) {
            String msgException = "";
            if (currentMember !=null) {
                try {
                    /*List<Member>members=Member.listAll(Member.class);
                    for (Member m:members)
                    {
                        System.out.println(m.getEmail());
                    }


                    List<Member>memberList=Member.find(Member.class,"email = ?",usuario);
                    if(memberList.isEmpty()){
                        msgException="esta vacio";
                    }
                     */

                  //  Member currentUser = Member.find(Member.class,getCol("email")+ " = ?",usuario).get(0);
                    MDToast mdToast = MDToast.makeText(LoginActivity.this, getString(R.string.login_successful) + " " + currentMember.getName() +"!", MDToast.LENGTH_LONG, MDToast.TYPE_SUCCESS );
                    mdToast.show();
                    Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                    i.putExtra("member",currentMember);
                    startActivity(i);
                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    finish();
                } catch (Exception ex) {
                    msgException = getString(R.string.exception) + ex.getMessage();
                }
            }

            if (dialog.isShowing()) {
                dialog.dismiss();
                if (currentMember ==null || !msgException.isEmpty() || !errMsg.isEmpty()) {
                    Popup.showAlert(LoginActivity.this, outMessage.toString() + "\n" + msgException + "\n" + errMsg);
                    btnIngresar.setEnabled(true);
                }
            }

        }
    }


    @SuppressLint("StaticFieldLeak")
    private class ConnectWithService2 extends AsyncTask<Void, Void, Boolean> {
        private ProgressDialog dialog;
        StringBuilder outMessage = new StringBuilder();
        String errMsg = "";

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(LoginActivity.this, R.style.ProgressDialog);
            dialog.setTitle(R.string.titulo_autenticacion);
            dialog.setMessage(getString(R.string.mensaje_autenticacion_validando));
            dialog.setCancelable(false);
            dialog.show();
        }

        protected Boolean doInBackground(Void... args) {
            try {
                return new SyncData(LoginActivity.this, 0)
                        .SyncLogin4(password, outMessage);

            } catch (Exception e) {
                errMsg = e.getMessage();

                return false;
            }

        }

        protected void onPostExecute(Boolean result) {
            String msgException = "";
            if (result) {
                try {
                    List<Member>members=Member.listAll(Member.class);
                    for (Member m:members)
                    {
                        System.out.println(m.getEmail());
                    }

                    List<Member>memberList=Member.find(Member.class,"email = ?",usuario);
                    if(memberList.isEmpty()){
                        msgException="esta vacio";
                    }
                    Member currentUser = Member.find(Member.class,getCol("email")+ " = ?",usuario).get(0);
                    MDToast mdToast = MDToast.makeText(LoginActivity.this, getString(R.string.login_successful) + " " + currentUser.getName() +"!", MDToast.LENGTH_LONG, MDToast.TYPE_SUCCESS );
                    mdToast.show();
                    Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                    i.putExtra(getString(R.string.intent_tipUser),usuario);
                    startActivity(i);
                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    finish();
                } catch (Exception ex) {
                    msgException = getString(R.string.exception) + ex.getMessage();
                }
            }

            if (dialog.isShowing()) {
                dialog.dismiss();
                if (!result || !msgException.isEmpty() || !errMsg.isEmpty()) {
                    Popup.showAlert(LoginActivity.this, outMessage.toString() + "\n" + msgException + "\n" + errMsg);
                    btnIngresar.setEnabled(true);
                }
            }

        }
    }
    //endregion

    @SuppressLint("StaticFieldLeak")
    private class ConnectWithService3 extends AsyncTask<Void, Void, Boolean> {
        private ProgressDialog dialog;
        StringBuilder outMessage = new StringBuilder();
        String errMsg = "";

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(LoginActivity.this, R.style.ProgressDialog);
            dialog.setTitle(R.string.titulo_autenticacion);
            dialog.setMessage(getString(R.string.mensaje_autenticacion_validando));
            dialog.setCancelable(false);
            dialog.show();
        }

        protected Boolean doInBackground(Void... args) {
            try {
                return new SyncData(LoginActivity.this, 0)
                        .SyncLogin4(password, outMessage);

            } catch (Exception e) {
                errMsg = e.getMessage();

                return false;
            }

        }

        protected void onPostExecute(Boolean result) {
            String msgException = "";
            if (result) {
                try {

                    memberDao=daoSession.getMemberDaoDao();
                    List<MemberDao>membersDao=memberDao.queryBuilder().where(MemberDaoDao.Properties.Email.eq(usuario)).list();
                    if(membersDao.isEmpty()){
                        msgException="esta vacio";
                    }
                        String name=membersDao.get(0).getName();
                   // Member currentUser = Member.find(Member.class,getCol("email")+ " = ?",usuario).get(0);
                    MDToast mdToast = MDToast.makeText(LoginActivity.this, getString(R.string.login_successful) + " " + name +"!", MDToast.LENGTH_LONG, MDToast.TYPE_SUCCESS );
                    mdToast.show();
                    Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                    i.putExtra(getString(R.string.intent_tipUser),usuario);
                    startActivity(i);
                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    finish();
                } catch (Exception ex) {
                    msgException = getString(R.string.exception) + ex.getMessage();
                }
            }

            if (dialog.isShowing()) {
                dialog.dismiss();
                if (!result || !msgException.isEmpty() || !errMsg.isEmpty()) {
                    Popup.showAlert(LoginActivity.this, outMessage.toString() + "\n" + msgException + "\n" + errMsg);
                    btnIngresar.setEnabled(true);
                }
            }

        }
    }

}
