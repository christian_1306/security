package com.ndp.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.ndp.R;
import com.ndp.models.model.ArticuloOrder;
import com.ndp.models.model.User;
import com.ndp.ui.adapters.ItemOrderAdapter;

import static com.ndp.utils.Const.CODE_ORDER;

public class OrderSaleActivity extends AppCompatActivity {

    private RecyclerView rv_order_items;
    private ArrayList<ArticuloOrder> articuloOrders;
    private ArrayList<ArticuloOrder> articuloOrdersMARK;
    private TextView tv_title_toolbar;
    private ImageView ivRetroceder;
    private ItemOrderAdapter adapter;
    private SearchView searchView;
    private LinearLayout lyt_empty;
    private User currentUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_sale);
        setElements();
    }

    //region setElements
    private void setElements() {
        currentUser = (User) getIntent().getSerializableExtra(getResources().getString(R.string.objUser));
        if (currentUser == null) this.finish();
        articuloOrders  = getIntent().getParcelableArrayListExtra(getResources().getString(R.string.valueActivity));
        articuloOrdersMARK = getIntent().getParcelableArrayListExtra(getResources().getString(R.string.valueActivityI));
        if (articuloOrdersMARK == null) articuloOrdersMARK = new ArrayList<>();
        if (articuloOrders != null && articuloOrders.size() > 0) {
            Toolbar toolbar = findViewById(R.id.tb_view);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null ) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setTitle("");
            } else {
                this.finish();
            }
            tv_title_toolbar = findViewById(R.id.tv_title_toolbar);
            tv_title_toolbar.setText(getResources().getString(R.string.title_order_items));
            rv_order_items = findViewById(R.id.rv_order_items);
            ivRetroceder = findViewById(R.id.iv_back_toolbar);
            lyt_empty = findViewById(R.id.lyt_empty);
            ivRetroceder.setOnClickListener(closeCustomer);
            recyclerViewAllData();
        } else {
            MDToast mdToast = MDToast.makeText(this, getString(R.string.not_items), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR );
            mdToast.show();
            this.finish();
        }
    }
    //endregion

    //region recyclerViewAllData
    public void recyclerViewAllData(){
        Collections.sort(articuloOrders, new Comparator<ArticuloOrder>() {
            public int compare(ArticuloOrder v1, ArticuloOrder v2) {
                return v1.getNombre().compareTo(v2.getNombre());
            }
        });
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(300);
        itemAnimator.setRemoveDuration(300);
        rv_order_items.setItemAnimator(itemAnimator);

        rv_order_items.setHasFixedSize(true);
        rv_order_items.setLayoutManager(new LinearLayoutManager(this));
        rv_order_items.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        adapter = new ItemOrderAdapter(articuloOrders, this, currentUser.getTipoUsuario() != null && currentUser.getTipoUsuario() == 2);
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                checkEmpty();
                super.onChanged();
            }
        });
        rv_order_items.setAdapter(adapter);

    }
    //endregion

    private void checkEmpty() {
        if (adapter.getItemCount() <= 0) {
            lyt_empty.setVisibility(View.VISIBLE);
            rv_order_items.setVisibility(View.GONE);
        } else {
            lyt_empty.setVisibility(View.GONE);
            rv_order_items.setVisibility(View.VISIBLE);
        }
    }

    private View.OnClickListener closeCustomer = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!searchView.isIconified()) {
                searchView.setIconified(true);
                return;
            }
            new returnItemsSelected().execute();
        }
    };


    @SuppressLint("StaticFieldLeak")
    private class returnItemsSelected extends AsyncTask<Void, String, Void> {
        private ProgressDialog dialog;
        private String errMsg = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(OrderSaleActivity.this);
            dialog.setTitle("Operación en curso");
            dialog.setMessage("Mensaje");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            dialog.setMessage(values[0]);
        }

        @Override
        protected Void doInBackground(Void... args) {
            try {
                publishProgress(getString(R.string.async_title_price));

                for (ArticuloOrder articuloOrder : articuloOrders) {
                    if (articuloOrder.getCantidadOrder() > 0) {
                         articuloOrdersMARK.add(articuloOrder);
                    }
                }

                articuloOrders.removeAll(articuloOrdersMARK);
            }catch (Exception ex){
                errMsg = (ex.getMessage() != null) ? ex.getMessage() : "-" ;
            }
            return null;
        }

        protected void onPostExecute(Void result) {

            if (dialog.isShowing() && dialog!=null) {
                dialog.dismiss();
                if (!errMsg.isEmpty()) {
                    MDToast mdToast = MDToast.makeText(OrderSaleActivity.this, errMsg, MDToast.LENGTH_LONG, MDToast.TYPE_ERROR );
                    mdToast.show();
                } else {
                    Intent i = new Intent();
                    i.putParcelableArrayListExtra(getResources().getString(R.string.valueActivity), articuloOrders);
                    i.putParcelableArrayListExtra(getResources().getString(R.string.valueActivityI), articuloOrdersMARK);
                    setResult(CODE_ORDER, i);
                    finish();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();

        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_title_toolbar.setVisibility(View.GONE);
                ivRetroceder.setVisibility(View.GONE);
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                tv_title_toolbar.setVisibility(View.VISIBLE);
                ivRetroceder.setVisibility(View.VISIBLE);
                return false;
            }
        });

        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_search:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            searchView.setIconified(true);
            return;
        }
        new returnItemsSelected().execute();
    }

}
